// Dashboard (Spendenvermittler)
jQuery('button.search_search').on('click', function(e) {
    e.preventDefault();
    var search_value = jQuery('input[name="search_input"]').val();
    if (search_value.length > 0) {
        jQuery('form#search_search_form').attr('action', '/suche/' + search_value);
        jQuery('form#search_search_form').submit();
    }
});
jQuery('input[name="search_input"]').keypress(function(e){
    if(e.which == 13){
        $('button.search_search').click();
    }
});

jQuery('button.donate_search').on('click', function(e) {
    e.preventDefault();
    var search_value = jQuery('input[name="donate_input"]').val();
    if (search_value.length > 0) {
        jQuery('form#search_donate_form').attr('action', '/suche/' + search_value);
        jQuery('form#search_donate_form').submit();
    }
});
jQuery('input[name="donate_input"]').keypress(function(e){
    if(e.which == 13){
        $('button.donate_search').click();
    }
});

// Suchformular
jQuery('button.main_search').on('click', function(e) {
    e.preventDefault();
    var search_value = jQuery('input[name="main_search_input"]').val();
    if (search_value.length > 0) {
        jQuery('form#search_main_form').attr('action', '/suche/' + search_value)
        jQuery('form#search_main_form').submit();
    }
});
jQuery('input[name="main_search_input"]').keypress(function(e){
    if(e.which == 13){
        $('button.main_search').click();
    }
});

// Öffentliche Suche (Navigation)
jQuery('button.main_nav_search').on('click', function(e) {
    e.preventDefault();
    var search_value = jQuery('input[name="nav_search_input"]').val();
    if (search_value.length > 0) {
        jQuery('form#nav_search_form').attr('action', '/suche/' + search_value);
        jQuery('form#nav_search_form').submit();
    }
});
jQuery('input[name="nav_search_input"]').keypress(function(e){
    if(e.which == 13){
        $('button.main_nav_search').click();
    }
});

// Öffentliche Suche (Spenden) Sidebar
jQuery('button.donations_sidebar_search').on('click', function(e) {
    e.preventDefault();
    var search_value = jQuery('input[name="donations_sidebar_input"]').val();
    if (search_value.length > 0) {
        jQuery('form#donations_sidebar_form').attr('action', '/suche/' + search_value);
        jQuery('form#donations_sidebar_form').submit();
    }
});
jQuery('input[name="donations_sidebar_input"]').keypress(function(e){
    if(e.which == 13){
        $('button.donations_sidebar_search').click();
    }
});

// Öffentliche Suche (Gesuche) Sidebar
jQuery('button.searches_sidebar_search').on('click', function(e) {
    e.preventDefault();
    var search_value = jQuery('input[name="searches_sidebar_input"]').val();
    if (search_value.length > 0) {
        jQuery('form#searches_sidebar_form').attr('action', '/suche/' + search_value);
        jQuery('form#searches_sidebar_form').submit();
    }
});
jQuery('input[name="searches_sidebar_input"]').keypress(function(e){
    if(e.which == 13){
        $('button.searches_sidebar_search').click();
    }
});