jQuery('button.meldungs_modal').on('click', function() {
    $('form#meldung_senden_form input, form#meldung_senden_form textarea').each(function() {
        jQuery(this).css('border', '1px solid #ccc');
    });
    jQuery('p.error').each(function() {
        jQuery(this).remove();
    });
});

jQuery('button.meldung_senden').on('click', function() {
    jQuery('form#meldung_senden_form').attr('action', jQuery(this).attr('data-url')).attr('method', 'post');
    var inputs = {};
    $('form#meldung_senden_form input, form#meldung_senden_form textarea').each(function() {
        inputs[$(this).attr("name")] = $(this).val();
    });

    $.ajax({
        url: "/Helpers/validateReport",
        type: 'POST',
        data: inputs,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        if (response['status'] === 'success') {
            if (typeof response['message'] === 'undefined') {
                jQuery('form#meldung_senden_form').submit();
            }
        } else {
            var messages = response['messages'];
            for (var key in messages) {
                var message = messages[key]['text'];
                var type = messages[key]['type'];
                if (type === "warning") {
                    continue;
                }
                var name = messages[key]['name'];
                if (name === 'empty_reason') {
                    jQuery('form#meldung_senden_form textarea[name="'+key+'"]').css('border', '1px solid #c0392b');
                    jQuery('form#meldung_senden_form textarea[name="'+key+'"]').after('<p class="error" style="color:red" id="'+key+'">'+message+'</p>')
                } else {
                    jQuery('form#meldung_senden_form input[name="' + key + '"]').css('border', '1px solid #c0392b');
                    jQuery('form#meldung_senden_form input[name="'+key+'"]').after('<p class="error" style="color:red" id="'+key+'">'+message+'</p>')
                }

            }
            $('form#meldung_senden_form > input, form#meldung_senden_form > textarea').on('keyup', function() {
                if (jQuery(this).val().length===0) {
                    jQuery(this).css('border', '1px solid #c0392b');
                    var element_name = (jQuey(this).attr('name'));
                    var target_element_id = "empty_"+element_name;
                    jQuery(this).next('p#'+target_element_id).fadeIn('fast');
                } else {
                    jQuery(this).css('border', '1px solid #ccc');
                    var element_name = (jQuery(this).attr('name'));
                    jQuery(this).next('p[name="' + element_name + '"]').fadeOut('fast');
                }
            });
        }
    });
});

jQuery('form#meldung_senden_form input, form#meldung_senden_form textarea').on('keyup', function() {
    var element_id = jQuery(this).attr('name');
    jQuery(this).css('border', '1px solid #ccc');
    jQuery('p#'+element_id+'.error').fadeOut('fast').remove();
});