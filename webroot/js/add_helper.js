$(document).ready(function() {
    checkKeywordsInSession();
    checkFilledInZip();
});

function checkKeywordsInSession() {
    $.ajax({
        url: "/Helpers/checkKeywords",
        type: 'POST',
        data: 1,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        if (response['status'] === 'success') {
            $('#keywords').importTags(response['keywords']);
            $('div#keywords_div').fadeIn('fast');
        }
    });
}

function checkFilledInZip() {
    var zip = $('input#zip').val();

    if (zip.length == 5) {
        var posted_data = {
            zip: zip
        };
        $.ajax({
            url: "/Helpers/validateZip",
            type: 'POST',
            data: posted_data,
            dataType: 'json',
            async: true,
            cache: false
        }).always(function (data) {
            var response = data['response'];

            if (response['status']==='success') {
                $('input[name="zip"]').css('border', '1px solid #5fb611');
                $('p#wrong_zip').fadeOut('fast').remove();
                $('input[name="city"]').val(response['city']).css('border', '1px solid #5fb611');
                $('div#notie-alert-inner').fadeOut('fast').remove();
            } else if (response['status']==='failed') {
                $('input[name="city"]').val('').css('border', '1px solid #c0392b');
                $('input[name="zip"]').css('border', '1px solid #c0392b').after('<p class="error" id="wrong_zip" style="color:red">' + response['message'] + '</p>').fadeIn('fast');
                $('input#zip').on('blur', function() {
                    notie.alert(4, response['alert_message'], 60);
                });            }
        });

    } else {
        $('input[name="city"]').css('border', '1px solid #ccc');
    }
}

function keywordsFilter(_this)
{
    $(_this).css('border', '1px solid #ccc');
    var element_name = $(_this).attr('name');
    $('p#' + element_name).fadeOut('fast').remove();
    var title = $('input#title').val();
    var description = $('textarea#description').val();

    var content = {
        title: title,
        description: description
    };

    $.ajax({
        url: "/Helpers/filterText",
        type: 'POST',
        data: content,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        if (response['status'] === 'success') {
            $('#keywords').importTags(response['tags']);
            $('div#keywords_div').css('height', 'auto');
        }
    });

    $('div#keywords_div').fadeIn('fast');
}

function checkKeypressZip(_this) {
    $('input#zip').next('p.error').fadeOut('fast').remove();
    $('input#zip').css('border', '1px solid #ccc');

    var zip = $('input#zip').val();

    if (zip.length == 5) {
        var posted_data = {
            zip: zip
        };
        $.ajax({
            url: "/Helpers/validateZip",
            type: 'POST',
            data: posted_data,
            dataType: 'json',
            async: true,
            cache: false
        }).always(function (data) {
            var response = data['response'];

            if (response['status']==='success') {
                $('p#wrong_zip').fadeOut('fast').remove();
                $('input[name="city"]').val(response['city']);
                $('div#notie-alert-inner').fadeOut('fast').remove();
            }
        });

    } else {
        $('input[name="city"]').css('border', '1px solid #ccc');
    }
}

var key_counter = 0;
$('input#title, textarea#description').on('keyup', function(e) {
    key_counter = key_counter+1;
    if (key_counter % 2 == 0) {
        keywordsFilter($(this));
    }
});

$('textarea#description').keypress(function(e) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var description_value = $('textarea#description').val();
        $('textarea#description').val(description_value + " ");

    }
});

$('#keywords').tagsInput({
    //'autocomplete_url': '/js/$.autocomplete.js',
});
/**
 * Breite des Schlagwörter-Formularfelds auf 500px gesetzt
 */
$('#keywords_tagsinput').css('width', '500px');

$('input#zip').on('keyup', function() {
    checkKeypressZip($(this));
});

function check_zip(this_element) {
    if ($(this_element).val().length == 5) {
        var posted_data = {
            zip: $(this_element)
        };

        $.ajax({
            url: "/Helpers/validateZip",
            type: 'POST',
            data: posted_data,
            dataType: 'json',
            async: true,
            cache: false
        }).always(function (data) {
            var response = data['response'];
            if (response['status'] === 'success') {
                $('p#wrong_zip').fadeOut('fast').remove();
                $('input[name="city"]').val(response['city']);
                $('div#notie-alert-inner').fadeOut('fast').remove();
            } /*
            else if (response['status'] === 'failed') {
                $('input[name="city"]').val('').css('border', '1px solid #c0392b');
                $('input[name="zip"]').css('border', '1px solid #c0392b').after('<p class="error" id="wrong_zip" style="color:red">' + response['message'] + '</p>').fadeIn('fast');
                notie.alert(4, response['alert_message'], 60);
            }
            */
        });
    }
}
/*
$('input#zip').on('blur', function() {
    check_zip($(this));
});
*/

$('input#zip').keypress(function(e) {
    if (e.which == 9) {
        check_zip($(this));
    }
});

$('input[name="time_checkbox"]').on('click', function() {
    if ($(this).prop('checked')) {
        var time_search_html =
            '<tr class="datetime_tr" id="time_row_1" style="border-top:1px solid #000000">' +
                '<td class="datetime_range" style="padding-right:20px;padding-top:20px;padding-bottom:20px">' +
                    '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><input readonly="readonly" class="form-control" type="text" name="datetime_range_1" /></div></div>' +
                '</td>' +
            '</tr>';
        $('div#time_div').fadeIn('fast');
        $('div#time_div > table#time_table').html(time_search_html);

        $('input[name="datetime_range_1"]').daterangepicker({
            timePicker: true,
            timePickerIncrement: 1,
            timePicker24Hour: true,
            locale: {
                format: 'DD.MM.YYYY HH:mm'
            }
        });
        $('table#time_table input').on('keyup', function() {
            var all_forms_filled = true;

            $('table#time_table input').each(function () {


                if ($(this).val() == '') {
                    all_forms_filled = false;
                }
            });
            if (all_forms_filled === true) {
                $('a.add_more_times').fadeIn('fast');
            }
        });
    } else {
        $('div#time_div').fadeOut('fast');
        $('div#time_div > table#time_table').html('');
    }
});



var counter = 2;
$('a.add_more_times').on('click', function() {
    var current_row = counter;
    var next_row = false;
    while (next_row!==true) {
        if ($('tr#datetime_row_'+current_row).length>0) {
            current_row = current_row+1;
            continue;
        }
        next_row = true;
    }
    var time_table_row =
        '<tr class="datetime_tr" id="time_row_'+current_row+'" style="border-top:1px solid #000000">' +
        '<td class="datetime_range" style="padding-right:20px;padding-top:20px;padding-bottom:20px">' +
        '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><input readonly="readonly" class="form-control" type="text" name="datetime_range_'+current_row+'" /></div></div>' +
        '</td>' +
        '</tr>';
    $('table#time_table').append(time_table_row);
    $('input[name="datetime_range_'+current_row+'"]').daterangepicker({
        timePicker: true,
        timePickerIncrement: 1,
        timePicker24Hour: true,
        locale: {
            format: 'DD.MM.YYYY HH:mm'
        }
    });
    $('a.delete_last_time').fadeIn('fast');
});

$('a.delete_last_time').on('click', function() {
    if ($("table#time_table tr.datetime_tr")[1]){
        $('table#time_table tr.datetime_tr:last-child').fadeOut('fast').remove();
    } else if ($("table#time_table tr.datetime_tr")[0]) {
        $('a.delete_last_time').fadeOut('fast');
        $('table#time_table tr.datetime_tr:last-child').fadeOut('fast').remove();
        $('a.add_more_times').fadeIn('fast');
    } else {
        $('a.delete_last_time').fadeOut('fast');
        $('a.add_more_times').fadeIn('fast');
    }
});

$('button.submit_new_form').on('click', function(e) {
    e.preventDefault();

    var button_error_counter = 0;

    $('p.error').fadeOut('fast').remove();
    $('p.warning').fadeOut('fast').remove();

    var inputs = {};
    /**
     * Alle Formularfelder inklusive der Werte in Array abspeichern
     */
    $('form#new_form input, form#new_form textarea').each(function() {
        $(this).css('border', '1px solid #ccc');
        if ($(this).attr("name")=="rights") {
            if ($(this).attr("type") == "checkbox") {
                var value = $(this).prop('checked');
                inputs[$(this).attr("name")] = value;
            } else {
            }
        } else {
            inputs[$(this).attr("name")] = $(this).val();
        }
    });

    if ($('a.MultiFile-remove.btn.btn-warning').length>0) {
        inputs['files'] = $('a.MultiFile-remove.btn.btn-warning').length;
    } else {
        inputs['files'] = 0;
    }

    $.ajax({
        url: "/Helpers/validateNew",
        type: 'POST',
        data: inputs,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        if (response['status'] === 'success') {
            /**
             * Falls keine Meldungen zur Validation vorhanden
             */
            if (typeof response['message'] === 'undefined') {
                $('form#new_form').submit();
                $('button.submit_new_form').attr('disabled', true);
            }
        } else if (response['status'] === 'possible') {
            var mail_subject = response['mail_subject'];
            var mail_content = response['mail_content'];
            mail_content = mail_content.replace("{{invalid_zip}}", $('input#zip').val());
            mail_content = mail_content.replace("{{invalid_city}}", $('input[name="city"]').val());
            mail_content = mail_content.replace("{{sender_firstname}}", $('input[name="firstname"]').val());
            mail_content = mail_content.replace("{{sender_lastname}}", $('input[name="lastname"]').val());
            mail_content = mail_content.replace("{{donation_title}}", $('input[name="title"]').val());
            if ($('input[name="email"]').length > 0) {
                mail_content = mail_content.replace("{{sender_email}}", $('input[name="email"]').val());
            }

            if ($('input[name="phone"]').length > 0) {
                mail_content = mail_content.replace("{{Telefonnummer}}", $('input[name="phone"]').val());
            }
            
            notie.confirm(
                "Deine Postleitzahl ist außerhalb der möglichen Region. Möchtest Du die Spende trotzdem spenden?",
                "Ja",
                "Nein",
                function () {
                    $('.bs-example-modal-sm .modal-body').append('Zur Bestätigung müsstest du eine E-Mail an&nbsp;<a class="send_email" href="mailto:fluechtlingshilfe@drk-kv-wf.de?subject=' + mail_subject + '&body=' + mail_content + '"><i class="fa fa-angle-double-right"></i>&nbsp;Flüchtinglingshilfe&nbsp;<i class="fa fa-angle-double-left"></i></a>&nbsp;senden. <br/><br/>(Nach Klick auf den Link wird die Spende im Hintergrund abgeschickt.)');
                    $('.bs-example-modal-sm').modal('show');
                    $('a.send_email').off().on('click', function () {
                        setTimeout(function () {
                            $('input[name="hidden_inactive_mail"]').val($('.bs-example-modal-sm .modal-body').html());
                            $('input[name="active"]').val(0);
                            $('form#new_form').submit();
                            $('button.submit_new_form').attr('disabled', true);
                        }, 3000);
                    });

                }
            );
            /**
             * Falls Meldungen zur Validation vorhanden
              */
        } else if (response['status'] !== 'success') {
            var show_notie_error = false;
            var messages = response['messages'];
            var email_shown = false;
            var images_shown = false;
            var counter = 0;
            for (var key in messages) {
                var message = messages[key];
                for (var message_key in message) {
                    var type = message[message_key]['type'];
                    var name = message[message_key]['name'];
                    var message_text = message[message_key]['text'];
                    switch (message_key) {
                        case "firstname":
                        case "lastname":
                        case "title":
                        case "keywords":
                        case "zip":
                            var show_notie_error = true;
                            $('input[name="' + message_key + '"]').css('border', '1px solid #c0392b');
                            $('input[name="' + message_key + '"]').after('<p style="color:#c0392b" id="' + message_key + '" class="error">' + message_text + '</p>');
                            break;
                        case "email":
                        case "email_confirm":
                            if (email_shown === true) {
                                continue;
                            }
                            switch (name) {
                                /**
                                 * Falls keine E-Mail-Adresse angegeben wurde
                                 */
                                case "no_email":
                                    /**
                                     * Falls angegebene Postleitzahl innerhalb des gültigen Postleitzahlenbereichs
                                     */
                                    if (show_notie_error === true) {
                                        continue;
                                    }
                                    email_shown = true;
                                    if (typeof response['invalid_zip'] === 'undefined') {
                                        $('input#email').css('border', '1px solid #ef9e0f');
                                        $('input#email').after('<p style="color:#ef9e0f" id="' + message_key + '" class="warning">' + message_text + '</p>');
                                        notie.confirm(
                                            "Ohne Eingabe der E-Mail-Adresse fortfahren?",
                                            "Ja",
                                            "Nein",
                                            function () {
                                                $('form#new_form').submit();
                                                $('button.submit_new_form').attr('disabled', true);
                                            }
                                        );
                                        /**
                                         * Falls angegebene Postleitzahl außerhalb des gültigen Postleitzahlenbereichs
                                         */
                                    } else {
                                        $('input#email').css('border', '1px solid #ef9e0f');
                                        $('input#email').after('<p style="color:#ef9e0f" id="email" class="error">- Wenn Du außerhalb des gültigen Postleitzahlenbereichs spenden möchtest, musst Du eine E-Mail-Adresse hinterlegen.</p>');
                                        $('input#zip').css('border', '1px solid #ef9e0f');
                                        $('input#zip').after('<p style="color:#ef9e0f" id="zip" class="error">Postleitzahl ungültig.</p>');
                                    }
                                    break;
                                case "no_email_confirm":
                                    $('input[name="' + message_key + '"]').css('border', '1px solid #c0392b');
                                    $('input[name="' + message_key + '"]').after('<p style="color:#c0392b" id="' + message_key + '" class="error">' + message_text + '</p>');
                                    break;
                                case "email_exists":
                                    $('input[name="' + message_key + '"]').css('border', '1px solid #c0392b');
                                    $('input[name="' + message_key + '"]').after('<p style="color:#c0392b" id="' + message_key + '" class="error">' + message_text + '</p>');
                                    break;
                                case "not_similiar_emails":
                                    $('input[name="' + message_key + '"]').css('border', '1px solid #c0392b');
                                    $('input[name="' + message_key + '"]').after('<p style="color:#c0392b" id="' + message_key + '" class="error">' + message_text + '</p>');
                                    break;
                            }
                        case "phone":
                            break;
                        case "description":
                            switch (name) {
                                /**
                                 * Falls Beschreibung leer
                                 */
                                case "empty_description":
                                    show_notie_error = true;
                                    $('textarea[name="' + message_key + '"]').css('border', '1px solid #c0392b');
                                    $('textarea[name="' + message_key + '"]').after('<p style="color:#c0392b" id="' + message_key + '" class="error">' + message_text + '</p>');
                                    break;
                            }
                            break;
                        case "images":
                            if (images_shown === true) {
                                continue;
                            }
                            switch (name) {
                                /**
                                 * Falls keine Bilder hinterlegt wurden
                                 */
                                case "no_images":
                                    if (show_notie_error === true) {
                                        continue;
                                    }
                                    counter = counter+1;
                                    images_shown = true;
                                    var submit = 0;
                                    notie.confirm(
                                        message_text,
                                        "Nein",
                                        "Ja",
                                        function () {
                                            // TODO: Wenn Kategorien eingebunden werden, wird anhand der gesetzten Kategorie (Pflichtfeld) ein Symbol ausgewählt und bei nicht vorhandenem Bild als Platzhalter eingesetzt
                                            submit = 1;
                                            /**
                                             * Falls angegebene Postleitzahl innerhalb des gültigen Postleitzahlenbereichs
                                             */
                                            if (typeof response['invalid_zip'] === 'undefined' && $('input#email').val().length==0) {
                                                $('input#email').css('border', '1px solid #ef9e0f');
                                                $('input#email').after('<p style="color:#ef9e0f" id="' + message_key + '" class="warning">' + message_text + '</p>');
                                                notie.confirm(
                                                    "Ohne Eingabe der E-Mail-Adresse fortfahren?",
                                                    "Ja",
                                                    "Nein",
                                                    function () {
                                                        $('form#new_form').submit();
                                                        $('button.submit_new_form').attr('disabled', true);
                                                    }
                                                );
                                                /**
                                                 * Falls angegebene Postleitzahl außerhalb des gültigen Postleitzahlenbereichs
                                                 */
                                            } else if ($('input#email-confirm').length>0) {
                                                /**
                                                 * Falls E-Mail-Adresse hinterlegt wurde
                                                 */
                                                if ($('input#email').val().length > 0 && $('input#email-confirm').val().length > 0) {
                                                    var mail_subject = response['mail_subject'];
                                                    var mail_content = response['mail_content'];
                                                    mail_content = mail_content.replace("{{invalid_zip}}", $('input#zip').val());
                                                    mail_content = mail_content.replace("{{invalid_city}}", $('input[name="city"]').val());
                                                    mail_content = mail_content.replace("{{sender_firstname}}", $('input[name="firstname"]').val());
                                                    mail_content = mail_content.replace("{{sender_lastname}}", $('input[name="lastname"]').val());
                                                    mail_content = mail_content.replace("{{donation_title}}", $('input[name="title"]').val());
                                                    if ($('input[name="email"]').length > 0) {
                                                        mail_content = mail_content.replace("{{sender_email}}", $('input[name="email"]').val());
                                                    } else {
                                                        /**
                                                         * Falls keine E-Mail-Adresse angegeben wurde, erscheint eine Fehlermeldung, dass bei einer Spende außerhalb des gültigen Postleitzahlenbereichs unbedingt eine E-Mail-Adresse hinterlegt werden muss
                                                         */
                                                        $('input#email').css('border', '1px solid #ef9e0f');
                                                        $('input#email').after('<p style="color:#ef9e0f" id="email" class="error">- Wenn Du außerhalb des gültigen Postleitzahlenbereichs spenden möchtest, musst Du eine E-Mail-Adresse hinterlegen.</p>');
                                                        $('input#zip').css('border', '1px solid #ef9e0f');
                                                        $('input#zip').after('<p style="color:#ef9e0f" id="' + message_key + '" class="error">Postleitzahl ungültig.</p>');
                                                        return;
                                                    }

                                                    /**
                                                     * Falls Telefonnummer angegeben wurde
                                                     */
                                                    if ($('input[name="phone"]').length > 0) {
                                                        mail_content = mail_content.replace("{{Telefonnummer}}", $('input[name="phone"]').val());
                                                    }

                                                    /**
                                                     * E-Mail muss zur Bestätigung versendet werden
                                                     */
                                                    notie.confirm(
                                                        "Deine Postleitzahl ist außerhalb der möglichen Region. Möchtest Du die Spende trotzdem spenden?",
                                                        "Ja",
                                                        "Nein",
                                                        function () {
                                                            $('.bs-example-modal-sm .modal-body').append('Zur Bestätigung müsstest du eine E-Mail an&nbsp;<a class="send_email" href="mailto:fluechtlingshilfe@drk-kv-wf.de?subject=' + mail_subject + '&body=' + mail_content + '"><i class="fa fa-angle-double-right"></i>&nbsp;Flüchtinglingshilfe&nbsp;<i class="fa fa-angle-double-left"></i></a>&nbsp;senden. <br/><br/>(Nach Klick auf den Link wird die Spende im Hintergrund abgeschickt.)');
                                                            $('.bs-example-modal-sm').modal('show');
                                                            $('a.send_email').off().on('click', function () {
                                                                setTimeout(function () {
                                                                    $('input[name="hidden_inactive_mail"]').val($('.bs-example-modal-sm .modal-body').html());
                                                                    $('input[name="active"]').val(0);
                                                                    $('form#new_form').submit();
                                                                    $('button.submit_new_form').attr('disabled', true);
                                                                }, 3000);
                                                            });

                                                        }
                                                    );
                                                } else {
                                                    if (email_shown===false && $('input#email').val().length==0) {
                                                        $('input#email').css('border', '1px solid #ef9e0f');
                                                        $('input#email').after('<p style="color:#ef9e0f" id="email" class="error">- Wenn Du außerhalb des gültigen Postleitzahlenbereichs spenden möchtest, musst Du eine E-Mail-Adresse hinterlegen.</p>');
                                                        $('input#zip').css('border', '1px solid #ef9e0f');
                                                        $('input#zip').after('<p style="color:#ef9e0f" id="zip" class="error">Postleitzahl ungültig.</p>');
                                                        return;
                                                    }
                                                }
                                            } else {
                                                $('form#new_form').submit();
                                                $('button.submit_new_form').attr('disabled', true);
                                            }
                                        }
                                    );
                                    if (submit===0) {
                                        $('input[type="file"]').trigger('click');
                                    }
                                    break;
                            }
                            break;
                        default:
                            switch (name) {
                                /**
                                 * Falls angegebene Uhrzeiten ungültig
                                 */
                                case "wrong_times":
                                    show_notie_error = true;
                                    $('table#time_table input').each(function () {
                                        $(this).css('border', '1px solid #c0392b');
                                    });
                                    $('a.add_more_times').before('<p style="color:#c0392b" id="' + message_key + '" class="error">' + message_text + '</p>');
                                    break;
                                /**
                                 * Falls Captcha nicht ausgefüllt wurde
                                 */
                                case "empty_captcha":
                                    show_notie_error = true;
                                    $('div.g-recaptcha iframe').css('border', '1px solid #c0392b');
                                    $('div.g-recaptcha').after('<p class="error" style="color:#c0392b" id="' + message_key + '">' + message_text + '</p>');
                                    break;
                                /**
                                 * Falls Rechte für angehängte Bilder nicht bestätigt wurden
                                 */
                                case "confirm_rights":
                                    show_notie_error = true;
                                    $('p#rights_uploads').after('<p class="error" style="color:#c0392b" id="' + message_key + '">' + message_text + '</p>');
                                    break;
                                /**
                                 * Falls mehr Schlagwörter angegeben werden müssen
                                 */
                                case "more_keywords":
                                    $('div#keywords_tagsinput').css('border', '1px solid #c0392b');
                                    $('input#keywords').after('<p class="error" style="color:#c0392b" id="' + key + '">' + message + '</p>');
                                    break;

                            }
                    }
                }
                $('p#'+name).html(message).fadeIn('fast');
            }

            if (show_notie_error === true) {
                $('html, body').animate({scrollTop: $('#spendendaten').offset().top}, 500);
                notie.alert(3, 'Bitte prüfe Deine Angaben.', 6);
            } else if (show_notie_error === false) {
                //$('html, body').animate({scrollTop: $('#kontaktdaten').offset().top}, 1500);
            }
        }
    });

    $('form#new_form input, form#new_form textarea').on('keyup', function() {
        if ($(this).val().length===0) {
            $(this).css('border', '1px solid #c0392b');
            var element_name = ($(this).attr('name'));
            var target_element_id = "empty_"+element_name;
            $(this).next('p#'+target_element_id).fadeIn('fast');
        } else {
            $(this).css('border', '1px solid #ccc');
            var element_name = ($(this).attr('name'));
            $(this).next('p[name="' + element_name + '"]').fadeOut('fast');
        }
    });
});

$('input[type="file"]').on('change', function() {
    if ($(this).val().length>0) {
        $('p#rights_uploads, p#rights_uploads > input[type="checkbox"]').fadeIn('fast');
    } else {
        $('p#rights_uploads, p#rights_uploads > input[type="checkbox"]').fadeOut('fast');
    }
});

$('input[type="checkbox"][name="rights"]').off().on('click', function() {
    if ($(this).prop('checked')) {
        $('p#rights.error').fadeOut('fast').remove();
    } else {
        $('p#rights_uploads').after('<p class="error" style="color:#c0392b" id="rights">- Bitte bestätigen Sie die Rechte an den Bildern.</p>');
    }
});

$('select#select_addresses').on('change', function() {
    var address_id = $(this).val();
    $.ajax({
        url: "/Helpers/getSelectedAddress/" + address_id,
        type: 'POST',
        data: '1',
        dataType: 'json',
        async: true,
        cache: false
    }).always(function(data) {
        var data = data['response'];
        if (data['status'] === 'success') {
            $('input[name="zip"]').val(data['address']['zip']);
            $('input[name="city"]').val(data['address']['city']);
            $('input[name="street"]').val(data['address']['street']);
            $('input[name="phone"]').val(data['address']['phone']);
            $('input[name="email"]').val(data['address']['email']);

        }
    });
});