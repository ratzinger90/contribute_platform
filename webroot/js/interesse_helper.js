jQuery('button.interesse_modal').on('click', function() {
    $('form#interesse_ueberpruefen input[name="code"]').each(function() {
        jQuery(this).css('border', '1px solid #ccc');
    });
    jQuery('form#interesse_ueberpruefen p.error').each(function() {
        jQuery(this).remove();
    });
});

jQuery('button.code_ueberpruefen').on('click', function(e) {
    e.preventDefault();
    $('p#code.error').remove();

    var inputs = {};
    $('form#interesse_ueberpruefen input').each(function() {
        inputs[$(this).attr("name")] = $(this).val();
    });

    if (jQuery('input[name="code"]').val().length==0) {
        jQuery('input[name="code"]').css('border', '1px solid red').after('<p class="error" id="code" name="empty_code" style="color:red">- Bitte Chiffre eingeben.</p>');
        return;
    } else {

        $.ajax({
            url: jQuery('form#interesse_ueberpruefen').attr('action'),
            type: 'POST',
            data: inputs,
            dataType: 'json',
            async: true,
            cache: false
        }).always(function (data) {
            var response = data['response'];
            if (response['status'] === 'success') {
                jQuery('input[name="broker_id"]').val(response['broker_id']);
                jQuery('div#interesse_spende div#check').removeClass('active').removeClass('in');
                jQuery('div#interesse_spende div#message').addClass('active in');
            } else {
                jQuery('input[name="code"]').css('border', '1px solid red').after('<p class="error" id="code" name="invalid_code" style="color:red">- Chiffre ist nicht korrekt.</p>');
            }
        });
    }
});

jQuery('button.interesse_senden').on('click', function(e) {
    e.preventDefault();

    $('p.error').remove();
    $('div.g-recaptcha iframe').css('border', 'none');

    var inputs = {};
    $('form#interesse_form input, form#interesse_form textarea').each(function() {
        jQuery(this).css('border', '1px solid #ccc');
        if ($(this).attr("name")=="agb") {
            if ($(this).attr("type") == "checkbox") {
                var value = $(this).prop('checked');
                inputs[$(this).attr("name")] = value;
            }
        } else {
            inputs[$(this).attr("name")] = $(this).val();
        }
    });


    $.ajax({
        url: "/Helpers/validateInterest",
        type: 'POST',
        data: inputs,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        console.log(inputs);
        if (response['status'] === 'success') {
            if (typeof response['message'] === 'undefined') {
                jQuery('form#interesse_form').submit();
            }
        } else {
            var messages = response['messages'];
            for (var key in messages) {
                var message = messages[key]['text'];
                var type = messages[key]['type'];
                if (type === "warning") {
                    continue;
                }
                var name = messages[key]['name'];
                if (name === 'empty_message') {
                    jQuery('textarea[name="'+key+'"]').css('border', '1px solid #c0392b');
                    jQuery('textarea[name="'+key+'"]').after('<p class="error" style="color:#c0392b" id="'+key+'">'+message+'</p>')
                } else {
                    jQuery('input[name="' + key + '"]').css('border', '1px solid #c0392b');
                    jQuery('input[name="'+key+'"]').after('<p class="error" style="color:#c0392b" id="'+key+'">'+message+'</p>')
                }
            }
            $('form#interesse_form > input, form#interesse_form > textarea').on('keyup', function() {
                if (jQuery(this).val().length===0) {
                    jQuery(this).css('border', '1px solid #c0392b');
                    var element_name = (jQuey(this).attr('name'));
                    var target_element_id = "empty_"+element_name;
                    jQuery(this).next('p#'+target_element_id).fadeIn('fast');
                } else {
                    jQuery(this).css('border', '1px solid #ccc');
                    var element_name = (jQuery(this).attr('name'));
                    jQuery(this).next('p[name="' + element_name + '"]').fadeOut('fast');
                }
            });
        }
    });
});

jQuery('form#interesse_ueberpruefen input, form#interesse_form input, form#interesse_form textarea').on('keyup', function() {
    var element_id = jQuery(this).attr('name');
    jQuery(this).css('border', '1px solid #ccc');
    jQuery('p#'+element_id+'.error').fadeOut('fast').remove();
});