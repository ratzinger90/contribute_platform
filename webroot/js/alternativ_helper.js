jQuery('button.wahl_spende').on('click', function() {
    $.ajax({
        url: jQuery(this).attr('data-url'),
        type: 'POST',
        data: 1,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        if (response['status'] === 'success') {
            location.href = response['target'];
        }
    });

});

jQuery('button.wahl_anfrage').on('click', function() {
    jQuery('div#alternativ_auswahl button.close').trigger('click');
    jQuery('div#kontakt_aufnehmen').modal('show');
});