jQuery('button.login_submit').on('click', function(e) {
    e.preventDefault();
    var posted_data = {
        cb930723a0719aac91a0b3ca09250f2a: jQuery('input#email').val(),
        c9c6d316d6dc4d952a789fd4b8858ed7: jQuery('input#password').val()
    };
    $.ajax({
        url: '/Users/check_login',
        type: 'POST',
        data: posted_data,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        if (response['status']==='failed') {
            notie.alert(3, 'E-Mail-Adresse / Passwort falsch.', 5);
            jQuery('div#failed_login_div').fadeIn('medium');

        } else if (response['status']==='success') {
            notie.alert(1, 'Login erfolgreich.', 5);
            location.href = response['location'];
        }
    });
});