jQuery('button.register_submit').on('click', function(e) {
    e.preventDefault();

    var button_error_counter = 0;

    $('p.error').remove();
    $('div.g-recaptcha iframe').css('border', 'none');

    var inputs = {};
    $('form#register_form input, form#register_form textarea').each(function() {
        jQuery(this).css('border', '1px solid #ccc');
        if ($(this).attr("name")=="agb") {
            if ($(this).attr("type") == "checkbox") {
                var value = $(this).prop('checked');
                inputs[$(this).attr("name")] = value;
            } else {
            }
        } else {
            inputs[$(this).attr("name")] = $(this).val();
        }
    });

    $.ajax({
        url: "/Helpers/validateRegister",
        type: 'POST',
        data: inputs,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        if (response['status'] === 'success') {
            if (typeof response['message'] === 'undefined') {
                jQuery('form#register_form').submit();
            }
        } else {
            var messages = response['messages'];
            for (var key in messages) {
                var message = messages[key]['text'];
                var type = messages[key]['type'];
                if (type === "warning") {
                    continue;
                }
                var name = messages[key]['name'];
                if (name === 'empty_broker_text' || name === 'more_broker_text') {
                    jQuery('textarea[name="' + key + '"]').css('border', '1px solid #c0392b');
                    jQuery('textarea[name="' + key + '"]').after('<p class="error" style="color:#c0392b" id="' + key + '">' + message + '</p>')
                } else if (name === 'empty_description') {
                    jQuery('textarea[name="' + key + '"]').css('border', '1px solid #c0392b');
                    jQuery('textarea[name="' + key + '"]').after('<p class="error" style="color:#c0392b" id="' + key + '">' + message + '</p>')
                } else if (name === 'not_accepted_agb') {
                    jQuery('input[name="' + key + '"][type="checkbox"]').css('border', '1px solid #c0392b');
                    jQuery('input[name="' + key + '"][type="checkbox"]').after('<p class="error" style="position:absolute;width:210px;color:#c0392b" id="' + key + '">' + message + '</p>')
                } else if (name === "empty_captcha") {
                    jQuery('div.g-recaptcha iframe').css('border', '1px solid #c0392b');
                    jQuery('div.g-recaptcha').after('<p class="error" style="color:#c0392b" id="' + key + '">' + message + '</p>')
                } else {
                    jQuery('input[name="' + key + '"]').css('border', '1px solid #c0392b');
                    jQuery('input[name="'+key+'"]').after('<p class="error" style="color:#c0392b" id="'+key+'">'+message+'</p>')
                }

                if (button_error_counter == 0) {
                    button_error_counter += 1;
                    notie.alert(3, 'Bitte prüfe Deine Angaben.', 6);
                }

            }
            $('form#register_form > input, form#register_form > textarea').on('keyup', function() {
                if (jQuery(this).val().length===0) {
                    jQuery(this).css('border', '1px solid #c0392b');
                    var element_name = (jQuey(this).attr('name'));
                    var target_element_id = "empty_"+element_name;
                    jQuery(this).next('p#'+target_element_id).fadeIn('fast');
                } else {
                    jQuery(this).css('border', '1px solid #ccc');
                    var element_name = (jQuery(this).attr('name'));
                    jQuery(this).next('p[name="' + element_name + '"]').fadeOut('fast');
                }
            });
        }
    });
});

jQuery('input#zip').on('click', function() {
    if (jQuery(this).val().length < 4) {
        jQuery('input[name="city"]').val('');
    }

});

jQuery('input#zip').on('keyup', function() {
    jQuery('p#zip.error').fadeOut('fast').remove();
    jQuery('input[name="zip"]').css('border', '1px solid #ccc');

    var zip = jQuery('input#zip').val();

    if (zip.length > 4) {
        var posted_data = {
            zip: zip
        };
        $.ajax({
            url: "/Helpers/getCityName",
            type: 'POST',
            data: posted_data,
            dataType: 'json',
            async: true,
            cache: false
        }).always(function (data) {
            var response = data['response'];
            if (response['status']==='success') {
                jQuery('input[name="city"]').css('border', '#ccc')
                jQuery('p#zip.error').fadeOut('fast');
                jQuery('input[name="city"]').val(response['city']);
            } else if (response['status']==='failed') {
                jQuery('form#register_form input[name="zip"]').css('border', '1px solid #c0392b');
                jQuery('form#register_form input[name="zip"]').after('<p class="error" style="color:red" id="zip">'+response['message']+'</p>')
            }
        });

    } else {
        jQuery('input[name="city"]').val('');
    }
});

jQuery('form#register_form input, form#register_form textarea').on('keyup', function() {
    var element_id = jQuery(this).attr('name');
    jQuery(this).css('border', '1px solid #ccc');
    jQuery('p#'+element_id+'.error').fadeOut('fast').remove();
});

jQuery('form#register_form input[type="checkbox"][name="agb"]').on('click', function() {
    jQuery('p#agb.error').fadeOut('fast').remove();
});

jQuery('textarea[name="broker_text"]').on('keyup', function() {
    var length = jQuery(this).val().length;
    if (length < 64) {
        var countdown_length = 64-length;
        jQuery('#broker_text_length').text('Noch ' + countdown_length + ' Zeichen.').css('color', '#D90016');
    } else {
        jQuery('#broker_text_length').text(length + ' Zeichen eingegeben.').css('color', '#009D00');
    }
});