jQuery('a.pin_anfragen_modal').on('click', function() {
    jQuery('div#interesse_spende').modal('hide');
    $('form#pin_anfragen_form input').each(function() {
        jQuery(this).css('border', '1px solid #ccc');
    });
    jQuery('form#pin_anfragen_form p.error').each(function() {
        jQuery(this).remove();
    });
});

jQuery('button.pin_anfragen').on('click', function(e) {
    e.preventDefault();

    $('p.error').remove();
    $('div.g-recaptcha iframe').css('border', 'none');

    var inputs = {};
    $('form#pin_anfragen_form input').each(function() {
        jQuery(this).css('border', '1px solid #ccc');
        inputs[$(this).attr("name")] = $(this).val();
    });

    $.ajax({
        url: "/Helpers/validatePinrequest",
        type: 'POST',
        data: inputs,
        dataType: 'json',
        async: true,
        cache: false
    }).always(function (data) {
        var response = data['response'];
        if (response['status'] === 'success') {
            if (typeof response['message'] === 'undefined') {
                jQuery('form#pin_anfragen_form').submit();
            }
        } else {
            var messages = response['messages'];
            for (var key in messages) {
                var message = messages[key]['text'];
                var type = messages[key]['type'];
                if (type === "warning") {
                    continue;
                }
                var name = messages[key]['name'];
                jQuery('input[name="' + key + '"]').css('border', '1px solid #c0392b');
                jQuery('input[name="'+key+'"]').after('<p class="error" style="color:#c0392b" id="'+key+'">'+message+'</p>')
            }
            $('form#pin_anfragen_form > input').on('keyup', function() {
                if (jQuery(this).val().length===0) {
                    jQuery(this).css('border', '1px solid #c0392b');
                    var element_name = (jQuey(this).attr('name'));
                    var target_element_id = "empty_"+element_name;
                    jQuery(this).next('p#'+target_element_id).fadeIn('fast');
                } else {
                    jQuery(this).css('border', '1px solid #ccc');
                    var element_name = (jQuery(this).attr('name'));
                    jQuery(this).next('p[name="' + element_name + '"]').fadeOut('fast');
                }
            });
        }
    });
});