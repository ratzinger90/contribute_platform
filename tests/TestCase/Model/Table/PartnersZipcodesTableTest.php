<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PartnersZipcodesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PartnersZipcodesTable Test Case
 */
class PartnersZipcodesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PartnersZipcodesTable
     */
    public $PartnersZipcodes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.partners_zipcodes',
        'app.partners',
        'app.valid_zipcodes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PartnersZipcodes') ? [] : ['className' => 'App\Model\Table\PartnersZipcodesTable'];
        $this->PartnersZipcodes = TableRegistry::get('PartnersZipcodes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PartnersZipcodes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
