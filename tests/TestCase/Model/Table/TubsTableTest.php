<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TubsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TubsTable Test Case
 */
class TubsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TubsTable
     */
    public $Tubs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tubs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tubs') ? [] : ['className' => 'App\Model\Table\TubsTable'];
        $this->Tubs = TableRegistry::get('Tubs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tubs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
