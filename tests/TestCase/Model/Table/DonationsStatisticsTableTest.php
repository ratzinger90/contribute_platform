<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DonationsStatisticsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DonationsStatisticsTable Test Case
 */
class DonationsStatisticsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DonationsStatisticsTable
     */
    public $DonationsStatistics;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.donations_statistics',
        'app.donations',
        'app.users',
        'app.addresses',
        'app.searches',
        'app.interceptions',
        'app.logs',
        'app.donates',
        'app.keywords',
        'app.keywords_donations',
        'app.keywords_searches',
        'app.time_searches',
        'app.file_uploads',
        'app.time_donations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DonationsStatistics') ? [] : ['className' => 'App\Model\Table\DonationsStatisticsTable'];
        $this->DonationsStatistics = TableRegistry::get('DonationsStatistics', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DonationsStatistics);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
