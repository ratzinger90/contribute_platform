<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use App\Controller\Component\FileUploadComponent;
use App\Controller\HelpersController;
use Cake\Log\Log;


/**
 * Donations Controller
 *
 * @property \App\Model\Table\DonationsTable $Donations
 */
class DonationsController extends AppController
{

    /**
     * Komponente für Dateiupload
     *
     * @var array
     */
    public $components = ['FileUpload', 'Helper'];

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        /**
         * Erlaubte Methoden in diesem Controller
         */
        $this->Auth->allow(['add', 'times', 'time', 'things', 'thing', 'view', 'viewMatches', 'confirmDonation', 'confirmDonationEmail', 'allDonations']);
    }

    /**
     * Methode zum Bestätigen einer abgegebenen Spende
     *
     * @param $token
     * @return \Cake\Network\Response|null
     */
    public function confirmDonation($token) {
        /**
         * Falls $token nicht gesetzt ist, Fehler ausgeben mit Fehlermeldung (Fehlerhafter Link)
         */
        if (!$token) {
            $this->Flash->error(__('Fehlerhafter Link.'));
            return $this->redirect('/');
        }

        /**
         * Falls $token gesetzt, base64_decode und explode an "|"
         */
        $token_array = "XXXX";

        /**
         * Benutzer zu Benutzer-ID = $token_array[0] und Benutzer-Email = $token_array[2] finden
         */
        $token_user = $this->Helper->__returnUser($token_array[0], $token_array[2]);

        /**
         * Falls kein Benutzer gefunden wurde, Fehler ausgeben mit Fehlermeldung (Fehlerhafter Link)
         */
        if (!$token_user) {
            $this->Flash->error(__('Fehlerhafter Link.'));
            return $this->redirect('/');
        }

        /**
         * Spende zu Spenden-ID = $token_array[3] und Benutzer-ID = $token_array[0] finden (Spende muss inaktiv sein, Donations.active = 0)
         */
        $token_donation = $this->Helper->__returnDonation($token_array[3], 0, $token_array[0]);

        /**
         * Falls keine Spende gefunden wurde, Fehler ausgeben mit Fehlermeldung (Fehlerhafter Link)
         */
        if (!$token_donation) {
            $this->Flash->error(__('Fehlerhafter Link.'));
            return $this->redirect('/');
        }

        /**
         * Falls Spende gefunden wurde, Spende aktiv schalten (active = 1)
         */
        $patched_token_donation = $this->Donations->patchEntity($token_donation, ['active' => 1]);
        if ($this->Donations->save($patched_token_donation)) {
            $this->Flash->success(__('Spende erfolgreich aktiviert.'));
            $this->redirect('/');
        } else {
            $this->Flash->error(__('Spende wurde nicht aktiviert.'));
            $this->redirect('/');
        }
    }

    /**
     * Methode zum Bestätigen einer Spende bei einer neuen E-Mail
     *
     * @param $token
     * @return \Cake\Network\Response|null
     */
    public function confirmDonationEmail($token) {

        /**
         * Falls $token nicht gesetzt ist, Fehler ausgeben mit Fehlermeldung (Fehlerhafter Link)
         */
        if (!$token) {
            $this->Flash->error(__('Fehlerhafter Link.'));
            return $this->redirect('/');
        }

        /**
         * Falls $token gesetzt, base64_decode und explode an "|"
         */
        $token_array = "XXXX";

        /**
         * Benutzer zu Benutzer-ID = $token_array[0] und Benutzer-Email = $token_array[2] finden
         */
        $token_user = $this->Helper->_returnUser($token_array[0], $token_array[2]);

        /**
         * Falls kein Benutzer gefunden wurde, Fehler ausgeben mit Fehlermeldung (Fehlerhafter Link)
         */
        if (!$token_user) {
            $this->Flash->error(__('Fehlerhafter Link.'));
            return $this->redirect('/');
        }

        /**
         * Spende zu Spenden-ID = $token_array[3] und Benutzer-ID = $token_array[0] finden (Spende muss inaktiv sein, Donations.active = 0)
         */
        $token_donation = $this->Helper->__returnDonation($token_array[3], 0, $token_array[0]);
        if (!$token_donation) {
            $this->Flash->error(__('Fehlerhafter Link.'));
            return $this->redirect('/');
        }

        /**
         * Falls Spende gefunden wurde, Spende aktivieren (confirm_invalid_zip = 1)
         */
        $patched_token_donation = $this->Donations->patchEntity($token_donation, ['confirm_invalid_zip' => 1]);
        if ($this->Donations->save($patched_token_donation)) {

            /**
             * Ersteller der aktuellen Spende holen
             */
            $token_donation_user = $this->Helper->__returnUser($token_donation->user_id);

            /**
             * Aktuelle Konfiguration holen
             */
            $configuration = $this->Helper->__returnConfiguration();

            /**
             * E-Mail-Template holen
             */
            $mail_template = $this->Helper->__returnEmailTemplate('spende_bestaetigen_postleitzahl');

            /**
             * Hash erzeugen
             */
            $hash = "XXXX";

            $generated_token = "XXXX";

            $token = "XXXX";

            /**
             * Bestätigungslink erstellen
             */
            $sender_mail_content_confirm_link = "XXXX";

            /**
             * Hole Vorlage des E-Mail-Inhalts aus Template
             */
            $mail_content = $mail_template->template_content;

            /**
             * Variable Vorname mit Ersteller der Spende ersetzen
             * Variable Host mit Host der Konfiguration ersetzen
             * Freischaltungslink mit dem erstellten Bestätigungslink ersetzen
             */
            $mail_content = str_replace('{{firstname}}', $token_donation_user->firstname, $mail_content);
            $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
            $mail_content = str_replace('{{donation_activate_link}}', $sender_mail_content_confirm_link, $mail_content);

            /**
             * Neue E-Mail vorbereiten
             */
            $email_data = [
                'type' => 'default',
                'mail_content' => $mail_content,
                'helpers' => ['Html', 'Text'],
                'template' => 'default',
                'emailFormat' => 'text',
                'sender' => [$configuration->from_mail => $configuration->from_big_title],
                'receiver' => ['RECEIVER@EMAIL.DE'],
                'subject' => $mail_template->template_subject
            ];
            $this->Helper->__sendEmail($email_data);

            $this->Flash->success(__('Spendenabgabe erfolgreich bestätigt.'));
            $this->redirect('/');
        } else {
            $this->Flash->error(__('Spendenabgabe wurde nicht bestätigt.'));
            $this->redirect('/');
        }
    }

    /**
     * Methode zur Auflistung aller Zeitspenden
     *
     * @param null $page
     * @param null $start_end_type
     * @param null $start_end_id
     */
    public function times($page = null, $start_end_type = null, $start_end_id = null)
    {
        if (isset($page) && $page) {
            $this->request->session()->write('page', $page);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_type', $start_end_type);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_id', $start_end_id);
        }
        if (!isset($page) && !isset($start_end_type) && !isset($start_end_id)) {
            $this->request->session()->delete('page');
            $this->request->session()->delete('start_end_type');
            $this->request->session()->delete('start_end_id');
        }
        return $this->redirect('/auflistung/' . base64_encode("donation|time"));
    }

    /**
     * Methode zur Darstellung einer Zeitspende
     *
     * @param null $url_title
     * @return \Cake\Network\Response|null
     */
    public function time($url_title = null)
    {
        return $this->redirect('/element/' . base64_encode('donation|time|' . $url_title));
    }

    /**
     * Methode zur Darstellung einer Sachspende
     *
     * @param $url_title
     * @return \Cake\Network\Response|null
     */
    public function thing($url_title)
    {
        return $this->redirect('/element/' . base64_encode('donation|thing|' . $url_title));
    }

    /**
     * Methode zur Auflistung von Sachspenden
     *
     * @param null $page
     * @param null $start_end_type
     * @param null $start_end_id
     */
    public function things($page = null, $start_end_type = null, $start_end_id = null)
    {
        if (isset($page) && $page) {
            $this->request->session()->write('page', $page);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_type', $start_end_type);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_id', $start_end_id);
        }
        if (!isset($page) && !isset($start_end_type) && !isset($start_end_id)) {
            $this->request->session()->delete('page');
            $this->request->session()->delete('start_end_type');
            $this->request->session()->delete('start_end_id');
        }
        return $this->redirect('/auflistung/' . base64_encode("donation|thing"));
    }

    /**
     * Methode zur Weiterleitung bei gefunden Matches / bei keinen gefunden Matches
     *
     * @param string|null $id Donation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('KeywordsDonations');
        $keywords_donations = $this->KeywordsDonations
            ->find()
            ->where(['KeywordsDonations.donation_id' => $id])
            ->andWhere([$this->request->session()->read('branch_filter')]);

        $searches_array = array();

        $keywords_donations_ids = array();

        foreach ($keywords_donations as $keyword_donation) {
            if (isset($keywords_donations_ids[$keyword_donation->keyword_id])) {
                continue;
            }
            $keywords_donations_ids[$keyword_donation->keyword_id] = $keyword_donation->keyword_id;
        }

        $this->loadModel('KeywordsSearches');
        $this->loadModel('Searches');
        $keywords_searches = $this->KeywordsSearches
            ->find()
            ->where(['KeywordsSearches.keyword_id IN' => $keywords_donations_ids])
            ->andWhere([$this->request->session()->read('branch_filter')])
            ->all();
        foreach ($keywords_searches as $keyword_search) {
            $searches = $this->Searches
                ->find()
                ->where(['Searches.id' => $keyword_search->search_id, 'Searches.active' => 1])
                ->andWhere([$this->request->session()->read('branch_filter_search')])
                ->all();
            foreach ($searches as $search) {
                if (isset($searches_array[$search->id])) {
                    $searches_array[$search->id] += 1;
                    continue;
                }
                $searches_array[$search->id] = 1;
            }
        }

        // Array sortieren (höchste Anzahl der Vorkommen zuerst

        $donation = $this->Donations->get($id);
        if ($donation->category == 'thing') {
            $case = "thing";
        } else if ($donation->category == 'time') {
            $case = "time";
        } else {
            $case = false;
        }

        if (!empty($searches_array)) {
            $this->request->session()->write('searches_array', $searches_array);
            $this->request->session()->write('donation_id', $donation->id);
            return $this->redirect('/matches_gefunden');
        } else if ($case != false) {
            $this->request->session()->write('flash', 'true');
            if ($case == 'thing') {
                return $this->redirect('/sachspende/' . $donation->url_title);
            } else if ($case == 'time') {
                return $this->redirect('/zeitspende/' . $donation->url_title);
            }
        } else if ($case == false) {
            $this->Flash->error(__('Fehlerhafte Weiterleitung.'));
            return $this->redirect('/spende_abgeben');
        }
    }

    /**
     * Methode zur Darstellung von Matches
     *
     * @return \Cake\Network\Response|null
     */
    public function viewMatches() {
        $this->set('title', 'Passende Gesuche');

        if (!$this->request->session()->read('searches_array')) {
            $this->Flash->error(__('Fehlerhafte Weiterleitung.'));
            return $this->redirect('/spende_abgeben');
        }

        $donation_id = $this->request->session()->read('donation_id');
        $searches_array = $this->request->session()->read('searches_array');

        $this->loadModel('Searches');
        if ($searches_array) {
            $searches = $this->Searches
                ->find()
                ->contain(['FileUploads'])
                ->where(['Searches.id IN' => $searches_array])
                ->andWhere([$this->request->session()->read('branch_filter_search')])
                ->limit(3);

            $this->loadModel('Matches');
            foreach ($searches as $search) {
                $new_match_entity = [
                    'configuration_id' => $this->request->session()->read('configuration_id'),
                    'search_id' => $search->id,
                    'donation_id' => $donation_id,
                    'contacted' => 0,
                    'seen' => 0
                ];
                $new_match = $this->Matches->newEntity($new_match_entity);
                $this->Matches->save($new_match);
            }

            $this->set('searches', $searches);

            //$this->request->session()->delete('searches_array');
            //$this->request->session()->delete('donation_id');
        }
    }

    /**
     * Methode zum Abgeben einer Spende
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($id_token = null)
    {
        $this->loadModel('Addresses');
        $this->loadModel('Cities');
        $this->loadModel('FileUploads');
        $this->loadModel('Keywords');
        $this->loadModel('KeywordsDonations');
        $this->loadModel('TimeDonations');
        $this->loadModel('Users');
        $this->loadModel('ValidZipcodes');
        $this->loadModel('Zipcodes');
        $this->set('title', 'Spende abgeben');
        $donation = $this->Donations->newEntity();
        if ($this->request->is(['patch', 'put', 'post'])) {

            /**
             * Captcha-Prüfung
             */
            if (in_array($_SERVER['HTTP_HOST'], $this->request->session()->read('captcha_free_hosts'))) {
            } else if ($this->Auth->user('role')!='super' &&
                $this->Auth->user('role')!='broker' &&
                $this->Auth->user('role')!='admin' &&
                $this->Auth->user('role')!='token_account') {
                $response = file_get_contents("XXXX");
                $response = json_decode($response);
                if ($response->success == false) {
                    $this->Flash->error(__('Es ist ein Fehler aufgetreten, bitte kontaktiere den Support.'));
                    return $this->redirect('/spende_abgeben');
                }
            }

            /**
             * Prüfung der Pflichtfelder und Werte
             */
            $helpers_controller = new HelpersController();
            $valid = $helpers_controller->validateNew("internal_request", $this->request->data);
            if (!$valid) {
                $this->Flash->error(__('Alle Pflichtfelder müssen ausgefüllt werden.'));
                return $this->redirect('/spende_abgeben');
            }

            /**
             * Falls E-Mail angegeben wurde -> token_account, falls nicht -> no_account
             */
            if (isset($this->request->data['email']) && trim($this->request->data['email'])!='') {
                $this->request->data['role'] = "token_account";
            } else {
                $this->request->data['role'] = "no_account";
            }

            /**
             * Falls E-Mail angegeben wurde -> Logindaten senden
             */
            if (isset($this->request->data['email']) && trim($this->request->data['email'])!='') {
                if (isset($this->request->data['role']) && $this->request->data['role'] == "token_account") {
                    $send_login = true;
                }
            }

            /**
             * Stadt aus Datenbank zu angegebener Postleitzahl liefern
             */
            if (isset($this->request->data['zip']) && strlen($this->request->data['zip'])) {
                $zipcode = $this->Zipcodes
                    ->find()
                    ->where(['Zipcodes.zipcode' => trim($this->request->data['zip'])])
                    ->first();
                if ($zipcode) {
                    $city = $this->Cities
                        ->find()
                        ->where(['Cities.id' => $zipcode->city_id])
                        ->first();
                    if ($city) {
                        $this->request->data['city'] = $city->name;
                    }
                }
            }

            /**
             * Falls Logindaten gesendet werden sollen
             */
            if (isset($send_login) && $send_login == true) {
                if (isset($this->request->data['email']) && trim($this->request->data['email']) != '') {

                    $user = $this->Helper->__returnEmailUser(trim($this->request->data['email']));

                    $configuration = $this->Helper->__returnConfiguration();

                    /**
                     * Falls E-Mail-Adresse noch frei -> neuen Nutzer anlegen
                     */
                    if (!$user) {

                        $new_user_entity = [
                            'configuration_id' => $this->request->session()->read('configuration_id'),
                            'firstname' => trim($this->request->data['firstname']),
                            'lastname' => trim($this->request->data['lastname']),
                            'email' => trim($this->request->data['email']),
                            'phone' => trim($this->request->data['phone']),
                            'role' => $this->request->data['role']
                        ];
                        $new_user = $this->Users->newEntity($new_user_entity);
                        $new_user = $this->Users->save($new_user);

                        $token = "XXXX";
                        $login_token = "XXXX";
                        $login_link = $configuration->host . $login_token;

                        if (isset($this->request->data['active']) && $this->request->data['active'] == 0 && isset($this->request->data['hidden_inactive_mail']) && strlen($this->request->data['hidden_inactive_mail'])>0) {
                        } else {
                            $mail_template = $this->Helper->__returnEmailTemplate('neue_spende_kein_benutzer');

                            $mail_content = $mail_template->template_content;
                            $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
                            $mail_content = str_replace('{{login_link}}', $login_link, $mail_content);
                            $mail_content = str_replace('{{firstname}}', $new_user->firstname, $mail_content);

                            /**
                             * Neue E-Mail vorbereiten
                             */
                            $email_data = [
                                'type' => 'default',
                                'mail_content' => $mail_content,
                                'helpers' => ['Html', 'Text'],
                                'template' => 'default',
                                'emailFormat' => 'text',
                                'sender' => [$configuration->from_mail => $configuration->from_big_title],
                                'receiver' => [trim($this->request->data['email'])],
                                'subject' => $mail_template->template_subject
                            ];
                            $this->Helper->__sendEmail($email_data);
                        }

                        /**
                         * Token für neuen Nutzer speichern
                         */
                        $update_user_entity = ['token' => $token];

                        $new_user = $this->Users->patchEntity($new_user, $update_user_entity);
                        $save_user = $this->Users->save($new_user);
                        $this->request->data['user_id'] = $save_user->id;

                        /**
                         * Falls Postleitzahl angegeben wurde -> neue Adresse erstellen
                         */
                        if (isset($this->request->data['zip']) && strlen($this->request->data['zip']) > 0 && isset($this->request->data['city']) && strlen($this->request->data['city']) > 0) {
                            $new_address_entity = [
                                'configuration_id' => $this->request->session()->read('configuration_id'),
                                'user_id' => $save_user->id,
                                'street' => $this->request->data['street'],
                                'zip' => $this->request->data['zip'],
                                'city' => $this->request->data['city'],
                                'phone' => $this->request->data['phone'],
                                'email' => $this->request->data['email']
                            ];
                            $new_address = $this->Addresses->newEntity($new_address_entity);
                            $save_address = $this->Addresses->save($new_address);
                        }
                        $this->request->data['address_id'] = $save_address->id;
                    } else {
                        /**
                         * Eingeloggter Nutzer
                         */
                        if ($this->Auth->user('id')>0) {
                            $mail_template = $this->Helper->__returnEmailTemplate('neue_spende_benutzer');

                            $user_address = $this->Helper->__returnUserAddress($user->id);
                            if ($user_address) {
                                $title = (isset($user_address->title) && $user_address->title) == 'frau' ? "Frau" : "Herr";
                            } else {
                                $title = "";
                            }

                            $mail_content = $mail_template->template_content;
                            $mail_content = str_replace('{{title}}', $title, $mail_content);
                            $mail_content = str_replace('{{firstname}}', $user->firstname, $mail_content);
                            $mail_content = str_replace('{{lastname}}', $user->lastname, $mail_content);
                            $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);

                            /**
                             * Neue E-Mail vorbereiten
                             */
                            $email_data = [
                                'type' => 'default',
                                'mail_content' => $mail_content,
                                'helpers' => ['Html', 'Text'],
                                'template' => 'default',
                                'emailFormat' => 'text',
                                'sender' => [$configuration->from_mail => $configuration->from_big_title],
                                'receiver' => [trim($this->request->data['email'])],
                                'subject' => $mail_template->template_subject
                            ];
                            $this->Helper->__sendEmail($email_data);

                            $address = $this->Helper->__returnUserAddress($user->id);
                            if (!$address) {
                                $address = $this->Helper->__returnEmailAddress(trim($this->request->data['email']));
                            }
                            if ($user->role == 'token_account') {
                                $save_address = $address;
                            } else {
                                $zipcode = $this->Zipcodes->find()->where(['Zipcodes.zipcode' => trim($this->request->data['zip'])])->first();
                                if ($zipcode) {
                                    $city = $this->Cities->find()->where(['Cities.id' => $zipcode->city_id])->first();
                                    if ($city) {
                                        $this->request->data['city'] = $city->name;
                                    }
                                }

                                if (trim($this->request->data['street']) != $address->street ||
                                    trim($this->request->data['zip']) != $address->zip ||
                                    $this->request->data['city'] != $address->city
                                ) {
                                    $new_address_entity = [
                                        'configuration_id' => $this->Helper->__returnConfiguration()->id,
                                        'user_id' => $user->id,
                                        'street' => $this->request->data['street'],
                                        'zip' => $this->request->data['zip'],
                                        'city' => $this->request->data['city']
                                    ];
                                    $new_address = $this->Addresses->newEntity($new_address_entity);
                                    $save_address = $this->Addresses->save($new_address);
                                } else {
                                    $save_address = $address;
                                }
                            }

                            $this->request->data['user_id'] = $user->id;
                            $this->request->data['address_id'] = $save_address->id;
                        } else {
                            $existing_user_without_login = true;

                            $user_address = $this->Helper->__returnUserAddress($user->id);
                            if ($user_address) {
                                $title = (isset($user_address->title) && $user_address->title) == 'frau' ? "Frau" : "Herr";
                            } else {
                                $title = "";
                            }

                            $address = $this->Helper->__returnUserAddress($user->id);
                            if (!$address) {
                                $address = $this->Helper->__returnEmailAddress(trim($this->request->data['email']));
                            }
                            if ($user->role == 'token_account') {
                                $save_address = $address;
                            } else {
                                $zipcode = $this->Zipcodes->find()->where(['Zipcodes.zipcode' => trim($this->request->data['zip'])])->first();
                                if ($zipcode) {
                                    $city = $this->Cities->find()->where(['Cities.id' => $zipcode->city_id])->first();
                                    if ($city) {
                                        $this->request->data['city'] = $city->name;
                                    }
                                }
                                if (trim($this->request->data['street']) != $address->street ||
                                    trim($this->request->data['zip']) != $address->zip ||
                                    $this->request->data['city'] != $address->city
                                ) {
                                    $new_address_entity = [
                                        'configuration' => $this->request->session()->read('configuration_id'),
                                        'user_id' => $user->id,
                                        'street' => $this->request->data['street'],
                                        'zip' => $this->request->data['zip'],
                                        'city' => $this->request->data['city'],
                                        'phone' => $this->request->data['phone'],
                                        'email' => $this->request->data['email']
                                    ];
                                    $new_address = $this->Addresses->newEntity($new_address_entity);
                                    $save_address = $this->Addresses->save($new_address);
                                } else {
                                    $save_address = $address;
                                }
                            }
                            $this->request->data['user_id'] = $user->id;
                            $this->request->data['address_id'] = $save_address->id;
                        }
                    }
                }
            } else if (isset($this->request->data['zip']) && strlen(trim($this->request->data['zip']))>0) {
                $new_user_entity = [
                    'role' => 'no_account',
                    'active' => 0,
                ];
                $new_user = $this->Users->newEntity($new_user_entity);
                $save_user = $this->Users->save($new_user);

                $valid_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => $this->request->data['zip']])->first();
                if ($valid_zipcode) {
                    $new_address_entity = [
                        'configuration_id' => $this->request->session()->read('configuration_id'),
                        'user_id' => $save_user->id,
                        'street' => trim($this->request->data['street']),
                        'zip' => trim($this->request->data['zip']),
                        'city' => $valid_zipcode->city,
                        'phone' => $this->request->data['phone'],
                        'email' => $this->request->data['email']
                    ];
                    $new_address = $this->Addresses->newEntity($new_address_entity);
                    $save_address = $this->Addresses->save($new_address);
                }
                $this->request->data['user_id'] = $save_user->id;
                $this->request->data['address_id'] = $save_address->id;
            }

            $this->request->data['category'] = 'thing';

            if (isset($this->request->data['datetime_range_1']) && strlen($this->request->data['datetime_range_1'])>0) {
                $this->request->data['category'] = 'time';
            }

            $this->request->data['url_title'] = str_replace(" ", "_", strtolower($helpers_controller->Helper->replaceChars($this->request->data['title'])));
            $this->request->data['url_title'] = str_replace("?", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("/", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("+", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("|", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("'", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("^", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("`", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("´", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace(":", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace(".", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace(",", "_", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace(";", "_", $this->request->data['url_title']);

            if (isset($this->request->data['active']) && $this->request->data['active'] == 0 && isset($this->request->data['hidden_inactive_mail']) && strlen($this->request->data['hidden_inactive_mail'])>0) {
                $this->request->data['active'] = 0;
                $this->request->data['hidden_inactive_mail'] = $this->request->data['hidden_inactive_mail'];
            }

            $donation = $this->Donations->patchEntity($donation, $this->request->data);
            $save_donation = $this->Donations->save($donation);

            /**
             * Falls Spende außerhalb des gültigen Postleitzahlenbereichs erfolgt
             */
            if (isset($this->request->data['active']) && $this->request->data['active'] == 0 && isset($this->request->data['hidden_inactive_mail']) && strlen($this->request->data['hidden_inactive_mail'])>0) {

                $mail_template = $this->Helper->__returnEmailTemplate('email_spende_bestaetigen');

                $donation_user = $this->Helper->__returnUser($save_donation->user_id);

                $hash = "XXXX";
                $generated_token = "XXXX";
                $token = "XXXX";

                $sender_mail_content_confirm_link = "XXXX";

                $login_token = "XXXX";
                $login_token = "XXXX";
                $login_link = $configuration->host . $login_token;

                $mail_content = $mail_template->template_content;
                $mail_content = str_replace('{{sender_mail_content}}', $this->request->data['hidden_inactive_mail'], $mail_content);
                $mail_content = str_replace('{{firstname}}', $donation_user->firstname, $mail_content);
                $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
                $mail_content = str_replace('{{sender_mail_content_confirm_link}}', $sender_mail_content_confirm_link, $mail_content);
                $mail_content = str_replace('{{login_link}}', $login_link, $mail_content);

                /**
                 * Neue E-Mail vorbereiten
                 */
                $email_data = [
                    'type' => 'default',
                    'mail_content' => $mail_content,
                    'helpers' => ['Html', 'Text'],
                    'template' => 'default',
                    'emailFormat' => 'text',
                    'sender' => [$configuration->from_mail => $configuration->from_big_title],
                    'receiver' => [trim($this->request->data['email'])],
                    'subject' => $mail_template->template_subject
                ];
                $this->Helper->__sendEmail($email_data);
            }

            if (isset($existing_user_without_login) && $existing_user_without_login == true) {
                $mail_template = $this->Helper->__returnEmailTemplate('neue_spende_benutzer_ohne_login');

                $hash = "XXXX";
                $generated_token = "XXXX";
                $token = "XXXX";
                $confirm_link = "XXXX";

                $mail_content = $mail_template->template_content;
                $mail_content = str_replace('{{firstname}}', $user->firstname, $mail_content);
                $mail_content = str_replace('{{lastname}}', $user->lastname, $mail_content);
                $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
                $mail_content = str_replace('{{confirm_link}}', $confirm_link, $mail_content);

                /**
                 * Neue E-Mail vorbereiten
                 */
                $email_data = [
                    'type' => 'default',
                    'mail_content' => $mail_content,
                    'helpers' => ['Html', 'Text'],
                    'template' => 'default',
                    'emailFormat' => 'text',
                    'sender' => [$configuration->from_mail => $configuration->from_big_title],
                    'receiver' => [trim($this->request->data['email'])],
                    'subject' => $mail_template->template_subject
                ];
                $this->Helper->__sendEmail($email_data);
            }

            /**
             * Falls Zeitraum gesetzt wurde, Zeiten für Zeitspende speichern
             */
            if (isset($this->request->data['datetime_range_1']) && strlen($this->request->data['datetime_range_1'])>0) {

                $this->request->data['category'] = 'time';
                for ($i=1;$i<15;$i++) {
                    if (isset($this->request->data['datetime_range_'.$i]) && strlen($this->request->data['datetime_range_'.$i])>0) {

                        $datetime_range_array = explode(' - ', $this->request->data['datetime_range_'.$i]);
                        $start_datetime = explode(' ', $datetime_range_array[0]);
                        $end_datetime = explode(' ', $datetime_range_array[1]);

                        $start_date_unformatted = $start_datetime[0];
                        $start_time_unformatted = $start_datetime[1];
                        $end_date_unformatted = $end_datetime[0];
                        $end_time_unformatted = $end_datetime[1];

                        $start_time_format = $start_time_unformatted.":00";
                        $end_time_format = $end_time_unformatted.":00";

                        $start_date = new\ DateTime($start_date_unformatted);
                        $end_date = new\ DateTime($end_date_unformatted);

                        $new_time_donation_entity = [
                            'configuration_id' => $this->request->session()->read('configuration_id'),
                            'donation_id' => $save_donation->id,
                            'start_date' => $start_date,
                            'end_date' => $end_date,
                            'start_time' => $start_time_format,
                            'end_time' => $end_time_format,
                        ];

                        $new_time_donation = $this->TimeDonations->newEntity($new_time_donation_entity);

                        $this->TimeDonations->save($new_time_donation);
                    } else {
                        break;
                    }
                }
            }

            /**
             * Keywords speichern
             */
            if (isset($this->request->data['keywords']) && strlen($this->request->data['keywords'])>0) {
                $keywords_array = explode(",", trim($this->request->data['keywords']));
                foreach ($keywords_array as $keyword) {

                    $keyword_exists = $this->Keywords->find()->where(['Keywords.keyword' => $keyword])->first();
                    if (!$keyword_exists) {
                        $new_keyword_entity = [
                            'configuration_id' => $this->request->session()->read('configuration_id'),
                            'keyword' => $keyword,
                            'created' => new\ DateTime(date('Y-m-d H:i:s')),
                            'modified' => new\ DateTime(date('Y-m-d H:i:s')),
                        ];
                        $new_keyword = $this->Keywords->newEntity($new_keyword_entity);
                        $keyword_exists = $this->Keywords->save($new_keyword);
                    }

                    $keyword_donation_exists = $this->KeywordsDonations->find()->where(['KeywordsDonations.donation_id' => $save_donation->id, 'KeywordsDonations.keyword_id' => $keyword_exists->id])->andWhere([$this->request->session()->read('branch_filter_keyword_donation')])->first();
                    if (!$keyword_donation_exists) {
                        $new_keyword_donation_entity = [
                            'configuration_id' => $this->request->session()->read('configuration_id'),
                            'donation_id' => $save_donation->id,
                            'keyword_id' => $keyword_exists->id,
                            'created' => new\ DateTime(date('Y-m-d H:i:s'))
                        ];
                        $new_keyword_donation = $this->KeywordsDonations->newEntity($new_keyword_donation_entity);
                        $this->KeywordsDonations->save($new_keyword_donation);
                    }
                }
            }

            /**
             * Upload durchführen
             */
            if (isset($this->request->data['uploads']) && !empty($this->request->data['uploads'])) {
                foreach ($this->request->data['uploads'] as $upload) {
                    $file_upload = $this->FileUpload->process($upload);

                    $new_fileupload_entity = [
                        'configuration_id' => $this->request->session()->read('configuration_id'),
                        'donation_id' => $save_donation->id,
                        'src' => $file_upload,
                        'type' => $upload['type'],
                        'filename' => $upload['name'],
                        'size' => $upload['size']
                    ];

                    $fileupload_entity = $this->FileUploads->newEntity($new_fileupload_entity);
                    $this->FileUploads->save($fileupload_entity);
                }
            }

            if ($save_donation) {
                if ($save_donation->active == 0 && strlen($save_donation->hidden_inactive_mail)>0) {
                    $this->Flash->success(__('Deine Spende wurde eingereicht. Bitte prüfe deinen E-Mail-Posteingang, um die Spendenabgabe zu bestätigen.'));
                    return $this->redirect(['action' => 'add']);
                } else {
                    $this->Flash->success(__('Deine Spende wurde erfolgreich veröffentlicht.'));
                    return $this->redirect(['action' => 'view', $save_donation->id]);
                }
            } else {
                $this->Flash->error(__('Deine Spende wurde nicht abgegeben. Bitte versuche es noch einmal.'));
            }
        }

        /**
         * Falls der Benutzer eingeloggt ist, prüfe ob eine Adresse vorhanden ist
         */
        if ($this->Auth->user('id')>0) {
            $user = $this->Users->find()->where(['Users.id' => $this->Auth->user('id')])->first();
            if ($user) {
                $this->set('user', $user);
                if ($user->role == 'broker' || $user->role == 'token_account') {
                    $address = $this->Helper->__returnUserAddress($this->Auth->user('id'));
                    if ($address) {
                        $this->set('address', $address);
                    }
                }
            }
        } else {
            $this->set('no_user', true);
        }

        if (isset($id_token) && strlen($id_token)>0) {
            $donation_keywords = $this->KeywordsDonations
                ->find()
                ->where(['KeywordsDonations.donation_id' => base64_decode($id_token)])
                ->andWhere([$this->request->session()->read('branch_filter_keyword_donation')])
                ->all();
            $keyword_ids = array();
            foreach ($donation_keywords as $donation_keyword) {
                $keyword_ids[] = $donation_keyword->keyword_id;
            }
            $similiar_keywords = $this->Keywords
                ->find()
                ->where(['Keywords.id IN' => $keyword_ids])
                ->andWhere([$this->request->session()->read('branch_filter_keyword')])
                ->all();

            $keywords_string = "";
            foreach ($similiar_keywords as $key => $similiar_keyword) {
                if ($key > 0) {
                    $keywords_string .= ", " . $similiar_keyword->keyword;
                } else {
                    $keywords_string .= $similiar_keyword->keyword;
                }
            }
            $this->set('keywords_string', $keywords_string);
        }
        $donation->uploads = array();
        $this->set(compact('donation'));
        $this->set('_serialize', ['donation']);
    }

    /**
     * Alle Spenden darstellen (unabhängig ob Sach- oder Zeitspende)
     *
     * @param null $page
     * @param null $start_end_type
     * @param null $start_end_id
     */
    public function allDonations($page = null, $start_end_type = null, $start_end_id = null)
    {
        if (isset($page) && $page) {
            $this->request->session()->write('page', $page);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_type', $start_end_type);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_id', $start_end_id);
        }
        if (!isset($page) && !isset($start_end_type) && !isset($start_end_id)) {
            $this->request->session()->delete('page');
            $this->request->session()->delete('start_end_type');
            $this->request->session()->delete('start_end_id');
        }
        return $this->redirect('/auflistung/' . base64_encode("donation|all"));

    }

    /**
     * Methode zum Bearbeiten einer Spende
     *
     * @param string|null $id Donation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $donation = $this->Donations->get($id, [
            'contain' => ['Keywords']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $donation = $this->Donations->patchEntity($donation, $this->request->data);
            if ($this->Donations->save($donation)) {
                $this->Flash->success(__('Die Spende wurde erfolgreich gespeichert.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Die Spende wurde nicht gespeichert. Bitte, nochmal versuchen.'));
            }
        }
        $users = $this->Donations->Users->find('list', ['limit' => 200]);
        $keywords = $this->Donations->Keywords->find('list', ['limit' => 200]);
        $this->set(compact('donation', 'users', 'keywords'));
        $this->set('_serialize', ['donation']);
    }

    /**
     * Methode zum Löschen einer Spende
     *
     * @param string|null $id Donation id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $donation = $this->Donations->get($id);
        if ($this->Donations->delete($donation)) {
            $this->Flash->success(__('Die Spende wurde erfolgreich gelöscht.'));
        } else {
            $this->Flash->error(__('Die Spende wurde nicht gelöscht. Bitte, nochmal versuchen.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
