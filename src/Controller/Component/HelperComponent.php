<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Mailer\Email;

/**
 * HelperComponent
 */
class HelperComponent extends Component {

    /**
     * Methode zum Erstellen eines Logs
     *
     * @param array $log_array
     * @return bool
     */
    public function __log(array $log_array) {
        $this->Logs = TableRegistry::get('Logs');
        $new_log = $this->Logs->newEntity($log_array);
        if ($this->Logs->save($new_log)) {
            return true;
        }
        return false;
    }

    /**
     * Methode zum Generieren von verwandten Spenden
     *
     * @param $donation_id
     * @return array
     */
    public function __getRelatedDonations($donation_id) {

        /**
         * Hole alle Keywords, zu denen die Spende mit $donation_id zugehörig ist
         */
        $this->KeywordsDonations = TableRegistry::get('KeywordsDonations');
        $keywords_donation = $this->KeywordsDonations
            ->find()
            ->select(['KeywordsDonations.keyword_id'])
            ->where(['KeywordsDonations.donation_id' => $donation_id])
            ->andWhere([$this->request->session()->read('branch_filter')])
            ->hydrate(false);

        /**
         * Hole alle zugehörigen Spenden, die zu den gefundenen Keywords aus $keywords_donation zugewiesen sind
         */
        $all_related_donations = $this->KeywordsDonations
            ->find()
            ->select(['KeywordsDonations.donation_id'])
            ->where(['KeywordsDonations.keyword_id IN' => $keywords_donation, 'KeywordsDonations.donation_id !=' => $donation_id])
            ->andWhere([$this->request->session()->read('branch_filter')])
            ->limit(3);

        $temp_related_ids = array();
        foreach ($all_related_donations as $related_donation) {
            if (isset($temp_related_ids[$related_donation->donation_id])) {
                $temp_related_ids[$related_donation->donation_id] += 1;
            }
            $temp_related_ids[$related_donation->donation_id] = 1;
        }

        $related_ids = array();
        foreach ($temp_related_ids as $id => $id_counts) {
            if ($id_counts > 1) {
                $related_ids[$id] = $id;
            }
        }

        /**
         * Gebe alle verwandten Spenden zur Spende mit $donation_id zurück
         */
        return $related_ids;
    }

    /**
     * Methode zum Generieren von verwandten Gesuche
     *
     * @param $search_id
     * @return array
     */
    public function __getRelatedSearches($search_id)
    {
        $this->KeywordsSearches = TableRegistry::get('KeywordsSearches');
        $keywords_search = $this->KeywordsSearches->find()->select(['KeywordsSearches.keyword_id'])->where(['KeywordsSearches.search_id' => $search_id])->hydrate(false);
        $all_related_searches = $this->KeywordsSearches->find()->select(['KeywordsSearches.search_id'])->where(['KeywordsSearches.keyword_id IN' => $keywords_search, 'KeywordsSearches.search_id !=' => $search_id])->limit(3);
        $temp_related_ids = array();
        foreach ($all_related_searches as $related_search) {
            if (isset($temp_related_ids[$related_search->search_id])) {
                $temp_related_ids[$related_search->search_id] += 1;
            }
            $temp_related_ids[$related_search->search_id] = 1;
        }

        $related_ids = array();
        foreach ($temp_related_ids as $id => $id_counts) {
            if ($id_counts > 1) {
                $related_ids[$id] = $id;
            }
        }

        return $related_ids;
    }

    /**
     * Methode zum Holen eines Benutzers anhand der Benutzer-ID und / oder der Benutzer-E-Mail-Adresse
     *
     * @param $user_id
     * @param null $user_email
     */
    public function __returnUser($user_id, $user_email = null) {
        $this->Users = TableRegistry::get('Users');
        if ($user_email) {
            return $this->Users->find()->where(['Users.id' => $user_id, 'Users.email' => $user_email])->first();
        }
        return $this->Users->get($user_id);
    }

    /**
     * Methode zum Holen eines Vermittler-Benutzers anhand der Benutzer-ID
     *
     * @param $user_id
     * @return mixed
     */
    public function __returnBrokerUser($user_id) {
        $this->Users = TableRegistry::get('Users');
        return $this->Users->find()->where(['Users.id' => $user_id, 'Users.role' => 'broker'])->first();
    }

    /**
     * Gibt den Benutzer anhand der Benutzer-E-Mail zurück
     *
     * @param $user_email
     * @return mixed
     */
    public function __returnEmailUser($user_email) {
        $this->Users = TableRegistry::get('Users');
        return $this->Users->find()->where(['Users.email' => trim($user_email)])->first();
    }

    /**
     * Methode zum Holen einer Adresse anhand der Adressen-ID und / oder der Benutzer-ID
     *
     * @param $address_id
     * @param $user_id
     * @return mixed
     */
    public function __returnAddress($address_id, $user_id = null) {
        $this->Addresses = TableRegistry::get('Addresses');
        if (isset($user_id) && $user_id>0) {
            return $this->Addresses->find()->where(['Addresses.id' => $address_id, 'Addresses.user_id' => $user_id])->first();
        }
        return $this->Addresses->find()->where(['Addresses.id' => $address_id])->first();
    }

    /**
     * Gibt die Adresse anhand der Benutzer-ID zurück
     *
     * @param $user_id
     * @return mixed
     */
    public function __returnUserAddress($user_id) {
        $this->Addresses = TableRegistry::get('Addresses');
        return $this->Addresses->find()->where(['Addresses.user_id' => $user_id])->first();
    }

    /**
     * Gibt die Adresse anhand der Benutzer-E-Mail zurück
     *
     * @param $user_email
     * @return mixed
     */
    public function __returnEmailAddress($user_email) {
        $this->Addresses = TableRegistry::get('Addresses');
        return $this->Addresses->find()->where(['Addresses.email' => $user_email])->first();
    }

    /**
     * Methode zum Holen einer Datei anhand des Typs (Gesuch oder Spende) und der entsprechenden Typ-ID
     *
     * @param $case
     * @param $case_id
     * @return mixed
     */
    public function __returnFileUpload($case, $case_id) {
        $this->FileUploads = TableRegistry::get('FileUploads');
        switch($case) {
            case "search":
                return $this->FileUploads->find()->where(['FileUploads.search_id' => $case_id])->first();
                break;
            case "donation":
                return $this->FileUploads->find()->where(['FileUploads.donation_id' => $case_id])->first();
                break;
        }
    }

    /**
     *
     *
     * @param $case
     * @param $case_id
     * @param bool $count
     * @return mixed
     */
    public function __returnFileUploads($case, $case_id, $count = false) {
        $this->FileUploads = TableRegistry::get('FileUploads');
        if ($count && $count == true) {
            switch ($case) {
                case "search":
                    return $this->FileUploads->find()->where(['FileUploads.search_id' => $case_id])->first();
                    break;
                case "donation":
                    return $this->FileUploads->find()->where(['FileUploads.donation_id' => $case_id])->first();
                    break;
            }
        } else {
            switch ($case) {
                case "search":
                    return $this->FileUploads->find()->where(['FileUploads.search_id' => $case_id]);
                    break;
                case "donation":
                    return $this->FileUploads->find()->where(['FileUploads.donation_id' => $case_id]);
                    break;
            }
        }
    }

    /**
     * Gibt das E-Mail-Template anhand des Templatenames zurück
     *
     * @param $name
     * @return mixed
     */
    public function __returnEmailTemplate($name) {
        $this->EmailTemplates = TableRegistry::get('EmailTemplates');
        return $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => $name])->first();
    }

    /**
     * Gibt die Spende anhand der Spenden-ID und / oder anhand der Benutzer-ID zurück
     *
     * @param $donation_id
     * @param $active_status
     * @param null $user_id
     */
    public function __returnDonation($donation_id, $active_status, $user_id = null) {
        $this->Donations = TableRegistry::get('Donations');
        if ($user_id) {
            return $this->Donations->find()->where(['Donations.id' => $donation_id, 'Donations.active' => $active_status, 'Donations.user_id' => $user_id])->first();
        }
        return $this->Donations->find()->where(['Donations.id' => $donation_id, 'Donations.active' => $active_status])->first();
    }

    /**
     * Gibt die aktuelle Konfiguration anhand der Konfigurations-ID in der Session zurück
     *
     * @return mixed
     */
    public function __returnConfiguration() {
        $this->Configurations = TableRegistry::get('Configurations');
        return $this->Configurations->get($this->request->session()->read('configuration_id'));
    }

    /**
     * Gibt die Fehlermeldung bei Validation zurück
     *
     * @param $name
     * @param $input
     * @param $type
     * @return mixed
     */
    public function __returnMessage($name, $input, $type = null) {
        $this->Messages = TableRegistry::get('Messages');
        if (isset($type) && ($type=='error' || $type=='warning')) {
            $message = $this->Messages->find()->where(['Messages.name' => $name, 'Messages.type' => $type])->first();
        } else {
            $message = $this->Messages->find()->where(['Messages.name' => $name])->first();
        }
        $return[$input]['name'] = $message->name;
        $return[$input]['text'] = $message->text;
        $return[$input]['type'] = $message->type;
        return $return;
    }

    public function __returnCity($city_id) {
        $this->Cities = TableRegistry::get('Cities');
        return $this->Cities->find()->where(['Cities.id' => $city_id])->first();
    }

    public function __returnZipcode($zipcode_id) {
        $this->Zipcodes = TableRegistry::get('Zipcodes');
        return $this->Zipcodes->find()->where(['Zipcodes.zipcode' => trim($zipcode_id)])->first();
    }

    /**
     * E-Mail versenden
     *
     * @param $email_data
     * @param $email_log_data
     */
    public function __sendEmail($email_data) {
        $email = new Email($email_data['type']);
        $email->helpers($email_data['helpers']);

        /**
         * Variablen an das Template übergeben werden sollen
         */
        $email->viewVars(
            array(
                'mail_content' => $email_data['mail_content']
            )
        );
        /**
         * Template der E-Mail
         */
        $email->template($email_data['template']);
        $email->emailFormat($email_data['emailFormat']);
        /**
         * E-Mail-Absender setzen
         */
        $email->from($email_data['sender']);
        /**
         * E-Mail-Empfänger setzen
         */
        $email->to($email_data['receiver']);
        /**
         * Betreff der E-Mail setzen
         */
        $email->subject($email_data['subject']);
        if ($email->send()) {
            /**
             * Neuen E-Mail-Log erstellen
             */
            /*
            $new_email_log_entity = [
                'configuration_id' => $this->__returnConfiguration()->id,
                'send' => 1,
                'subject' => $email_data['subject'],
                'content' => $email_data['content'],
                'recipient_name' => "",
                'recipient_email' => $email_data['receiver'],
                'sender_name' => $this->__returnConfiguration()->from_big_title,
                'sender_email' => $this->__returnConfiguration()->from_mail
            ];
            $this->__saveEmailLog($new_email_log_entity);
            */
        }
    }

    /**
     * Gesendete E-Mail speichern
     *
     * @param $email_log_entity
     */
    public function __saveEmailLog($email_log_entity) {
        $this->EmailLogs = TableRegistry::get('EmailLogs');
        $new_email_log = $this->EmailLogs->newEntity($email_log_entity);
        $this->EmailLogs->save($new_email_log);
    }

    /**
     * Gibt ein Element (Spende oder Gesuch) anhand des URL-Titels zurück
     *
     * @param $url_title
     * @return bool
     */
    public function __returnItem($url_title) {
        $this->Donations = TableRegistry::get('Donations');
        $this->Searches = TableRegistry::get('Searches');
        $try_donation = $this->Donations->find()->where(['Donations.url_title' => $url_title])->first();
        if (!$try_donation) {
            $try_search = $this->Searches->find()->where(['Searches.url_title' => $url_title])->first();
            if (!$try_search) {
                return false;
            } else {
                $try_search->type = "search";
                return $try_search;
            }
        } else {
            $try_donation->type = "donation";
            return $try_donation;
        }
    }

    /**
     * Öffentliche Methode zum Ersetzen von Umlauten
     *
     * @param $string
     * @return mixed
     */
    public function replaceChars($string) {
        return $this->__replaceChars($string);
    }

    /**
     * Öffentliche Methode zum Ersetzen von Umlauten (rückwärts)
     *
     * @param $string
     * @return mixed
     */
    public function replaceCharsBackwards($string) {
        return $this->__replaceCharsBackwards($string);
    }

    /**
     * Private Methode zum Ersetzen von Umlauten
     *
     * @param $string
     * @return mixed
     */
    private function __replaceChars($string) {
        $string = str_replace("ä", "ae", $string);
        $string = str_replace("ü", "ue", $string);
        $string = str_replace("ö", "oe", $string);
        $string = str_replace("Ä", "Ae", $string);
        $string = str_replace("Ü", "Ue", $string);
        $string = str_replace("Ö", "Oe", $string);
        $string = str_replace("ß", "ss", $string);
        $string = str_replace("´", "", $string);
        return $string;
    }

    /**
     * Private Methode zum Ersetzen von Umlauten (rückwärts)
     *
     * @param $string
     * @return mixed
     */
    private function __replaceCharsBackwards($string) {
        $string = str_replace("ae", "ä", $string);
        $string = str_replace("ue", "ü", $string);
        $string = str_replace("oe", "ö", $string);
        $string = str_replace("Ae", "Ä", $string);
        $string = str_replace("Ue", "Ü", $string);
        $string = str_replace("Oe", "Ö", $string);
        return $string;
    }
}