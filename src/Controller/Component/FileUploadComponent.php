<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

/**
 * FileUploadComponent
 */
class FileUploadComponent extends Component {
    const IMAGE_JPG = "jpg";
    const IMAGE_PNG = "png";

    /**
     * @var array
     */
    public $imageMimeTypes = ['image/png' => 'png', 'image/jpeg' => 'jpg', 'image/gif' => 'gif', 'image/jpg' => 'jpg'];

    /**
     * Methode zur Initialisierung aller Dateiuploads
     *
     * @param array $config
     */
    public function initialize(array $config) {
        $this->FileUploads = TableRegistry::get('FileUploads');
    }

    /**
     * Methode zur Überprüfung eines Pfades
     *
     * @param $path
     */
    private function __checkPathExists($path) {
        @mkdir($path, 0775, true);
    }

    /**
     * Methode zur Abarbeitung eines Dateiuploads
     *
     * @param $file
     * @param $type
     * @return string
     */
    private function __handleUpload($file) {
        if (!array_key_exists($file['type'], $this->imageMimeTypes)) {
            return "invalid_file_type";
        }

        if (array_key_exists($file['type'], $this->imageMimeTypes)) {
            $fileExtension = $this->imageMimeTypes[$file['type']];
        }

        $basePath = "/files/donations/";
        $this->__checkPathExists(WWW_ROOT.$basePath);
        $uploading_file = $basePath.uniqid().".".$fileExtension;
        $this->__imageFixOrientation($file['tmp_name'], $file['type']);

        move_uploaded_file($file['tmp_name'], rtrim(WWW_ROOT, "/") . $uploading_file);

        if (empty($uploading_file)) {
            return "failed_upload";
        }
        return $uploading_file;
    }

    /**
     *
     *
     * @param $requestData
     * @param $type
     * @return string
     */
    public function process($requestData) {
        return $this->__handleUpload($requestData);
    }

    /**
     * Methode zur Reparatur der Bild-Orientierung
     *
     * @param $path
     * @param $filetype
     * @return bool
     */
    private function __imageFixOrientation($path, $filetype)
    {
        Log::debug($filetype);
        switch($filetype) {
            case "image/png":
                $image = imagecreatefrompng($path);
                break;
            case "image/jpg":
            case "image/jpeg":
                $image = imagecreatefromjpeg($path);
                break;
            case "image/gif":
                $image = imagecreatefromgif($path);
                break;
        }
        $exif = exif_read_data($path);

        if (empty($exif['Orientation']))
        {
            return false;
        }

        switch ($exif['Orientation'])
        {
            case 3:
                $image = imagerotate($image, 180, 0);
                break;
            case 6:
                $image = imagerotate($image, - 90, 0);
                break;
            case 8:
                $image = imagerotate($image, 90, 0);
                break;
        }

        switch($filetype) {
            case "image/png":
                imagepng($image, $path);
                break;
            case "image/jpg":
            case "image/jpeg":
                imagejpeg($image, $path);
                break;
            case "image/gif":
                imagegif($image, $path);
                break;
        }

        return true;
    }
}