<?php
namespace App\Controller\Broker;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Event\Event;
use App\Controller\Component\FileUploadComponent;
use App\Controller\HelpersController;
use Cake\Log\Log;


/**
 * Donations Controller
 *
 * @property \App\Model\Table\DonationsTable $Donations
 */
class DonationsController extends AppController
{

    /**
     * Komponente für Dateiupload
     *
     * @var array
     */
    public $components = ['FileUpload'];

    /**
     * @param null $id
     * @return bool
     */
    private function __checkAccess($id = null) {

        if ($this->Auth->user('id')>0 && ($this->Auth->user('role') == 'broker' || $this->Auth->user('role') == 'admin')) {
            return true;
        } else if (isset($id) && $id>0) {
            $donation = $this->Donations->get($id);
            if ($donation->user_id == $this->Auth->user('id')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        $this->loadModel('Addresses');
        $this->loadModel('Cities');
        $this->loadModel('Configurations');
        $this->loadModel('Donations');
        $this->loadModel('EmailTemplates');
        $this->loadModel('FileUploads');
        $this->loadModel('Keywords');
        $this->loadModel('KeywordsDonations');
        $this->loadModel('KeywordsSearches');
        $this->loadModel('Logs');
        $this->loadModel('Matches');
        $this->loadModel('Searches');
        $this->loadModel('TimeDonations');
        $this->loadModel('TimeSearches');
        $this->loadModel('Users');
        $this->loadModel('ValidZipcodes');
        $this->loadModel('Zipcodes');
    }


    /**
     * Methode zur Darstellung des Dashboards für Vermittler
     *
     * @return void
     */
    public function index()
    {

        $donations = $this->Donations->find()->all();
        $donations_array = array();
        foreach ($donations as $donation) {

            $keywords_donations = $this->KeywordsDonations->find()->where(['KeywordsDonations.donation_id' => $donation->id])->all();
            $keywords = array();
            foreach ($keywords_donations as $keywords_donation) {
                $keyword = $this->Keywords->find()->where(['Keywords.id' => $keywords_donation->keyword_id])->first();
                $keywords[] = $keyword->keyword;
            }
            $donation->keywords = $keywords;
            $donations_array[] = $donation;
        }
        $this->set('donations', $donations_array);
        $this->set('_serialize', ['donations']);

        $this->paginate = [
            'contain' => ['Users']
        ];
        $this->set('donations', $this->paginate($this->Donations));
        $this->set('_serialize', ['donations']);
    }

    /**
     * View method
     *
     * @param string|null $id Donation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $keywords_donations = $this->KeywordsDonations->find()->where(['KeywordsDonations.donation_id' => $id]);

        $searches_array = array();

        $keywords_donations_ids = array();

        foreach ($keywords_donations as $keyword_donation) {
            if (isset($keywords_donations_ids[$keyword_donation->keyword_id])) {
                continue;
            }
            $keywords_donations_ids[$keyword_donation->keyword_id] = $keyword_donation->keyword_id;
        }

        $keywords_searches = $this->KeywordsSearches->find()->where(['KeywordsSearches.keyword_id IN' => $keywords_donations_ids])->all();
        foreach ($keywords_searches as $keyword_search) {
            $searches = $this->Searches->find()->where(['Searches.id' => $keyword_search->search_id, 'Searches.active' => 1])->all();
            foreach ($searches as $search) {
                if (isset($searches_array[$search->id])) {
                    $searches_array[$search->id] += 1;
                    continue;
                }
                $searches_array[$search->id] = 1;
            }
        }

        // Array sortieren (höchste Anzahl der Vorkommen zuerst

        $donation = $this->Donations->get($id);
        if ($donation->category == 'thing') {
            $case = "thing";
        } else if ($donation->category == 'time') {
            $case = "time";
        } else {
            $case = false;
        }

        if (!empty($searches_array)) {
            $this->request->session()->write('searches_array', $searches_array);
            $this->request->session()->write('donation_id', $donation->id);
            return $this->redirect('/matches_gefunden');
        } else if ($case != false) {
            $this->request->session()->write('flash', 'true');
            if ($case == 'thing') {
                return $this->redirect('/sachspende/' . $donation->url_title);
            } else if ($case == 'time') {
                return $this->redirect('/zeitspende/' . $donation->url_title);
            }
        } else if ($case == false) {
            $this->Flash->error(__('Fehlerhafte Weiterleitung.'));
            return $this->redirect('/spende_abgeben');
        }
    }

    /**
     * Methode zur Darstellung von Matches
     *
     * @return \Cake\Network\Response|null
     */
    public function viewMatches() {
        $this->set('title', 'Passende Gesuche');

        if (!$this->request->session()->read('searches_array')) {
            $this->Flash->error(__('Fehlerhafte Weiterleitung.'));
            return $this->redirect('/spende_abgeben');
        }

        $donation_id = $this->request->session()->read('donation_id');
        $searches_array = $this->request->session()->read('searches_array');

        if ($searches_array) {
            $searches = $this->Searches->find()->contain(['FileUploads'])->where(['Searches.id IN' => $searches_array])->limit(3);

            foreach ($searches as $search) {
                $new_match_entity = [
                    'search_id' => $search->id,
                    'donation_id' => $donation_id,
                    'contacted' => 0,
                    'seen' => 0
                ];
                $new_match = $this->Matches->newEntity($new_match_entity);
                $this->Matches->save($new_match);
            }

            $this->set('searches', $searches);

            //$this->request->session()->delete('searches_array');
            //$this->request->session()->delete('donation_id');
        }
    }

    /**
     * Methode zum Abgeben einer Spende
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($id_token = null)
    {
        $this->set('title', 'Spende abgeben');
        $donation = $this->Donations->newEntity();
        if ($this->request->is(['patch', 'put', 'post'])) {


            if ($this->Auth->user('role')!='super' && $this->Auth->user('role')!='broker' && $this->Auth->user('role')!='admin' && $this->Auth->user('role')!='token_account') {
                $response = file_get_contents("XXXX");
                $response = json_decode($response);
                if ($response->success == false) {
                    $this->Flash->error(__('Es ist ein Fehler aufgetreten, bitte kontaktiere den Support.'));
                    return $this->redirect('/spende_abgeben');
                }
            }

            // HelpersController initialisieren
            $helpers_controller = new HelpersController();
            $valid = $helpers_controller->validateNew("internal_request", $this->request->data);
            if (!$valid) {
                $this->Flash->error(__('Alle Pflichtfelder müssen ausgefüllt werden.'));
                return $this->redirect('/spende_abgeben');
            }

            // Falls E-Mail-Adresse angegeben wurde, erstellen "token_account", falls keine E-Mail-Adresse angegeben wurde, erstelle "no_account"
            if (isset($this->request->data['email']) && trim($this->request->data['email'])!='') {
                $this->request->data['role'] = "token_account";
            } else {
                $this->request->data['role'] = "no_account";
            }

            // E-Mail überprüfen
            if (isset($this->request->data['email']) && trim($this->request->data['email'])!='') {
                if (isset($this->request->data['role']) && $this->request->data['role'] == "token_account") {
                    $send_login = true;
                }
            }

            // Postleitzahl überprüfen
            if (isset($this->request->data['zip']) && strlen($this->request->data['zip'])) {
                $valid_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => trim($this->request->data['zip'])])->first();
                if ($valid_zipcode) {
                    $this->request->data['city'] = $valid_zipcode->city;
                }
            }

            // Falls E-Mail-Adresse angegeben wurde, Logininformationen senden
            if (isset($send_login) && $send_login == true) {
                if (isset($this->request->data['email']) && trim($this->request->data['email']) != '') {

                    $user = $this->Users->find()->where(['Users.email' => trim($this->request->data['email'])])->first();

                    $configuration = $this->Configurations->get(1);

                    // Falls Benutzer nicht existiert, erstelle neuen Benutzer
                    if (!$user) {

                        $new_user_entity = [
                            'firstname' => trim($this->request->data['firstname']),
                            'lastname' => trim($this->request->data['lastname']),
                            'email' => trim($this->request->data['email']),
                            'phone' => trim($this->request->data['phone']),
                            'role' => $this->request->data['role']
                        ];
                        $new_user = $this->Users->newEntity($new_user_entity);

                        $new_user = $this->Users->save($new_user);

                        $token = base64_encode($new_user->id . "|" . 'hdeaxezKkigC' . "|" . $new_user->email);

                        $login_token = "/CqikKzexaedh/" . $token;
                        $login_link = $configuration->host . $login_token;

                        $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'neue_spende_kein_benutzer'])->first();

                        $mail_content = $mail_template->template_content;
                        $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
                        $mail_content = str_replace('{{login_link}}', $login_link, $mail_content);
                        $mail_content = str_replace('{{firstname}}', $new_user->firstname, $mail_content);

                        $email = new email('default');
                        $email->helpers(array('Html', 'Text'));
                        $email->viewvars(
                            array(
                                'mail_content' => $mail_content
                            )
                        );
                        $email->template('default');
                        $email->emailformat('text');
                        $email->from(array($configuration->from_mail => $configuration->from_big_title));
                        $email->to(array($this->request->data['email']));
                        $email->subject($mail_template->template_subject);
                        if ($email->send()) {
                            $this->loadModel('EmailLogs');
                            $new_email_log_entity = [
                                'send' => 1,
                                'subject' => $mail_template->template_subject,
                                'content' => $mail_content,
                                'recipient_name' => "",
                                'recipient_email' => $this->request->data['email'],
                                'sender_name' => $configuration->from_big_title,
                                'sender_email' => $configuration->from_mail
                            ];
                            $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                            $this->EmailLogs->save($new_email_log);
                        }

                        $update_user_entity = [
                            'token' => $token
                        ];

                        $new_user = $this->Users->patchEntity($new_user, $update_user_entity);

                        $save_user = $this->Users->save($new_user);
                        $this->request->data['user_id'] = $save_user->id;

                        if (isset($this->request->data['zip']) && strlen($this->request->data['zip'])>0 && isset($this->request->data['city']) && strlen($this->request->data['city'])>0) {
                            $new_address_entity = [
                                'user_id' => $save_user->id,
                                'street' => $this->request->data['street'],
                                'zip' => $this->request->data['zip'],
                                'city' => $this->request->data['city']
                            ];
                            $new_address = $this->Addresses->newEntity($new_address_entity);
                            $save_address = $this->Addresses->save($new_address);
                        }
                        $this->request->data['address_id'] = $save_address->id;

                    } else {

                        // Eingeloggter Nutzer
                        if ($this->Auth->user('id')>0) {
                            $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'neue_spende_benutzer'])->first();

                            $user_address = $this->Addresses->find()->where(['Addresses.user_id' => $user->id])->first();
                            if ($user_address) {
                                $title = (isset($user_address->title) && $user_address->title) == 'frau' ? "Frau" : "Herr";
                            } else {
                                $title = "";
                            }

                            $mail_content = $mail_template->template_content;
                            $mail_content = str_replace('{{title}}', $title, $mail_content);
                            $mail_content = str_replace('{{firstname}}', $user->firstname, $mail_content);
                            $mail_content = str_replace('{{lastname}}', $user->lastname, $mail_content);
                            $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);

                            $email = new email('default');
                            $email->helpers(array('Html', 'Text'));
                            $email->viewvars(
                                array(
                                    'mail_content' => $mail_content
                                )
                            );
                            $email->template('default');
                            $email->emailformat('text');
                            $email->from(array($configuration->from_mail => $configuration->from_big_title));
                            $email->to(array($this->request->data['email']));
                            $email->subject($mail_template->template_subject);
                            if ($email->send()) {
                                $this->loadModel('EmailLogs');
                                $new_email_log_entity = [
                                    'send' => 1,
                                    'subject' => $mail_template->template_subject,
                                    'content' => $mail_content,
                                    'recipient_name' => "",
                                    'recipient_email' => $this->request->data['email'],
                                    'sender_name' => $configuration->from_big_title,
                                    'sender_email' => $configuration->from_mail
                                ];
                                $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                                $this->EmailLogs->save($new_email_log);
                            }

                            $address = $this->Addresses->find()->where(['Addresses.user_id' => $user->id])->first();
                            if (!$address) {
                                $address = $this->Addresses->find()->where(['Addresses.email' => $this->request->data['email']])->first();
                            }
                            if ($user->role == 'token_account') {
                                $save_address = $address;
                            } else {
                                $zipcode = $this->Zipcodes->find()->where(['Zipcodes.zipcode' => trim($this->request->data['zip'])])->first();
                                if ($zipcode) {
                                    $city = $this->Cities->find()->where(['Cities.id' => $zipcode->city_id])->first();
                                    if ($city) {
                                        $this->request->data['city'] = $city->name;
                                    }
                                }

                                if (trim($this->request->data['street']) != $address->street ||
                                    trim($this->request->data['zip']) != $address->zip ||
                                    $this->request->data['city'] != $address->city
                                ) {
                                    $new_address_entity = [
                                        'user_id' => $user->id,
                                        'street' => $this->request->data['street'],
                                        'zip' => $this->request->data['zip'],
                                        'city' => $this->request->data['city']
                                    ];
                                    $new_address = $this->Addresses->newEntity($new_address_entity);
                                    $save_address = $this->Addresses->save($new_address);
                                } else {
                                    $save_address = $address;
                                }
                            }

                            $this->request->data['user_id'] = $user->id;
                            $this->request->data['address_id'] = $save_address->id;
                        } else {
                            $existing_user_without_login = true;

                            $user_address = $this->Addresses->find()->where(['Addresses.user_id' => $user->id])->first();
                            if ($user_address) {
                                $title = (isset($user_address->title) && $user_address->title) == 'frau' ? "Frau" : "Herr";
                            } else {
                                $title = "";
                            }

                            $address = $this->Addresses->find()->where(['Addresses.user_id' => $user->id])->first();
                            if (!$address) {
                                $address = $this->Addresses->find()->where(['Addresses.email' => $this->request->data['email']])->first();
                            }
                            if ($user->role == 'token_account') {
                                $save_address = $address;
                            } else {
                                $zipcode = $this->Zipcodes->find()->where(['Zipcodes.zipcode' => trim($this->request->data['zip'])])->first();
                                if ($zipcode) {
                                    $city = $this->Cities->find()->where(['Cities.id' => $zipcode->city_id])->first();
                                    if ($city) {
                                        $this->request->data['city'] = $city->name;
                                    }
                                }

                                if (trim($this->request->data['street']) != $address->street ||
                                    trim($this->request->data['zip']) != $address->zip ||
                                    $this->request->data['city'] != $address->city
                                ) {
                                    $new_address_entity = [
                                        'title' => $title,
                                        'user_id' => $user->id,
                                        'street' => $this->request->data['street'],
                                        'zip' => $this->request->data['zip'],
                                        'city' => $this->request->data['city']
                                    ];
                                    $new_address = $this->Addresses->newEntity($new_address_entity);
                                    $save_address = $this->Addresses->save($new_address);
                                } else {
                                    $save_address = $address;
                                }
                            }

                            $this->request->data['user_id'] = $user->id;
                            $this->request->data['address_id'] = $save_address->id;
                        }
                    }
                }
            } else if (isset($this->request->data['zip']) && strlen(trim($this->request->data['zip']))>0) {
                $new_user_entity = [
                    'role' => 'no_account',
                    'active' => 0,
                ];
                $new_user = $this->Users->newEntity($new_user_entity);
                $save_user = $this->Users->save($new_user);

                $valid_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => $this->request->data['zip']])->first();
                if ($valid_zipcode) {
                    $new_address_entity = [
                        'user_id' => $save_user->id,
                        'street' => trim($this->request->data['street']),
                        'zip' => trim($this->request->data['zip']),
                        'city' => $valid_zipcode->city
                    ];
                    $new_address = $this->Addresses->newEntity($new_address_entity);
                    $save_address = $this->Addresses->save($new_address);
                }

                $this->request->data['user_id'] = $save_user->id;
                $this->request->data['address_id'] = $save_address->id;
            }

            $this->request->data['category'] = 'thing';

            if (isset($this->request->data['datetime_range_1']) && strlen($this->request->data['datetime_range_1'])>0) {
                $this->request->data['category'] = 'time';
            }

            $this->request->data['url_title'] = str_replace(" ", "-", strtolower($helpers_controller->replaceChars($this->request->data['title'])));
            $this->request->data['url_title'] = str_replace("?", "", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("/", "|", $this->request->data['url_title']);

            $donation = $this->Donations->patchEntity($donation, $this->request->data);
            $save_donation = $this->Donations->save($donation);

            if (isset($existing_user_without_login) && $existing_user_without_login == true) {
                $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'neue_spende_benutzer_ohne_login'])->first();

                $hash = "XXXX";
                $generated_token = "XXXX";
                $token = "XXXX";
                $confirm_link = "XXXX";

                $mail_content = $mail_template->template_content;
                $mail_content = str_replace('{{firstname}}', $user->firstname, $mail_content);
                $mail_content = str_replace('{{lastname}}', $user->lastname, $mail_content);
                $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
                $mail_content = str_replace('{{confirm_link}}', $confirm_link, $mail_content);

                $email = new email('default');
                $email->helpers(array('Html', 'Text'));
                $email->viewvars(
                    array(
                        'mail_content' => $mail_content
                    )
                );
                $email->template('default');
                $email->emailformat('text');
                $email->from(array($configuration->from_mail => $configuration->from_big_title));
                $email->to(array($this->request->data['email']));
                $email->subject($mail_template->template_subject);
                if ($email->send()) {
                    $this->loadModel('EmailLogs');
                    $new_email_log_entity = [
                        'send' => 1,
                        'subject' => $mail_template->template_subject,
                        'content' => $mail_content,
                        'recipient_name' => "",
                        'recipient_email' => $this->request->data['email'],
                        'sender_name' => $configuration->from_big_title,
                        'sender_email' => $configuration->from_mail
                    ];
                    $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                    $this->EmailLogs->save($new_email_log);
                }
            }

            if (isset($this->request->data['datetime_range_1']) && strlen($this->request->data['datetime_range_1'])>0) {

                $this->request->data['category'] = 'time';
                for ($i=1;$i<15;$i++) {
                    if (isset($this->request->data['datetime_range_'.$i]) && strlen($this->request->data['datetime_range_'.$i])>0) {

                        $datetime_range_array = explode(' - ', $this->request->data['datetime_range_'.$i]);
                        $start_datetime = explode(' ', $datetime_range_array[0]);
                        $end_datetime = explode(' ', $datetime_range_array[1]);

                        $start_date_unformatted = $start_datetime[0];
                        $start_time_unformatted = $start_datetime[1];
                        $end_date_unformatted = $end_datetime[0];
                        $end_time_unformatted = $end_datetime[1];

                        $start_time_format = $start_time_unformatted.":00";
                        $end_time_format = $end_time_unformatted.":00";

                        $start_date = new\ DateTime($start_date_unformatted);
                        $end_date = new\ DateTime($end_date_unformatted);

                        $new_time_donation_entity = [
                            'donation_id' => $save_donation->id,
                            'start_date' => $start_date,
                            'end_date' => $end_date,
                            'start_time' => $start_time_format,
                            'end_time' => $end_time_format,
                        ];

                        $new_time_donation = $this->TimeDonations->newEntity($new_time_donation_entity);

                        $this->TimeDonations->save($new_time_donation);
                    } else {
                        break;
                    }
                }
            }

            // Keywords speichern
            if (isset($this->request->data['keywords']) && strlen($this->request->data['keywords'])>0) {
                $keywords_array = explode(",", trim($this->request->data['keywords']));
                foreach ($keywords_array as $keyword) {

                    $keyword_exists = $this->Keywords->find()->where(['Keywords.keyword' => $keyword])->first();
                    if (!$keyword_exists) {
                        $new_keyword_entity = [
                            'keyword' => $keyword,
                            'created' => new\ DateTime(date('Y-m-d H:i:s')),
                            'modified' => new\ DateTime(date('Y-m-d H:i:s')),
                        ];
                        $new_keyword = $this->Keywords->newEntity($new_keyword_entity);
                        $keyword_exists = $this->Keywords->save($new_keyword);
                    }

                    $keyword_donation_exists = $this->KeywordsDonations->find()->where(['KeywordsDonations.donation_id' => $save_donation->id, 'KeywordsDonations.keyword_id' => $keyword_exists->id])->first();
                    if (!$keyword_donation_exists) {
                        $new_keyword_donation_entity = [
                            'donation_id' => $save_donation->id,
                            'keyword_id' => $keyword_exists->id,
                            'created' => new\ DateTime(date('Y-m-d H:i:s'))
                        ];
                        $new_keyword_donation = $this->KeywordsDonations->newEntity($new_keyword_donation_entity);
                        $this->KeywordsDonations->save($new_keyword_donation);
                    }
                }
            }

            // Upload durchführen
            if (isset($this->request->data['uploads']) && !empty($this->request->data['uploads'])) {
                foreach ($this->request->data['uploads'] as $upload) {
                    $file_upload = $this->FileUpload->process($upload);

                    $new_fileupload_entity = [
                        'donation_id' => $save_donation->id,
                        'src' => $file_upload,
                        'type' => $upload['type'],
                        'filename' => $upload['name'],
                        'size' => $upload['size']
                    ];

                    $fileupload_entity = $this->FileUploads->newEntity($new_fileupload_entity);
                    $this->FileUploads->save($fileupload_entity);

                }
            }


            if ($save_donation) {
                return $this->redirect(['action' => 'view', $save_donation->id]);
            } else {
                $this->Flash->error(__('Deine Spende wurde nicht abgegeben. Bitte versuche es noch einmal.'));
            }
        }

        if ($this->Auth->user('id')>0) {
            $user = $this->Users->find()->where(['Users.id' => $this->Auth->user('id')])->first();
            if ($user) {
                $this->set('user', $user);
                if ($user->role == 'broker' || $user->role == 'token_account') {
                    $address = $this->Addresses->find()->where(['Addresses.user_id' => $this->Auth->user('id')])->first();
                    if ($address) {
                        $this->set('address', $address);
                    }
                }
            }
        } else {
            $this->set('no_user', true);
        }

        if (isset($id_token) && strlen($id_token)>0) {
            $donation_keywords = $this->KeywordsDonations->find()->where(['KeywordsDonations.donation_id' => base64_decode($id_token)])->all();
            $keyword_ids = array();
            foreach ($donation_keywords as $donation_keyword) {
                $keyword_ids[] = $donation_keyword->keyword_id;
            }
            $similiar_keywords = $this->Keywords->find()->where(['Keywords.id IN' => $keyword_ids])->all();

            $keywords_string = "";
            foreach ($similiar_keywords as $key => $similiar_keyword) {
                if ($key > 0) {
                    $keywords_string .= ", " . $similiar_keyword->keyword;
                } else {
                    $keywords_string .= $similiar_keyword->keyword;
                }
            }
            $this->set('keywords_string', $keywords_string);
        }

        $donation->uploads = array();
        $this->set(compact('donation'));
        $this->set('_serialize', ['donation']);
    }

    /**
     * Methode zum Bearbeiten einer Spende
     *
     * @param string|null $id Donation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $donation = $this->Donations->get($id, [
            'contain' => ['Keywords']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $donation = $this->Donations->patchEntity($donation, $this->request->data);
            if ($this->Donations->save($donation)) {
                $this->Flash->success(__('The donation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The donation could not be saved. Please, try again.'));
            }
        }
        $users = $this->Donations->Users->find('list', ['limit' => 200]);
        $keywords = $this->Donations->Keywords->find('list', ['limit' => 200]);
        $this->set(compact('donation', 'users', 'keywords'));
        $this->set('_serialize', ['donation']);
    }

    /**
     * Methode zum Löschen einer Spende
     *
     * @param string|null $id Donation id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $donation = $this->Donations->get($id);
        if ($this->Donations->delete($donation)) {
            $this->Flash->success(__('The donation has been deleted.'));
        } else {
            $this->Flash->error(__('The donation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
