<?php
namespace App\Controller\Broker;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Utility\Security;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    /**
     * Überprüft Zugriff
     *
     * @param $input
     * @return bool
     */
    private function __checkAccess($input) {
        $user = $this->Users->get($this->Auth->user('id'));

        if ((new DefaultPasswordHasher)->check($input, $user->password)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'addSuccess', 'login', 'checkLogin', 'resetPassword', 'autoLogin', 'register', 'confirmAccount']);
        $this->loadModel('EmailTemplates');
    }

    /**
     * Überprüft den Zugriff bei Ajax-Aufrufen
     */
    public function checkAccess() {
        $this->viewBuilder()->layout = "ajax";
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            if ($this->__checkAccess($this->request->data['value'])) {
                $response = [
                    'status' => 'success'
                ];
            } else {
                $response = [
                    'status' => 'failed'
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode zum Einloggen
     *
     * @return void
     */
    public function login() {
        return $this->redirect('/login');
    }

    /**
     * Methode zum Ausloggen
     *
     * @return \Cake\Network\Response|null
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Methode zum Bearbeiten der eigenen Kontodaten
     *
     * Stellt eine Übersicht der allgemeinen Informationen bereit
     */
    public function account() {
        $this->set('title', 'Kontodaten');
        $user = $this->Addresses->find()->select(['Addresses.id', 'Addresses.title', 'Addresses.first_name', 'Addresses.last_name', 'Addresses.phone', 'Addresses.fax', 'Addresses.status', 'Addresses.status_description'])->where(['Addresses.user_id' => $this->Auth->user('id')])->first();

        if ($this->request->is(['post', 'put'])) {
            $patch_address_entity = $this->Addresses->patchEntity($user, $this->request->data);

            if ($this->Addresses->save($patch_address_entity)) {
                $this->Flash->success(__('Änderungen erfolgreich gespeichert!'));
            } else {
                $this->Flash->error(__('Änderungen wurden nicht gespeichert!'));
            }
        }

        $this->set('user', $user);

        $this->set('anreden', ['herr' => 'Herr', 'frau' => 'Frau']);

    }

    /**
     * Methode zur Darstellung des Dashboards für Vermittler
     *
     * @return void
     */
    public function index() {
        $this->set('title', 'Geschützter Bereich');

        $this->loadModel('Donations');
        $this->loadModel('Matches');
        $this->loadModel('Searches');
        $matches = $this->Matches->find()->order(['Matches.created' => 'DESC'])->limit(5);

        $matches_array = array();

        foreach ($matches as $key => $match) {
            $match_donation = $this->Donations->find()->select(['Donations.category', 'Donations.url_title', 'Donations.title'])->where(['Donations.id' => $match->donation_id])->first();
            $match_search = $this->Searches->find()->select(['Searches.category', 'Searches.url_title', 'Searches.title'])->where(['Searches.id' => $match->search_id])->first();

            if (!$match_donation || !$match_search) {
                continue;
            }

            switch($match_donation->category) {
                case "time":
                    $matches_array[$key]['donation']['href'] = '/zeitspende/' . $match_donation->url_title;
                    break;
                case "thing":
                    $matches_array[$key]['donation']['href'] = '/sachspende/' . $match_donation->url_title;
                    break;
            }

            switch($match_search->category) {
                case "time":
                    $matches_array[$key]['search']['href'] = '/zeitgesuch/' . $match_search->url_title;
                    break;
                case "thing":
                    $matches_array[$key]['search']['href'] = '/sachgesuch/' . $match_search->url_title;
                    break;
            }

            $matches_array[$key]['donation']['title'] = $match_donation->title;
            $matches_array[$key]['search']['title'] = $match_search->title;

        }
        $this->set('matches', $matches_array);
    }

    /**
     * Methode zum Verwalten von Adressen
     */
    public function indexAddresses() {
        $this->set('title', 'Kontaktdaten verwalten');

        $this->loadModel('Addresses');
        $addresses = $this->Addresses->find()->where(['Addresses.user_id' => $this->Auth->user('id')])->all();

        $this->set('addresses', $addresses);
    }

    /**
     * Methode zum Bearbeiten einer Adresse
     *
     * @param $id
     * @return \Cake\Network\Response|null
     */
    public function editAddress($id) {
        $this->set('title', 'Kontaktdaten bearbeiten');

        $this->loadModel('Addresses');

        $address = $this->Addresses->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $patched_address = $this->Addresses->patchEntity($address, $this->request->data);
            if ($this->Addresses->save($patched_address)) {
                $this->Flash->success(__('Kontaktdaten erfolgreich gespeichert.'));
                return $this->redirect('/broker/kontaktdaten_verwalten');
            } else {
                $this->Flash->error(__('Kontaktdaten nicht gespeichert.'));
            }

        }
        $this->set('address', $address);
    }

}
