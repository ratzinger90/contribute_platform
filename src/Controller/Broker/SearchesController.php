<?php
namespace App\Controller\Broker;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Event\Event;
use Cake\Log\Log;

/**
 * Searches Controller
 *
 * @property \App\Model\Table\SearchesTable $Searches
 */
class SearchesController extends AppController
{
    /**
     * Komponente für den Dateiupload
     *
     * @var array
     */
    public $components = ['FileUpload'];

    /**
     * Überprüft Zugriff
     *
     * @param null $id
     * @return bool
     */
    private function __checkAccess($id = null) {

        if ($this->Auth->user('id')>0 && ($this->Auth->user('role') == 'broker' || $this->Auth->user('role') == 'admin')) {
            return true;
        } else if (isset($id) && $id>0) {
            $search = $this->Searches->get($id);
            if ($search->user_id == $this->Auth->user('id')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['times', 'time', 'things', 'thing']);

        $this->loadModel('Addresses');
        $this->loadModel('Cities');
        $this->loadModel('Donations');
        $this->loadModel('FileUploads');
        $this->loadModel('Keywords');
        $this->loadModel('KeywordsDonations');
        $this->loadModel('KeywordsSearches');
        $this->loadModel('Logs');
        $this->loadModel('Matches');
        $this->loadModel('Searches');
        $this->loadModel('TimeSearches');
        $this->loadModel('Users');
        $this->loadModel('ValidZipcodes');
        $this->loadModel('Zipcodes');
    }

    /**
     * Methode zur Auflistung von Gesuche
     *
     * @return void
     */
    public function index()
    {
        $searches = $this->Searches->find()->all();
        $searches_array = array();
        foreach ($searches as $search) {

            $keywords_searches = $this->KeywordsSearches->find()->where(['KeywordsSearches.search_id' => $search->id])->all();
            $keywords = array();
            foreach ($keywords_searches as $keywords_search) {
                $keyword = $this->Keywords->find()->where(['Keywords.id' => $keywords_search->keyword_id])->first();
                $keywords[] = $keyword->keyword;
            }
            $search->keywords = $keywords;
            $searches_array[] = $search;
        }
        $this->set('searches', $searches_array);
        $this->set('_serialize', ['searches']);
    }

    /**
     * View method
     *
     * @param string|null $id Search id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {

        $keywords_searches = $this->KeywordsSearches->find()->where(['KeywordsSearches.search_id' => $id]);

        $donations_array = array();

        $keywords_searches_ids = array();

        foreach ($keywords_searches as $keyword_search) {
            if (isset($keywords_searches_ids[$keyword_search->keyword_id])) {
                continue;
            }
            $keywords_searches_ids[$keyword_search->keyword_id] = $keyword_search->keyword_id;
        }

        $keywords_donations = $this->KeywordsDonations->find()->where(['KeywordsDonations.keyword_id IN' => $keywords_searches_ids])->all();
        foreach ($keywords_donations as $keyword_donation) {
            $donation = $this->Donations->find()->where(['Donations.id' => $keyword_donation->donation_id, 'Donations.active' => 1])->first();
            if (isset($donations_array[$donation->id])) {
                $donations_array[$donation->id] += 1;
                continue;
            }
            $donations_array[$donation->id] = 1;
        }

        // TODO: Array sortieren (höchste Anzahl der Vorkommen zuerst)

        $donation_results = array();

        foreach ($donations_array as $key => $value) {
            $donation_results[] = $key;
        }

        $search = $this->Searches->get($id);
        if ($search->category == 'thing') {
            $case = "thing";
        } else if ($search->category == 'time') {
            $case = "time";
        } else {
            $case = false;
        }

        if (!empty($donations_array)) {
            $this->request->session()->write('donations_array', $donation_results);
            $this->request->session()->write('search_id', $search->id);
            return $this->redirect('/broker/matches_gefunden');
        } else if ($case != false) {
            $this->request->session()->write('flash', 'true');
            if ($case == 'thing') {
                return $this->redirect('/sachgesuch/' . $search->url_title);
            } else if ($case == 'time') {
                return $this->redirect('/zeitgesuch/' . $search->url_title);
            }
        } else if ($case == false) {
            $this->Flash->error(__('Fehlerhafte Weiterleitung.'));
            return $this->redirect('/broker/dashboard');
        }
    }

    /**
     * Methode zum Ansehen von Matches
     *
     * @return \Cake\Network\Response|null
     */
    public function viewMatches() {
        $this->set('title', 'Passende Spenden');

        if (!$this->request->session()->read('donations_array')) {
            $this->Flash->error(__('Fehlerhafte Weiterleitung.'));
            return $this->redirect('/startseite');
        }

        $search_id = $this->request->session()->read('search_id');
        $donations_array = $this->request->session()->read('donations_array');

        $donations = $this->Donations->find()->contain(['FileUploads'])->where(['Donations.id IN' => $donations_array, 'Donations.active' => 1])->limit(3);

        foreach ($donations as $donation) {
            $new_match_entity = [
                'donation_id' => $donation->id,
                'search_id' => $search_id,
                'contacted' => 0,
                'seen' => 0
            ];
            $new_match = $this->Matches->newEntity($new_match_entity);
            $this->Matches->save($new_match);
        }

        $this->set('donations', $donations);

        //$this->request->session()->delete('donations_array');
        //$this->request->session()->delete('search_id');
    }

    /**
     * Methode zum Aufgeben eines Gesuchs
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Gesuch aufgeben');
        $search = $this->Searches->newEntity();

        $current_user = $this->Users->find()->where(['Users.id' => $this->Auth->user('id')])->first();
        $standard_address = $this->Addresses->find()->where(['Addresses.user_id' => $this->Auth->user('id')])->first();

        if ($this->request->is(['post', 'patch', 'put'])) {

            $helpers_controller = new HelpersController();
            $valid = $helpers_controller->validateNew("internal_request", $this->request->data);
            if (!$valid) {
                $this->Flash->error(__('Alle Pflichtfelder müssen ausgefüllt werden.'));
                return $this->redirect('/broker/gesuch_aufgeben');
            }

            if ((isset($this->request->data['email']) && $current_user->email != trim($this->request->data['email'])) ||
                (isset($this->request->data['phone']) && $current_user->phone != trim($this->request->data['phone'])) ||
                (isset($this->request->data['street']) && $standard_address->street != trim($this->request->data['street']))) {

                $new_address_entity['email'] = trim($this->request->data['email']);
                $new_address_entity['phone'] = trim($this->request->data['phone']);
                $new_address_entity['street'] = trim($this->request->data['street']);
                $new_address_entity['zip'] = trim($this->request->data['zip']);
                $zipcode = $this->Zipcodes->find()->where(['Zipcodes.zipcode' => $new_address_entity['zip']])->first();
                if ($zipcode) {
                    $city = $this->Cities->find()->where(['Cities.id' => $zipcode->city_id])->first();
                    if ($city) {
                        $new_address_entity['city'] = $city->name;
                    }
                }

                // Keinen neuen Benutzer anlegen
                $new_address_entity['user_id'] = $current_user->id;

                $new_address = $this->Addresses->newEntity($new_address_entity);
                $save_address = $this->Addresses->save($new_address);

                $this->request->data['address_id'] = $save_address->id;
            } else {
                $this->request->data['address_id'] = $standard_address->id;
            }

            $this->request->data['user_id'] = $this->Auth->user('id');

            $this->request->data['url_title'] = str_replace(" ", "-", strtolower($helpers_controller->replaceChars($this->request->data['title'])));
            $this->request->data['url_title'] = str_replace("?", "", $this->request->data['url_title']);
            $this->request->data['url_title'] = str_replace("/", "|", $this->request->data['url_title']);

            $this->request->data['category'] = 'thing';

            if (isset($this->request->data['datetime_range_1']) && strlen($this->request->data['datetime_range_1'])>0) {
                $this->request->data['category'] = 'time';
            }

            $search = $this->Searches->patchEntity($search, $this->request->data);
            $save_search = $this->Searches->save($search);

            // Zeitgesuch speichern
            if (isset($this->request->data['datetime_range_1']) && strlen($this->request->data['datetime_range_1'])>0) {

                $this->request->data['category'] = 'time';
                for ($i=1;$i<15;$i++) {
                    if (isset($this->request->data['datetime_range_'.$i]) && strlen($this->request->data['datetime_range_'.$i])>0) {

                        $datetime_range_array = explode(' - ', $this->request->data['datetime_range_'.$i]);
                        $start_datetime = explode(' ', $datetime_range_array[0]);
                        $end_datetime = explode(' ', $datetime_range_array[1]);

                        $start_date_unformatted = $start_datetime[0];
                        $start_time_unformatted = $start_datetime[1];
                        $end_date_unformatted = $end_datetime[0];
                        $end_time_unformatted = $end_datetime[1];

                        $start_time_format = $start_time_unformatted.":00";
                        $end_time_format = $end_time_unformatted.":00";

                        $start_date = new\ DateTime($start_date_unformatted);
                        $end_date = new\ DateTime($end_date_unformatted);

                        $new_time_search_entity = [
                            'donation_id' => $save_search->id,
                            'start_date' => $start_date,
                            'end_date' => $end_date,
                            'start_time' => $start_time_format,
                            'end_time' => $end_time_format,
                        ];

                        $new_time_search = $this->TimeSearches->newEntity($new_time_search_entity);

                        $this->TimeSearches->save($new_time_search);
                    } else {
                        break;
                    }
                }
            }

            // Schlagwörter speichern
            if (isset($this->request->data['keywords']) && strlen($this->request->data['keywords'])>0) {
                $keywords_array = explode(',', $this->request->data['keywords']);
                foreach ($keywords_array as $keyword) {
                    $keyword = strtolower($keyword);
                    $keyword = $this->__replaceChars($keyword);
                    $db_keyword = $this->Keywords->find()->where(['Keywords.keyword' => $keyword])->first();
                    if ($db_keyword) {
                        $new_keyword_search_entity = [
                            'keyword_id' => $db_keyword->id,
                            'search_id' => $save_search->id,
                        ];
                        $new_keyword_search = $this->KeywordsSearches->newEntity($new_keyword_search_entity);
                        $this->KeywordsSearches->save($new_keyword_search);
                    } else {
                        $new_keyword_entity = [
                            'keyword' => $keyword,
                        ];
                        $new_keyword = $this->Keywords->newEntity($new_keyword_entity);
                        $save_keyword = $this->Keywords->save($new_keyword);

                        $new_keyword_search_entity = [
                            'keyword_id' => $save_keyword->id,
                            'search_id' => $save_search->id,
                        ];
                        $new_keyword_search = $this->KeywordsSearches->newEntity($new_keyword_search_entity);
                        $this->KeywordsSearches->save($new_keyword_search);
                    }
                }
            }

            // Upload durchführen
            if (isset($this->request->data['uploads']) && !empty($this->request->data['uploads'])) {
                foreach ($this->request->data['uploads'] as $upload) {
                    $file_upload = $this->FileUpload->process($upload);

                    $new_fileupload_entity = [
                        'search_id' => $save_search->id,
                        'src' => $file_upload,
                        'type' => $upload['type'],
                        'filename' => $upload['name'],
                        'size' => $upload['size']
                    ];

                    $fileupload_entity = $this->FileUploads->newEntity($new_fileupload_entity);
                    $this->FileUploads->save($fileupload_entity);

                }
            }

            if ($save_search) {
                return $this->redirect(['action' => 'view', $save_search->id]);
            } else {
                $this->Flash->error(__('Das Gesuch wurde nicht aufgegeben. Bitte versuche es noch einmal.'));
                return $this->redirect('/broker/gesuch_aufgaben');
            }
        }

        $this->set('current_user', $current_user);
        $this->set('standard_address', $standard_address);

        $addresses = $this->Addresses->find()->where(['Addresses.user_id' => $this->Auth->user('id')])->all();

        $addresses_array = array();

        foreach ($addresses as $address) {
            if (isset($addresses_array[$address->id])) {
                continue;
            }

            if (strlen($address->email)==0) {
                if (strlen($address->phone)==0) {
                    $addresses_array[$address->id] = $address->street . ", " . $address->zip . " " . $address->city . " | " . $current_user->email . " | " . $current_user->phone;
                } else {
                    $addresses_array[$address->id] = $address->street . ", " . $address->zip . " " . $address->city . " | " . $current_user->email . " | " . $address->phone;
                }
            } else {
                $addresses_array[$address->id] = $address->street . ", " . $address->zip . " " . $address->city . " | " . $address->email . " | " . $address->phone;
            }
        }
        $this->set('addresses_array', $addresses_array);

        $search->uploads = array();

        $this->set(compact('search'));
        $this->set('_serialize', ['search']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Search id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $search = $this->Searches->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $search = $this->Searches->patchEntity($search, $this->request->data);
            if ($this->Searches->save($search)) {
                $this->Flash->success(__('The search has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The search could not be saved. Please, try again.'));
            }
        }
        $users = $this->Searches->Users->find('list', ['limit' => 200]);
        $this->set(compact('search', 'users'));
        $this->set('_serialize', ['search']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Search id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $search = $this->Searches->get($id);
        if ($this->Searches->delete($search)) {
            $this->Flash->success(__('The search has been deleted.'));
        } else {
            $this->Flash->error(__('The search could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Methode zum Deaktivieren von Gesuchen
     *
     * @param null $id
     */
    public function inactive($id = null)
    {
        $this->request->allowMethod(['post']);
        $search = $this->Searches->get($id);
        $search = $this->Searches->patchEntity($search, ['active' => 0]);
        if ($this->Searches->save($search)) {
            $this->Flash->success(__('Das Gesuch wurde erfolgreich deaktiviert.'));
        } else {
            $this->Flash->error(__('Das Gesuche wurde nicht deaktiviert.'));
        }
    }

    /**
     * Methode zum Aktivieren von Gesuchen
     *
     * @param null $id
     */
    public function active($id = null)
    {
        $this->request->allowMethod(['post']);
        $search = $this->Searches->get($id);
        $search = $this->Searches->patchEntity($search, ['active' => 1]);
        if ($this->Searches->save($search)) {
            $this->Flash->success(__('Das Gesuch wurde erfolgreich aktiviert.'));
        } else {
            $this->Flash->error(__('Das Gesuche wurde nicht aktiviert.'));
        }
    }

    /**
     * Methode zum Ersetzen von Umlauten
     *
     * @param $string
     * @return mixed
     */
    private function __replaceChars($string)
    {
        $string = str_replace("ä", "ae", $string);
        $string = str_replace("ü", "ue", $string);
        $string = str_replace("ö", "oe", $string);
        $string = str_replace("Ä", "Ae", $string);
        $string = str_replace("Ü", "Ue", $string);
        $string = str_replace("Ö", "Oe", $string);
        $string = str_replace("ß", "ss", $string);
        $string = str_replace("´", "", $string);
        return $string;
    }
}
