<?php
namespace App\Controller\Broker;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\Query;
use App\Model\Entity\Keyword;

/**
 * Helpers Controller
 *
 */
class HelpersController extends AppController
{

    /**
     * @var array
     */
    public $components = ['FileUpload', 'Helper'];

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        $this->loadModel('Addresses');
        $this->loadModel('BrokersZipcodes');
        $this->loadModel('Donations');
        $this->loadModel('FileUploads');
        $this->loadModel('Keywords');
        $this->loadModel('KeywordsDonations');
        $this->loadModel('KeywordsSearches');
        $this->loadModel('Logs');
        $this->loadModel('Searches');
        $this->loadModel('TimeDonations');
        $this->loadModel('TimeSearches');
        $this->loadModel('Users');
        $this->loadModel('ValidZipcodes');
    }

    /**
     * Methode zur Überprüfung einer Postleitzahl auf Validität
     */
    public function validateZip() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->is('ajax')) {

            $this->loadModel('ValidZipcodes');
            $validZipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => $this->request->data['zip']])->first();

            if ($validZipcode) {
                $response = [
                    'status' => 'success',
                    'city' => $validZipcode->city
                ];
            } else {
                $this->loadModel('Messages');
                $wrong_zipcode = $this->Messages->find()->where(['Messages.name' => 'wrong_zip'])->first();
                $response = [
                    'status' => 'failed',
                    'message' => $wrong_zipcode->text
                ];
            }

            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * @param null $internal
     * @param array|null $internal_request_data
     * @return bool
     */
    public function validateNew($internal = null, array $internal_request_data = null) {
        if (isset($internal) && $internal = "internal_request") {
            $this->request->data = $internal_request_data;
        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }

        $this->loadModel('Messages');

        $messages = array();

        // Titel überprüfen
        if (isset($this->request->data['title']) && strlen($this->request->data['title'])==0) {
            $empty_title = $this->Messages->find()->where(['Messages.name' => 'empty_title'])->first();
            $messages['title']['text'] = $empty_title->text;
            $messages['title']['type'] = $empty_title->type;
            $messages['title']['name'] = $empty_title->name;
        }

        // Postleitzahl überprüfen
        if (isset($this->request->data['zip']) && strlen($this->request->data['zip'])==0) {
            $empty_zip = $this->Messages->find()->where(['name' => 'empty_zip'])->first();
            $messages['zip']['text'] = $empty_zip->text;
            $messages['zip']['type'] = $empty_zip->type;
            $messages['zip']['name'] = $empty_zip->name;
        } else if (isset($this->request->data['zip']) && strlen($this->request->data['zip'])>0) {
            $this->loadModel('ValidZipcodes');
            $zip_exists = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => trim($this->request->data['zip'])])->first();
            if (!$zip_exists) {
                $wrong_zip = $this->Messages->find()->where(['Messages.name' => 'wrong_zip'])->first();
                $messages['zip']['text'] = $wrong_zip->text;
                $messages['zip']['type'] = $wrong_zip->type;
                $messages['zip']['name'] = $wrong_zip->name;
            }
        }

        // Ort überprüfen
        if (isset($this->request->data['city']) && strlen($this->request->data['city'])==0) {
            $empty_city = $this->Messages->find()->where(['Messages.name' => 'empty_city'])->first();
            $messages['city']['text'] = $empty_city->text;
            $messages['city']['type'] = $empty_city->type;
            $messages['city']['name'] = $empty_city->name;
        }

        // Beschreibung überprüfen
        if (isset($this->request->data['description']) && strlen($this->request->data['description'])==0) {
            $empty_description = $this->Messages->find()->where(['Messages.name' => 'empty_description'])->first();
            $messages['description']['text'] = $empty_description->text;
            $messages['description']['type'] = $empty_description->type;
            $messages['description']['name'] = $empty_description->name;
        }

        // Schlagwörter überprüfen
        if (isset($this->request->data['keywords']) && strlen($this->request->data['keywords'])==0) {
            $empty_keywords = $this->Messages->find()->where(['Messages.name' => 'empty_keywords'])->first();
            $messages['keywords']['text'] = $empty_keywords->text;
            $messages['keywords']['type'] = $empty_keywords->type;
            $messages['keywords']['name'] = $empty_keywords->name;
        } else if (isset($this->request->data['keywords']) && strlen($this->request->data['keywords'])>0) {
            $count_keywords = substr_count(trim($this->request->data['keywords']), ',');
            if ($count_keywords<2) {
                $more_keywords = $this->Messages->find()->where(['Messages.name' => 'more_keywords'])->first();
                $messages['keywords']['text'] = $more_keywords->text;
                $messages['keywords']['type'] = $more_keywords->type;
                $messages['keywords']['name'] = $more_keywords->name;
            }
        }

        // Zeitspende überprüfen
        if (isset($this->request->data['start_date_day_1']) && strlen($this->request->data['start_date_day_1'])) {

            $valid = true;
            for ($i = 1; $i < 15; $i++) {
                if (isset($this->request->data['start_date_day_' . $i]) && strlen($this->request->data['start_date_day_' . $i]) > 0) {
                    // Startdatum
                    if (isset($this->request->data['start_date_month_' . $i])) {
                        if (strlen($this->request->data['start_date_month_' . $i]) == 1) {
                            $this->request->data['start_date_month_' . $i] = "0" . $this->request->data['start_date_month_' . $i];
                        }
                    }
                    if (isset($this->request->data['start_date_day_' . $i])) {
                        if (strlen($this->request->data['start_date_day_' . $i]) == 1) {
                            $this->request->data['start_date_day_' . $i] = "0" . $this->request->data['start_date_day_' . $i];
                        }
                    }
                    if (isset($this->request->data['start_date_year_' . $i])) {
                        if (strlen($this->request->data['start_date_year_' . $i]) == 2) {
                            $this->request->data['start_date_year_' . $i] = "20" . $this->request->data['start_date_year_' . $i];
                        }
                    }

                    // Enddatum
                    if (isset($this->request->data['end_date_month_' . $i])) {
                        if (strlen($this->request->data['end_date_month_' . $i]) == 1) {
                            $this->request->data['end_date_month_' . $i] = "0" . $this->request->data['end_date_month_' . $i];
                        }
                    }
                    if (isset($this->request->data['end_date_day_' . $i])) {
                        if (strlen($this->request->data['end_date_day_' . $i]) == 1) {
                            $this->request->data['end_date_day_' . $i] = "0" . $this->request->data['end_date_day_' . $i];
                        }
                    }
                    if (isset($this->request->data['end_date_year_' . $i])) {
                        if (strlen($this->request->data['end_date_year_' . $i]) == 2) {
                            $this->request->data['end_date_year_' . $i] = "20" . $this->request->data['end_date_year_' . $i];
                        }
                    }

                    // Startuhrzeit
                    if (isset($this->request->data['start_time_hours_' . $i])) {
                        if (strlen($this->request->data['start_time_hours_' . $i]) == 1) {
                            $this->request->data['start_time_hours_' . $i] = "0" . $this->request->data['start_time_hours_' . $i];
                        }
                    }
                    if (isset($this->request->data['start_time_minutes_' . $i])) {
                        if (strlen($this->request->data['start_time_minutes_' . $i]) == 1) {
                            $this->request->data['start_time_minutes_' . $i] = "0" . $this->request->data['start_time_minutes_' . $i];
                        }
                    }

                    // Enduhrzeit
                    if (isset($this->request->data['end_time_hours_' . $i])) {
                        if (strlen($this->request->data['end_time_hours_' . $i]) == 1) {
                            $this->request->data['end_time_hours_' . $i] = "0" . $this->request->data['end_time_hours_' . $i];
                        }
                    }
                    if (isset($this->request->data['end_time_minutes_' . $i])) {
                        if (strlen($this->request->data['end_time_minutes_' . $i]) == 1) {
                            $this->request->data['end_time_minutes_' . $i] = "0" . $this->request->data['end_time_minutes_' . $i];
                        }
                    }

                    if ($this->request->data['start_time_hours_' . $i]>23 || $this->request->data['end_time_hours_' . $i]>23 ||
                        $this->request->data['start_time_minutes_' . $i]>59 || $this->request->data['end_time_minutes_' . $i]>59 ||
                        $this->request->data['start_date_day_' . $i]>31 || $this->request->data['end_date_day_' . $i]>31 ||
                        $this->request->data['start_date_month_' . $i]>12 || $this->request->data['end_date_month_' . $i]>12 ) {
                        $valid = false;
                    }

                    if (strlen($this->request->data['start_time_hours_' . $i])==0 ||
                        strlen($this->request->data['end_time_hours_' . $i])==0 ||
                        strlen($this->request->data['start_time_minutes_' . $i])==0 ||
                        strlen($this->request->data['end_time_minutes_' . $i])==0 ||
                        strlen($this->request->data['start_date_day_' . $i])==0 ||
                        strlen($this->request->data['end_date_day_' . $i])==0 ||
                        strlen($this->request->data['start_date_month_' . $i])==0 ||
                        strlen($this->request->data['end_date_month_' . $i])==0 ||
                        strlen($this->request->data['start_date_year_' . $i])==0 ||
                        strlen($this->request->data['end_date_year_' . $i])==0) {
                        $valid = false;
                        continue;

                    }

                    $start_time_format = $this->request->data['start_time_hours_' . $i] . ":" . $this->request->data['start_time_minutes_' . $i] . ":00";
                    $end_time_format = $this->request->data['end_time_hours_' . $i] . ":" . $this->request->data['end_time_minutes_' . $i] . ":00";

                    $start_date = new\ DateTime($this->request->data['start_date_year_'.$i]."-".$this->request->data['start_date_month_'.$i]."-".$this->request->data['start_date_day_'.$i]);
                    $end_date = new\ DateTime($this->request->data['end_date_year_'.$i]."-".$this->request->data['end_date_month_'.$i]."-".$this->request->data['end_date_day_'.$i]);

                    if ($start_date == $end_date) {
                        if ($start_time_format>$end_time_format) {
                            $valid = false;
                        }
                    }
                    if ($start_date>$end_date) {
                        $valid = false;
                    }
                    if ($start_date->format('Y-m-d') < (new\ DateTime(date('Y-m-d')))->format('Y-m-d') || $end_date->format('Y-m-d') < (new\ DateTime(date('Y-m-d')))->format('Y-m-d')) {
                        $valid = false;
                    }
                }
            }
            if (!$valid) {
                $wrong_times = $this->Messages->find()->where(['Messages.name' => 'wrong_times'])->first();
                $messages['times']['text'] = $wrong_times->text;
                $messages['times']['type'] = $wrong_times->type;
                $messages['times']['name'] = $wrong_times->name;
            }
        }

        if (isset($internal) && $internal == "internal_request") {
            if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                $this->request->data['email'] = trim($this->request->data['email']);

                $valid = true;
                if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    $valid = false;
                }

                if (!$valid) {
                    $wrong_email = $this->Messages->find()->where(['Messages.name' => 'wrong_email'])->first();
                    $messages['email']['text'] = $wrong_email->text;
                    $messages['email']['type'] = $wrong_email->type;
                    $messages['email']['name'] = $wrong_email->name;
                }
            }
        } else {

            // E-Mail überprüfen
            if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                $this->request->data['email'] = trim($this->request->data['email']);

                $valid = true;
                if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    $valid = false;
                }

                if (!$valid) {
                    $wrong_email = $this->Messages->find()->where(['Messages.name' => 'wrong_email'])->first();
                    $messages['email']['text'] = $wrong_email->text;
                    $messages['email']['type'] = $wrong_email->type;
                    $messages['email']['name'] = $wrong_email->name;
                }
            } else if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                $no_email = $this->Messages->find()->where(['Messages.name' => 'no_email'])->first();
                $messages['email']['text'] = $no_email->text;
                $messages['email']['type'] = $no_email->type;
                $messages['email']['name'] = $no_email->name;
            }
        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else if ($this->request->is(['patch', 'post', 'put', 'ajax'])) {
            if (sizeof($messages)>1) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }

    }

    /**
     * Überprüfung von Pflichtfeldern bei Registrierung
     *
     * @param null $internal
     * @param array|null $internal_request_data
     * @return bool
     */
    public function validateRegister($internal = null, array $internal_request_data = null) {
        if (isset($internal) && $internal = "internal_request") {

        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }

        $this->loadModel('Messages');

        $messages = array();

        // Vorname überprüfen
        if (isset($this->request->data['firstname']) && strlen($this->request->data['firstname'])==0) {
            $empty_firstname = $this->Messages->find()->where(['Messages.name' => 'empty_firstname'])->first();
            $messages['firstname']['text'] = $empty_firstname->text;
            $messages['firstname']['type'] = $empty_firstname->type;
            $messages['firstname']['name'] = $empty_firstname->name;
        }

        // Nachname überprüfen
        if (isset($this->request->data['lastname']) && strlen($this->request->data['lastname'])==0) {
            $empty_lastname = $this->Messages->find()->where(['Messages.name' => 'empty_lastname'])->first();
            $messages['lastname']['text'] = $empty_lastname->text;
            $messages['lastname']['type'] = $empty_lastname->type;
            $messages['lastname']['name'] = $empty_lastname->name;
        }

        // Straße überprüfen
        if (isset($this->request->data['street']) && strlen($this->request->data['street'])==0) {
            $empty_street = $this->Messages->find()->where(['Messages.name' => 'empty_street'])->first();
            $messages['street']['text'] = $empty_street->text;
            $messages['street']['type'] = $empty_street->type;
            $messages['street']['name'] = $empty_street->name;
        }

        // Postleitzahl überprüfen
        if (isset($this->request->data['zip']) && strlen($this->request->data['zip'])==0) {
            $empty_zip = $this->Messages->find()->where(['name' => 'empty_zip'])->first();
            $messages['zip']['text'] = $empty_zip->text;
            $messages['zip']['type'] = $empty_zip->type;
            $messages['zip']['name'] = $empty_zip->name;
        } else if (isset($this->request->data['zip']) && strlen($this->request->data['zip'])>0) {
            $this->loadModel('ValidZipcodes');
            $zip_exists = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => trim($this->request->data['zip'])])->first();
            if (!$zip_exists) {
                $wrong_zip = $this->Messages->find()->where(['Messages.name' => 'wrong_zip'])->first();
                $messages['zip']['text'] = $wrong_zip->text;
                $messages['zip']['type'] = $wrong_zip->type;
                $messages['zip']['name'] = $wrong_zip->name;
            }
        }

        // Ort überprüfen
        if (isset($this->request->data['city']) && strlen($this->request->data['city'])==0) {
            $empty_city = $this->Messages->find()->where(['Messages.name' => 'empty_city'])->first();
            $messages['city']['text'] = $empty_city->text;
            $messages['city']['type'] = $empty_city->type;
            $messages['city']['name'] = $empty_city->name;
        }

        // Telefon überprüfen
        if (isset($this->request->data['phone']) && strlen($this->request->data['phone'])==0) {
            $empty_phone = $this->Messages->find()->where(['Messages.name' => 'empty_phone'])->first();
            $messages['phone']['text'] = $empty_phone->text;
            $messages['phone']['type'] = $empty_phone->type;
            $messages['phone']['name'] = $empty_phone->name;
        }

        // AGB-Häkchen überprüfen
        if (isset($this->request->data['agb']) && $this->request->data['agb'] == 'false') {
            $not_accepted_agb = $this->Messages->find()->where(['Messages.name' => 'not_accepted_agb'])->first();
            $messages['agb']['text'] = $not_accepted_agb->text;
            $messages['agb']['type'] = $not_accepted_agb->type;
            $messages['agb']['name'] = $not_accepted_agb->name;
        }

        // Recaptcha-Response überprüfen
        if (isset($this->request->data['g-recaptcha-response']) && strlen($this->request->data['g-recaptcha-response'])==0) {
            $empty_captcha = $this->Messages->find()->where(['Messages.name' => 'empty_captcha'])->first();
            $messages['captcha']['text'] = $empty_captcha->text;
            $messages['captcha']['type'] = $empty_captcha->type;
            $messages['captcha']['name'] = $empty_captcha->name;
        }

        if (isset($internal) && $internal == "internal_request") {
            if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                $this->request->data['email'] = trim($this->request->data['email']);

                $valid = true;
                if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    $valid = false;
                }

                $exists = $this->Users->find()->where(['Users.email' => $this->request->data['email']])->first();

                if ($exists) {
                    $exists_email = $this->Messages->find()->where(['Messages.name' => 'exists_email'])->first();
                    $messages['email']['text'] = $exists_email->text;
                    $messages['email']['type'] = $exists_email->type;
                    $messages['email']['name'] = $exists_email->name;
                } else if (!$valid) {
                    $wrong_email = $this->Messages->find()->where(['Messages.name' => 'wrong_email'])->first();
                    $messages['email']['text'] = $wrong_email->text;
                    $messages['email']['type'] = $wrong_email->type;
                    $messages['email']['name'] = $wrong_email->name;
                } else {
                    if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) > 0) {
                        if (trim($this->request->data['email']) != trim($this->request->data['email_repeat'])) {
                            $not_similiar_emails = $this->Messages->find()->where(['Messages.name' => 'not_similiar_emails'])->first();
                            $messages['email_repeat']['text'] = $not_similiar_emails->text;
                            $messages['email_repeat']['type'] = $not_similiar_emails->type;
                            $messages['email_repeat']['name'] = $not_similiar_emails->name;
                        }
                    } else if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                        $no_email_confirm = $this->Messages->find()->where(['Messages.name' => 'no_email_confirm'])->first();
                        $messages['email_repeat']['text'] = $no_email_confirm->text;
                        $messages['email_repeat']['type'] = $no_email_confirm->type;
                        $messages['email_repeat']['name'] = $no_email_confirm->name;
                    }
                }
            } else if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                $empty_email = $this->Messages->find()->where(['Messages.name' => 'empty_email', 'type' => 'error'])->first();
                $messages['email']['text'] = $empty_email->text;
                $messages['email']['type'] = $empty_email->type;
                $messages['email']['name'] = $empty_email->name;
                if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                    $empty_email_repeat = $this->Messages->find()->where(['Messages.name' => 'empty_email_repeat', 'type' => 'error'])->first();
                    $messages['email_repeat']['text'] = $empty_email_repeat->text;
                    $messages['email_repeat']['type'] = $empty_email_repeat->type;
                    $messages['email_repeat']['name'] = $empty_email_repeat->name;
                }

            }

            if (isset($this->request->data['password']) && strlen($this->request->data['password']) > 0) {
                $this->request->data['password'] = trim($this->request->data['password']);

                if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                    $no_password_confirm = $this->Messages->find()->where(['Messages.name' => 'no_password_confirm'])->first();
                    $messages['password_repeat']['text'] = $no_password_confirm->text;
                    $messages['password_repeat']['type'] = $no_password_confirm->type;
                    $messages['password_repeat']['name'] = $no_password_confirm->name;
                } else if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) > 0) {
                    if (trim($this->request->data['password']) != trim($this->request->data['password_repeat'])) {
                        $not_similiar_passwords = $this->Messages->find()->where(['Messages.name' => 'not_similiar_passwords'])->first();
                        $messages['password_repeat']['text'] = $not_similiar_passwords->text;
                        $messages['password_repeat']['type'] = $not_similiar_passwords->type;
                        $messages['password_repeat']['name'] = $not_similiar_passwords->name;
                    }
                }
            } else if (isset($this->request->data['password']) && strlen($this->request->data['password']) == 0) {
                $empty_password = $this->Messages->find()->where(['Messages.name' => 'empty_password'])->first();
                $messages['password']['text'] = $empty_password->text;
                $messages['password']['type'] = $empty_password->type;
                $messages['password']['name'] = $empty_password->name;
                if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                    $empty_password_repeat = $this->Messages->find()->where(['Messages.name' => 'empty_password_repeat'])->first();
                    $messages['password_repeat']['text'] = $empty_password_repeat->text;
                    $messages['password_repeat']['type'] = $empty_password_repeat->type;
                    $messages['password_repeat']['name'] = $empty_password_repeat->name;
                }
            }
        } else {
            if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                $this->request->data['email'] = trim($this->request->data['email']);

                $valid = true;
                if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    $valid = false;
                }

                $exists = $this->Users->find()->where(['Users.email' => $this->request->data['email']])->first();

                if ($exists) {
                    $exists_email = $this->Messages->find()->where(['Messages.name' => 'exists_email'])->first();
                    $messages['email']['text'] = $exists_email->text;
                    $messages['email']['type'] = $exists_email->type;
                    $messages['email']['name'] = $exists_email->name;
                } else if (!$valid) {
                    $wrong_email = $this->Messages->find()->where(['Messages.name' => 'wrong_email'])->first();
                    $messages['email']['text'] = $wrong_email->text;
                    $messages['email']['type'] = $wrong_email->type;
                    $messages['email']['name'] = $wrong_email->name;

                } else {
                    if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) > 0) {
                        if (trim($this->request->data['email']) != trim($this->request->data['email_repeat'])) {
                            $not_similiar_emails = $this->Messages->find()->where(['Messages.name' => 'not_similiar_emails'])->first();
                            $messages['email_repeat']['text'] = $not_similiar_emails->text;
                            $messages['email_repeat']['type'] = $not_similiar_emails->type;
                            $messages['email_repeat']['name'] = $not_similiar_emails->name;
                        }
                    } else if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                        $no_email_confirm = $this->Messages->find()->where(['Messages.name' => 'no_email_confirm'])->first();
                        $messages['email_repeat']['text'] = $no_email_confirm->text;
                        $messages['email_repeat']['type'] = $no_email_confirm->type;
                        $messages['email_repeat']['name'] = $no_email_confirm->name;
                    }
                }
                // E-Mail überprüfen
            } else if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                $empty_email = $this->Messages->find()->where(['Messages.name' => 'empty_email', 'type' => 'error'])->first();
                $messages['email']['text'] = $empty_email->text;
                $messages['email']['type'] = $empty_email->type;
                $messages['email']['name'] = $empty_email->name;
                if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                    $empty_email_repeat = $this->Messages->find()->where(['Messages.name' => 'empty_email_repeat', 'type' => 'error'])->first();
                    $messages['email_repeat']['text'] = $empty_email_repeat->text;
                    $messages['email_repeat']['type'] = $empty_email_repeat->type;
                    $messages['email_repeat']['name'] = $empty_email_repeat->name;
                }
            }

            if (isset($this->request->data['password']) && strlen($this->request->data['password']) > 0) {
                $this->request->data['password'] = trim($this->request->data['password']);

                if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                    $no_password_confirm = $this->Messages->find()->where(['Messages.name' => 'no_password_confirm'])->first();
                    $messages['password_repeat']['text'] = $no_password_confirm->text;
                    $messages['password_repeat']['type'] = $no_password_confirm->type;
                    $messages['password_repeat']['name'] = $no_password_confirm->name;
                } else if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) > 0) {
                    if (trim($this->request->data['password']) != trim($this->request->data['password_repeat'])) {
                        $not_similiar_passwords = $this->Messages->find()->where(['Messages.name' => 'not_similiar_passwords'])->first();
                        $messages['password_repeat']['text'] = $not_similiar_passwords->text;
                        $messages['password_repeat']['type'] = $not_similiar_passwords->type;
                        $messages['password_repeat']['name'] = $not_similiar_passwords->name;
                    }
                }
                // Passwörter überprüfen
            } else if (isset($this->request->data['password']) && strlen($this->request->data['password']) == 0) {
                $empty_password = $this->Messages->find()->where(['Messages.name' => 'empty_password'])->first();
                $messages['password']['text'] = $empty_password->text;
                $messages['password']['type'] = $empty_password->type;
                $messages['password']['name'] = $empty_password->name;
                if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                    $empty_password_repeat = $this->Messages->find()->where(['Messages.name' => 'empty_password_repeat'])->first();
                    $messages['password_repeat']['text'] = $empty_password_repeat->text;
                    $messages['password_repeat']['type'] = $empty_password_repeat->type;
                    $messages['password_repeat']['name'] = $empty_password_repeat->name;
                }
            }
        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else if ($this->request->is(['patch', 'post', 'put', 'ajax'])) {
            if (sizeof($messages)>0) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }

    }

    /**
     * Extrahierung der Schlagwörter aus Titel und Beschreibung
     */
    public function filterText() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->is('ajax')) {

            $title_words = explode(" ", $this->request->data['title']);
            $description_words = explode(" ", $this->request->data['description']);

            $words = array_merge($title_words, $description_words);

            $formatted_words = array();
            foreach ($words as $word) {
                $word = strtolower($this->__replaceChars($word));
                $word = str_replace(".", "", $word);
                $word = str_replace(",", "", $word);
                $word = str_replace(":", "", $word);
                $word = str_replace("/", "", $word);
                $word = str_replace("|", "", $word);
                $word = str_replace("(", "", $word);
                $word = str_replace(")", "", $word);
                $word = str_replace("[", "", $word);
                $word = str_replace("]", "", $word);
                $word = str_replace("{", "", $word);
                $word = str_replace("}", "", $word);
                $formatted_words[] = $word;

            }

            // Nicht relevante Wörter
            $not_relevant_words = [
                'ab', 'aber', 'abermals', 'abgeben', 'abgerufen', 'abgerufene', 'abgerufener', 'abgerufenes', 'abzugeben', 'ähnlich', 'alle',
                'allein', 'allem', 'allemal', 'allen', 'allenfalls', 'allenthalben', 'aller', 'allerdings', 'allerlei', 'alles', 'allesamt',
                'allgemein', 'allmählich', 'allzu', 'als', 'alsbald', 'also', 'alt', 'am', 'an', 'andauernd', 'andere', 'anderem', 'anderen',
                'anderer', 'andererseits', 'anderes', 'andern', 'andernfalls', 'anders', 'anerkannt', 'anerkannte', 'anerkannter', 'anerkanntes',
                'angesetzt', 'angesetzte', 'angesetzter', 'anscheinend', 'anschliessen', 'anstatt', 'auch', 'auf', 'auffallend', 'aufgrund', 'aufs',
                'augenscheinlich', 'aus', 'aus-', 'ausdrücklich', 'ausdrückt', 'ausdrückte', 'ausgang', 'ausgedrückt', 'ausgenommen', 'ausgerechnet',
                'ausnahmslos', 'außen', 'außer', 'außerdem', 'außerhalb', 'äußerst', 'bald', 'bei', 'beide', 'beiden', 'beiderlei', 'beides', 'beim',
                'beinahe', 'bekannt', 'bekannte', 'bekannter', 'bekanntlich', 'bekomme', 'bekommen', 'benoetige', 'benoetigen', 'bereits', 'besonders',
                'besser', 'bestenfalls', 'bestimmt', 'beträchtlich', 'bevor', 'bezüglich', 'bin', 'bis', 'bisher', 'bislang', 'bist', 'bloß', 'Bsp',
                'bzw', 'ca', 'Co', 'da', 'dabei', 'dadurch', 'dafür', 'dagegen', 'daher', 'dahin', 'damals', 'damit', 'danach', 'daneben', 'dank',
                'danke', 'dann', 'dannen', 'daran', 'darauf', 'daraus', 'darf', 'darfst', 'darin', 'darüber', 'darum', 'darunter', 'das', 'dass',
                'dasselbe', 'davon', 'davor', 'dazu', 'dein', 'deine', 'deinem', 'deinen', 'deiner', 'deines', 'dem', 'demgegenüber', 'demgemäß',
                'demnach', 'demselben', 'den', 'denen', 'denkbar', 'denn', 'dennoch', 'denselben', 'der', 'derart', 'derartig', 'deren', 'derer',
                'derjenige', 'derjenigen', 'derselbe', 'derselben', 'derzeit', 'des', 'deshalb', 'desselben', 'dessen', 'desto', 'deswegen', 'dich',
                'die', 'diejenige', 'dies', 'diese', 'dieselbe', 'dieselben', 'diesem', 'diesen', 'dieser', 'dieses', 'diesmal', 'diesseits', 'dir',
                'direkt', 'direkte', 'direkten', 'direkter', 'doch', 'dort', 'dorther', 'dorthin', 'drin', 'drüber', 'drunter', 'du', 'dunklen', 'durch',
                'durchaus', 'durchweg', 'e. K.', 'eben', 'ebenfalls', 'ebenso', 'ehe', 'eher', 'eigenen', 'eigenes', 'eigentlich', 'ein', 'ein-', 'eine',
                'einem', 'einen', 'einer', 'einerseits', 'eines', 'einfach', 'einfachen', 'eingang', 'einig', 'einige', 'einigem', 'einigen', 'einiger',
                'einigermaßen', 'einiges', 'einmal', 'einseitig', 'einseitige', 'einseitigen', 'einseitiger', 'einst', 'einstmals', 'einzig', 'empfangen',
                'entsprechend', 'entweder', 'er', 'ergo', 'erhält', 'erheblich', 'erneut', 'erst', 'ersten', 'es', 'etc', 'etliche', 'etwa', 'etwas', 'euch',
                'euer', 'eure', 'eurem', 'euren', 'eurer', 'eures', 'falls', 'fast', 'ferner', 'fluechtlinge', 'folgende  ', 'folgenden', 'folgender',
                'folgendermaßen', 'folgendes', 'folglich', 'förmlich', 'fortwährend', 'fraglos', 'Frau', 'frei', 'freie', 'freies', 'freilich',
                'funktioniert', 'für', 'gab', 'gängig', 'gängige', 'gängigen', 'gängiger', 'gängiges', 'ganz', 'ganze', 'ganzem', 'ganzen', 'ganzer',
                'ganzes', 'gänzlich', 'gar', 'GbdR', 'GbR', 'gebe', 'geben', 'geehrte', 'geehrten', 'geehrter', 'gefälligst', 'gegen', 'gehabt',
                'gekauft', 'gekonnt', 'gelegentlich', 'gemacht', 'gemäß', 'gemeinhin', 'gemocht', 'genau', 'genommen', 'genug', 'geradezu', 'gern',
                'gestern', 'gestrige', 'gesuch', 'gesuche', 'getan', 'geteilt', 'geteilte', 'getragen', 'gewesen', 'gewiss', 'gewisse', 'gewissermaßen',
                'gewollt', 'geworden', 'ggf', 'gib', 'gibt', 'gleich', 'gleichsam', 'gleichwohl', 'gleichzeitig', 'glücklicherweise', 'gmbh',
                'Gott sei Dank', 'größtenteils', 'Grunde', 'gute', 'guten', 'hab', 'habe', 'haben', 'halb', 'hallo', 'halt', 'hast', 'hat',
                'hatte', 'hätte', 'hätten', 'hattest', 'hattet', 'häufig', 'heraus', 'herein', 'heute', 'heutige', 'hier', 'hiermit', 'hiesige',
                'hilfsbedürftige', 'hin', 'hinein', 'hingegen', 'hinlänglich', 'hinten', 'hinter', 'hinterher', 'hoch', 'höchst', 'höchstens',
                'ich', 'ihm', 'ihn', 'ihnen', 'ihr', 'ihre', 'ihrem', 'ihren', 'ihrer', 'ihres', 'im', 'immer', 'immerhin', 'immerzu', 'in',
                'indem', 'indessen', 'infolge', 'infolgedessen', 'innen', 'innerhalb', 'input', 'ins', 'insbesondere', 'insofern', 'inzwischen',
                'irgend', 'irgendein', 'irgendeine', 'irgendjemand', 'irgendwann', 'irgendwas', 'irgendwen', 'irgendwer', 'irgendwie',
                'irgendwo', 'ist', 'ja', 'jährig', 'jährige', 'jährigen', 'jähriges', 'je', 'jede', 'jedem', 'jeden', 'jedenfalls', 'jeder',
                'jederlei', 'jedes', 'jedoch', 'jemals', 'jemand', 'jene', 'jenem', 'jenen', 'jener', 'jenes', 'jenseits', 'jetzt', 'kam',
                'kann', 'kannst', 'kaum', 'kein', 'keine', 'keinem', 'keinen', 'keiner', 'keinerlei', 'keines', 'keinesfalls', 'keineswegs',
                'kg', 'klar', 'klare', 'klaren', 'klares', 'klein', 'kleinen', 'kleiner', 'kleines', 'koennen', 'koennte', 'konkret', 'konkrete',
                'konkreten', 'konkreter', 'konkretes', 'können', 'könnt', 'konnte', 'könnte', 'konnten', 'könnten', 'künftig', 'lag', 'lagen',
                'langsam', 'längst', 'längstens', 'lassen', 'laut', 'lediglich', 'leer', 'leicht', 'leider', 'lesen', 'letzten', 'letztendlich',
                'letztens', 'letztes', 'letztlich', 'lichten', 'links', 'Ltd', 'mag', 'magst', 'mal', 'man', 'manche', 'manchem', 'manchen',
                'mancher', 'mancherorts', 'manches', 'manchmal', 'Mann', 'marke', 'mehr', 'mehrere', 'mehrfach', 'mein', 'meine', 'meinem',
                'meinen', 'meiner', 'meines', 'meinetwegen', 'meint', 'meinte', 'meinten', 'meist', 'meiste', 'meisten', 'meistens', 'meistenteils',
                'meta', 'mich', 'mindestens', 'mir', 'mit', 'mithin', 'mitunter', 'moechte', 'moeglicherweise', 'möglich', 'mögliche',
                'möglichen', 'möglicher', 'möglicherweise', 'möglichst', 'morgen', 'morgige', 'muss', 'müssen', 'musst', 'müsst', 'musste',
                'müsste', 'müssten', 'nach', 'nachdem', 'nachher', 'nachhinein', 'nächste', 'nämlich', 'naturgemäß', 'natürlich', 'neben',
                'nebenan', 'nebenbei', 'nein', 'neu', 'neue', 'neuem', 'neuen', 'neuer', 'neuerdings', 'neuerlich', 'neues', 'neulich', 'nicht',
                'nichts', 'nichtsdestotrotz', 'nichtsdestoweniger', 'nie', 'niemals', 'niemand', 'nimm', 'nimmer', 'nimmt', 'nirgends',
                'nirgendwo', 'noch', 'nötigenfalls', 'nun', 'nunmehr', 'nur', 'ob', 'oben', 'oberhalb', 'obgleich', 'obschon', 'obwohl',
                'oder', 'offenbar', 'offenkundig', 'offensichtlich', 'oft', 'OHG', 'ohne', 'ohnedies', 'OK', 'output', 'paar', 'partout',
                'per', 'persönlich', 'pfui', 'plötzlich', 'praktisch', 'pro', 'quasi', 'recht', 'rechts', 'regelmäßig', 'reichlich',
                'relativ', 'restlos', 'richtiggehend', 'riesig', 'rund', 'rundheraus', 'rundum', 'samt', 'sämtliche', 'sattsam',
                'schaetzen', 'schaetzt', 'schaetzte', 'schaetzten', 'schlechter', 'schlicht', 'schlichtweg', 'schließlich', 'schlussendlich',
                'schnell', 'schon', 'Schreibens', 'Schreiber', 'schwerlich', 'schwierig', 'sehr', 'sei', 'seid', 'sein', 'seine', 'seinem',
                'seinen', 'seiner', 'seines', 'seit', 'seitdem', 'Seite', 'Seiten', 'seither', 'selber', 'selbst', 'selbstredend', 'selbstverstaendlich',
                'selten', 'seltsamerweise', 'sich', 'sicher', 'sicherlich', 'sie', 'siehe', 'sieht', 'sind', 'so', 'sobald', 'sodass', 'soeben',
                'sofern', 'sofort', 'sog', 'sogar', 'solange', 'solch', 'solche', 'solchem', 'solchen', 'solcher', 'solches', 'soll', 'sollen', 'sollst',
                'sollt', 'sollte', 'sollten', 'solltest', 'somit', 'sondern', 'sonders', 'sonst', 'sooft', 'soviel', 'soweit', 'sowie', 'sowieso',
                'sowohl', 'sozusagen', 'später', 'spende', 'spenden', 'spielen', 'startet', 'startete', 'starteten', 'statt', 'stattdessen', 'steht',
                'stellenweise', 'stets', 'stimme', 'Tages', 'tat', 'tatsächlich', 'tatsächlichen', 'tatsächlicher', 'tatsächliches', 'teile', 'total',
                'trotzdem', 'übel', 'über', 'überall', 'überallhin', 'überaus', 'überdies', 'überhaupt', 'üblicher', 'übrig', 'übrigens', 'ueber',
                'um', 'umso', 'umständehalber', 'unbedingt', 'unbeschreiblich', 'und', 'unerhört', 'ungefähr', 'ungemein', 'ungewöhnlich', 'ungleich',
                'unglücklicherweise', 'unlängst', 'unmaßgeblich', 'unmöglich', 'unmögliche', 'unmöglichen', 'unmöglicher', 'unnötig', 'uns', 'unsagbar',
                'unsäglich', 'unser', 'unsere', 'unserem', 'unseren', 'unserer', 'unseres', 'unserm', 'unstreitig', 'unten', 'unter', 'unterbrach',
                'unterbrechen', 'unterhalb', 'unwichtig', 'unzweifelhaft', 'usw', 'vergleichsweise', 'vermutlich', 'veröffentlichen', 'veröffentlicht',
                'veröffentlichte', 'veröffentlichten', 'veröffentlichtes', 'verstaerken', 'viel', 'viele', 'vielen', 'vieler', 'vieles', 'vielfach',
                'vielleicht', 'vielmals', 'voll', 'vollends', 'völlig', 'vollkommen', 'vollständig', 'vom', 'von', 'vor', 'voran', 'vorbei',
                'vorgestern', 'vorher', 'vorne', 'vorüber', 'während', 'währenddessen', 'wahrscheinlich', 'wann', 'war', 'wäre', 'waren', 'wären',
                'warst', 'warum', 'was', 'weder', 'weg', 'wegen', 'weidlich', 'weil', 'Weise', 'weiß', 'weitem', 'weiter', 'weitere',
                'weiterem', 'weiteren', 'weiterer', 'weiteres', 'weiterhin', 'weitgehend', 'welche', 'welchem', 'welchen', 'welcher', 'welches',
                'wem', 'wen', 'wenig', 'wenige', 'weniger', 'wenigstens', 'wenn', 'wenngleich', 'wer', 'werde', 'werden', 'werdet', 'weshalb',
                'wessen', 'wichtig', 'wie', 'wieder', 'wiederum', 'wieso', 'wiewohl', 'will', 'willst', 'wir', 'wird', 'wirklich', 'wirst',
                'wo', 'wobei', 'wodurch', 'wogegen', 'woher', 'wohin', 'wohingegen', 'wohl', 'wohlgemerkt', 'wohlweislich', 'wollen', 'wollt',
                'wollte', 'wollten', 'wolltest', 'wolltet', 'womit', 'womöglich', 'woraufhin', 'woraus', 'worin', 'wurde', 'würde', 'würden',
                'z. B.', 'zahlreich', 'zeitweise', 'ziemlich', 'zu', 'zudem', 'zuerst', 'zufolge', 'zugegeben', 'zugleich', 'zuletzt', 'zum',
                'zumal', 'zumeist', 'zur', 'zurück', 'zusammen', 'zusehends', 'zuvor', 'zuweilen', 'zwar', 'zweifellos', 'zweifelsfrei',
                'zweifelsohne', 'zwischen'
            ];

            $tags = "";

            foreach ($formatted_words as $formatted_word) {
                if (in_array($formatted_word, $not_relevant_words)) {
                    continue;
                }
                if (strlen($tags)>0) {
                    if (stripos($tags, $formatted_word) !== false) {
                        continue;
                    }
                }
                $tags.=",".$formatted_word;
            }

            if (strlen($tags)>0) {
                $response = [
                    'status' => 'success',
                    'tags' => $tags
                ];
            } else {
                $response = [
                    'status' => 'failed',
                ];
            }
            $this->set('response', $response);
            $this->render('response');

        }
    }

    /**
     * Methode zum Suchen von Gesuchen oder Spenden
     *
     * @param null $search_input
     */
    public function search($search_input = null) {
        if ($search_input) {
            $decoded = explode('&', $search_input);
            $this->set('title', 'Deine Suche: ' . $decoded[1]);
            $search_value_array = str_replace('-', ' ', $decoded[1]);
            $search_value_array = str_replace('_', ' ', $search_value_array);
            $search_value_array = explode(' ', $search_value_array);
            switch($decoded[0]) {

                // Gesuche durchsuchen
                case "gesuche":
                    $all_results = array();
                    $search_input_array = explode(" ", $decoded[1]);
                    foreach ($search_input_array as $search_input_key) {
                        $search_input_key_normal = $search_input_key;
                        $search_input_key = $this->__replaceChars($search_input_key);
                        $search_input_key = strtolower($search_input_key);
                        $this->loadModel('Searches');
                        $all_searches_ids = $this->Searches->find()->select(['Searches.id'])->where(['OR' => [['Searches.title LIKE' => "%".$search_input_key_normal."%"],['Searches.description LIKE' => "%".$search_input_key_normal."%"]]])->all();
                        foreach ($all_searches_ids as $searches_id) {
                            $all_results[$searches_id->id] = $searches_id->id;
                        }
                        $this->loadModel('Keywords');
                        $all_keyword_ids = $this->Keywords->find()->select(['Keywords.id'])->where(['Keywords.keyword LIKE' => "%".$search_input_key."%"])->all();
                        foreach ($all_keyword_ids as $keyword_id) {
                            $this->loadModel('KeywordsSearches');
                            $all_keyword_searches_ids = $this->KeywordsSearches->find()->select(['KeywordsSearches.search_id'])->where(['KeywordsSearches.keyword_id' => $keyword_id->id])->all();
                            foreach ($all_keyword_searches_ids as $keyword_searches_id) {
                                if (isset($all_results[$keyword_searches_id->search_id])) {
                                    continue;
                                } else {
                                    $all_results[$keyword_searches_id->search_id] = $keyword_searches_id->search_id;
                                }
                            }
                        }
                    }
                    $searches_array = array();
                    foreach ($all_results as $key => $result) {
                        $search = $this->Searches->find()->where(['Searches.id' => $result])->first();
                        if ($search) {
                            $searches_array[] = $search;
                        }
                    }

                    $this->set('results', $searches_array);
                    $this->set('result_type', 'gesuche');
                    break;

                // Spenden durchsuchen
                case "spenden":
                    $all_results = array();
                    $search_input_array = explode(" ", $decoded[1]);
                    foreach ($search_input_array as $search_input_key) {
                        $search_input_key_normal = $search_input_key;
                        $search_input_key = $this->__replaceChars($search_input_key);
                        $search_input_key = strtolower($search_input_key);
                        $this->loadModel('Donations');
                        $all_donations_ids = $this->Donations->find()->select(['Donations.id'])->where(['OR' => [['Donations.title LIKE' => "%".$search_input_key_normal."%"],['Donations.description LIKE' => "%".$search_input_key_normal."%"]]])->all();
                        foreach ($all_donations_ids as $donations_id) {
                            $all_results[$donations_id->id] = $donations_id->id;
                        }
                        $this->loadModel('Keywords');
                        $all_keyword_ids = $this->Keywords->find()->select(['Keywords.id'])->where(['Keywords.keyword LIKE' => "%".$search_input_key."%"])->all();
                        foreach ($all_keyword_ids as $keyword_id) {
                            $this->loadModel('KeywordsDonations');
                            $all_keyword_donations_ids = $this->KeywordsDonations->find()->select(['KeywordsDonations.donation_id'])->where(['KeywordsDonations.keyword_id' => $keyword_id->id])->all();
                            foreach ($all_keyword_donations_ids as $keyword_donations_id) {
                                if (isset($all_results[$keyword_donations_id->donation_id])) {
                                    continue;
                                } else {
                                    $all_results[$keyword_donations_id->search_id] = $keyword_donations_id->donations_id;
                                }
                            }
                        }
                    }
                    $donations_array = array();
                    foreach ($all_results as $key => $result) {
                        $donation = $this->Donations->find()->where(['Donations.id' => $result])->first();
                        if ($donation) {
                            $donations_array[] = $donation;
                        }
                    }

                    $this->set('results', $donations_array);
                    $this->set('result_type', 'spenden');
                    break;
            }
        } else {
            $this->set('title', 'Suchformular');
        }

        if (isset($search_input) && strlen($search_input)>0) {
            $this->set('search_user_input', $decoded[1]);
            $this->set('search_user_category', $decoded[0]);
        }
    }

    /**
     * Methode zum Verwalten von Gesuchen oder Spenden
     *
     * @param null $case
     */
    public function manage($case = null) {

        $brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $this->Auth->user('id')])->all();
        if ($brokers_zipcodes) {
            $related_zipcodes_array = array();
            foreach ($brokers_zipcodes as $brokers_zipcode) {
                $related_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.id' => $brokers_zipcode->valid_zipcodes_id])->first();
                if ($related_zipcode) {
                    $related_zipcodes_array[] = $related_zipcode->zipcode;
                }
            }
        }


        $case_array = explode('|', base64_decode($case));
        switch ($case_array[0]) {
            // Gesuche verwalten
            case "gesuche":
                $this->set('title', 'Verwaltung - Aktuelle Gesuche');
                if (sizeof($related_zipcodes_array)>0) {
                    $data = $this->Searches->find()->contain(['Addresses'])->where(['Searches.active' => 1, 'Addresses.zip IN' => $related_zipcodes_array])->order(['Searches.created' => 'DESC', 'Searches.category' => 'ASC']);
                } else {
                    $data = array();
                }
                $columns = ['#', 'Titel', 'Art', 'Erstellt', 'Zuletzt geändert', 'Aktionen'];
                break;

            // Spenden verwalten
            case "spenden":
                $this->set('title', 'Verwaltung - Spenden');
                if (sizeof($related_zipcodes_array)>0) {
                    $data = $this->Donations->find()->contain(['Addresses'])->where(['Donations.active' => 1, 'Addresses.zip IN' => $related_zipcodes_array])->order(['Donations.created' => 'DESC', 'Donations.category' => 'ASC']);
                } else {
                    $data = array();
                }
                $columns = ['#', 'Titel', 'Art', 'Erstellt', 'Zuletzt geändert', 'Aktionen'];
                break;

            // Eigene Spenden verwalten
            case "spenderspenden":
                $this->set('title', 'Meine Spenden');
                $data = $this->Donations->find()->where(['Donations.user_id' => $this->Auth->user('id')])->order(['Donations.active' => 'DESC', 'Donations.created' => 'DESC']);
                $columns = ['Status', 'Titel', 'Art', 'Erstellt', 'Zuletzt geändert', 'Aktionen'];
                break;
        }

        $this->set('data', $data);
        $this->set('columns', $columns);
        $this->set('type', $case_array[0]);

    }

    /**
     * Methode zur Auflistung von archivierten Spenden und Gesuchen
     *
     * @param null $case
     */
    public function archive($case = null) {

        $brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $this->Auth->user('id')])->all();
        if ($brokers_zipcodes) {
            $related_zipcodes_array = array();
            foreach ($brokers_zipcodes as $brokers_zipcode) {
                $related_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.id' => $brokers_zipcode->valid_zipcodes_id])->first();
                if ($related_zipcode) {
                    $related_zipcodes_array[] = $related_zipcode->zipcode;
                }
            }
        }

        $case_array = explode('|', base64_decode($case));
        switch ($case_array[0]) {

            // Gesuche verwalten
            case "gesuche":
                $this->set('title', 'Verwaltung - Archivierte Gesuche');
                $data = $this->Searches->find()->contain(['Addresses'])->where(['Searches.active' => 0, 'Addresses.zip IN' => $related_zipcodes_array])->order(['Searches.created' => 'DESC', 'Searches.category' => 'ASC']);
                $columns = ['#', 'Titel', 'Art', 'Erstellt', 'Zuletzt geändert', 'Aktionen'];
                break;

            // Spenden verwalten
            case "spenden":
                $this->set('title', 'Verwaltung - Archivierte Spenden');
                $data = $this->Donations->find()->contain(['Addresses'])->where(['Donations.active' => 0, 'Addresses.zip IN' => $related_zipcodes_array])->order(['Donations.created' => 'DESC', 'Donations.category' => 'ASC']);
                $columns = ['#', 'Titel', 'Art', 'Erstellt', 'Zuletzt geändert', 'Aktionen'];
                break;
        }

        $this->set('data', $data);
        $this->set('columns', $columns);
        $this->set('type', $case_array[0]);
    }

    /**
     * Methode zum Archivieren von Gesuchen und Spenden
     *
     * @param $input_token
     * @return \Cake\Network\Response|null
     */
    public function toArchive($input_token) {
        $token_array = explode('|', base64_decode($input_token));
        switch($token_array[2]) {
            case "gesuche":
                $search = $this->Searches->find()->where(['Searches.id' => $token_array[0]])->first();
                if ($search) {
                    $patched_search = $this->Searches->patchEntity($search, ['active' => 0]);
                    if ($this->Searches->save($patched_search)) {
                        $this->Flash->success(__('Erfolgreich deaktiviert.'));
                    }
                }
                break;
            case "spenden":
                $donation = $this->Donations->find()->where(['Donations.id' => $token_array[0]])->first();
                if ($donation) {
                    $this->loadModel('DonationsStatistics');
                    $new_donations_statistics_entity_array = [
                        'donation_id' => $token_array[0],
                        'reason' => $this->request->data['reason'],
                        'notice' => $this->request->data['reason_optional']
                    ];
                    $new_donations_statistics_entity = $this->DonationsStatistics->newEntity($new_donations_statistics_entity_array);
                    $this->DonationsStatistics->save($new_donations_statistics_entity);
                    $patched_donation = $this->Donations->patchEntity($donation, ['active' => 0]);
                    if ($this->Donations->save($patched_donation)) {
                        $this->Flash->success(__('Erfolgreich deaktiviert. Begründung eingetragen.'));
                    }

                }
                break;
        }
        return $this->redirect($this->referer());
    }

    /**
     * Methode zur Aktivierung von Gesuchen und Spenden
     *
     * @param $input_token
     * @return \Cake\Network\Response|null
     */
    public function toActive($input_token) {
        $token_array = explode('|', base64_decode($input_token));
        switch($token_array[2]) {
            case "gesuche":
                $search = $this->Searches->find()->where(['Searches.id' => $token_array[0]])->first();
                if ($search) {
                    $patched_search = $this->Searches->patchEntity($search, ['active' => 1]);
                    if ($this->Searches->save($patched_search)) {
                        $this->Flash->success(__('Erfolgreich aktiviert.'));
                    }
                }
                break;
            case "spenden":
                $donation = $this->Donations->find()->where(['Donations.id' => $token_array[0]])->first();
                if ($donation) {
                    $patched_donation = $this->Donations->patchEntity($donation, ['active' => 1]);
                    if ($this->Donations->save($patched_donation)) {
                        $this->Flash->success(__('Erfolgreich aktiviert.'));
                    }

                }
                break;
        }
        return $this->redirect($this->referer());
    }

    /**
     * Methode zur Darstellung der Detailseite eines Gesuchs oder einer Spende
     *
     * @param null $case
     */
    public function details($case = null) {
        $case_array = explode('|', base64_decode($case));
        switch ($case_array[2]) {
            case "gesuche":
                $gesuch = $this->Searches->get($case_array[0], ['contain' => ['FileUploads', 'TimeSearches', 'Users', 'Users.Addresses']]);
                $this->set('title', 'Details - ' . $gesuch->title);
                $contact = $this->Addresses->find()->where(['Addresses.user_id' => $gesuch->user_id, 'Addresses.id' => $gesuch->address_id])->first();
                $contact_user = $this->Users->find()->where(['Users.id' => $gesuch->user_id])->first();
                if ($contact && $contact_user) {
                    $contact_array = [
                        'firstname' => $contact_user->firstname,
                        'lastname' => $contact_user->lastname,
                        'phone' => $contact_user->phone,
                        'fax' => $contact_user->fax,
                        'street' => $contact->street,
                        'zip' => $contact->zip,
                        'city' => $contact->city
                    ];
                    $this->set('contact_array', $contact_array);
                }
                $this->set('search', $gesuch);
                $this->set('type', 'gesuche');
                break;
            case "spenden":
                $spende = $this->Donations->get($case_array[0], ['contain' => ['FileUploads', 'TimeDonations']]);
                $this->set('title', 'Details - ' . $spende->title);
                if (isset($spende->user_id) && $spende->user_id>0) {
                    $contact = $this->Addresses->find()->where(['Addresses.user_id' => $spende->user_id, 'Addresses.id' => $spende->address_id])->first();
                    $contact_user = $this->Users->find()->where(['Users.id' => $spende->user_id])->first();
                    if ($contact && $contact_user) {
                        $contact_array = [
                            'firstname' => $contact_user->firstname,
                            'lastname' => $contact_user->lastname,
                            'phone' => $contact_user->phone,
                            'fax' => $contact_user->fax,
                            'street' => $contact->street,
                            'zip' => $contact->zip,
                            'city' => $contact->city
                        ];
                        $this->set('contact_array', $contact_array);
                    }
                } else {
                    $this->set('contact_array', ['zip' => '', 'city' => '']);
                }
                $this->set('donation', $spende);
                $this->set('type', 'spenden');
                break;
        }

        $broker = $this->Users->get($this->Auth->user('id'));
        $this->set('broker', $broker);
    }

    /**
     * Methode zum Bearbeiten einer Spende
     *
     * @param null $donation_token
     * @return \Cake\Network\Response|null
     */
    public function edit($donation_token = null) {
        $this->set('title', 'Spende bearbeiten');

        $donation_token_array = explode('|', base64_decode($donation_token));

        $this->loadModel('Donations');
        $donation = $this->Donations->find()->where(['Donations.id' => $donation_token_array[0]])->first();
        if (!$donation) {
            $this->Flash->error(__('Spende wurde nicht gefunden.'));
            return $this->redirect($this->referer());
        }

        if ($this->request->is(['post', 'put', 'patch'])) {
            $this->loadModel('FileUploads');

            // Hochgeladene Bilder speichern
            if (isset($this->request->data['uploads']) && !empty($this->request->data['uploads'])) {
                if (!isset($this->request->data['rights'])) {
                    $this->Flash->error(__('Rechte an den Bildern bestätigen.'));
                    return $this->redirect($this->referer());
                }
                foreach ($this->request->data['uploads'] as $upload) {
                    $file_upload = $this->FileUpload->process($upload);

                    $new_fileupload_entity = [
                        'donation_id' => $donation->id,
                        'src' => $file_upload,
                        'type' => $upload['type'],
                        'filename' => $upload['name'],
                        'size' => $upload['size']
                    ];

                    $fileupload_entity = $this->FileUploads->newEntity($new_fileupload_entity);
                    $this->FileUploads->save($fileupload_entity);

                }
            }
            if (isset($this->request->data['upload_id1'])) {
                for ($id_counter=1;$id_counter<10;$id_counter++) {
                    if (isset($this->request->data['upload_id'.$id_counter]) && is_numeric($this->request->data['upload_id'.$id_counter]) && $this->request->data['upload_id'.$id_counter]>0) {
                        $file_upload = $this->FileUploads->find()->where(['FileUploads.id' => $this->request->data['upload_id'.$id_counter]])->first();
                        if (!$file_upload) {
                            continue;
                        }
                        @unlink($file_upload->src);
                        $this->FileUploads->delete($file_upload);
                    } else {
                        continue;
                    }
                }
            }

            $patch_entity = $this->Donations->patchEntity($donation, $this->request->data);
            if ($this->Donations->save($patch_entity)) {
                $this->Flash->success(__('Änderungen gespeichert.'));
            } else {
                $this->Flash->error(__('Änderungen nicht gespeichert.'));
            }
            return $this->redirect(['action' => 'manage', base64_encode('spender_spenden')."|"]);
        }
        $this->loadModel('Addresses');
        $address = $this->Addresses->find()->where(['Addresses.id' => $donation->address_id])->first();
        if ($address) {
            $this->set('address', $address);
        }
        $uploads = array();
        $donation->uploads = $uploads;
        $this->set('donation', $donation);

        if ($donation->has('category') && $donation->category == 'time') {
            $this->loadModel('TimeDonations');
            $donation_times = $this->TimeDonations->find()->where(['TimeDonations.donation_id' => $donation->id])->all();
            if ($donation_times) {
                $this->set('donation_times', $donation_times);
            }
        }

        $this->loadModel('FileUploads');
        $donation_uploads = $this->FileUploads->find()->where(['FileUploads.donation_id' => $donation->id])->all();
        if ($donation_uploads) {
            $this->set('donation_uploads', $donation_uploads);
        }

    }

    /**
     * Methode zum Löschen eines Gesuchs oder einer Spende
     *
     * @param null $input_token
     * @return \Cake\Network\Response|null
     */
    public function delete($input_token = null) {
        $this->loadModel('Donations');
        $this->loadModel('Searches');
        $token_array = explode('|', base64_decode($input_token));
        switch($token_array[2]) {
            case "gesuche":
                $search = $this->Searches->find()->where(['Searches.id' => $token_array[0]])->first();
                if ($search) {
                    if ($this->Searches->delete($search)) {
                        $this->Flash->success(__('Erfolgreich gelöscht.'));
                    }
                }
                break;
            case "spenden":
                $donation = $this->Donations->find()->where(['Donations.id' => $token_array[0]])->first();
                if ($donation) {
                    if ($this->Donations->delete($donation)) {
                        $this->Flash->success(__('Erfolgreich gelöscht.'));
                    }

                }
                break;
        }
        return $this->redirect($this->referer());
    }

    /**
     * Methode zum Senden einer Anfrage
     */
    public function request() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->is('ajax')) {

            //$this->request->data['type'] = $this->request->data['cb930723a0719aac'];
            $this->request->data['search_id'] = base64_decode($this->request->data['c9c6d316d6dc4d952a789fd4b8858ed8']);
            $this->request->data['sender_name'] = $this->request->data['cb930723a0719aac91a0b3ca09250f2a'];
            $this->request->data['sender_email'] = $this->request->data['c9c6d316d6dc4d952a789fd4b8858ed7'];
            $this->request->data['message'] = $this->request->data['cb930723a0719aac91a0b3ca09250f2b'];

            // Anfrage speichern
            $log_array = [
                'user_id' => ($this->Auth->user('id') > 0) ? $this->Auth->user('id') : NULL,
                'search_id' => $this->request->data['search_id'],
                'log_title' => 'Anfrage gesendet',
                'log_text' => $this->request->data['sender_name'] . " hat eine neue Anfrage gesendet.",
            ];

            $logged = $this->__log($log_array);


            if ($logged) {
                $response = [
                    'status' => 'success'
                ];
            } else {
                $response = [
                    'status' => 'failed',
                    'logged' => $logged,
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }

    }

    /**
     * Methode zum Finden von Matches
     */
    public function matches() {

        $this->set('title', 'Matches');

        $this->loadModel('Matches');

        $matches = $this->Matches->find()->select(['Matches.id'])->order(['Matches.created' => 'DESC']);
        $matches_ids = array();
        foreach ($matches as $match) {
            $match_search = $this->Searches->find()->where(['Searches.id' => $match->search_id])->first();
            if ($match_search && $match_search->user_id = $this->Auth->user('id')) {
                $matches_ids[$match->id] = $match->id;
            }
        }

        $matches = $this->Matches->find()->contain(['Donations', 'Searches'])->where(['Matches.id IN' => $matches_ids])->order(['Matches.created' => 'DESC']);

        $logs = $this->Logs->find()->where(['Logs.log_title' => 'Anfrage gesendet', 'Logs.donation_id >' => 0])->all();

        $logs_array = array();

        foreach ($logs as $log) {
            $logs_array[$log->donation_id] = 1;
        }

        $this->set('logs', $logs_array);

        $this->set('matches', $matches);
    }

    /**
     * Methode zum Generieren von Spenden nach Schlagwörtern gruppiert
     *
     * @param $case
     * @param $keyword
     */
    public function keywordDonations($case, $keyword) {
        $this->set('title', $keyword . " - Spenden");

        $all_keywords = $this->Keywords->find()->select(['Keywords.id'])->where(['Keywords.keyword' => $this->replaceChars(strtolower($keyword))])->all();
        $donations_ids = array();
        foreach ($all_keywords as $all_keyword) {
            $all_connections = $this->KeywordsDonations->find()->select(['KeywordsDonations.donation_id'])->where(['KeywordsDonations.keyword_id' => $all_keyword->id])->all();
            foreach ($all_connections as $connection) {
                if (isset($donations_ids[$connection->donation_id])) {
                    continue;
                }
                $donations_ids[$connection->donation_id] = $connection->donation_id;
            }
        }
        switch($case) {
            case "sach":
                $case = "thing";
                break;
            case "zeit":
                $case = "time";
                break;
        }
        $this->set('donations', $this->Donations->find()->where(['Donations.id IN' => $donations_ids, 'Donations.category' => $case])->all());
    }

    /**
     * Öffentliche Methode zum Ersetzen von Umlauten
     *
     * @param $string
     * @return mixed
     */
    public function replaceChars($string) {
        return $this->__replaceChars($string);
    }

    /**
     * Öffentliche Methode zum Ersetzen von Umlauten (rückwärts)
     *
     * @param $string
     * @return mixed
     */
    public function replaceCharsBackwards($string) {
        return $this->__replaceCharsBackwards($string);
    }

    /**
     * Private Methode zum Ersetzen von Umlauten
     *
     * @param $string
     * @return mixed
     */
    private function __replaceChars($string) {
        $string = str_replace("ä", "ae", $string);
        $string = str_replace("ü", "ue", $string);
        $string = str_replace("ö", "oe", $string);
        $string = str_replace("Ä", "Ae", $string);
        $string = str_replace("Ü", "Ue", $string);
        $string = str_replace("Ö", "Oe", $string);
        $string = str_replace("ß", "ss", $string);
        $string = str_replace("´", "", $string);
        return $string;
    }

    /**
     * Private Methode zum Ersetzen von Umlauten (rückwärts)
     *
     * @param $string
     * @return mixed
     */
    private function __replaceCharsBackwards($string) {
        $string = str_replace("ae", "ä", $string);
        $string = str_replace("ue", "ü", $string);
        $string = str_replace("oe", "ö", $string);
        $string = str_replace("Ae", "Ä", $string);
        $string = str_replace("Ue", "Ü", $string);
        $string = str_replace("Oe", "Ö", $string);
        $string = str_replace("ss", "ß", $string);
        return $string;
    }
}