<?php
namespace App\Controller\Super;

use App\Controller\AppController;

/**
 * ValidZipcodes Controller
 *
 * @property \App\Model\Table\ValidZipcodesTable $ValidZipcodes
 */
class ValidZipcodesController extends AppController
{

    /**
     * Methode zur Darstellung aller gültigen Postleitzahlen
     *
     * @return void
     */
    public function index()
    {
        $this->set('title', 'Regionen verwalten');
        $this->set('validZipcodes', $this->ValidZipcodes->find()->all());
        $this->set('_serialize', ['validZipcodes']);
    }

    /**
     * Methode zum Bearbeiten einer Zuweisung eines Adminstrators zu einer gültigen Postleitzahl
     *
     * @param string|null $id Valid Zipcode id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        if (is_numeric($id)) {
            $this->set('title', 'Eintrag bearbeiten');
            $validZipcode = $this->ValidZipcodes->get($id);
        } else {
            $this->set('title', 'Eintrag anlegen');
            $validZipcode = $this->ValidZipcodes->newEntity($this->request->data);
        }

        $this->loadModel('AdminsZipcodes');
        $this->loadModel('BrokersZipcodes');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $new_brokers_zipcodes_ids = array();
            if (isset($this->request->data['brokers_ids']) && $this->request->data['brokers_ids']!='') {
                foreach ($this->request->data['brokers_ids'] as $brokers_id) {
                    $new_brokers_zipcodes_ids[] = $brokers_id;
                    $broker_zipcode_exists = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.valid_zipcodes_id' => $id, 'BrokersZipcodes.broker_id' => $brokers_id])->first();
                    if ($broker_zipcode_exists) {
                        continue;
                    }
                    $new_broker_zipcode_entity = [
                        'broker_id' => $brokers_id,
                        'valid_zipcodes_id' => $id
                    ];
                    $new_broker_zipcode = $this->BrokersZipcodes->newEntity($new_broker_zipcode_entity);
                    $this->BrokersZipcodes->save($new_broker_zipcode);
                }

                if (sizeof($new_brokers_zipcodes_ids)>0) {
                    $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.valid_zipcodes_id' => $id, 'BrokersZipcodes.broker_id NOT IN' => $new_brokers_zipcodes_ids])->all();
                } else {
                    $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.valid_zipcodes_id' => $id])->all();
                }
                if ($delete_brokers_zipcodes) {
                    foreach ($delete_brokers_zipcodes as $delete_brokers_zipcode) {
                        $this->BrokersZipcodes->delete($delete_brokers_zipcode);
                    }
                }
            } else {
                $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.valid_zipcodes_id' => $id])->all();
                if ($delete_brokers_zipcodes) {
                    foreach ($delete_brokers_zipcodes as $delete_brokers_zipcode) {
                        $this->BrokersZipcodes->delete($delete_brokers_zipcode);
                    }
                }
            }

            $new_admins_zipcodes_ids = array();
            if (isset($this->request->data['admins_ids']) && $this->request->data['admins_ids']!='') {
                foreach ($this->request->data['admins_ids'] as $admins_id) {
                    $new_admins_zipcodes_ids[] = $admins_id;
                    $admin_zipcode_exists = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.valid_zipcodes_id' => $id, 'AdminsZipcodes.admin_id' => $admins_id])->first();
                    if ($admin_zipcode_exists) {
                        continue;
                    }
                    $new_admin_zipcode_entity = [
                        'admin_id' => $admins_id,
                        'valid_zipcodes_id' => $id
                    ];
                    $new_admin_zipcode = $this->AdminsZipcodes->newEntity($new_admin_zipcode_entity);
                    $this->AdminsZipcodes->save($new_admin_zipcode);
                }

                if (sizeof($new_admins_zipcodes_ids)>0) {
                    $delete_admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.valid_zipcodes_id' => $id, 'AdminsZipcodes.admin_id NOT IN' => $new_admins_zipcodes_ids])->all();
                } else {
                    $delete_admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.valid_zipcodes_id' => $id])->all();
                }
                if ($delete_admins_zipcodes) {
                    foreach ($delete_admins_zipcodes as $delete_admins_zipcode) {
                        $this->AdminsZipcodes->delete($delete_admins_zipcode);
                    }
                }
            } else {
                $delete_admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.valid_zipcodes_id' => $id])->all();
                if ($delete_admins_zipcodes) {
                    foreach ($delete_admins_zipcodes as $delete_admins_zipcode) {
                        $this->AdminsZipcodes->delete($delete_admins_zipcode);
                    }
                }
            }

            $validZipcode = $this->ValidZipcodes->patchEntity($validZipcode, $this->request->data);
            if ($this->ValidZipcodes->save($validZipcode)) {
                $this->Flash->success(__('Postleitzahl erfolgreich gespeichert.'));
                return $this->redirect('/super/plz_verwalten');
            } else {
                $this->Flash->error(__('Postleitzahl wurde nicht gespeichert.'));
            }
        }

        $related_brokers = array();
        $brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.valid_zipcodes_id' => $id])->all();
        $this->loadModel('Users');
        foreach ($brokers_zipcodes as $brokers_zipcode) {
            $user = $this->Users->find()->where(['Users.id' => $brokers_zipcode->broker_id, 'Users.role' => 'broker'])->first();
            if (!$user) {
                continue;
            }
            $related_brokers[] = $brokers_zipcode->broker_id;
        }
        $brokers = $this->Users->find()->where(['Users.active' => 1, 'Users.role' => 'broker'])->all();
        $brokers_array = array();
        foreach ($brokers as $broker) {
            $brokers_array[$broker->id] = $broker->firstname . " " . $broker->lastname;
        }
        $this->set('brokers', $brokers_array);
        $validZipcode->brokers_ids = $related_brokers;


        $related_admins = array();
        $admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.valid_zipcodes_id' => $id])->all();
        $this->loadModel('Users');
        foreach ($admins_zipcodes as $admins_zipcode) {
            $user = $this->Users->find()->where(['Users.id' => $admins_zipcode->admin_id, 'Users.role' => 'admin'])->first();
            if (!$user) {
                continue;
            }
            $related_admins[] = $admins_zipcode->admin_id;
        }
        $admins = $this->Users->find()->where(['Users.active' => 1, 'Users.role' => 'admin'])->all();
        $admins_array = array();
        foreach ($admins as $admin) {
            $admins_array[$admin->id] = $admin->firstname . " " . $admin->lastname;
        }
        $this->set('admins', $admins_array);
        $validZipcode->admins_ids = $related_admins;

        $this->set(compact('validZipcode'));
        $this->set('_serialize', ['validZipcode']);
    }

    /**
     * Methode zum Löschen einer gültigen Postleitzahl
     *
     * @param string|null $id Valid Zipcode id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $validZipcode = $this->ValidZipcodes->get($id);
        if ($this->ValidZipcodes->delete($validZipcode)) {
            $this->Flash->success(__('Postleitzahl erfolgreich gelöscht.'));
        } else {
            $this->Flash->error(__('Postleitzahl wurde nicht gelöscht.'));
        }
        return $this->redirect('/super/plz_verwalten');
    }
}
