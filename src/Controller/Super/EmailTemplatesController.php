<?php
namespace App\Controller\Super;

use App\Controller\AppController;

/**
 * EmailTemplates Controller
 *
 * @property \App\Model\Table\EmailTemplatesTable $EmailTemplates
 */
class EmailTemplatesController extends AppController
{

    /**
     * Methode zur Darstellung aller E-Mail-Templates
     *
     * @return void
     */
    public function index()
    {
        $this->set('title', 'E-Mails verwalten');
        $this->set('emailTemplates', $this->paginate($this->EmailTemplates));
        $this->set('_serialize', ['emailTemplates']);
    }

    /**
     * Methode zum Bearbeiten eines E-Mail-Templates
     *
     * @param string|null $id Email Template id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'E-Mail bearbeiten');
        $emailTemplate = $this->EmailTemplates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailTemplate = $this->EmailTemplates->patchEntity($emailTemplate, $this->request->data);
            if ($this->EmailTemplates->save($emailTemplate)) {
                $this->Flash->success(__('Die E-Mail wurde erfolgreich gespeichert.'));
                return $this->redirect('/super/emails_verwalten');
            } else {
                $this->Flash->error(__('Die E-Mail wurde nicht erfolgreich gespeichert.'));
            }
        }
        $this->set(compact('emailTemplate'));
        $this->set('_serialize', ['emailTemplate']);
    }

}
