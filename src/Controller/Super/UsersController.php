<?php
namespace App\Controller\Super;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Utility\Security;
use Cake\Log\Log;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    /**
     * @return \Cake\Network\Response|null
     */
    public function login() {
        return $this->redirect('/login');
    }

    /**
     * @param $input
     * @return string
     */
    private function __decrypt($input) {
        $key = "123";
        $result = Security::decrypt($input, $key);
        return $result;
    }

    /**
     * Methode zum Auflisten aller Partner
     */
    public function partnersIndex()
    {
        $this->set('title', __('Partner verwalten'));

        $this->loadModel('Partners');
        $this->loadModel('PartnersZipcodes');

        $partners = $this->Partners->find()->contain(['ValidZipcodes']);

        $this->set('partners', $partners);
    }

    /**
     * Methode zum Löschen eines Partners
     *
     * @param null $id
     * @return \Cake\Network\Response|null
     */
    public function partnersDelete($id = null)
    {
        $this->loadModel('Partners');
        $this->loadModel('PartnersZipcodes');

        $partner = $this->Partners->find()->where(['Partners.id' => $id])->first();

        $partner_zipcodes = $this->PartnersZipcodes->find()->where(['PartnersZipcodes.partner_id' => $id]);

        if ($partner_zipcodes) {
            foreach ($partner_zipcodes as $partner_zipcode) {
                $this->PartnersZipcodes->delete($partner_zipcode);
            }
        }

        if ($this->Partners->delete($partner)) {
            $this->Flash->success(__('Partner erfolgreich gelöscht.'));
            return $this->redirect('/super/partner_verwalten');
        } else {
            $this->Flash->error(__('Partner wurde nicht gelöscht.'));
            return $this->redirect($this->referer());
        }
    }

    /**
     * Methode zum Bearbeiten eines Partners
     *
     * @param null $id
     */
    public function partnersEdit($id = null)
    {
        $this->loadModel('Partners');
        $this->loadModel('PartnersZipcodes');
        $this->loadModel('ValidZipcodes');
        if (!$id) {
            $partner = $this->Partners->newEntity($this->request->data);
            $this->set('title', __('Partner anlegen'));
        } else {
            $partner = $this->Partners->find()->where(['Partners.id' => $id])->first();
            $this->set('title', __('Partner bearbeiten'));
        }

        if ($this->request->is(['post', 'put', 'patch'])) {

            $patched_partner = $this->Partners->patchEntity($partner, $this->request->data);
            if ($saved_partner = $this->Partners->save($patched_partner)) {

                $partner_zipcodes = $this->PartnersZipcodes->find()->where(['PartnersZipcodes.partner_id' => $saved_partner->id]);
                foreach ($partner_zipcodes as $partner_zipcode) {
                    $this->PartnersZipcodes->delete($partner_zipcode);
                }

                foreach ($this->request->data['zipcodes'] as $zipcode) {
                    $valid_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => $zipcode])->first();
                    $new_partner_zipcode_entity_array = [
                        'partner_id' => $saved_partner->id,
                        'valid_zipcode_id' => $valid_zipcode->id
                    ];
                    $new_partner_zipcode_entity = $this->PartnersZipcodes->newEntity($new_partner_zipcode_entity_array);
                    $this->PartnersZipcodes->save($new_partner_zipcode_entity);
                }
                $this->Flash->success(__('Partner erfolgreich gespeichert.'));
                return $this->redirect('/super/partner_verwalten');
            } else {
                $this->Flash->error(__('Partner wurde nicht gespeichert.'));
                return $this->redirect($this->referer());
            }
        }

        $zipcodes = $this->ValidZipcodes->find()
            ->select(['ValidZipcodes.zipcode', 'ValidZipcodes.city']);

        $this->set('zipcodes', $zipcodes);

        $this->set('partner', $partner);

        $partner_zipcodes = $this->PartnersZipcodes->find()->where(['PartnersZipcodes.partner_id' => $partner->id]);

        $partner_zipcodes_array = array();
        foreach ($partner_zipcodes as $partner_zipcode) {
            $zipcode_find = $this->ValidZipcodes->find()->where(['ValidZipcodes.id' => $partner_zipcode->valid_zipcode_id])->first();
            if ($zipcode_find) {
                if (isset($partner_zipcodes_array[$zipcode_find->zipcode])) {
                    continue;
                }

                $partner_zipcodes_array[$zipcode_find->zipcode] = $zipcode_find->zipcode;
            }
        }
        $this->set('partner_zipcodes_array', $partner_zipcodes_array);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->set('title', 'Super-Administrator Bereich');

        $this->loadModel('Supports');

        $users = $this->Users->find()->where(['Users.role' != 'super'])->all();
        $users_array = array();

        foreach ($users as $user) {
            $users_array[$user->id] = $user->firstname . " " . $user->lastname;
        }

        $this->set('users_array', $users_array);

        $supports = $this->Supports->find()->order(['Supports.created' => 'DESC']);
        $this->set('supports', $supports);
    }

    /**
     * Methode zum Auflisten aller Nutzer
     */
    public function usersIndex() {
        $this->set('title', 'Nutzer verwalten');

        $users = $this->Users->find()->where(['Users.role !=' => 'super'])->andWhere(['Users.role !=' => 'no_account'])->order(['Users.created' => 'DESC'])->all();

        $this->set('users', $users);
    }

    /**
     * Methode zum Bearbeiten eines Nutzers
     *
     * @param $id
     * @return \Cake\Network\Response|null
     */
    public function usersEdit($id) {
        $this->set('title', 'Nutzer bearbeiten');

        $this->loadModel('AdminsZipcodes');
        $this->loadModel('BrokersZipcodes');
        $this->loadModel('ValidZipcodes');

        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($user->role != $this->request->data['role']) {
                switch($user->role) {
                    case "admin":
                        $old_admin_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.admin_id' => $user->id])->all();
                        foreach ($old_admin_zipcodes as $old_admin_zipcode) {
                            $this->AdminsZipcodes->delete($old_admin_zipcode);
                        }
                    break;
                    case "broker":
                        $old_broker_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id])->all();
                        foreach ($old_broker_zipcodes as $old_broker_zipcode) {
                            $this->BrokersZipcodes->delete($old_broker_zipcode);
                        }
                    break;
                }
                if (isset($this->request->data['related_zipcodes_ids']) && $this->request->data['related_zipcodes_ids']!='') {
                    foreach ($this->request->data['related_zipcodes_ids'] as $related_zipcodes_id) {

                        $new_related_zipcode_entity = [
                            $this->request->data['role'] . '_id' => $user->id,
                            'valid_zipcodes_id' => $related_zipcodes_id
                        ];
                        switch($this->request->data['role']) {
                            case "admin":
                                $new_related_zipcode = $this->AdminsZipcodes->newEntity($new_related_zipcode_entity);
                                $this->AdminsZipcodes->save($new_related_zipcode);
                                break;
                            case "broker":
                                $new_related_zipcode = $this->BrokersZipcodes->newEntity($new_related_zipcode_entity);
                                $this->BrokersZipcodes->save($new_related_zipcode);
                                break;
                        }
                    }
                }
            } else {
                switch ($user->role) {
                    case "admin":
                        $new_related_zipcodes_ids = array();
                        if (isset($this->request->data['related_zipcodes_ids']) && $this->request->data['related_zipcodes_ids']!='') {
                            foreach ($this->request->data['related_zipcodes_ids'] as $related_zipcodes_id) {
                                $new_related_zipcodes_ids[] = $related_zipcodes_id;
                                $related_zipcode_exists = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.admin_id' => $user->id, 'AdminsZipcodes.valid_zipcodes_id' => $related_zipcodes_id])->first();
                                if ($related_zipcode_exists) {
                                    continue;
                                }
                                $new_related_zipcode_entity = [
                                    'admin_id' => $user->id,
                                    'valid_zipcodes_id' => $related_zipcodes_id
                                ];
                                $new_related_zipcode = $this->AdminsZipcodes->newEntity($new_related_zipcode_entity);
                                $this->AdminsZipcodes->save($new_related_zipcode);
                            }

                            if (sizeof($new_related_zipcodes_ids)>0) {
                                $delete_admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.admin_id' => $user->id, 'AdminsZipcodes.valid_zipcodes_id NOT IN' => $new_related_zipcodes_ids])->all();
                            } else {
                                $delete_admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.admin_id' => $user->id])->all();
                            }

                            if ($delete_admins_zipcodes) {
                                foreach ($delete_admins_zipcodes as $delete_admins_zipcode) {
                                    $this->AdminsZipcodes->delete($delete_admins_zipcode);
                                }
                            }
                        } else {
                            $delete_admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.admin_id' => $user->id])->all();
                            if ($delete_admins_zipcodes) {
                                foreach ($delete_admins_zipcodes as $delete_admins_zipcode) {
                                    $this->AdminsZipcodes->delete($delete_admins_zipcode);
                                }
                            }
                        }
                        break;
                    case "broker":
                        $new_related_zipcodes_ids = array();
                        if (isset($this->request->data['related_zipcodes_ids']) && $this->request->data['related_zipcodes_ids']!='') {
                            foreach ($this->request->data['related_zipcodes_ids'] as $related_zipcodes_id) {
                                $new_related_zipcodes_ids[] = $related_zipcodes_id;
                                $related_zipcode_exists = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id, 'BrokersZipcodes.valid_zipcodes_id' => $related_zipcodes_id])->first();
                                if ($related_zipcode_exists) {
                                    continue;
                                }
                                $new_related_zipcode_entity = [
                                    'broker_id' => $user->id,
                                    'valid_zipcodes_id' => $related_zipcodes_id
                                ];
                                $new_related_zipcode = $this->BrokersZipcodes->newEntity($new_related_zipcode_entity);
                                $this->BrokersZipcodes->save($new_related_zipcode);
                            }

                            if (sizeof($new_related_zipcodes_ids)>0) {
                                $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id, 'BrokersZipcodes.valid_zipcodes_id NOT IN' => $new_related_zipcodes_ids])->all();
                            } else {
                                $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id])->all();
                            }

                            if ($delete_brokers_zipcodes) {
                                foreach ($delete_brokers_zipcodes as $delete_brokers_zipcode) {
                                    $this->BrokersZipcodes->delete($delete_brokers_zipcode);
                                }
                            }

                        } else {
                            $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id])->all();
                            if ($delete_brokers_zipcodes) {
                                foreach ($delete_brokers_zipcodes as $delete_brokers_zipcode) {
                                    $this->BrokersZipcodes->delete($delete_brokers_zipcode);
                                }
                            }
                        }
                        break;
                }
            }

            $patched_user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($patched_user)) {
                $this->Flash->success(__('Nutzer erfolgreich gespeichert.'));
            } else {
                $this->Flash->error(__('Nutzer wurde nicht gespeichert.'));
            }
            return $this->redirect('/super/nutzer_verwalten');
        }

        if ($user->role == "admin" || $user->role == "broker") {
            $related_zipcodes_ids = array();
            switch ($user->role) {
                case "admin":
                    $admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.admin_id' => $user->id])->all();
                    if ($admins_zipcodes) {
                        foreach ($admins_zipcodes as $admins_zipcode) {
                            $related_zipcodes_ids[] = $admins_zipcode->valid_zipcodes_id;
                        }
                    }
                    break;
                case "broker":
                    $brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id])->all();
                    if ($brokers_zipcodes) {
                        foreach ($brokers_zipcodes as $brokers_zipcode) {
                            $related_zipcodes_ids[] = $brokers_zipcode->valid_zipcodes_id;
                        }
                    }
                    break;
            }
            $user->related_zipcodes_ids = $related_zipcodes_ids;
        }

        $valid_zipcodes = $this->ValidZipcodes->find()->all();
        $valid_zipcodes_array = array();
        foreach ($valid_zipcodes as $valid_zipcode) {
            $valid_zipcodes_array[$valid_zipcode->id] = $valid_zipcode->zipcode . " | " . $valid_zipcode->city;
        }
        $this->set('valid_zipcodes', $valid_zipcodes_array);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Activate method
     *
     * @param $id
     * @param $case
     * @return \Cake\Network\Response|null
     */
    public function activate($id) {
        $broker = $this->Users->get($id);

        $patched_broker = $this->Users->patchEntity($broker, ['active' => 1]);

        $this->loadModel('Configurations');
        $configuration = $this->Configurations->find()->where(['Configurations.id' => 1])->first();

        $token = $broker->token;

        if (strlen($token)==0) {
            $token = "XXXX";
            $patched_broker = $this->Users->patchEntity($broker, ['token' => $token]);
        }

        $login_link = "XXXX";

        $this->loadModel('EmailTemplates');
        $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'bewerber_freigeschaltet'])->first();

        $title = isset($broker->title) && $this->request->data['title'] == 'frau' ? "Frau" : "Herr";

        $mail_content = $mail_template->template_content;
        $mail_content = str_replace('{{title}}', $title, $mail_content);
        $mail_content = str_replace('{{firstname}}', $broker->firstname, $mail_content);
        $mail_content = str_replace('{{lastname}}', $broker->lastname, $mail_content);
        $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
        $mail_content = str_replace('{{login_link}}', $login_link, $mail_content);

        if ($this->Users->save($patched_broker)) {
            $email = new Email('default');
            $email->helpers(array('Html', 'Text'));
            $email->viewVars(
                array(
                    'mail_content' => $mail_content
                )
            );
            $email->template('default');
            $email->emailFormat('text');
            $email->from(array($configuration->from_mail => $configuration->from_big_title));
            $email->to(array($broker->email => $broker->firstname . " " . $broker->lastname));
            $email->subject($mail_template->template_subject);
            if ($email->send()) {
                $this->loadModel('EmailLogs');
                $new_email_log_entity = [
                    'send' => 1,
                    'subject' => $mail_template->template_subject,
                    'content' => $mail_content,
                    'recipient_name' => $broker->firstname . " " . $broker->lastname,
                    'recipient_email' => $broker->email,
                    'sender_name' => $configuration->from_big_title,
                    'sender_email' => $configuration->from_mail
                ];
                $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                $this->EmailLogs->save($new_email_log);
                $this->Flash->success(__('Spendenvermittler erfolgreich aktiviert.'));
            }
        } else {
            $this->Flash->error(__('Spendenvermittler wurde nicht aktiviert.'));
        }
        return $this->redirect('/super/nutzer_verwalten');
    }

    /**
     * Deactivate method
     *
     * @param $id
     * @param $case
     * @return \Cake\Network\Response|null
     */
    public function deactivate($id, $case) {
        $broker = $this->Users->get($id);

        if ($broker->applied == 1) {
            $patched_broker = $this->Users->patchEntity($broker, ['active' => 1, 'applied' => 0, 'role' => 'token_account', 'broker_text' => '']);
        } else {
            $patched_broker = $this->Users->patchEntity($broker, ['active' => 0]);
        }

        if ($this->Users->save($patched_broker)) {
            $this->Flash->success(__('Nutzer erfolgreich deaktiviert.'));
        } else {
            $this->Flash->error(__('Nutzer wurde nicht deaktiviert.'));
        }
        return $this->redirect('/super/nutzer_verwalten');
    }

    /**
     * Messages method
     */
    public function messages() {
        $this->set('title', 'Meldungen');

        $this->loadModel('Messages');

        $messages = $this->Messages->find()->all();

        $this->set('messages', $messages);
    }

    /**
     * EditMessage method
     *
     * @param $id
     */
    public function messagesEdit($id) {
        $this->set('title', 'Meldung bearbeiten');
        $this->loadModel('Messages');
        $message = $this->Messages->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $patch_message = $this->Messages->patchEntity($message, $this->request->data);
            if ($this->Messages->save($patch_message)) {
                $this->Flash->success(__('Meldung erfolgreich gespeichert.'));
                $this->redirect('/super/meldungen_verwalten');
            } else {
                $this->Flash->error(__('Meldung nicht erfolgreich.'));
            }
        }

        $this->set('message', $message);
    }

    /**
     * Methode zum Bearbeiten der Konfiguration
     */
    public function configurationEdit() {
        $this->set('title', 'Konfiguration bearbeiten');

        $this->loadModel('Configurations');
        $configuration = $this->Configurations->get(1);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $patched_configuration = $this->Configurations->patchEntity($configuration, $this->request->data);
            if ($this->Configurations->save($patched_configuration)) {
                $this->Flash->success(__('Konfiguration erfolgreich gespeichert.'));
            } else {
                $this->Flash->error(__('Konfiguration wurde nicht gespeichert.'));
            }

        }

        $this->set(compact('configuration'));
        $this->set('_serialize', ['configuration']);

    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //$this->request->allowMethod(['post', 'delete']);

        $user = $this->Users->get($id);
        if ($user->applied == 1) {
            $patch_user = $this->Users->patchEntity($user, ['active' => 1, 'applied' => 0, 'role' => 'token_account', 'broker_text' => '']);
            if ($this->Users->save($patch_user)) {
                $this->Flash->success(__('Der Vermittler wurde erfolgreich als Spender zurückgesetzt.'));
            } else {
                $this->Flash->success(__('Vermittler wurde nicht als Spender zurückgesetzt.'));
            }
        } else {

            $this->loadModel('Addresses');
            $address = $this->Addresses->find()->where(['Addresses.user_id' => $id])->first();
            if ($address) {
                $this->Addresses->delete($address);
            }
            if ($this->Users->delete($user)) {
                $donations = $this->Donations->find()->where(['Donations.user_id' => $id]);
                if ($donations) {
                    foreach ($donations as $donation) {
                        $patched_donation = $this->Donations->patchEntity($donation, ['active' => 0]);
                        $this->Donations->save($patched_donation);
                    }
                }
                $searches = $this->Searches->find()->where(['Searches.user_id' => $id]);
                if ($searches) {
                    foreach ($searches as $search) {
                        $patched_search = $this->Searches->patchEntity($search, ['active' => 0]);
                        $this->Searches->save($patched_search);
                    }
                }

                $this->Flash->success(__('Der Nutzer wurde gelöscht.'));
            } else {
                $this->Flash->error(__('Der Nutzer wurde nicht gelöscht.'));
            }
        }
        return $this->redirect('/super/nutzer_verwalten');
    }

    /**
     * Details method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function details($id = null)
    {
        $this->set('title', 'Details');
        $this->loadModel('Addresses');
        $user = $this->Users->get($id, ['contain' => ['Addresses']]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }
}
