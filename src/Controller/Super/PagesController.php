<?php
namespace App\Controller\Super;

use App\Controller\AppController;

/**
 * Pages Controller
 *
 * @property \App\Model\Table\PagesTable $Pages
 */
class PagesController extends AppController
{

    /**
     * Methode zur Darstellung aller Seiten
     *
     * @return void
     */
    public function index()
    {
        $this->set('title', 'Seiten verwalten');
        $this->set('pages', $this->paginate($this->Pages));
        $this->set('_serialize', ['pages']);
    }

    /**
     * Methode zum Bearbeiten einer Seite
     *
     * @param string|null $id Page id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Seite bearbeiten');
        $page = $this->Pages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $page = $this->Pages->patchEntity($page, $this->request->data);
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('Die Seite wurde erfolgreich gespeichert.'));
                return $this->redirect('/super/seiten_verwalten');
            } else {
                $this->Flash->error(__('Die Seite wurde nicht gespeichert.'));
            }
        }
        $this->set(compact('page'));
        $this->set('_serialize', ['page']);
    }

}
