<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Event\Event;
use Cake\ORM\Query;
use App\Model\Entity\Keyword;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Log\Log;
use App\Controller\Component\FileUploadComponent;

/**
 * Helpers Controller
 *
 */
class HelpersController extends AppController
{
    /**
     * @var array
     */
    public $components = ['FileUpload', 'Helper'];

    /**
     * Methode die die Zugriffsrechte des aktuellen Nutzers überprüft
     *
     * @param null $id
     * @return bool
     */
    private function __checkAccess($id = null) {

        if ($this->Auth->user('id')>0 && ($this->Auth->user('role') == 'token_account' || $this->Auth->user('role') == 'broker' || $this->Auth->user('role') == 'admin')) {
            return true;
        } else if (isset($id) && $id>0) {
            $this->loadModel('Searches');
            $search = $this->Searches->find()->where(['Searches.id' => $id])->first();
            if ($search && $search->user_id == $this->Auth->user('id')) {
                return true;
            }
            $this->loadModel('Donations');
            $donation = $this->Donations->find()->where(['Donations.id' => $id])->first();
            if ($donation && $donation->user_id == $this->Auth->user('id')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['branchChange', 'items', 'item', 'getPageContent', 'search', 'support', 'validateChiffre', 'validateInterest', 'interest', 'institutions', 'validateZip', 'validatePinrequest', 'pinRequest', 'filterText', 'validateNew', 'validateRegister', 'validateRequest', 'validateReport', 'keywordDonations', 'keywordSearches', 'checkKeywords', 'writeSessionKeywords', 'prepareDonation', 'request', 'getCityName', 'insertDeletedWord', 'sitemap']);
    }

    /**
     * Methode die ein gelöschtes Wort in einer Blacklist einfügt
     *
     * @param $word
     */
    public function insertDeletedWord($word) {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->is(['ajax'])) {
            $this->loadModel('DeletedWords');
            $exists = $this->DeletedWords->find()->where(['DeletedWords.keyword' => $word])->first();

            if ($exists) {
                $new_count = ($exists->deleted_count)+1;
                $patched_deleted_word = $this->DeletedWords->patchEntity($exists, ['deleted_count' => $new_count]);
                $this->DeletedWords->save($patched_deleted_word);
            } else {

                $new_deleted_word_entity = [
                    'keyword' => $word,
                    'deleted_count' => 1
                ];
                $new_deleted_word = $this->DeletedWords->newEntity($new_deleted_word_entity);
                $this->DeletedWords->save($new_deleted_word);
            }

        }
    }

    /**
     * Methode die eine gegebene Postleitzahl auf Validität (Gültigkeit) überprüft
     */
    public function validateZip() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $this->loadModel('Messages');

        if ($this->request->is(['ajax'])) {
            $zipcode = $this->Helper->__returnZipcode($this->request->data['zip']);

            if ($zipcode) {
                $city = $this->Helper->__returnCity($zipcode->city_id);
                $response = [
                    'status' => 'success',
                    'city' => $city->name
                ];
            } else {
                $wrong_zipcode = $this->Messages->find()->where(['Messages.name' => 'wrong_zip'])->first();
                $wrong_zipcode_alert = $this->Messages->find()->where(['Messages.name' => 'wrong_zip', 'Messages.type' => 'alert'])->first();
                $response = [
                    'status' => 'failed',
                    'message' => $wrong_zipcode->text,
                    'alert_message' => $wrong_zipcode_alert->text,
                ];
            }

            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode die die Stadt zu einer angefragten Postleitzahl liefert
     */
    public function getCityName() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $this->loadModel('Zipcodes');
        $this->loadModel('Cities');
        $this->loadModel('Messages');

        if ($this->request->is(['ajax'])) {
            $zipcode = $this->Zipcodes->find()->where(['Zipcodes.zipcode' => $this->request->data['zip']])->first();
            if ($zipcode) {
                $city = $this->Cities->find()->where(['Cities.id' => $zipcode->city_id])->first();

                if ($city) {
                    $response = [
                        'status' => 'success',
                        'city' => $city->name
                    ];
                }
            } else {
                $not_exists_zipcode = $this->Messages->find()->where(['Messages.name' => 'not_exists_zip'])->first();
                $response = [
                    'status' => 'failed',
                    'message' => $not_exists_zipcode->text
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * @param $address_id
     */
    public function getSelectedAddress($address_id) {
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');

        $address = $this->Helper->__returnAddress($address_id);

        $user = $this->Helper->__returnUser($this->Auth->user('id'));
        if (strlen($address->phone)==0) {
            $address->phone = $user->phone;
        }

        if (strlen($address->email)==0) {
            $address->email = $user->email;
        }
        if ($address) {
            $response = [
                'status' => 'success',
                'address' => $address
            ];
        } else {
            $response = [
                'status' => 'failed'
            ];
        }

        $this->set('response', $response);
        $this->render('response');
    }

    /**
     * Methode zur Überprüfung einer PIN-Anfrage
     *
     * @param null $internal
     * @param array|null $internal_request_data
     * @return bool
     */
    public function validatePinrequest($internal = null, array $internal_request_data = null) {
        $this->loadModel('Messages');
        if (isset($internal) && $internal = "internal_request") {
            $this->request->data = $internal_request_data;
        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }

        $messages = array();

        /**
         * Falls E-Mail nicht gesetzt ist
         *
         * TODO: Überprüfen, ob E-Mail-Adresse valide ist
         */
        if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_email', 'email');
        }

        /**
         * Falls Name nicht gesetzt
         */
        if (isset($this->request->data['name']) && strlen($this->request->data['name']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_name', 'name');
        }

        /**
         * Falls Postleitzahl nicht gesetzt ist
         */
        if (isset($this->request->data['zip']) && strlen($this->request->data['zip']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_zip', 'zip');
        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else {
            if (sizeof($messages)>0) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode zur Überprüfung einer Anfrage
     *
     * @param null $internal
     * @param array|null $internal_request_data
     * @return bool
     */
    public function validateRequest($internal = null, array $internal_request_data = null) {
        $this->loadModel('Messages');
        if (isset($internal) && $internal = "internal_request") {
            $this->request->data = $internal_request_data;
        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }

        $messages = array();

        /**
         * Falls Name nicht gesetzt ist
         */
        if (isset($this->request->data['sender']) && strlen($this->request->data['sender']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_sender', 'sender');
        }

        /**
         * Falls E-Mail-Adresse nicht gesetzt ist
         */
        if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_email', 'email');
        }

        /**
         * Falls Nachricht nicht gesetzt ist
         */
        if (isset($this->request->data['message']) && strlen($this->request->data['message']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_message', 'message');
        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else if ($this->request->is(['patch', 'post', 'put', 'ajax'])) {
            if (sizeof($messages)>1) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode zur Überprüfung einer Fehlermeldung
     *
     * @param null $internal
     * @param array|null $internal_request_data
     * @return bool
     */
    public function validateReport($internal = null, array $internal_request_data = null) {
        $this->loadModel('Messages');
        if (isset($internal) && $internal = "internal_request") {
            $this->request->data = $internal_request_data;
        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }

        $messages = array();

        /**
         * Falls Name nicht gesetzt ist
         */
        if (isset($this->request->data['sender']) && strlen($this->request->data['sender']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_sender', 'sender');
        }

        /**
         * Falls E-Mail-Adresse nicht gesetzt ist
         */
        if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_email', 'email');
        }

        /**
         * Falls Begründung nicht angegeben wurde
         */
        if (isset($this->request->data['reason']) && strlen($this->request->data['reason']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_reason', 'reason');
        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else if ($this->request->is(['patch', 'post', 'put', 'ajax'])) {
            if (sizeof($messages)>0) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode zur Überprüfung einer Spendenabgabe
     *
     * @param null $internal
     * @param array|null $internal_request_data
     */
    public function validateNew($internal = null, array $internal_request_data = null)
    {
        $this->loadModel('Messages');
        if (isset($internal) && $internal = "internal_request") {
            $this->request->data = $internal_request_data;
        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }

        $messages = array();

        /**
         * Falls Vorname nicht gesetzt ist
         */
        if (isset($this->request->data['firstname']) && strlen($this->request->data['firstname'])==0) {
            $messages[] = $this->Helper->__returnMessage('empty_firstname', 'firstname');
        }

        /**
         * Falls Nachname nicht gesetzt ist
         */
        if (isset($this->request->data['lastname']) && strlen($this->request->data['lastname'])==0) {
            $messages[] = $this->Helper->__returnMessage('empty_lastname', 'lastname');
        }

        /**
         * Falls Titel nicht gesetzt ist
         */
        if (isset($this->request->data['title']) && strlen($this->request->data['title']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_title', 'title');
        }

        /**
         * Falls Postleitzahl nicht gesetzt ist
         */
        if (isset($this->request->data['zip']) && strlen($this->request->data['zip']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_zip', 'zip');
        }

        /**
         * Falls Beschreibung nicht gesetzt ist
         */
        if (isset($this->request->data['description']) && strlen($this->request->data['description']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_description', 'description');
        }

        /**
         * Falls Schlagwörter nicht gesetzt sind
         */
        if (isset($this->request->data['keywords']) && strlen($this->request->data['keywords']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_keywords', 'keywords');
            /**
             * Falls Schlagwörter nicht ausreichend sind
             */
        } else if (isset($this->request->data['keywords']) && strlen($this->request->data['keywords']) > 0) {
            $count_keywords = substr_count(trim($this->request->data['keywords']), ',');
            if ($count_keywords < 2) {
                $messages[] = $this->Helper->__returnMessage('more_keywords', 'keywords');
            }
        }

        /**
         * Falls Dateien hochgeladen werden
         */
        if (isset($this->request->data['files']) && $this->request->data['files']>=1) {
            /**
             * Falls Rechte für Dateien nicht gesetzt
             */
            if ($this->request->data['rights'] == 'false') {
                $messages[] = $this->Helper->__returnMessage('confirm_rights', 'rights');
            }
        }

        /**
         * Falls Captcha nicht gesetzt ist
         */
        if (!in_array($_SERVER['HTTP_HOST'], $this->request->session()->read('captcha_free_hosts'))) {
            if (isset($this->request->data['g-recaptcha-response']) && strlen($this->request->data['g-recaptcha-response']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_captcha', 'captcha');
            }
        }

        /**
         * Falls interne Anfrage (nicht über das Frontend)
         */
        if (isset($internal) && $internal == "internal_request") {
            if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                $this->request->data['email'] = trim($this->request->data['email']);

                $valid = true;
                if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    $valid = false;
                }

                /**
                 * Falls Benutzer eingeloggt
                 */
                if ($this->Auth->user('id') > 0) {
                    $email_exists_db = false;
                } else {
                    $email_exists_db = $this->Helper->__returnEmailUser($this->request->data['email']);

                    /**
                     * Falls E-Mail-Wiederholung nicht gesetzt wurde
                     */
                    if (isset($this->request->data['email_confirm']) && strlen($this->request->data['email_confirm']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('no_email_confirm', 'email_confirm');
                    /**
                     * Falls E-Mail und E-Mail-Wiederholung nicht identisch
                     */
                    } else if (isset($this->request->data['email_confirm']) && strlen($this->request->data['email_confirm']) > 0 && $this->request->data['email'] != $this->request->data['email_confirm']) {
                        $messages[] = $this->Helper->__returnMessage('not_similiar_emails', 'email_confirm');
                    }
                }

                /**
                 * Falls E-Mail-Adresse nicht gültig
                 */
                if (!$valid) {
                    $messages[] = $this->Helper->__returnMessage('wrong_email', 'email');
                /**
                 * Falls E-Mail-Adresse bereits in Datenbank existiert
                 */
                } else if ($email_exists_db) {
                    $messages[] = $this->Helper->__returnMessage('email_exists', 'email');
                }
            }
        } else {
            /**
             * Falls keine Bilder angehängt wurden
             */
            if (isset($this->request->data['uploads']) && $this->request->data['uploads'][0]=='' && isset($this->request->data['files']) && $this->request->data['files']==0) {
                $messages[] = $this->Helper->__returnMessage('no_images', 'images');
            }

            /**
             * Falls E-Mail-Adresse angegeben
             */
            if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                $this->request->data['email'] = trim($this->request->data['email']);

                $valid = true;
                /**
                 * Prüft, ob eine E-Mail-Adresse gültig ist
                 */
                if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    $valid = false;
                }
                /*
                 * TODO: verifyEmailDomain - Funktion verbessern (GMX, Gmail etc werden als ungültig eingestuft)
                $email_domain_check = $this->Helper->__verifyEmailDomain($this->request->data['email']);

                if ($email_domain_check == 'invalid') {
                    $valid = false;
                } */

                /**
                 * Falls Benutzer eingeloggt
                 */
                if ($this->Auth->user('id') > 0) {
                    $email_exists_db = false;
                } else {
                    $email_exists_db = $this->Helper->__returnEmailUser($this->request->data['email']);

                    /**
                     * Falls E-Mail-Wiederholung nicht gesetzt wurde
                     */
                    if (isset($this->request->data['email_confirm']) && strlen($this->request->data['email_confirm']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('no_email_confirm', 'email_confirm');
                        /**
                         * Falls E-Mail und E-Mail-Wiederholung nicht identisch
                         */
                    } else if (isset($this->request->data['email_confirm']) && strlen($this->request->data['email_confirm']) > 0 && $this->request->data['email'] != $this->request->data['email_confirm']) {
                        $messages[] = $this->Helper->__returnMessage('not_similiar_emails', 'email_confirm');
                    }
                }

                /**
                 * Falls E-Mail-Adresse ungültig
                 */
                if (!$valid) {
                    $messages[] = $this->Helper->__returnMessage('wrong_email', 'email');
                }
                /**
                 * Falls E-Mail-Adresse bereits in Datenbank vorhanden
                 */
                if ($email_exists_db) {
                    $messages[] = $this->Helper->__returnMessage('email_exists', 'email');
                }
            /**
             * Falls E-Mail-Adresse niht gesetzt wurde
             */
            } else if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                $messages[] = $this->Helper->__returnMessage('no_email', 'email');

                /**
                 * Falls Telefonnummer nicht angegeben
                 */
                if (isset($this->request->data['phone']) && strlen($this->request->data['phone'])==0) {
                    $messages[] = $this->Helper->__returnMessage('empty_phone', 'phone');
                } else if (isset($this->request->data['phone']) && strlen($this->request->data['phone'])>0) {
                    // TODO: Eingabe überprüfen, ob gültige Handynummer
                    /*$phone = trim($this->request->data['phone']);
                    if (!(preg_match('/^\+[0-9]{0,3}[1-9] \([0-9]*[1-9]+\) ([0-9]| - )*[1-9]+$/', $phone))) {
                        $wrong_phone = $this->Messages->find()->where(['Messages.name' => 'wrong_phone'])->first();
                        $messages['phone']['text'] = $wrong_phone->text;
                        $messages['phone']['type'] = $wrong_phone->type;
                        $messages['phone']['name'] = $wrong_phone->name;
                    }*/
                }
            }

            /**
             * Falls Telefonnummer nicht angegeben
             */
            if (isset($this->request->data['phone']) && strlen($this->request->data['phone']) == 0) {

                /**
                 * Falls E-Mail-Adresse nicht gesetzt wurde (und Telefonnummer auch nicht gesetzt wurde)
                 */
                if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('no_email', 'email');
                    $messages[] = $this->Helper->__returnMessage('empty_phone', 'phone');
                }
            } else if (isset($this->request->data['phone']) && strlen($this->request->data['phone'])>0) {
                // TODO: Eingabe überprüfen, ob gültige Handynummer
                /*$phone = trim($this->request->data['phone']);
                $invalid_festnetz = !(preg_match('/^\+[0-9]{0,3}[1-9] \([0-9]*[1-9]+\) ([0-9]| - )*[1-9]+$/', $phone));
                $invalid_mobil =
                if () {
                    $wrong_phone = $this->Messages->find()->where(['Messages.name' => 'wrong_phone'])->first();
                    $messages['phone']['text'] = $wrong_phone->text;
                    $messages['phone']['type'] = $wrong_phone->type;
                    $messages['phone']['name'] = $wrong_phone->name;
                } */
            }

            /**
             * Falls Benutzer eingeloggt
             */
            if (!($this->Auth->user('id')>0)) {
                if (isset($this->request->data['phone']) && strlen($this->request->data['phone']) == 0 && isset($this->request->data['email']) && strlen($this->request->data['email'])==0) {
                    $messages[] = $this->Helper->__returnMessage('empty_phone', 'phone');
                }
            }
        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else if ($this->request->is(['patch', 'post', 'put', 'ajax'])) {

            $check_zip_exists = true;
            /**
             * Falls Postleitzahl angegeben -> prüfen ob innerhalb des gültigen Postleitzahlenbereichs
             * TODO: Aktuelle Branch berücksichtigen
             */
            if (isset($this->request->data['zip']) && strlen($this->request->data['zip']) > 0) {
                $this->loadModel('ValidZipcodes');
                $zip_exists = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => trim($this->request->data['zip'])])->first();
                if (!$zip_exists) {
                    $check_zip_exists = false;
                }
            }

            if (sizeof($messages)>0) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];

                /**
                 * Falls Postleitzahl außerhalb des gültigen Postleitzahlenbereichs
                 */
                if (!$check_zip_exists) {
                    /**
                     * E-Mail-Template holen für Bestätigung einer Spende außerhalb des gültigen Postleitzahlenbereichs
                     */
                    $mail_template = $this->Helper->__returnEmailTemplate('email_postleitzahl_bestaetigen');

                    /**
                     * Konfiguration holen
                     */
                    $configuration = $this->Helper->__returnConfiguration();

                    $donation_type = "/sachspende/";
                    if (isset($this->request->data['datetime_range_1']) && strlen($this->request->data['datetime_range_1'])>0) {
                        $donation_type = "/zeitspende/";
                    }

                    /**
                     * Symbole aus dem URL-Titel entfernen
                     */
                    $donation_url_title = str_replace(" ", "-", strtolower($this->Helper->replaceChars($this->request->data['title'])));
                    $donation_url_title = str_replace("?", "_", $donation_url_title);
                    $donation_url_title = str_replace("/", "_", $donation_url_title);
                    $donation_url_title = str_replace("+", "_", $donation_url_title);
                    $donation_url_title = str_replace("|", "_", $donation_url_title);
                    $donation_url_title = str_replace("'", "_", $donation_url_title);
                    $donation_url_title = str_replace("^", "_", $donation_url_title);
                    $donation_url_title = str_replace("`", "_", $donation_url_title);
                    $donation_url_title = str_replace("´", "_", $donation_url_title);
                    $donation_url_title = str_replace(":", "_", $donation_url_title);
                    $donation_url_title = str_replace(".", "_", $donation_url_title);
                    $donation_url_title = str_replace(",", "_", $donation_url_title);
                    $donation_url_title = str_replace(";", "_", $donation_url_title);
                    $donation_link = $configuration->host . $donation_type . $donation_url_title;
                    $mail_template->template_content = str_replace("{{donation_link}}", $donation_link, $mail_template->template_content);

                    $response = [
                        'mail_subject' => $mail_template->template_subject,
                        'mail_content' => $mail_template->template_content,
                        'status' => 'failed',
                        'messages' => $messages,
                        'invalid_zip' => true
                    ];
                }
            /**
             * Falls Postleitzahl nicht innerhalb des gültigen Bereichs
             */
            } else if (!$check_zip_exists) {
                /**
                 * E-Mail-Template holen für Bestätigung einer Spende außerhalb des gültigen Postleitzahlenbereichs
                 */
                $mail_template = $this->Helper->__returnEmailTemplate('email_postleitzahl_bestaetigen');

                /**
                 * Konfiguration holen
                 */
                $configuration = $this->Helper->__returnConfiguration();

                $donation_type = "/sachspende/";
                if (isset($this->request->data['datetime_range_1']) && strlen($this->request->data['datetime_range_1'])>0) {
                    $donation_type = "/zeitspende/";
                }

                /**
                 * Symbole aus dem URL-Titel entfernen
                 */
                $donation_url_title = str_replace(" ", "-", strtolower($this->Helper->replaceChars($this->request->data['title'])));
                $donation_url_title = str_replace("?", "_", $donation_url_title);
                $donation_url_title = str_replace("/", "_", $donation_url_title);
                $donation_url_title = str_replace("+", "_", $donation_url_title);
                $donation_url_title = str_replace("|", "_", $donation_url_title);
                $donation_url_title = str_replace("'", "_", $donation_url_title);
                $donation_url_title = str_replace("^", "_", $donation_url_title);
                $donation_url_title = str_replace("`", "_", $donation_url_title);
                $donation_url_title = str_replace("´", "_", $donation_url_title);
                $donation_url_title = str_replace(":", "_", $donation_url_title);
                $donation_url_title = str_replace(".", "_", $donation_url_title);
                $donation_url_title = str_replace(",", "_", $donation_url_title);
                $donation_url_title = str_replace(";", "_", $donation_url_title);
                $donation_link = $configuration->host . $donation_type . $donation_url_title;
                $mail_template->template_content = str_replace("{{donation_link}}", $donation_link, $mail_template->template_content);

                $response = [
                    'mail_subject' => $mail_template->template_subject,
                    'mail_content' => $mail_template->template_content,
                    'status' => 'possible',
                    'messages' => $messages
                ];
            } else if ($check_zip_exists) {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }

    }

    /**
     * Methode zur Überprüfung einer Registrierung
     *
     * @param null $internal
     * @param array|null $internal_request_data
     * @return bool
     */
    public function validateRegister($internal = null, array $internal_request_data = null) {
        if (isset($internal) && $internal = "internal_request") {
            $this->request->data = $internal_request_data;
        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }

        $messages = array();

        if (isset($this->request->data['broker_partner']) && strlen($this->request->data['broker_partner']) == 0) {
            $messages[] = $this->Helper->__returnMessage('empty_broker_partner', 'broker_partner');
        } else if (isset($this->request->data['broker_partner']) && strlen($this->request->data['broker_partner'])>0) {
            if (!is_numeric($this->request->data['broker_partner'])) {
                // TODO: Falls keinen Regionaladmin für Postleitzahl vorhanden / Postleitzahl nicht in ValidZipcodes -> Fehler
                $messages[] = $this->Helper->__returnMessage('wrong_broker_partner', 'broker_partner');
            }
        }

        /**
         * Falls Benutzer eingeloggtte
         */
        if ($this->Auth->user('id') > 0) {
            if (isset($this->request->data['broker_text']) && strlen($this->request->data['broker_text']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_broker_text', 'broker_text');
            } else if (isset($this->request->data['broker_text']) && strlen($this->request->data['broker_text']) < 64) {
                $messages[] = $this->Helper->__returnMessage('more_broker_text', 'broker_text');
            }

            /**
             * Falls AGB's nicht akzeptiert wurden
             */
            if (isset($this->request->data['agb']) && $this->request->data['agb'] == 'false') {
                $messages[] = $this->Helper->__returnMessage('not_accepted_agb', 'agb');
            }
        } else {
            /**
             * Falls kein Vorname angegeben wurde
             */
            if (isset($this->request->data['firstname']) && strlen($this->request->data['firstname']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_firstname', 'firstname');
            }

            /**
             * Falls kein Nachname angegeben wurde
             */
            if (isset($this->request->data['lastname']) && strlen($this->request->data['lastname']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_lastname', 'lastname');
            }

            /**
             * Falls keine Straße angegeben wurde
             */
            if (isset($this->request->data['street']) && strlen($this->request->data['street']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_street', 'street');
            }

            /**
             * Falls keine Postleitzahl angegeben wurde
             */
            if (isset($this->request->data['zip']) && strlen($this->request->data['zip']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_zip', 'zip');
                /**
                 * Falls Postleitzahl angegeben wurde
                 */
            } else if (isset($this->request->data['zip']) && strlen($this->request->data['zip']) > 0) {
                $this->loadModel('Zipcodes');
                $valid_zipcode = $this->Helper->__returnZipcode($this->request->data['zip']);
                /**
                 * Falls Postleitzahl außerhalb des gültigen Postleitzahlenbereichs
                 */
                if (!$valid_zipcode) {
                    $messages[] = $this->Helper->__returnMessage('not_exists_zip', 'zip');
                }
            }

            /**
             * Falls keine Telefonnummer angegeben wurde
             */
            if (isset($this->request->data['phone']) && strlen($this->request->data['phone']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_phone', 'phone');
            } else if (isset($this->request->data['phone']) && strlen($this->request->data['phone']) > 0) {
                // TODO: Eingabe überprüfen ob gültige Handynummer
                /*$phone = trim($this->request->data['phone']);
                if (!(preg_match('/^\+[0-9]{0,3}[1-9] \([0-9]*[1-9]+\) ([0-9]| - )*[1-9]+$/', $phone))) {
                    $wrong_phone = $this->Messages->find()->where(['Messages.name' => 'wrong_phone'])->first();
                    $messages['phone']['text'] = $wrong_phone->text;
                    $messages['phone']['type'] = $wrong_phone->type;
                    $messages['phone']['name'] = $wrong_phone->name;
                }*/
            }

            /**
             * Falls AGB's nicht akzeptiert wurden
             */
            if (isset($this->request->data['agb']) && $this->request->data['agb'] == 'false') {
                $messages[] = $this->Helper->__returnMessage('not_accepted_agb', 'agb');
            }

            /**
             * Captcha überprüfen
             */
            if (!in_array($_SERVER['HTTP_HOST'], $this->request->session()->read('captcha_free_hosts'))) {
                if (isset($this->request->data['g-recaptcha-response']) && strlen($this->request->data['g-recaptcha-response']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_captcha', 'captcha');
                }
            }

            /**
             * Falls interne Anfrage
             */
            if (isset($internal) && $internal == "internal_request") {
                if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                    $this->request->data['email'] = trim($this->request->data['email']);

                    $valid = true;
                    if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                        $valid = false;
                    }

                    /**
                     * Hole Benutzer anhand der E-Mail-Adresse
                     */
                    $exists = $this->Helper->__returnEmailuser($this->request->data['email']);

                    if ($exists) {
                        /**
                         * Falls E-Mail-Adresse schon in Datenbank vorhanden
                         */
                        $messages[] = $this->Helper->__returnMessage('exists_email', 'email');
                    } else if (!$valid) {
                        /**
                         * Falls E-Mail-Adresse nicht gültig
                         */
                        $messages[] = $this->Helper->__returnMessage('wrong_email', 'email');
                    } else {
                        if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) > 0) {
                            /**
                             * Falls E-Mail-Adresse und E-Mail-Wiederholung nicht identisch
                             */
                            if (trim($this->request->data['email']) != trim($this->request->data['email_repeat'])) {
                                $messages[] = $this->Helper->__returnMessage('not_similiar_emails', 'email_repeat');
                            }
                            /**
                             * Falls E-Mail-Wiederholung nicht angegeben wurde
                             */
                        } else if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                            $messages[] = $this->Helper->__returnMessage('no_email_confirm', 'email_repeat');
                        }
                    }
                    /**
                     * Falls E-Mail-Adresse nicht angegeben wurde
                     */
                } else if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_email', 'email', 'error');
                    /**
                     * Falls E-Mail-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('empty_email_repeat', 'email', 'error');
                    }

                }

                if (isset($this->request->data['password']) && strlen($this->request->data['password']) > 0) {
                    $this->request->data['password'] = trim($this->request->data['password']);

                    /**
                     * Falls Passwort-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('no_password_confirm', 'password_repeat');
                    } else if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) > 0) {
                        /**
                         * Falls Passwort und Passwort-Wiederholung nicht identisch
                         */
                        if (trim($this->request->data['password']) != trim($this->request->data['password_repeat'])) {
                            $messages[] = $this->Helper->__returnMessage('not_similiar_passwords', 'password_repeat');
                        }
                    }
                    /**
                     * Falls Passwort nicht angegeben wurde
                     */
                } else if (isset($this->request->data['password']) && strlen($this->request->data['password']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_password', 'password');
                    /**
                     * Falls Passwort-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('empty_password_repeat', 'password_repeat');
                    }
                }
            } else {
                /**
                 * Falls E-Mail-Adresse angegeben wurde
                 */
                if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                    $this->request->data['email'] = trim($this->request->data['email']);

                    $valid = true;
                    if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                        $valid = false;
                    }

                    /**
                     * Hole Benutzer anhand der E-Mail-Adresse
                     */
                    $exists = $this->Helper->__returnEmailUser($this->request->data['email']);

                    if ($exists) {
                        $messages[] = $this->Helper->__returnMessage('exists_email', 'email');
                    } else if (!$valid) {
                        /**
                         * Falls E-Mail-Adresse nicht gültig
                         */
                        $messages[] = $this->Helper->__returnMessage('wrong_email', 'email');
                    } else {
                        if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) > 0) {
                            /**
                             * Falls E-Mail-Adresse und E-Mail-Wiederholung nicht identisch
                             */
                            if (trim($this->request->data['email']) != trim($this->request->data['email_repeat'])) {
                                $messages[] = $this->Helper->__returnMessage('not_similiar_emails', 'email_repeat');
                            }
                            /**
                             * Falls E-Mail-Wiederholung nicht angegeben wurde
                             */
                        } else if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                            $messages[] = $this->Helper->__returnMessage('no_email_confirm', 'email_repeat');
                        }
                    }
                    /**
                     * Falls E-Mail-Adresse nicht angegeben wurde
                     */
                } else if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_email', 'email', 'error');
                    /**
                     * Falls E-Mail-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('empty_email_repeat', 'email_repeat', 'error');
                    }
                }

                if (isset($this->request->data['password']) && strlen($this->request->data['password']) > 0) {
                    $this->request->data['password'] = trim($this->request->data['password']);

                    /**
                     * Falls Passwort-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('no_password_confirm', 'password_repeat');
                    } else if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) > 0) {
                        /**
                         * Falls Passwort und Passwort-Wiederholung nicht identisch
                         */
                        if (trim($this->request->data['password']) != trim($this->request->data['password_repeat'])) {
                            $messages[] = $this->Helper->__returnMessage('not_similiar_passwords', 'password_repeat');
                        }
                    }
                    /**
                     * Falls Passwort nicht angegeben wurde
                     */
                } else if (isset($this->request->data['password']) && strlen($this->request->data['password']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_password', 'password');
                    /**
                     * Falls Passwort-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('empty_password_repeat', 'password_repeat');
                    }
                }
            }

            if (isset($this->request->data['broker_text']) && strlen($this->request->data['broker_text']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_broker_text', 'broker_text');
            } else if (isset($this->request->data['broker_text']) && strlen($this->request->data['broker_text']) < 64) {
                $messages[] = $this->Helper->__returnMessage('more_broker_text', 'broker_text');
            }
        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else if ($this->request->is(['patch', 'post', 'put', 'ajax'])) {
            if (sizeof($messages)>0) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode zur Überprüfung einer Bewerbung eines Spendenvermittlers
     *
     * @param null $internal
     * @param array|null $internal_request_data
     * @return bool
     */
    public function validateAdd($internal = null, array $internal_request_data = null) {
        $this->loadModel('Messages');
        if (isset($internal) && $internal = "internal_request") {
            $this->request->data = $internal_request_data;
        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }
        $messages = array();

        if ($this->Auth->user('id') > 0) {
            if (isset($this->request->data['broker_text']) && strlen($this->request->data['broker_text']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_broker_text', 'broker_text');
            } else if (isset($this->request->data['broker_text']) && strlen($this->request->data['broker_text']) < 64) {
                $messages[] = $this->Helper->__returnMessage('more_broker_text', 'broker_text');
            }
            /**
             * Falls AGB's nicht akzeptiert wurden
             */
            if (isset($this->request->data['agb']) && $this->request->data['agb'] == 'false') {
                $messages[] = $this->Helper->__returnMessage('not_accepted_agb', 'agb');
            }
        } else {
            /**
             * Falls Vorname nicht angegeben wurde
             */
            if (isset($this->request->data['firstname']) && strlen($this->request->data['firstname']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_firstname', 'firstname');
            }

            /**
             * Falls Nachname nicht angegeben wurde
             */
            if (isset($this->request->data['lastname']) && strlen($this->request->data['lastname']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_lastname', 'lastname');
            }

            /**
             * Falls Straße nicht angegeben wurde
             */
            if (isset($this->request->data['street']) && strlen($this->request->data['street']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_street', 'street');
            }

            /**
             * Falls Postleitzahl nicht angegeben wurde
             */
            if (isset($this->request->data['zip']) && strlen($this->request->data['zip']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_zip', 'zip');
            }

            /**
             * Falls Telefonnummer nicht angegeben wurde
             */
            if (isset($this->request->data['phone']) && strlen($this->request->data['phone']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_phone', 'phone');
            } else if (isset($this->request->data['phone']) && strlen($this->request->data['phone']) > 0) {
                // TODO: Eingabe überprüfen ob gültige Handynummer
                /*$phone = trim($this->request->data['phone']);
                if (!(preg_match('/^\+[0-9]{0,3}[1-9] \([0-9]*[1-9]+\) ([0-9]| - )*[1-9]+$/', $phone))) {
                    $wrong_phone = $this->Messages->find()->where(['Messages.name' => 'wrong_phone'])->first();
                    $messages['phone']['text'] = $wrong_phone->text;
                    $messages['phone']['type'] = $wrong_phone->type;
                    $messages['phone']['name'] = $wrong_phone->name;
                }*/
            }

            /**
             * Falls AGB's nicht akzeptiert wurde
             */
            if (isset($this->request->data['agb']) && $this->request->data['agb'] == 'false') {
                $messages[] = $this->Helper->__returnMessage('not_accepted_agb', 'agb');
            }

            /**
             * Captcha überprüfen
             */
            if (!in_array($_SERVER['HTTP_HOST'], $this->request->session()->read('captcha_free_hosts'))) {
                if (isset($this->request->data['g-recaptcha-response']) && strlen($this->request->data['g-recaptcha-response']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_captcha', 'captcha');
                }
            }

            /**
             * Falls interne Anfrage
             */
            if (isset($internal) && $internal == "internal_request") {
                /**
                 * Falls E-Mail-Adresse angegeben wurde
                 */
                if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                    $this->request->data['email'] = trim($this->request->data['email']);

                    $valid = true;
                    if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                        $valid = false;
                    }

                    /**
                     * Hole Benutzer anhand der E-Mail-Adresse
                     */
                    $exists = $this->Helper->__returnEmailUser(trim($this->request->data['email']));

                    if ($exists && $exists->role == 'broker') {
                        $messages[] = $this->Helper->__returnMessage('exists_email', 'email');
                    } else if (!$valid) {
                        $messages[] = $this->Helper->__returnMessage('wrong_email', 'email');
                    } else {
                        /**
                         * Falls E-Mail-Wiederholung angegeben wurde
                         */
                        if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) > 0) {
                            /**
                             * Falls E-Mail-Adresse und E-Mail-Wiederholung nicht identisch
                             */
                            if (trim($this->request->data['email']) != trim($this->request->data['email_repeat'])) {
                                $messages[] = $this->Helper->__returnMessage('not_similiar_emails', 'email_repeat');
                            }
                            /**
                             * Falls E-Mail-Wiederholung nicht angegeben wurde
                             */
                        } else if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                            $messages[] = $this->Helper->__returnMessage('no_email_confirm', 'email_repeat');
                        }
                    }
                    /**
                     * Falls E-Mail-Adresse nicht angegeben wurde
                     */
                } else if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_email', 'email', 'error');
                    /**
                     * Falls E-Mail-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('empty_email_repeat', 'email_repeat', 'error');
                    }
                }

                /**
                 * Falls Passwort angegeben wurde
                 */
                if (isset($this->request->data['password']) && strlen($this->request->data['password']) > 0) {
                    $this->request->data['password'] = trim($this->request->data['password']);

                    /**
                     * Falls Passwort-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('no_password_confirm', 'password_repeat');
                        /**
                         * Falls Passwort-Wiederholung angegeben wurde
                         */
                    } else if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) > 0) {
                        if (trim($this->request->data['password']) != trim($this->request->data['password_repeat'])) {
                            $messages[] = $this->Helper->__returnMessage('not_similiar_passwords', 'password_repeat');
                        }
                    }
                    /**
                     * Falls Passwort nicht angegeben wurde
                     */
                } else if (isset($this->request->data['password']) && strlen($this->request->data['password']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_password', 'password');
                    /**
                     * Falls Passwort-Wiederholung angegeben wurde
                     */
                    if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('empty_password_repeat', 'password_repeat');
                    }
                }
            } else {
                /**
                 * Falls E-Mail-Adresse angegeben wurde
                 */
                if (isset($this->request->data['email']) && strlen($this->request->data['email']) > 0) {
                    $this->request->data['email'] = trim($this->request->data['email']);

                    $valid = true;
                    if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                        $valid = false;
                    }

                    /**
                     * Hole Benutzer anhand der E-Mail-Adresse
                     */
                    $exists = $this->Helper->__returnEmailUser($this->request->data['email']);

                    if ($exists && $exists->role == 'broker') {
                        /**
                         * Falls E-Mail-Adresse bereits existiert
                         */
                        $messages[] = $this->Helper->__returnMessage('exists_email', 'email');
                    } else if (!$valid) {
                        /**
                         * Falls E-Mail-Adresse ungültig
                         */
                        $messages[] = $this->Helper->__returnMessage('wrong_email', 'email');
                    } else {
                        /**
                         * Falls E-Mail-Wiederholung angegeben wurde
                         */
                        if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) > 0) {
                            /**
                             * Falls E-Mail-Adresse und E-Mail-Wiederholung nicht identisch
                             */
                            if (trim($this->request->data['email']) != trim($this->request->data['email_repeat'])) {
                                $messages[] = $this->Helper->__returnMessage('not_similiar_emails', 'email_repeat');
                            }
                            /**
                             * Falls E-Mail-Wiederholung nicht angegeben wurde
                             */
                        } else if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                            $messages[] = $this->Helper->__returnMessage('no_email_confirm', 'email_repeat');
                        }
                    }
                    /**
                     * Falls E-Mail-Adresse nicht angegeben wurde
                     */
                } else if (isset($this->request->data['email']) && strlen($this->request->data['email']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_email', 'email', 'error');
                    /**
                     * Falls E-Mail-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['email_repeat']) && strlen($this->request->data['email_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('empty_email_repeat', 'email_repeat', 'error');
                    }
                }

                /**
                 * Falls Passwort angegeben wurde
                 */
                if (isset($this->request->data['password']) && strlen($this->request->data['password']) > 0) {
                    $this->request->data['password'] = trim($this->request->data['password']);

                    /**
                     * Falls Passwort-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('no_password_confirm', 'password_repeat');
                        /**
                         * Falls Passwort-Wiederholung angegeben wurde
                         */
                    } else if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) > 0) {
                        /**
                         * Falls Passwort und Passwort-Wiederholung nicht identisch
                         */
                        if (trim($this->request->data['password']) != trim($this->request->data['password_repeat'])) {
                            $messages[] = $this->Helper->__returnMessage('not_similiar_passwords', 'password_repeat');
                        }
                    }
                    /**
                     * Falls Passwort nicht angegeben wurde
                     */
                } else if (isset($this->request->data['password']) && strlen($this->request->data['password']) == 0) {
                    $messages[] = $this->Helper->__returnMessage('empty_password', 'password');
                    /**
                     * Falls Passwort-Wiederholung nicht angegeben wurde
                     */
                    if (isset($this->request->data['password_repeat']) && strlen($this->request->data['password_repeat']) == 0) {
                        $messages[] = $this->Helper->__returnMessage('empty_password_repeat', 'password_repeat');
                    }
                }
            }

            if (isset($this->request->data['broker_text']) && strlen($this->request->data['broker_text']) == 0) {
                $messages[] = $this->Helper->__returnMessage('empty_broker_text', 'broker_text');
            } else if (isset($this->request->data['broker_text']) && strlen($this->request->data['broker_text']) < 64) {
                $messages[] = $this->Helper->__returnMessage('more_broker_text', 'broker_text');
            }

        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else if ($this->request->is(['patch', 'post', 'put', 'ajax'])) {
            if (sizeof($messages)>0) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }

    }

    /**
     * Methode zur Überprüfung der Vermittler-PIN
     */
    public function validateChiffre() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->is('ajax')) {
            $donation_id = base64_decode($this->request->data['donation']);
            $code = $this->request->data['code'];

            $donation = $this->Helper->__returnDonation($donation_id, 1);
            if (!$donation) {
                $this->set('response', ['status' => 'failed', 'reason' => 'no_donation']);
                $this->render('response');
            } else {

                $address = $this->Helper->__returnAddress($donation->address_id);

                if (!$address) {
                    $this->set('response', ['status' => 'failed', 'reason' => 'no_address']);
                    $this->render('response');
                } else {

                    $this->loadModel('ValidZipcodes');
                    $zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => $address->zip])->first();

                    if (!$zipcode) {
                        $this->set('response', ['status' => 'failed', 'reason' => 'no_zipcode']);
                        $this->render('response');
                    } else {

                        $this->loadModel('BrokersZipcodes');

                        $broker_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.valid_zipcodes_id' => $zipcode->id]);

                        foreach ($broker_zipcodes as $broker_zipcode) {
                            $broker = $this->Helper->__returnBrokerUser($broker_zipcode->broker_id);
                            if ($broker && $broker->has('created') && $broker->created) {
                                $broker_code = "XXXX";
                            }
                            if (isset($broker_code) && $broker_code == $code && strlen($broker_code) == strlen($code)) {
                                $this->set('response', ['status' => 'success', 'broker_id' => base64_encode($broker->id)]);
                                $this->render('response');
                                return;
                            }

                        }

                        $broker = $this->Users->find()->where(['Users.role' => 'broker', 'Users.active' => 1])->first();
                        if ($broker) {
                            $broker_code = "XXXX";
                            if ($broker_code == $code && strlen($broker_code) == strlen($code)) {
                                $this->set('response', ['status' => 'success', 'broker_id' => base64_encode($broker->id), 'extra' => 'only_broker']);
                                $this->render('response');
                                return;
                            } else {
                                $this->set('response', ['status' => 'failed', 'reason' => 'code_incorrect']);
                                $this->render('response');
                                return;
                            }
                        } else {
                            $this->set('response', ['status' => 'failed', 'reason' => 'no_one_broker']);
                            $this->render('response');
                            return;
                        }
                    }
                }
            }
        }
    }

    /**
     * Methode zur Überprüfung einer Interessensanfrage
     *
     * @param null $internal
     * @param array|null $internal_request_data
     * @return bool
     */
    public function validateInterest($internal = null, array $internal_request_data = null) {
        $this->loadModel('Messages');
        if (isset($internal) && $internal = "internal_request") {
            $this->request->data = $internal_request_data;
        } else {
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
        }

        $messages = array();

        /**
         * Falls Name angegeben wurde
         */
        if (isset($this->request->data['name']) && strlen($this->request->data['name']) == 0) {
            $messages[] = $this->Helper->returnMessage('empty_sender', 'name');
        }

        /**
         * Falls Nachricht angegeben wurde
         */
        if (isset($this->request->data['message']) && strlen($this->request->data['message']) == 0) {
            $messages[] = $this->Helper->returnMessage('empty_message', 'message');
        }

        if (isset($internal) && $internal == "internal_request") {
            if (sizeof($messages)>0) {
                return false;
            } else {
                return true;
            }
        } else if ($this->request->is(['patch', 'post', 'put', 'ajax'])) {
            if (sizeof($messages)>1) {
                $response = [
                    'status' => 'failed',
                    'messages' => $messages
                ];
            } else {
                $response = [
                    'status' => 'success',
                    'messages' => $messages
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode zur Filterung von Titel und Beschreibung bei Abgabe einer Spende oder Aufgabe eines Gesuchs
     */
    public function filterText() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->is('ajax')) {

            $title_words = explode(" ", $this->request->data['title']);
            $description_words = explode(" ", $this->request->data['description']);

            $words = array_merge($title_words, $description_words);

            $formatted_words = array();
            foreach ($words as $word) {
                $word = strtolower($this->Helper->replaceChars($word));
                $word = str_replace(".", "", $word);
                $word = str_replace(",", "", $word);
                $word = str_replace(":", "", $word);
                $word = str_replace("/", "", $word);
                $word = str_replace("|", "", $word);
                $word = str_replace("(", "", $word);
                $word = str_replace(")", "", $word);
                $word = str_replace("[", "", $word);
                $word = str_replace("]", "", $word);
                $word = str_replace("{", "", $word);
                $word = str_replace("}", "", $word);
                // TODO: Zahlen aus Schlagwörtern ausschließen
                //$word = preg_replace('/[0-9]+/', null, $word);
                $formatted_words[] = $word;
            }

            $formatted_words = preg_replace('/[0-9]+/', '', $formatted_words);

            /**
             * Unrelevante Wörter
             */
            $not_relevant_words = [
                'ab', 'aber', 'abermals', 'abgeben', 'abgerufen', 'abgerufene', 'abgerufener', 'abgerufenes', 'abzugeben', 'ähnlich', 'alle',
                'allein', 'allem', 'allemal', 'allen', 'allenfalls', 'allenthalben', 'aller', 'allerdings', 'allerlei', 'alles', 'allesamt',
                'allgemein', 'allmählich', 'allzu', 'als', 'alsbald', 'also', 'alt', 'am', 'an', 'andauernd', 'andere', 'anderem', 'anderen',
                'anderer', 'andererseits', 'anderes', 'andern', 'andernfalls', 'anders', 'anerkannt', 'anerkannte', 'anerkannter', 'anerkanntes',
                'angesetzt', 'angesetzte', 'angesetzter', 'anscheinend', 'anschliessen', 'anstatt', 'auch', 'auf', 'auffallend', 'aufgrund', 'aufs',
                'augenscheinlich', 'aus', 'aus-', 'ausdrücklich', 'ausdrückt', 'ausdrückte', 'ausgang', 'ausgedrückt', 'ausgenommen', 'ausgerechnet',
                'ausnahmslos', 'außen', 'außer', 'außerdem', 'außerhalb', 'äußerst', 'bald', 'bei', 'beide', 'beiden', 'beiderlei', 'beides', 'beim',
                'beinahe', 'bekannt', 'bekannte', 'bekannter', 'bekanntlich', 'bekomme', 'bekommen', 'benoetige', 'benoetigen', 'bereits', 'besonders',
                'besser', 'bestenfalls', 'bestimmt', 'beträchtlich', 'bevor', 'bezüglich', 'bin', 'bis', 'bisher', 'bislang', 'bist', 'bloß', 'Bsp',
                'bzw', 'ca', 'Co', 'da', 'dabei', 'dadurch', 'dafür', 'dagegen', 'daher', 'dahin', 'damals', 'damit', 'danach', 'daneben', 'dank',
                'danke', 'dann', 'dannen', 'daran', 'darauf', 'daraus', 'darf', 'darfst', 'darin', 'darüber', 'darum', 'darunter', 'das', 'dass',
                'dasselbe', 'davon', 'davor', 'dazu', 'dein', 'deine', 'deinem', 'deinen', 'deiner', 'deines', 'dem', 'demgegenüber', 'demgemäß',
                'demnach', 'demselben', 'den', 'denen', 'denkbar', 'denn', 'dennoch', 'denselben', 'der', 'derart', 'derartig', 'deren', 'derer',
                'derjenige', 'derjenigen', 'derselbe', 'derselben', 'derzeit', 'des', 'deshalb', 'desselben', 'dessen', 'desto', 'deswegen', 'dich',
                'die', 'diejenige', 'dies', 'diese', 'dieselbe', 'dieselben', 'diesem', 'diesen', 'dieser', 'dieses', 'diesmal', 'diesseits', 'dir',
                'direkt', 'direkte', 'direkten', 'direkter', 'doch', 'dort', 'dorther', 'dorthin', 'drin', 'drüber', 'drunter', 'du', 'dunklen', 'durch',
                'durchaus', 'durchweg', 'e. K.', 'eben', 'ebenfalls', 'ebenso', 'ehe', 'eher', 'eigenen', 'eigenes', 'eigentlich', 'ein', 'ein-', 'eine',
                'einem', 'einen', 'einer', 'einerseits', 'eines', 'einfach', 'einfachen', 'eingang', 'einig', 'einige', 'einigem', 'einigen', 'einiger',
                'einigermaßen', 'einiges', 'einmal', 'einseitig', 'einseitige', 'einseitigen', 'einseitiger', 'einst', 'einstmals', 'einzig', 'empfangen',
                'entsprechend', 'entweder', 'er', 'ergo', 'erhält', 'erheblich', 'erneut', 'erst', 'ersten', 'es', 'etc', 'etliche', 'etwa', 'etwas', 'euch',
                'euer', 'eure', 'eurem', 'euren', 'eurer', 'eures', 'falls', 'fast', 'ferner', 'fluechtlinge', 'folgende  ', 'folgenden', 'folgender',
                'folgendermaßen', 'folgendes', 'folglich', 'förmlich', 'fortwährend', 'fraglos', 'Frau', 'frei', 'freie', 'freies', 'freilich',
                'funktioniert', 'für', 'gab', 'gängig', 'gängige', 'gängigen', 'gängiger', 'gängiges', 'ganz', 'ganze', 'ganzem', 'ganzen', 'ganzer',
                'ganzes', 'gänzlich', 'gar', 'GbdR', 'GbR', 'gebe', 'geben', 'geehrte', 'geehrten', 'geehrter', 'gefälligst', 'gegen', 'gehabt',
                'gekauft', 'gekonnt', 'gelegentlich', 'gemacht', 'gemäß', 'gemeinhin', 'gemocht', 'genau', 'genommen', 'genug', 'geradezu', 'gern',
                'gestern', 'gestrige', 'gesuch', 'gesuche', 'getan', 'geteilt', 'geteilte', 'getragen', 'gewesen', 'gewiss', 'gewisse', 'gewissermaßen',
                'gewollt', 'geworden', 'ggf', 'gib', 'gibt', 'gleich', 'gleichsam', 'gleichwohl', 'gleichzeitig', 'glücklicherweise', 'gmbh',
                'Gott sei Dank', 'größtenteils', 'Grunde', 'gute', 'guten', 'hab', 'habe', 'haben', 'halb', 'hallo', 'halt', 'hast', 'hat',
                'hatte', 'hätte', 'hätten', 'hattest', 'hattet', 'häufig', 'heraus', 'herein', 'heute', 'heutige', 'hier', 'hiermit', 'hiesige',
                'hilfsbedürftige', 'hin', 'hinein', 'hingegen', 'hinlänglich', 'hinten', 'hinter', 'hinterher', 'hoch', 'höchst', 'höchstens',
                'ich', 'ihm', 'ihn', 'ihnen', 'ihr', 'ihre', 'ihrem', 'ihren', 'ihrer', 'ihres', 'im', 'immer', 'immerhin', 'immerzu', 'in',
                'indem', 'indessen', 'infolge', 'infolgedessen', 'innen', 'innerhalb', 'input', 'ins', 'insbesondere', 'insofern', 'inzwischen',
                'irgend', 'irgendein', 'irgendeine', 'irgendjemand', 'irgendwann', 'irgendwas', 'irgendwen', 'irgendwer', 'irgendwie',
                'irgendwo', 'ist', 'ja', 'jährig', 'jährige', 'jährigen', 'jähriges', 'je', 'jede', 'jedem', 'jeden', 'jedenfalls', 'jeder',
                'jederlei', 'jedes', 'jedoch', 'jemals', 'jemand', 'jene', 'jenem', 'jenen', 'jener', 'jenes', 'jenseits', 'jetzt', 'jeweils', 'kam',
                'kann', 'kannst', 'kaum', 'kein', 'keine', 'keinem', 'keinen', 'keiner', 'keinerlei', 'keines', 'keinesfalls', 'keineswegs',
                'kg', 'klar', 'klare', 'klaren', 'klares', 'klein', 'kleinen', 'kleiner', 'kleines', 'koennen', 'koennte', 'konkret', 'konkrete',
                'konkreten', 'konkreter', 'konkretes', 'können', 'könnt', 'konnte', 'könnte', 'konnten', 'könnten', 'künftig', 'lag', 'lagen',
                'langsam', 'längst', 'längstens', 'lassen', 'laut', 'lediglich', 'leer', 'leicht', 'leider', 'lesen', 'letzten', 'letztendlich',
                'letztens', 'letztes', 'letztlich', 'lichten', 'links', 'Ltd', 'mag', 'magst', 'mal', 'man', 'manche', 'manchem', 'manchen',
                'mancher', 'mancherorts', 'manches', 'manchmal', 'Mann', 'marke', 'mehr', 'mehrere', 'mehrfach', 'mein', 'meine', 'meinem',
                'meinen', 'meiner', 'meines', 'meinetwegen', 'meint', 'meinte', 'meinten', 'meist', 'meiste', 'meisten', 'meistens', 'meistenteils',
                'meta', 'mich', 'mindestens', 'mir', 'mit', 'mithin', 'mitunter', 'moechte', 'moeglicherweise', 'möglich', 'mögliche',
                'möglichen', 'möglicher', 'möglicherweise', 'möglichst', 'morgen', 'morgige', 'muss', 'müssen', 'musst', 'müsst', 'musste',
                'müsste', 'müssten', 'nach', 'nachdem', 'nachher', 'nachhinein', 'nächste', 'nämlich', 'naturgemäß', 'natürlich', 'neben',
                'nebenan', 'nebenbei', 'nein', 'neu', 'neue', 'neuem', 'neuen', 'neuer', 'neuerdings', 'neuerlich', 'neues', 'neulich', 'nicht',
                'nichts', 'nichtsdestotrotz', 'nichtsdestoweniger', 'nie', 'niemals', 'niemand', 'nimm', 'nimmer', 'nimmt', 'nirgends',
                'nirgendwo', 'noch', 'nötigenfalls', 'nun', 'nunmehr', 'nur', 'ob', 'oben', 'oberhalb', 'obgleich', 'obschon', 'obwohl',
                'oder', 'offenbar', 'offenkundig', 'offensichtlich', 'oft', 'OHG', 'ohne', 'ohnedies', 'OK', 'output', 'paar', 'partout',
                'per', 'persönlich', 'pfui', 'plötzlich', 'praktisch', 'pro', 'quasi', 'recht', 'rechts', 'regelmäßig', 'reichlich',
                'relativ', 'restlos', 'richtiggehend', 'riesig', 'rund', 'rundheraus', 'rundum', 'samt', 'sämtliche', 'sattsam',
                'schaetzen', 'schaetzt', 'schaetzte', 'schaetzten', 'schlechter', 'schlicht', 'schlichtweg', 'schließlich', 'schlussendlich',
                'schnell', 'schon', 'Schreibens', 'Schreiber', 'schwerlich', 'schwierig', 'sehr', 'sei', 'seid', 'sein', 'seine', 'seinem',
                'seinen', 'seiner', 'seines', 'seit', 'seitdem', 'Seite', 'Seiten', 'seither', 'selber', 'selbst', 'selbstredend', 'selbstverstaendlich',
                'selten', 'seltsamerweise', 'sich', 'sicher', 'sicherlich', 'sie', 'siehe', 'sieht', 'sind', 'so', 'sobald', 'sodass', 'soeben',
                'sofern', 'sofort', 'sog', 'sogar', 'solange', 'solch', 'solche', 'solchem', 'solchen', 'solcher', 'solches', 'soll', 'sollen', 'sollst',
                'sollt', 'sollte', 'sollten', 'solltest', 'somit', 'sondern', 'sonders', 'sonst', 'sooft', 'soviel', 'soweit', 'sowie', 'sowieso',
                'sowohl', 'sozusagen', 'später', 'spende', 'spenden', 'spielen', 'startet', 'startete', 'starteten', 'statt', 'stattdessen', 'steht',
                'stellenweise', 'stets', 'stimme', 'Tages', 'tat', 'tatsächlich', 'tatsächlichen', 'tatsächlicher', 'tatsächliches', 'teile', 'total',
                'trotzdem', 'übel', 'über', 'überall', 'überallhin', 'überaus', 'überdies', 'überhaupt', 'üblicher', 'übrig', 'übrigens', 'ueber',
                'um', 'umso', 'umständehalber', 'unbedingt', 'unbeschreiblich', 'und', 'unerhört', 'ungefähr', 'ungemein', 'ungewöhnlich', 'ungleich',
                'unglücklicherweise', 'unlängst', 'unmaßgeblich', 'unmöglich', 'unmögliche', 'unmöglichen', 'unmöglicher', 'unnötig', 'uns', 'unsagbar',
                'unsäglich', 'unser', 'unsere', 'unserem', 'unseren', 'unserer', 'unseres', 'unserm', 'unstreitig', 'unten', 'unter', 'unterbrach',
                'unterbrechen', 'unterhalb', 'unwichtig', 'unzweifelhaft', 'usw', 'vergleichsweise', 'vermutlich', 'veröffentlichen', 'veröffentlicht',
                'veröffentlichte', 'veröffentlichten', 'veröffentlichtes', 'verstaerken', 'viel', 'viele', 'vielen', 'vieler', 'vieles', 'vielfach',
                'vielleicht', 'vielmals', 'voll', 'vollends', 'völlig', 'vollkommen', 'vollständig', 'vom', 'von', 'vor', 'voran', 'vorbei',
                'vorgestern', 'vorher', 'vorne', 'vorüber', 'während', 'währenddessen', 'wahrscheinlich', 'wann', 'war', 'wäre', 'waren', 'wären',
                'warst', 'warum', 'was', 'weder', 'weg', 'wegen', 'weidlich', 'weil', 'Weise', 'weiß', 'weitem', 'weiter', 'weitere',
                'weiterem', 'weiteren', 'weiterer', 'weiteres', 'weiterhin', 'weitgehend', 'welche', 'welchem', 'welchen', 'welcher', 'welches',
                'wem', 'wen', 'wenig', 'wenige', 'weniger', 'wenigstens', 'wenn', 'wenngleich', 'wer', 'werde', 'werden', 'werdet', 'weshalb',
                'wessen', 'wichtig', 'wie', 'wieder', 'wiederum', 'wieso', 'wiewohl', 'will', 'willst', 'wir', 'wird', 'wirklich', 'wirst',
                'wo', 'wobei', 'wodurch', 'wogegen', 'woher', 'wohin', 'wohingegen', 'wohl', 'wohlgemerkt', 'wohlweislich', 'wollen', 'wollt',
                'wollte', 'wollten', 'wolltest', 'wolltet', 'womit', 'womöglich', 'woraufhin', 'woraus', 'worin', 'wurde', 'würde', 'würden',
                'z. B.', 'zahlreich', 'zeitweise', 'ziemlich', 'zu', 'zudem', 'zuerst', 'zufolge', 'zugegeben', 'zugleich', 'zuletzt', 'zum',
                'zumal', 'zumeist', 'zur', 'zurück', 'zusammen', 'zusehends', 'zuvor', 'zuweilen', 'zwar', 'zweifellos', 'zweifelsfrei',
                'zweifelsohne', 'zwischen', 'wf', 'bs', 'wir', 'er', 'sie', 'ich', 'es', '-damit', 'besteht', 'bestehen', 'eins', 'zwei', 'drei',
                'vier', 'fuenf', 'fünf', 'sechs', 'sieben', 'acht', 'neun', 'zehn', 'jahr', 'jahre', 'wunsch', 'absprache', 'gutem', 'jo', 'joo',
                'ausser', 'richtig', 'außer', 'account', 'bild', 'missachten', 'teste', 'testspende', 'noetig', 'braucht', 'glaube', 'macht', 'cn', 'nichts', 'klappt',
                'sh', 'brauche', 'egal', 'isst', 'muesste', 'abgeholt', 'keine', 'ok', 'gut', 'zunächst', 'zunaechst', 'hierbei', 'spielt', '!', '/', '#', '+', '*',
                '-', '_', ',', '.', ';', ':', '<', '>', '≤', '°', '^', '´', '`', '?', ')', '(', '|', ']', '[', '%', '$', '§', '!', '¡', '‘', '±', '…', '∞', '}', '{', '¢', '¶',
                'handy', 'bitte', 'voraus', 'dankbar', 'liegen', 'aussortiert', 'zb', 'zB', 'z.B', 'z.B.', 'gehen', 'anzuschaffen', 'fehlt', 'spaeter', 'lebhaft', 'x',
                'angebracht', 'für', 'fuer', 'mhd'
            ];

            $tags = "";

            $this->loadModel('DeletedWords');
            foreach ($formatted_words as $formatted_word) {
                $exists = $this->DeletedWords->find()->where(['DeletedWords.keyword' => $formatted_word, 'DeletedWords.deleted_count >' => 5])->first();
                if ($exists) {
                    continue;
                }
                if (in_array($formatted_word, $not_relevant_words)) {
                    continue;
                }
                if (strlen($tags)>0) {
                    if (stripos($tags, $formatted_word) !== false) {
                        continue;
                    }
                }
                $tags.=",".$formatted_word;
            }

            if (strlen($this->request->session()->read('keywords_string'))>0) {
                $tags.=$this->request->session()->read('keywords_string');
            }

            if (strlen($tags)>0) {
                $response = [
                    'status' => 'success',
                    'tags' => $tags
                ];
            } else {
                $response = [
                    'status' => 'failed',
                ];
            }
            $this->set('response', $response);
            $this->render('response');

        }
    }

    /**
     * Methode zur Suche von Gesuchen und Spenden
     *
     * @param null $title_search_input
     */
    public function search($title_search_input = null) {

        $search_input = utf8_decode($title_search_input);

        $this->loadModel('Searches');
        $this->loadModel('Keywords');
        $this->loadModel('KeywordsSearches');
        $this->loadModel('Donations');
        $this->loadModel('KeywordsDonations');
        if ($search_input) {
            $this->set('title', 'Deine Suche: ' . $title_search_input);

            $results = array();

            $search_input_array = explode(" ", $search_input);

            $all_results = array();
            foreach ($search_input_array as $search_input_key) {
                $search_input_key_normal = $search_input_key;
                $search_input_key = $this->Helper->replaceChars($search_input_key);
                $search_input_key = strtolower($search_input_key);
                $all_searches_ids = $this->Searches
                    ->find()
                    ->select(['Searches.id'])
                    ->where(['OR' => [['Searches.title LIKE' => "%".$search_input_key_normal."%"],['Searches.description LIKE' => "%".$search_input_key_normal."%"]]])
                    ->andWhere(['Searches.active' => 1])
                    ->andWhere([$this->request->session()->read('branch_filter_search')]);
                foreach ($all_searches_ids as $searches_id) {
                    $all_results[$searches_id->id]['id'] = $searches_id->id;
                    $all_results[$searches_id->id]['type'] = 'search';
                }
                $all_keyword_ids = $this->Keywords
                    ->find()
                    ->select(['Keywords.id'])
                    ->where(['OR' => [['Keywords.keyword LIKE' => "%".$search_input_key."%"], ['Keywords.keyword LIKE' => "%".$search_input."%"], ['Keywords.keyword' => $search_input], ['Keywords.keyword' => $search_input_key]]])
                    ->andWhere([$this->request->session()->read('branch_filter_keyword')]);
                foreach ($all_keyword_ids as $keyword_id) {
                    $all_keyword_searches_ids = $this->KeywordsSearches
                        ->find()
                        ->select(['KeywordsSearches.search_id'])
                        ->where(['KeywordsSearches.keyword_id' => $keyword_id->id])
                        ->andWhere([$this->request->session()->read('branch_filter_keyword_search')]);

                    foreach ($all_keyword_searches_ids as $keyword_searches_id) {
                        if (isset($all_results[$keyword_searches_id->search_id])) {
                            continue;
                        } else {
                            $all_results[$keyword_searches_id->search_id]['id'] = $keyword_searches_id->search_id;
                            $all_results[$keyword_searches_id->search_id]['type'] = 'search';
                        }
                    }
                }
            }
            foreach ($all_results as $key => $result) {
                $search = $this->Searches
                    ->find()
                    ->contain(['FileUploads'])
                    ->where(['Searches.id' => $result['id']])
                    ->andWhere([$this->request->session()->read('branch_filter_search')])
                    ->first();
                if ($search) {
                    $search->type = "gesuche";
                    $results[] = $search;
                }
            }

            foreach ($search_input_array as $search_input_key) {
                $search_input_key_normal = $search_input_key;
                $search_input_key = $this->Helper->replaceChars($search_input_key);
                $search_input_key = strtolower($search_input_key);
                $all_donations_ids = $this->Donations
                    ->find()
                    ->select(['Donations.id'])
                    ->where(['OR' => [['Donations.title LIKE' => '%'.$search_input_key_normal.'%'],['Donations.description LIKE' => '%'.$search_input_key_normal.'%']]])
                    ->andWhere(['Donations.active' => 1])
                    ->andWhere([$this->request->session()->read('branch_filter_donation')]);
                foreach ($all_donations_ids as $donations_id) {
                    $all_results[$donations_id->id]['id'] = $donations_id->id;
                    $all_results[$donations_id->id]['type'] = 'donation';
                }
                $all_keyword_ids = $this->Keywords
                    ->find()
                    ->select(['Keywords.id'])
                    ->where(['OR' => [['Keywords.keyword LIKE' => "%".$search_input_key."%"], ['Keywords.keyword LIKE' => "%".$search_input."%"], ['Keywords.keyword' => $search_input], ['Keywords.keyword' => $search_input_key]]])
                    ->andWhere([$this->request->session()->read('branch_filter_keyword')]);
                foreach ($all_keyword_ids as $keyword_id) {
                    $all_keyword_donations_ids = $this->KeywordsDonations
                        ->find()
                        ->select(['KeywordsDonations.donation_id'])
                        ->where(['KeywordsDonations.keyword_id' => $keyword_id->id])
                        ->andWhere([$this->request->session()->read('branch_filter_keyword_donation')]);
                    foreach ($all_keyword_donations_ids as $keyword_donations_id) {
                        if (isset($all_results[$keyword_donations_id->donation_id])) {
                            continue;
                        } else {
                            $all_results[$keyword_donations_id->donation_id]['id'] = $keyword_donations_id->donation_id;
                            $all_results[$keyword_donations_id->donation_id]['type'] = 'donation';
                        }
                    }
                }
            }

            foreach ($all_results as $key => $result) {
                if ($result['type']=='search') {
                    continue;
                }
                $donation = $this->Donations
                    ->find()
                    ->contain(['FileUploads'])
                    ->where(['Donations.id' => $result['id'], 'Donations.active' => 1])
                    ->andWhere([$this->request->session()->read('branch_filter_donation')])
                    ->first();
                if ($donation) {
                    $donation->type = "spenden";
                    $results[] = $donation;
                }
            }

            if (sizeof($results)==0) {
                $this->Flash->error(__('Keine Suchergebnisse.'));
            }
            $this->set('results', $results);
        } else {
            $this->set('title', 'Suchformular');
        }

        if (isset($search_input) && strlen($search_input)>0) {
            $this->set('search_user_input', $title_search_input);
        }
    }

    /**
     * Methode zum Setzen eines neuen Passworts
     *
     * @return \Cake\Network\Response|null
     */
    public function setNewPassword() {

        if (base64_decode($this->request->session()->read('autologin'))!='yes') {
            $this->Flash->error(__('Fehlerhafte Weiterleitung.'));
            return $this->redirect($this->Auth->logout());
        }

        $this->loadModel('Users');
        $user = $this->Users->find()->where(['Users.id' => $this->Auth->user('id')])->first();

        if (!$user) {
            $this->Flash->error(__('Fehlerhafte Weiterleitung.'));
            return $this->redirect($this->Auth->logout());
        }

        $this->set('title', 'Neues Passwort vergeben');

        if ($this->request->is(['post', 'put', 'patch'])) {
            if (strlen($this->request->data['new_password'])==0 || strlen($this->request->data['new_password_confirm'])==0 || $this->request->data['new_password']!=$this->request->data['new_password_confirm']) {
                $this->Flash->error(__('Ungültige Eingaben.'));
                return $this->redirect('/neues_passwort_vergeben');
            }
            $hashed_password = "XXXX";
            $patched_user = $this->Users->patchEntity($user, ['password' => $hashed_password, 'active' => 1]);
            $this->request->session()->delete('autologin');
            if ($this->Users->save($patched_user)) {
                $this->Flash->success(__('Passwort erfolgreich vergeben.'));

            } else {
                $this->Flash->error(__('Passwort wurde nicht vergeben.'));
            }

            switch($user->role) {
                case "broker":
                    return $this->redirect('/broker/dashboard');
                    break;
                case "admin":
                    return $this->redirect('/admin/dashboard');
                    break;
                case "super":
                    return $this->redirect('/super/dashboard');
                    break;
                case "token_account":
                    return $this->redirect('/dashboard');
                    break;
            }

        }

        $this->set('user', $user);
    }

    /**
     * Methode zur Bereitstellung der Sitemap
     */
    public function sitemap() {
        $this->set('title', 'Sitemap');

        $this->loadModel('Pages');
        $dynamic_pages = $this->Pages->find()->select(['Pages.slug', 'Pages.title'])->where(['AND' => [['Pages.slug !=' => 'fehler_gemeldet'], ['Pages.slug !=' => 'erfolgreich_ausgeloggt'], ['Pages.slug !=' => 'erfolgreich_bestaetigt']]]);

        $this->set('dynamic_pages', $dynamic_pages);

        $hard_pages = [
            ['0' => ['slug' => 'startseite', 'title' => 'Startseite']],
            ['1' => ['slug' => 'spende_abgeben', 'title' => 'Spende abgeben']],
            ['2' => ['slug' => 'zeitspenden', 'title' => 'Zeitspenden']],
            ['3' => ['slug' => 'sachspenden', 'title' => 'Sachspenden']],
            ['4' => ['slug' => 'zeitgesuche', 'title' => 'Zeitgesuche']],
            ['5' => ['slug' => 'sachgesuche', 'title' => 'Sachgesuche']],
            ['6' => ['slug' => 'neues_passwort_vergeben', 'title' => 'Neues Passwort']],
            ['7' => ['slug' => 'login', 'title' => 'Login']],
            ['8' => ['slug' => 'register', 'title' => 'Registrieren']],
            ['9' => ['slug' => 'bewerben', 'title' => 'Bewerben']],
            ['10' => ['slug' => 'suche', 'title' => 'Suche']]
        ];

        $this->set('hard_pages', $hard_pages);
    }

    /**
     * Methode zum Versenden von Fehlerberichten
     *
     * @return \Cake\Network\Response|null
     */
    public function support() {
        if ($this->request->is(['post', 'put', 'patch'])) {

            if (isset($this->request->data['message']) && strlen($this->request->data['message'])==0) {
                $this->Flash->error(__('Ungültige Eingaben.'));
                return $this->redirect(trim($this->request->data['current_url']));
            }

            $configuration = $this->Helper->__returnConfiguration();

            $mail_template = $this->Helper->__returnEmailTemplate('support');

            $mail_content = $mail_template->template_content;
            $mail_content = str_replace("{{current_url}}", trim($this->request->data['current_url']), $mail_content);
            $mail_content = str_replace("{{http_user_agent}}", trim($this->request->data['http_user_agent']), $mail_content);
            $mail_content = str_replace("{{controller}}", trim($this->request->data['controller']), $mail_content);
            $mail_content = str_replace("{{action}}", trim($this->request->data['action']), $mail_content);
            $mail_content = str_replace("{{pass}}", trim($this->request->data['pass']), $mail_content);
            $mail_content = str_replace("{{message}}", trim($this->request->data['message']), $mail_content);
            $mail_content = str_replace("{{host}}", $configuration->host, $mail_content);
            if ($this->Auth->user('id')>0) {
                $user = $this->Helper->__returnUser($this->Auth->user('id'));
                if ($user) {
                    $mail_content = str_replace("{{user}}", $user->firstname . " " . $user->lastname . " | " . $user->id, $mail_content);
                }
            } else {
                $mail_content = str_replace("{{user}}", "Öffentlich", $mail_content);
            }

            $new_support_entity = [
                'current_url' => trim($this->request->data['current_url']),
                'http_user_agent' => trim($this->request->data['http_user_agent']),
                'controller' => trim($this->request->data['controller']),
                'action' => trim($this->request->data['action']),
                'pass' => trim($this->request->data['pass']),
                'message' => trim($this->request->data['message']),
            ];

            if ($this->Auth->user('id')>0) {
                $new_support_entity['user_id'] = $this->Auth->user('id');
            }

            $this->loadModel('Supports');
            $new_support = $this->Supports->newEntity($new_support_entity);

            $this->Supports->save($new_support);

            $email = new email('default');
            $email->helpers(array('Html', 'Text'));
            $email->viewvars(
                array(
                    'mail_content' => $mail_content,
                )
            );
            $email->template('default');
            $email->emailformat('text');
            $email->from(array($configuration->from_mail => $configuration->from_big_title));
            $email->to(array($configuration->support_mail => $configuration->support_big_title));
            $email->subject($mail_template->template_subject);

            if ($email->send()) {

                $this->request->session()->write('referer', $this->request->data['current_url']);
                return $this->redirect('/fehler_gemeldet');
            } else {
                return $this->redirect($this->request->data['current_url']);
            }

        }
    }

    /**
     * Methode zur Verwaltung eigener Spenden
     *
     * @param null $case
     * @return \Cake\Network\Response|null
     */
    public function manage($case = null) {
        if ($this->__checkAccess()) {
            $this->loadModel('Donations');
            $case_array = explode('|', base64_decode($case));
            switch ($case_array[0]) {
                case "spender_spenden":
                    $this->set('title', 'Meine Spenden');
                    $data = $this->Donations->find()->where(['Donations.user_id' => $this->Auth->user('id')])->order(['Donations.active' => 'DESC', 'Donations.created' => 'DESC']);
                    $columns = ['Status', 'Titel', 'Art', 'Erstellt', 'Aktionen'];
                    break;
            }

            $this->set('data', $data);
            $this->set('columns', $columns);
            $this->set('type', $case_array[0]);
        } else {
            $this->Flash->error(__('Keine Berechtigung.'));
            return $this->redirect($this->Auth->logout());
        }

    }

    /**
     * Methode zum Archivieren von Gesuchen oder Spenden
     *
     * @param $input_token
     * @return \Cake\Network\Response|null
     */
    public function toArchive($input_token) {
        $this->loadModel('Donations');
        $this->loadModel('Searches');
        $token_array = explode('|', base64_decode($input_token));
        switch($token_array[1]) {
            case "gesuche":
                $search = $this->Searches->find()->where(['Searches.id' => $token_array[0]])->first();
                if ($search) {
                    $patched_search = $this->Searches->patchEntity($search, ['active' => 0]);
                    if ($this->Searches->save($patched_search)) {
                        $this->Flash->success(__('Erfolgreich gelöscht.'));
                    }
                }
                break;
            case "spenden":
                $donation = $this->Donations->find()->where(['Donations.id' => $token_array[0]])->first();
                if ($donation) {
                    $patched_donation = $this->Donations->patchEntity($donation, ['active' => 0]);
                    if ($this->Donations->save($patched_donation)) {
                        $this->Flash->success(__('Erfolgreich gelöscht.'));
                    }

                }
                break;
        }
        return $this->redirect($this->referer());
    }

    /**
     * Methode zum Löschen von Gesuchen oder Spenden
     *
     * @param null $input_token
     * @return \Cake\Network\Response|null
     */
    public function delete($input_token = null) {
        $this->loadModel('Donations');
        $this->loadModel('Searches');
        $token_array = explode('|', base64_decode($input_token));
        switch($token_array[2]) {
            case "gesuche":
                $search = $this->Searches->find()->where(['Searches.id' => $token_array[0]])->first();
                if ($search) {
                    if ($this->Searches->delete($search)) {
                        $this->Flash->success(__('Erfolgreich gelöscht.'));
                    }
                }
                break;
            case "spenden":
                $donation = $this->Donations->find()->where(['Donations.id' => $token_array[0]])->first();
                if ($donation) {
                    if ($this->Donations->delete($donation)) {
                        $this->Flash->success(__('Erfolgreich gelöscht.'));
                    }

                }
                break;
        }
        return $this->redirect($this->referer());
    }

    /**
     * Methode zur Darstellung von Details zu Gesuchen oder Spenden
     *
     * @param null $case
     * @return \Cake\Network\Response|null
     */
    public function details($case = null) {
        if ($this->__checkAccess()) {
            $this->loadModel('Addresses');
            $this->loadModel('Donations');
            $this->loadModel('Searches');
            $this->loadModel('Users');
            $case_array = explode('|', base64_decode($case));
            switch ($case_array[2]) {
                case "gesuche":
                    $gesuch = $this->Searches->get($case_array[0], ['contain' => ['FileUploads', 'TimeSearches', 'Users', 'Users.Addresses']]);
                    $this->set('title', 'Details - ' . $gesuch->title);
                    $contact = $this->Addresses->find()->where(['Addresses.user_id' => $gesuch->user_id])->first();
                    $contact_user = $this->Users->find()->where(['Users.id' => $gesuch->user_id])->first();
                    if ($contact && $contact_user) {
                        $contact_array = [
                            'firstname' => $contact_user->firstname,
                            'lastname' => $contact_user->lastname,
                            'phone' => $contact_user->phone,
                            'fax' => $contact_user->fax,
                            'street' => $contact->street,
                            'zip' => $contact->zip,
                            'city' => $contact->city
                        ];
                        $this->set('contact_array', $contact_array);
                    }
                    $this->set('search', $gesuch);
                    $this->set('type', 'gesuche');
                    break;
                case "spenden":
                    $spende = $this->Donations->get($case_array[0], ['contain' => ['FileUploads', 'TimeDonations']]);
                    $this->set('title', 'Details - ' . $spende->title);
                    if (isset($spende->user_id) && $spende->user_id>0) {
                        $contact = $this->Addresses->find()->where(['Addresses.user_id' => $spende->user_id])->first();
                        $contact_user = $this->Users->find()->where(['Users.id' => $spende->user_id])->first();
                        if ($contact && $contact_user) {
                            $contact_array = [
                                'firstname' => $contact_user->firstname,
                                'lastname' => $contact_user->lastname,
                                'phone' => $contact_user->phone,
                                'fax' => $contact_user->fax,
                                'street' => $contact->street,
                                'zip' => $contact->zip,
                                'city' => $contact->city
                            ];
                            $this->set('contact_array', $contact_array);
                        }
                    } else {
                        $this->set('contact_array', ['zip' => '', 'city' => '']);
                    }
                    $this->set('donation', $spende);
                    $this->set('type', 'spenden');
                    break;
            }
        } else {
            return $this->redirect($this->Auth->logout());
        }
    }

    /**
     * Methode zum Versenden einer PIN-Anfrage
     *
     * @return \Cake\Network\Response|null
     */
    public function pinRequest() {

        if ($this->request->is(['post', 'put', 'patch'])) {

            $this->loadModel('Configurations');
            $configuration = $this->Configurations->get(1);

            if (!in_array($_SERVER['HTTP_HOST'], $this->request->session()->read('captcha_free_hosts'))) {
                if ($this->Auth->user('role') != 'super' && $this->Auth->user('role') != 'broker' && $this->Auth->user('role') != 'admin' && $this->Auth->user('role') != 'token_account') {
                    $response = file_get_contents("XXXX");
                    $response = json_decode($response);
                    if ($response->success == false) {
                        $this->Flash->error(__('Es ist ein Fehler aufgetreten, bitte kontaktiere den Support.'));
                        return $this->redirect($this->request->data['referer']);
                    }
                }
            }

            $valid = $this->validatePinrequest("internal_request", $this->request->data);
            if (!$valid) {
                $this->Flash->error(__('Alle Pflichtfelder müssen ausgefüllt werden.'));
                return $this->redirect($this->request->data['referer']);
            }

            $this->loadModel('EmailTemplates');
            $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'pin_anfordern'])->first();

            $this->loadModel('Donations');
            $donation = $this->Donations->find()->where(['Donations.id' => base64_decode($this->request->data['donation'])])->first();
            $donation_link = $configuration->host;
            if ($donation->category == "thing") {
                $donation_link .= "/sachspende/" . $donation->url_title;
            } else if ($donation->category == "time") {
                $donation_link .= "/zeitspende/" . $donation->url_title;
            }

            $this->loadModel('AdminsZipcodes');
            $this->loadModel('Users');
            $admin_zips = $this->AdminsZipcodes->find()->contain(['ValidZipcodes'])->where(['ValidZipcodes.zipcode' => trim($this->request->data['zip'])]);
            if ($admin_zips) {
                foreach ($admin_zips as $admin_zip) {
                    $admin = $this->Users->find()->where(['Users.role' => 'admin', 'Users.id' => $admin_zip->admin_id])->first();
                    if ($admin) {
                        $admin_name = $admin->firstname;
                    } else {
                        continue;
                    }

                    $mail_subject = $mail_template->template_subject;
                    $mail_content = $mail_template->template_content;
                    $mail_content = str_replace("{{donation_link}}", $donation_link, $mail_content);
                    $mail_content = str_replace("{{admin_firstname}}", $admin_name, $mail_content);
                    $mail_content = str_replace("{{sender_name}}", trim($this->request->data['name']), $mail_content);
                    $mail_content = str_replace("{{sender_email}}", trim($this->request->data['email']), $mail_content);
                    $mail_content = str_replace("{{zip}}", trim($this->request->data['zip']), $mail_content);
                    $mail_content = str_replace("{{message}}", trim($this->request->data['message']), $mail_content);
                    $mail_content = str_replace("{{additional}}", trim($this->request->data['additional']), $mail_content);
                    $mail_content = str_replace("{{host}}", $configuration->host, $mail_content);

                    $email = new email('default');
                    $email->helpers(array('Html', 'Text'));
                    $email->viewvars(
                        array(
                            'mail_content' => $mail_content,
                        )
                    );
                    $email->template('default');
                    $email->emailformat('text');
                    $email->from(array($configuration->from_mail => $configuration->from_big_title));
                    $email->to(array($admin->email => $admin->firstname . " " . $admin->lastname));
                    $email->subject($mail_subject);

                    if ($email->send()) {

                        $this->loadModel('EmailLogs');
                        $new_email_log_entity = [
                            'send' => 1,
                            'subject' => $mail_subject,
                            'content' => $mail_content,
                            'recipient_name' => $admin->firstname . " " . $admin->lastname,
                            'recipient_email' => $admin->email,
                            'sender_name' => $configuration->from_big_title,
                            'sender_email' => $configuration->from_mail
                        ];
                        $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                        $this->EmailLogs->save($new_email_log);
                    }
                }
                $this->Flash->success(__('Anforderung erfolgreich gesendet.'));
                $this->request->session()->write('referer', $this->request->data['referer']);
                return $this->redirect($this->request->data['cu']);
            } else {
                $this->Flash->error(__('Anforderung nicht gesendet.'));
                $this->request->session()->write('referer', $this->request->data['referer']);
                return $this->redirect($this->request->data['cu']);
            }

        }
    }

    /**
     * Methode zum Versenden einer Anfrage
     *
     * @return \Cake\Network\Response|null
     */
    public function request() {

        if ($this->request->is(['post', 'put', 'patch'])) {

            $this->loadModel('Configurations');
            $configuration = $this->Configurations->get(1);

            $request_array = [
                'message' => trim($this->request->data['message']),
                'sender' => trim($this->request->data['sender']),
                'case' => isset($this->request->data['donation']) ? 'donation' : 'search',
                'email' => trim($this->request->data['email']),
            ];

            $case = isset($request_array['case']) && $request_array['case'] == 'donation' ? 'deiner Spende' : 'deinem Gesuch';

            $this->loadModel('EmailTemplates');
            $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'anfrage_senden'])->first();

            $this->loadModel('Donations');
            $this->loadModel('Searches');
            $link = $configuration->host;
            switch($request_array['case']) {
                case "donation":
                    $entity = $this->Donations->find()->where(['Donations.id' => base64_decode($this->request->data['donation'])])->first();
                    switch ($entity->category) {
                        case "thing":
                            $link.= "/sachspende/";
                            break;
                        case "time":
                            $link.= "/zeitspende/";
                            break;
                    }
                    break;
                case "search":
                    $entity = $this->Searches->find()->where(['Searches.id' => base64_decode($this->request->data['search'])])->first();
                    switch($entity->category) {
                        case "thing":
                            $link.= "/sachgesuch/";
                            break;
                        case "time":
                            $link.= "/zeitgesuch/";
                            break;
                    }
                    break;
            }
            $link.= $entity->url_title;

            $mail_content = $mail_template->template_content;
            $mail_content = str_replace("{{sender_name}}", $request_array['sender'], $mail_content);
            $mail_content = str_replace("{{case}}", $case, $mail_content);
            $mail_content = str_replace("{{message}}", $request_array['message'], $mail_content);
            $mail_content = str_replace("{{link}}", $link, $mail_content);
            $mail_content = str_replace("{{host}}", $configuration->host, $mail_content);
            $mail_content = str_replace("{{sender_email}}", $request_array['email'], $mail_content);

            $mail_subject = str_replace("{{case}}", $case, $mail_template->template_subject);

            $this->loadModel('Users');
            $user = $this->Users->find()->select(['Users.email', 'Users.firstname', 'Users.lastname'])->where(['Users.id' => $entity->user_id])->first();

            if (!$user) {
                $this->Flash->error(__('Spende anbieten fehlgeschlagen.'));
                return $this->redirect($this->referer());
            }

            $email = new email('default');
            $email->helpers(array('Html', 'Text'));
            $email->viewvars(
                array(
                    'mail_content' => $mail_content,
                )
            );
            $email->template('default');
            $email->emailformat('text');
            $email->from(array($configuration->from_mail => $configuration->from_big_title));
            $email->to(array($user->email => $user->firstname . " " . $user->lastname));
            $email->subject($mail_subject);

            if ($email->send()) {

                $this->loadModel('EmailLogs');
                $new_email_log_entity = [
                    'send' => 1,
                    'subject' => $mail_subject,
                    'content' => $mail_content,
                    'recipient_name' => $user->firstname . " " . $user->lastname,
                    'recipient_email' => $user->email,
                    'sender_name' => $configuration->from_big_title,
                    'sender_email' => $configuration->from_mail
                ];
                $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                $this->EmailLogs->save($new_email_log);

                if (isset($this->request->data['donation'])) {
                    $entity = $this->Donations->find()->where(['Donations.id' => base64_decode($this->request->data['donation'])])->first();
                    $new_report_entity['donation_id'] = base64_decode($this->request->data['donation']);
                    $log_array['donation_id'] = base64_decode($this->request->data['donation']);

                    $log_array = [
                        'user_id' => ($this->Auth->user('id') > 0) ? $this->Auth->user('id') : NULL,
                        'donation_id' => base64_decode($this->request->data['donation']),
                        'log_title' => 'Anfrage gesendet',
                        'log_text' => $this->request->data['sender'] . " hat eine neue Anfrage gesendet.",
                    ];

                } else if (isset($this->request->data['search'])) {
                    $entity = $this->Searches->find()->where(['Searches.id' => base64_decode($this->request->data['search'])])->first();
                    $new_report_entity['search_id'] = base64_decode($this->request->data['search']);
                    $log_array['search_id'] = base64_decode($this->request->data['search']);

                    $log_array = [
                        'user_id' => ($this->Auth->user('id') > 0) ? $this->Auth->user('id') : NULL,
                        'search_id' => base64_decode($this->request->data['search']),
                        'log_title' => 'Anfrage gesendet',
                        'log_text' => $this->request->data['sender'] . " hat eine neue Anfrage gesendet.",
                    ];
                }

                $this->Helper->__log($log_array);
                $this->Flash->success(__('Anfrage erfolgreich gesendet.'));
                $this->request->session()->write('referer', $this->request->data['referer']);
                return $this->redirect($this->request->data['cu']);
            } else {
                $this->Flash->error(__('Anfrage nicht gesendet.'));
                $this->request->session()->write('referer', $this->request->data['referer']);
                return $this->redirect($this->request->data['cu']);
            }
        }

    }

    /**
     * Methode zum Versenden einer Beitragsmeldung
     *
     * @return \Cake\Network\Response|null
     */
    public function report() {

        if ($this->request->is(['post', 'put', 'patch'])) {

            $valid = $this->validateReport("internal_request", $this->request->data);
            if (!$valid) {
                $this->Flash->error(__('Alle Pflichtfelder müssen ausgefüllt werden.'));
                unset($this->request->data);
                return $this->redirect(['action' => 'add']);
            }

            $new_report_entity = $log_array = array();

            if (isset($this->request->data['donation'])) {

                $this->loadModel('Donations');
                $case = "Spende";
                $entity = $this->Donations->find()->where(['Donations.id' => base64_decode($this->request->data['donation'])])->first();
                $redirect_case = $entity->category;
                $new_report_entity['donation_id'] = base64_decode($this->request->data['donation']);
                $log_array['donation_id'] = base64_decode($this->request->data['donation']);
            } else if (isset($this->request->data['search'])) {

                $this->loadModel('Searches');

                $case = "Gesuch";
                $entity = $this->Searches->find()->where(['Searches.id' => base64_decode($this->request->data['search'])])->first();
                $redirect_case = $entity->category;
                $new_report_entity['search_id'] = base64_decode($this->request->data['search']);
                $log_array['search_id'] = base64_decode($this->request->data['search']);
            }

            $new_report_entity['user_id'] =  $this->Auth->user('id');
            $new_report_entity['email'] = $this->request->data['email'];
            $new_report_entity['name'] = $this->request->data['sender'];
            $new_report_entity['reason'] = $this->request->data['reason'];

            $this->loadModel('Reports');
            $new_report = $this->Reports->newEntity($new_report_entity);
            $save_report = $this->Reports->save($new_report);

            $this->loadModel('Users');
            $user = $this->Users->find()->where(['Users.id' => $this->Auth->user('id')])->first();

            $log_array['user_id'] = ($this->Auth->user('id') > 0) ? $this->Auth->user('id') : NULL;
            $log_array['log_title']= 'Beitrag gemeldet';
            $log_array['log_text'] = $user->firstname . " " . $user->lastname . " hat diesen Beitrag gemeldet.";

            $logged = $this->Helper->__log($log_array);

            if ($save_report && $logged) {

                $this->loadModel('Configurations');
                $configuration = $this->Configurations->get(1);

                $this->loadModel('EmailTemplates');
                $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'beitrag_melden'])->first();

                $mail_content = $mail_template->template_content;
                $mail_content = str_replace("{{host}}", $configuration->host, $mail_content);
                $mail_content = str_replace("{{report_name}}", $save_report->name, $mail_content);
                $mail_content = str_replace("{{report_email}}", $save_report->email, $mail_content);
                $mail_content = str_replace("{{report_reason}}", $save_report->reason, $mail_content);

                $case_entity_url = $configuration->host;
                switch($case) {
                    case "Spende":
                        $case = "Folgende Spende wurde gemeldet:";
                        switch($redirect_case) {
                            case "thing":
                                $case_entity_url.= "/sachspende/";
                                break;
                            case "time":
                                $case_entity_url.= "/zeitspende/";
                                break;
                        }
                        break;
                    case "Gesuch":
                        $case = "Folgendes Gesuch wurde gemeldet:";
                        switch($redirect_case) {
                            case "thing":
                                $case_entity_url.= "/sachgesuch/";
                                break;
                            case "time":
                                $case_entity_url.= "/zeitgesuch/";
                                break;
                        }
                        break;
                }

                $case_entity_url.= $entity->url_title;
                $mail_content = str_replace("{{case_entity_url}}", $case_entity_url, $mail_content);
                $mail_content = str_replace("{{case}}", $case, $mail_content);

                $email = new email('default');
                $email->helpers(array('Html', 'Text'));
                $email->viewvars(
                    array(
                        'mail_content' => $mail_content
                    )
                );
                $email->template('default');
                $email->emailformat('text');
                $email->from(array($save_report->email => $save_report->name));
                $email->to(array($configuration->support_mail => $configuration->support_big_title));
                $admins = $this->Users->find()->where(['Users.role' => 'admin'])->all();
                foreach ($admins as $admin) {
                    $email->addTo(array($admin->email => $admin->firstname . " " . $admin->lastname));
                }
                $email->subject($mail_template->template_subject . ' | ' . $entity->title);
                if ($email->send()) {
                    $this->loadModel('EmailLogs');
                    $new_email_log_entity = [
                        'send' => 1,
                        'subject' => $mail_template->template_subject . ' | ' . $entity->title,
                        'content' => $mail_content,
                        'recipient_name' => $configuration->support_big_title,
                        'recipient_email' => $configuration->support_mail,
                        'sender_name' => $save_report->name,
                        'sender_email' => $save_report->email
                    ];
                    $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                    $this->EmailLogs->save($new_email_log);
                }


                $this->Flash->success(__('Beitrag erfolgreich gemeldet.'));
            } else {
                $this->Flash->error(__('Beitrag wurde nicht gemeldet.'));
            }
            $this->request->session()->write('referer', $this->request->data['referer']);
            return $this->redirect($this->request->data['cu']);

        }
    }

    /**
     * Methode zum Finden aller Schlagwörter zu einer Spende
     *
     * @param $case
     * @param $keyword
     */
    public function keywordDonations($case, $keyword) {
        $this->set('title', $keyword . " - Spenden");

        $this->loadModel('Donations');
        $this->loadModel('Keywords');
        $this->loadModel('KeywordsDonations');
        $all_keywords = $this->Keywords->find()->select(['Keywords.id'])->where(['Keywords.keyword LIKE' => '%'.$keyword.'%']);
        $donations_ids = array();
        foreach ($all_keywords as $all_keyword) {
            $all_connections = $this->KeywordsDonations->find()->select(['KeywordsDonations.donation_id'])->where(['KeywordsDonations.keyword_id' => $all_keyword->id])->all();
            foreach ($all_connections as $connection) {
                if (isset($donations_ids[$connection->donation_id])) {
                    continue;
                }
                $donations_ids[$connection->donation_id] = $connection->donation_id;
            }
        }
        switch($case) {
            case "sach":
                $case = "thing";
                break;
            case "zeit":
                $case = "time";
                break;
        }

        $all_keywords_donations = $this->KeywordsDonations->find();
        $keywords = array();
        foreach ($all_keywords_donations as $all_keyword_donation) {
            $donation = $this->Donations->find()->where(['Donations.category' => $case, 'Donations.active' => 1, 'Donations.id' => $all_keyword_donation->donation_id])->first();
            if (!$donation) {
                continue;
            }
            $keyword = $this->Keywords->find()->select(['Keywords.keyword'])->where(['Keywords.id' => $all_keyword_donation->keyword_id])->first();

            if (isset($keywords[$keyword->keyword])) {
                $keywords[$keyword->keyword]['count'] += 1;
                continue;
            }

            $keywords[$keyword->keyword]['count'] = 1;
            $keywords[$keyword->keyword]['name'] = $keyword->keyword;
            $keywords[$keyword->keyword]['word'] = ucfirst($this->Helper->replaceCharsBackwards($keyword->keyword));
        }

        $this->loadModel('DeletedWords');
        foreach ($keywords as $key => $keyword) {
            $deleted_word = $this->DeletedWords->find()->where(['DeletedWords.keyword' => $keyword['name'], 'DeletedWords.deleted_count >' => 4])->first();
            if ($deleted_word) {
                unset($keywords[$key]);
            }
        }

        $count = array();
        $name = array();
        foreach ($keywords as $key => $row) {
            $count[$key] = $row['count'];
            $name[$key] = $row['name'];
        }

        $keywords = array_multisort($count, SORT_DESC, $name, SORT_ASC, $keywords);

        $this->set('keywords', $keywords);

        $this->set('donations', $this->Donations->find()->contain(['FileUploads', 'TimeDonations'])->where(['Donations.id IN' => $donations_ids, 'Donations.category' => $case])->all());
        $this->set('case', $case);
    }

    /**
     * Methode zum Finden aller Schlagwörter zu einem Gesuch
     *
     * @param $case
     * @param $keyword
     */
    public function keywordSearches($case, $keyword) {
        $this->set('title', $keyword . " - Spenden");

        $this->loadModel('Keywords');
        $this->loadModel('KeywordsSearches');
        $this->loadModel('Searches');
        $all_keywords = $this->Keywords->find()->select(['Keywords.id'])->where(['Keywords.keyword LIKE' => '%'.$keyword.'%']);
        $searches_ids = array();
        foreach ($all_keywords as $all_keyword) {
            $all_connections = $this->KeywordsSearches->find()->select(['KeywordsSearches.search_id'])->where(['KeywordsSearches.keyword_id' => $all_keyword->id])->all();
            foreach ($all_connections as $connection) {
                if (isset($searches_ids[$connection->search_id])) {
                    continue;
                }
                $searches_ids[$connection->search_id] = $connection->search_id;
            }
        }
        switch($case) {
            case "sach":
                $case = "thing";
                break;
            case "zeit":
                $case = "time";
                break;
        }

        $all_keywords_searches = $this->KeywordsSearches->find();
        $keywords = array();
        foreach ($all_keywords_searches as $all_keyword_search) {
            $search = $this->Searches->find()->contain(['FileUploads', 'TimeSearches'])->where(['Searches.category' => $case, 'Searches.active' => 1, 'Searches.id' => $all_keyword_search->search_id])->first();
            if (!$search) {
                continue;
            }
            $keyword = $this->Keywords->find()->select(['Keywords.keyword'])->where(['Keywords.id' => $all_keyword_search->keyword_id])->first();

            if (isset($keywords[$keyword->keyword])) {
                $keywords[$keyword->keyword]['count'] += 1;
                continue;
            }

            $keywords[$keyword->keyword]['count'] = 1;
            $keywords[$keyword->keyword]['name'] = $keyword->keyword;
            $keywords[$keyword->keyword]['word'] = ucfirst($this->Helper->replaceCharsBackwards($keyword->keyword));
        }

        $this->loadModel('DeletedWords');
        foreach ($keywords as $key => $keyword) {
            $deleted_word = $this->DeletedWords->find()->where(['DeletedWords.keyword' => $keyword['name'], 'DeletedWords.deleted_count >' => 4])->first();
            if ($deleted_word) {
                unset($keywords[$key]);
            }
        }

        $count = array();
        $name = array();
        foreach ($keywords as $key => $row) {
            $count[$key] = $row['count'];
            $name[$key] = $row['name'];
        }

        array_multisort($count, SORT_DESC, $name, SORT_ASC, $keywords);

        $this->set('keywords', $keywords);

        $this->set('searches', $this->Searches->find()->contain(['FileUploads', 'TimeSearches'])->where(['Searches.id IN' => $searches_ids, 'Searches.category' => $case])->all());
        $this->set('case', $case);
    }

    /**
     * Methode zur Auflistung von Einrichtungen
     */
    public function institutions() {
        $this->loadModel('Institutions');
        $this->loadModel('FileUploads');

        $this->set('title', __('Einrichtungen'));

        $institutions = $this->Institutions->find()->where(['Institutions.configuration_id' => $this->request->session()->read('configuration_id')]);
        $institutions_count = $institutions->count();
        if ($institutions && $institutions_count>0) {
            $institutions_array = array();

            foreach ($institutions as $institution) {
                $file_upload = $this->FileUploads->find()->where(['FileUploads.institution_id' => $institution->id])->first();
                $institution->file_upload = $file_upload;
                $institutions_array[$institution->id] = $institution;
            }
            $this->set('institutions', $institutions_array);
        }
    }

    /**
     * Methode zum Versenden einer Interessensanfrage
     *
     * @return \Cake\Network\Response|null
     */
    public function interest() {
        if ($this->request->is(['post', 'put', 'patch'])) {
            $configuration = $this->Helper->__returnConfiguration();

            $request_array = [
                'message' => trim($this->request->data['message']),
                'name' => trim($this->request->data['name']),
                'email' => trim($this->request->data['email']),
                'phone' => trim($this->request->data['phone']),
            ];

            if ($this->Auth->user('id')>0) {
                /**
                 * Aktuellen Benutzer holen
                 */
                $user = $this->Helper->__returnUser($this->Auth->user('id'));
            }

            /**
             * E-Mail-Template holen
             */
            $mail_template = $this->Helper->__returnEmailTemplate('interesse_senden');

            $link = $configuration->host;
            $this->loadModel('Donations');
            $donation = $this->Donations->find()->where(['Donations.id' => base64_decode($this->request->data['donation'])])->first();
            switch ($donation->category) {
                case "thing":
                    $link.= "/sachspende/";
                    break;
                case "time":
                    $link.= "/zeitspende/";
                    break;
            }
            $link.= $donation->url_title;

            /**
             * Vermittler holen
             */
            $broker = $this->Helper->__returnUser(base64_decode($this->request->data['broker_id']));

            $mail_content = $mail_template->template_content;
            $mail_content = str_replace("{{name}}", $request_array['name'], $mail_content);
            $mail_content = str_replace("{{message}}", $request_array['message'], $mail_content);
            $mail_content = str_replace("{{link}}", $link, $mail_content);
            $mail_content = str_replace("{{host}}", $configuration->host, $mail_content);
            $mail_content = str_replace("{{email}}", $request_array['email'], $mail_content);
            $mail_content = str_replace("{{phone}}", $request_array['phone'], $mail_content);
            $mail_content = str_replace("{{receiver_name}}", $broker->firstname, $mail_content);

            /**
             * Neue E-Mail vorbereiten
             */
            $email_data = [
                'type' => 'default',
                'mail_content' => $mail_content,
                'helpers' => ['Html', 'Text'],
                'template' => 'default',
                'emailFormat' => 'text',
                'sender' => [$configuration->from_mail => $configuration->from_big_title],
                'receiver' => [$broker->email => $broker->firstname . " " . $broker->lastname],
                'subject' => $mail_template->template_subject
            ];
            $email = $this->Helper->__sendEmail($email_data);

            if ($email) {

                /*
                $this->loadModel('EmailLogs');
                $new_email_log_entity = [
                    'send' => 1,
                    'subject' => $mail_subject,
                    'content' => $mail_content,
                    'recipient_name' => $broker->firstname . " " . $broker->lastname,
                    'recipient_email' => $broker->email,
                    'sender_name' => $configuration->from_big_title,
                    'sender_email' => $configuration->from_mail
                ];
                $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                $this->EmailLogs->save($new_email_log);
                */

                if (isset($this->request->data['donation'])) {
                    $donation = $this->Donations->find()->where(['Donations.id' => base64_decode($this->request->data['donation'])])->first();
                    $new_report_entity['donation_id'] = base64_decode($this->request->data['donation']);
                    $log_array['donation_id'] = base64_decode($this->request->data['donation']);

                    $log_array = [
                        'user_id' => ($this->Auth->user('id') > 0) ? $this->Auth->user('id') : NULL,
                        'donation_id' => base64_decode($this->request->data['donation']),
                        'log_title' => 'Interesse gesendet',
                        'log_text' => trim($this->request->data['name']) . " hat Interesse angemerkt. Benutzer: " . isset($user) && $user->id > 0 ? $user->id : "Nicht gesetzt",
                    ];

                }

                $this->Helper->__log($log_array);
                $this->Flash->success(__('Nachricht erfolgreich gesendet.'));
                $this->request->session()->write('referer', $this->request->data['referer']);
                return $this->redirect($this->request->data['cu']);
            } else {
                $this->Flash->error(__('Nachricht nicht gesendet.'));
                $this->request->session()->write('referer', $this->request->data['referer']);
                return $this->redirect($this->request->data['cu']);
            }
        }
    }

    /**
     * Methode die Keywords einer Spende in die Session schreibt
     *
     * @param $id
     */
    public function prepareDonation($id) {
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');

        $this->loadModel('Searches');
        $search = $this->Searches->find()->select(['Searches.id'])->where(['Searches.id' => $id])->first();

        $this->loadModel('KeywordsSearches');
        $search_keyword_ids = $this->KeywordsSearches->find()->where(['KeywordsSearches.search_id' => $search->id])->all();

        $tags = "";

        $this->loadModel('Keywords');
        foreach ($search_keyword_ids as $search_keyword_id) {
            $keyword = $this->Keywords->find()->where(['Keywords.id' => $search_keyword_id->keyword_id])->first();
            if (strlen($tags)>0) {
                if (stripos($tags, $keyword->keyword) !== false) {
                    continue;
                }
            }
            $tags.=",".$keyword->keyword;
        }

        if (sizeof($tags)>0) {
            $this->request->session()->write('donation_keywords', $tags);
            $this->request->session()->write('donation_for_search', true);

            $response = [
                'status' => 'success',
                'target' => '/spende_abgeben'
            ];
        } else {
            $response = [
                'status' => 'failed',
            ];
        }

        $this->set('response', $response);
        $this->render('response');

    }

    /**
     * Methode die in der Session gespeicherte Schlagwörter liefert
     */
    public function checkKeywords() {
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');

        if ($this->request->session()->read('donation_for_search')==true) {
            $tags = $this->request->session()->read('donation_keywords');

            $response = [
                'status' => 'success',
                'keywords' => $tags
            ];
        } else {
            $response = [
                'status' => 'failed',
            ];
        }
        $this->set('response', $response);
        $this->render('response');
    }

    /**
     * Methode zum Speichern von Schlagwörtern (einer Spende) in der Session
     */
    public function writeSessionKeywords() {
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');

        // TODO: undefined index 'keywords_string'
        if (isset($this->request->data['keywords_string']) && strlen($this->request->data['keywords_string'])>0) {
            $this->request->session()->write('keywords_string', $this->request->data['keywords_string']);
        }
        $response = [
            'status' => 'success',
        ];
        $this->set('response', $response);
        $this->render('response');
    }

    /**
     * Methode zum Versenden einer Kontaktanfrage
     *
     * @return \Cake\Network\Response|null
     */
    public function contact() {
        if ($this->request->is(['post', 'put', 'patch'])) {
            $configuration = $this->Configurations->get($this->request->session()->read('configuration_id'));

            $mail_template = $this->Helper->__returnEmailTemplate('kontakt_aufnehmen');

            $this->loadModel('Users');
            $user = $this->Helper->__returnUser($this->request->data['receiver_user_id']);
            $sender_user = $this->Helper->__returnUser($this->Auth->user('id'));

            if (!$user || !$sender_user) {
                Log::debug("HelpersController::contact - Senden fehlgeschlagen: " . $this->request->data['receiver_user_id'] . " | " . $this->Auth->user('id'));
                $this->Flash->error(__('Nachricht senden fehlgeschlagen.'));
                return $this->redirect($this->referer());
            }

            $mail_content = $mail_template->template_content;
            $mail_content = str_replace("{{new_broker_name}}", $user->firstname . " " . $user->lastname, $mail_content);
            $mail_content = str_replace("{{sender_name}}", $sender_user->firstname . " " . $sender_user->lastname, $mail_content);
            $mail_content = str_replace("{{sender_email}}", $sender_user->email, $mail_content);
            $mail_content = str_replace("{{message}}", trim($this->request->data['message']), $mail_content);
            $mail_content = str_replace("{{host}}", $configuration->host, $mail_content);

            /**
             * Neue E-Mail vorbereiten
             */
            $email_data = [
                'type' => 'default',
                'mail_content' => $mail_content,
                'helpers' => ['Html', 'Text'],
                'template' => 'default',
                'emailFormat' => 'text',
                'sender' => [$configuration->from_mail => $configuration->from_big_title],
                'receiver' => [$user->email => $user->firstname . " " . $user->lastname],
                'subject' => $mail_template->template_subject
            ];

            $email = $this->Helper->__sendEmail($email_data);

            if ($email) {

                /**
                 * Email-Log speichern
                 */
                $new_email_log_entity = [
                    'send' => 1,
                    'subject' => $mail_template->template_subject,
                    'content' => $mail_content,
                    'recipient_name' => $user->firstname . " " . $user->lastname,
                    'recipient_email' => $user->email,
                    'sender_name' => $configuration->from_big_title,
                    'sender_email' => $configuration->from_mail
                ];
                $this->Helper->__saveEmailLog($new_email_log_entity);

                $log_array = [
                    'user_id' => ($this->Auth->user('id') > 0) ? $this->Auth->user('id') : NULL,
                    'log_title' => 'Kontakt aufgenommen',
                    'log_text' => $sender_user->firstname . " " . $sender_user->lastname . " hat eine E-Mail an " . $user->firstname . " " . $user->lastname . " gesendet.",
                ];

                $this->Helper->__log($log_array);
                $this->Flash->success(__('Nachricht erfolgreich gesendet.'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Nachricht nicht gesendet.'));
                return $this->redirect($this->referer());
            }
        }
    }

    /**
     * Methode zum Bearbeiten einer Spende
     *
     * @param null $donation_token
     * @return \Cake\Network\Response|null
     */
    public function edit($donation_token = null) {
        $this->set('title', 'Spende bearbeiten');

        $donation_token_array = explode('|', base64_decode($donation_token));

        $this->loadModel('Donations');
        $donation = $this->Donations->find()->where(['Donations.id' => $donation_token_array[0]])->first();
        if (!$donation) {
            $this->Flash->error(__('Spende wurde nicht gefunden.'));
            return $this->redirect($this->referer());
        }

        if ($this->request->is(['post', 'put', 'patch'])) {
            $this->loadModel('FileUploads');
            if (isset($this->request->data['uploads']) && !empty($this->request->data['uploads']) &&
                strlen($this->request->data['uploads'][0]['name'])>0 && $this->request->data['uploads'][0]['size']>0) {
                if (!isset($this->request->data['rights'])) {
                    $this->Flash->error(__('Rechte an den Bildern bestätigen.'));
                    return $this->redirect($this->referer());
                }
                foreach ($this->request->data['uploads'] as $upload) {
                    $file_upload = $this->FileUpload->process($upload);

                    $new_fileupload_entity = [
                        'donation_id' => $donation->id,
                        'src' => $file_upload,
                        'type' => $upload['type'],
                        'filename' => $upload['name'],
                        'size' => $upload['size']
                    ];

                    $fileupload_entity = $this->FileUploads->newEntity($new_fileupload_entity);
                    $this->FileUploads->save($fileupload_entity);

                }
            }
            if (isset($this->request->data['upload_id1'])) {
                for ($id_counter=1;$id_counter<10;$id_counter++) {
                    if (isset($this->request->data['upload_id'.$id_counter]) && is_numeric($this->request->data['upload_id'.$id_counter]) && $this->request->data['upload_id'.$id_counter]>0) {
                        $file_upload = $this->FileUploads->find()->where(['FileUploads.id' => $this->request->data['upload_id'.$id_counter]])->first();
                        if (!$file_upload) {
                            continue;
                        }
                        @unlink($file_upload->src);
                        $this->FileUploads->delete($file_upload);
                    } else {
                        continue;
                    }
                }
            }

            $patch_entity = $this->Donations->patchEntity($donation, $this->request->data);
            if ($this->Donations->save($patch_entity)) {
                $this->Flash->success(__('Änderungen gespeichert.'));
            } else {
                $this->Flash->error(__('Änderungen nicht gespeichert.'));
            }
            return $this->redirect(['action' => 'manage', base64_encode('spender_spenden')."|"]);
        }
        $this->loadModel('Addresses');
        $address = $this->Addresses->find()->where(['Addresses.id' => $donation->address_id])->first();
        if ($address) {
            $this->set('address', $address);
        }
        $uploads = array();
        $donation->uploads = $uploads;
        $this->set('donation', $donation);

        if ($donation->has('category') && $donation->category == 'time') {
            $this->loadModel('TimeDonations');
            $donation_times = $this->TimeDonations->find()->where(['TimeDonations.donation_id' => $donation->id])->all();
            if ($donation_times) {
                $this->set('donation_times', $donation_times);
            }
        }

        $this->loadModel('FileUploads');
        $donation_uploads = $this->FileUploads->find()->where(['FileUploads.donation_id' => $donation->id])->all();
        if ($donation_uploads) {
            $this->set('donation_uploads', $donation_uploads);
        }

    }

    /**
     * Methode zur Auflistung
     * @param $base64_param
     */
    public function items($base64_param)
    {
        $base64_decoded = explode("|", base64_decode($base64_param));
        $type = $base64_decoded[0];
        $case = $base64_decoded[1];
        $page = $this->request->session()->read('page');
        $start_end_type = $this->request->session()->read('start_end_type');
        $start_end_id = $this->request->session()->read('start_end_id');

        $additional_donation_branch_filter = "";
        $additional_keyword_branch_filter = "";
        $additional_search_branch_filter = "";
        $additional_deletedword_branch_filter = "";
        if ($this->request->session()->read('configuration_id')>1) {
            $additional_donation_branch_filter = "Donations.configuration_id = " . $this->request->session()->read('configuration_id');
            $additional_search_branch_filter = "Searches.configuration_id = " . $this->request->session()->read('configuration_id');
            $additional_keyword_branch_filter = "Keywords.configuration_id = " . $this->request->session()->read('configuration_id');
            $additional_deletedword_branch_filter = "DeletedWords.configuration_id = " . $this->request->session()->read('configuration_id');
        }

        switch($case) {
            case "time":
            case "thing":
                $filter = $case;
                break;
            case "all":
                break;
        }

        switch($type) {
            /**
             * Spenden
             */
            case "donation":
                $this->loadModel('DeletedWords');
                $this->loadModel('Donations');
                $this->loadModel('FileUploads');
                $this->loadModel('Keywords');
                $this->loadModel('KeywordsDonations');
                $this->loadModel('TimeDonations');
                $this->loadModel('Users');
                $this->loadModel('Users.Addresses');
                switch($case) {
                    case "time":
                        $this->set('title', 'Zeitspenden');
                        $this->set('case', 'zeit');
                        $this->set('type', 'donation');
                        break;
                    case "thing":
                        $this->set('title', 'Sachspenden');
                        $this->set('case', 'sach');
                        $this->set('type', 'donation');
                        break;
                    case "all":
                        $this->set('title', 'Alle Spenden');
                        $this->set('case', 'alle');
                        $this->set('type', 'donation');
                        break;
                }
                if (isset($page) && $page>1) {
                    if (isset($start_end_type) &&
                        strlen($start_end_type)>0 &&
                        isset($start_end_id) &&
                        $start_end_id>0) {
                        switch($start_end_type) {
                            case "next":
                                if (isset($filter) && strlen($filter)>0) {
                                    $donations = $this->Donations
                                        ->find()
                                        ->contain(['FileUploads', 'TimeDonations', 'Users', 'Users.Addresses'])
                                        ->where(['Donations.active' => 1, 'Donations.id <' => $start_end_id, $additional_donation_branch_filter])
                                        ->andWhere(['Donations.category' => $filter])
                                        ->order(['Donations.id' => 'DESC'])
                                        ->limit(10);
                                } else {
                                    $donations = $this->Donations
                                        ->find()
                                        ->contain(['FileUploads', 'TimeDonations', 'Users', 'Users.Addresses'])
                                        ->where(['Donations.active' => 1, 'Donations.id <' => $start_end_id, $additional_donation_branch_filter])
                                        ->order(['Donations.id' => 'DESC'])
                                        ->limit(10);
                                }
                                if (isset($filter) && strlen($filter)>0) {
                                    $total_donations = $this->Donations
                                        ->find()
                                        ->where(['Donations.active' => 1, 'Donations.id <' => $start_end_id, $additional_donation_branch_filter])
                                        ->andWhere(['Donations.category' => $filter])
                                        ->count();
                                } else {
                                    $total_donations = $this->Donations
                                        ->find()
                                        ->where(['Donations.active' => 1, 'Donations.id <' => $start_end_id, $additional_donation_branch_filter])
                                        ->count();
                                }
                                break;
                            case "last":
                                if (isset($filter) && strlen($filter)>0) {
                                    $donations = $this->Donations
                                        ->find()
                                        ->contain(['FileUploads', 'TimeDonations', 'Users', 'Users.Addresses'])
                                        ->where(['Donations.active' => 1, 'Donations.id >' => $start_end_id, $additional_donation_branch_filter])
                                        ->andWhere(['Donations.category' => $filter])
                                        ->order(['Donations.id' => 'DESC'])
                                        ->limit(10);
                                } else {
                                    $donations = $this->Donations
                                        ->find()
                                        ->contain(['FileUploads', 'TimeDonations', 'Users', 'Users.Addresses'])
                                        ->where(['Donations.active' => 1, 'Donations.id >' => $start_end_id, $additional_donation_branch_filter])
                                        ->order(['Donations.id' => 'DESC'])
                                        ->limit(10);
                                }
                                if (isset($filter) && strlen($filter)>0) {
                                    $total_donations = $this->Donations
                                        ->find()
                                        ->where(['Donations.active' => 1, 'Donations.id >' => $start_end_id, $additional_donation_branch_filter])
                                        ->andWhere(['Donations.category' => $filter])
                                        ->count();
                                } else {
                                    $total_donations = $this->Donations
                                        ->find()
                                        ->where(['Donations.active' => 1, 'Donations.id >' => $start_end_id, $additional_donation_branch_filter])
                                        ->count();
                                }
                                break;
                        }
                    }
                    $this->set('page', $page);
                } else {
                    $this->set('page', 1);
                    if (isset($filter) && strlen($filter)>0) {
                        $donations = $this->Donations
                            ->find()
                            ->contain(['FileUploads', 'Users', 'Users.Addresses'])
                            ->where(['Donations.active' => 1, $additional_donation_branch_filter])
                            ->andWhere(['Donations.category' => $filter])
                            ->order(['Donations.id' => 'DESC'])
                            ->limit(10);
                    } else {
                        $donations = $this->Donations
                            ->find()
                            ->contain(['FileUploads', 'Users', 'Users.Addresses'])
                            ->where(['Donations.active' => 1, $additional_donation_branch_filter])
                            ->order(['Donations.id' => 'DESC'])
                            ->limit(10);
                    }
                    if (isset($filter) && strlen($filter)>0) {
                        $total_donations = $this->Donations
                            ->find()
                            ->where(['Donations.active' => 1, $additional_donation_branch_filter])
                            ->andWhere(['Donations.category' => $filter])
                            ->count();
                    } else {
                        $total_donations = $this->Donations
                            ->find()
                            ->where(['Donations.active' => 1, $additional_donation_branch_filter])
                            ->count();
                    }
                }
                $this->set('total_items', $total_donations);
                $this->set('items', $donations);
                $all_keywords_donations = $this->KeywordsDonations->find();
                $keywords = array();
                foreach ($all_keywords_donations as $all_keyword_donation) {
                    if (isset($filter) && strlen($filter)>0) {
                        $donation = $this->Donations
                            ->find()
                            ->where(['Donations.active' => 1, 'Donations.id' => $all_keyword_donation->donation_id, $additional_donation_branch_filter])
                            ->andWhere(['Donations.category' => $filter])
                            ->first();
                    } else {
                        $donation = $this->Donations
                            ->find()
                            ->where(['Donations.active' => 1, 'Donations.id' => $all_keyword_donation->donation_id, $additional_donation_branch_filter])
                            ->first();
                    }
                    if (!$donation) {
                        continue;
                    }
                    $keyword = $this->Keywords
                        ->find()
                        ->select(['Keywords.keyword'])
                        ->where(['Keywords.id' => $all_keyword_donation->keyword_id, $additional_keyword_branch_filter])
                        ->first();

                    if (isset($keyword) && $keyword) {
                        if (isset($keywords[$keyword->keyword])) {
                            $keywords[$keyword->keyword]['count'] += 1;
                            continue;
                        }

                        $keywords[$keyword->keyword]['count'] = 1;
                        $keywords[$keyword->keyword]['name'] = $keyword->keyword;
                        $keywords[$keyword->keyword]['word'] = ucfirst($this->Helper->replaceCharsBackwards($keyword->keyword));
                    }
                }

                if (isset($keywords) && $keywords) {
                    foreach ($keywords as $key => $keyword) {
                        $deleted_word = $this->DeletedWords
                            ->find()
                            ->where(['DeletedWords.keyword' => $keyword['name'], 'DeletedWords.deleted_count >' => 4, $additional_deletedword_branch_filter])
                            ->first();
                        if ($deleted_word) {
                            unset($keywords[$key]);
                        }
                    }

                    $count = array();
                    $name = array();
                    foreach ($keywords as $key => $row) {
                        $count[$key] = $row['count'];
                        $name[$key] = $row['name'];
                    }

                    array_multisort($count, SORT_DESC, $name, SORT_ASC, $keywords);
                    array_multisort($count, SORT_DESC, $name, SORT_ASC, $keywords);

                    $this->set('keywords', $keywords);
                }
                break;
            /**
             * Gesuche
             */
            case "search":
                $this->loadModel('DeletedWords');
                $this->loadModel('FileUploads');
                $this->loadModel('Keywords');
                $this->loadModel('KeywordsSearches');
                $this->loadModel('Searches');
                $this->loadModel('TimeSearches');
                $this->loadModel('Users');
                switch($case) {
                    case "time":
                        $this->set('title', 'Zeitgesuche');
                        $this->set('case', 'zeit');
                        $this->set('type', 'search');
                        break;
                    case "thing":
                        $this->set('title', 'Sachgesuche');
                        $this->set('case', 'sach');
                        $this->set('type', 'search');
                        break;
                    case "all":
                        $this->set('title', 'Alle Gesuche');
                        $this->set('case', 'alle');
                        $this->set('type', 'search');
                        break;
                }
                if (isset($page) && $page>1) {
                    if (isset($start_end_type) &&
                        strlen($start_end_type)>0 &&
                        isset($start_end_id) &&
                        $start_end_id>0) {
                        switch($start_end_type) {
                            case "next":
                                if (isset($filter) && strlen($filter)>0) {
                                    $searches = $this->Searches->find()->contain(['FileUploads', 'TimeSearches', 'Users'])->where(['Searches.category' => $filter, 'Searches.active' => 1, 'Searches.id <' => $start_end_id, $additional_search_branch_filter])->order(['Searches.id' => 'DESC'])->limit(10);
                                    $total_searches = $this->Searches->find()->where(['Searches.category' => $filter, 'Searches.active' => 1, 'Searches.id <' => $start_end_id, $additional_search_branch_filter])->count();
                                } else {
                                    $searches = $this->Searches->find()->contain(['FileUploads', 'TimeSearches', 'Users'])->where(['Searches.active' => 1, 'Searches.id <' => $start_end_id, $additional_search_branch_filter])->order(['Searches.id' => 'DESC'])->limit(10);
                                    $total_searches = $this->Searches->find()->where(['Searches.active' => 1, 'Searches.id <' => $start_end_id, $additional_search_branch_filter])->count();
                                }
                                break;
                            case "last":
                                if (isset($filter) && strlen($filter)>0) {
                                    $searches = $this->Searches->find()->contain(['FileUploads', 'TimeSearches', 'Users'])->where(['Searches.category' => 'time', 'Searches.active' => 1, 'Searches.id >' => $start_end_id, $additional_search_branch_filter])->order(['Searches.id' => 'DESC'])->limit(10);
                                    $total_searches = $this->Searches->find()->where(['Searches.category' => 'time', 'Searches.active' => 1, 'Searches.id >' => $start_end_id, $additional_search_branch_filter])->count();
                                } else {
                                    $searches = $this->Searches->find()->contain(['FileUploads', 'TimeSearches', 'Users'])->where(['Searches.active' => 1, 'Searches.id >' => $start_end_id, $additional_search_branch_filter])->order(['Searches.id' => 'DESC'])->limit(10);
                                    $total_searches = $this->Searches->find()->where(['Searches.active' => 1, 'Searches.id >' => $start_end_id, $additional_search_branch_filter])->count();
                                }
                                break;
                        }
                    }
                    $this->set('page', $page);
                } else {
                    $this->set('page', 1);
                    if (isset($filter) && strlen($filter)>0) {
                        $searches = $this->Searches->find()->contain(['FileUploads', 'TimeSearches', 'Users'])->where(['Searches.category' => $filter, 'Searches.active' => 1, $additional_search_branch_filter])->order(['Searches.id' => 'DESC'])->limit(10);
                        $total_searches = $this->Searches->find()->where(['Searches.category' => $filter, 'Searches.active' => 1, $additional_search_branch_filter])->count();
                    } else {
                        $searches = $this->Searches->find()->contain(['FileUploads', 'TimeSearches', 'Users'])->where(['Searches.active' => 1, $additional_search_branch_filter])->order(['Searches.id' => 'DESC'])->limit(10);
                        $total_searches = $this->Searches->find()->where(['Searches.active' => 1, $additional_search_branch_filter])->count();
                    }
                }
                $this->set('total_items', $total_searches);
                $this->set('items', $searches);
                $all_keywords_searches = $this->KeywordsSearches->find();
                $keywords = array();

                if (isset($all_keywords_searches) && $all_keywords_searches) {
                    foreach ($all_keywords_searches as $all_keyword_search) {
                        if (isset($filter) && strlen($filter) > 0) {
                            $search = $this->Searches->find()->where(['Searches.category' => $filter, 'Searches.active' => 1, 'Searches.id' => $all_keyword_search->search_id, $additional_search_branch_filter])->first();

                        } else {
                            $search = $this->Searches->find()->where(['Searches.active' => 1, 'Searches.id' => $all_keyword_search->search_id, $additional_search_branch_filter])->first();
                        }
                        if (!$search) {
                            continue;
                        }
                        $keyword = $this->Keywords->find()->select(['Keywords.keyword'])->where(['Keywords.id' => $all_keyword_search->keyword_id, $additional_keyword_branch_filter])->first();

                        if (isset($keywords[$keyword->keyword])) {
                            $keywords[$keyword->keyword]['count'] += 1;
                            continue;
                        }

                        $keywords[$keyword->keyword]['count'] = 1;
                        $keywords[$keyword->keyword]['name'] = $keyword->keyword;
                        $keywords[$keyword->keyword]['word'] = ucfirst($this->Helper->replaceCharsBackwards($keyword->keyword));
                    }

                    foreach ($keywords as $key => $keyword) {
                        $deleted_word = $this->DeletedWords->find()->where(['DeletedWords.keyword' => $keyword['name'], 'DeletedWords.deleted_count >' => 4, $additional_deletedword_branch_filter])->first();
                        if ($deleted_word) {
                            unset($keywords[$key]);
                        }
                    }

                    $count = array();
                    $name = array();
                    foreach ($keywords as $key => $row) {
                        $count[$key] = $row['count'];
                        $name[$key] = $row['name'];
                    }

                    array_multisort($count, SORT_DESC, $name, SORT_ASC, $keywords);

                    $this->set('keywords', $keywords);
                }
                break;
        }
    }

    /**
     * Methode zur Darstellung eines Elements
     * @param $base64_param
     * @param bool $encoded_bool
     */
    public function item($base64_param, $encoded_bool = false)
    {
        $base64_decoded = explode("|", base64_decode($base64_param));
        if (!isset($base64_decoded[0]) || !isset($base64_decoded[1])) {
            $item = $this->Helper->__returnItem($base64_param);
            if ($item && $item != false) {
                $type = $item->type;
                $case = $item->category;
                $url_title = $item->url_title;
            } else {
                return $this->redirect('/');
            }
        } else {
            $type = $base64_decoded[0];
            $case = $base64_decoded[1];
            $url_title = $base64_decoded[2];
        }
        switch($case) {
            case "time":
            case "thing":
                $filter = $case;
                break;
        }

        /**
         * Falls Branch gesetzt ist, Branch-Filter setzen
         */
        $additional_donation_branch_filter = "";
        $additional_search_branch_filter = "";
        if ($this->request->session()->read('configuration_id')>1) {
            $additional_donation_branch_filter = "Donations.configuration_id = " . $this->request->session()->read('configuration_id');
            $additional_search_branch_filter = "Searches.configuration_id = " . $this->request->session()->read('configuration_id');
        }

        switch($type) {
            /**
             * Spenden
             */
            case "donation":
                $case_old = $case;
                switch($case) {
                    /**
                     * Zeitspenden
                     */
                    case "time":
                        $this->set('case', 'zeit');
                        $this->set('type', 'donation');
                        break;
                    /**
                     * Sachspenden
                     */
                    case "thing":
                        $this->set('case', 'sach');
                        $this->set('type', 'donation');
                        break;
                }
                /**
                 * Tabellen laden
                 */
                $this->loadModel('Addresses');
                $this->loadModel('Donations');
                $this->loadModel('FileUploads');
                $this->loadModel('TimeDonations');
                $this->loadModel('Users');
                if (isset($filter) && strlen($filter)>0) {
                    $donation = $this->Donations
                        ->find()
                        ->contain(['FileUploads', 'TimeDonations', 'Users', 'Users.Addresses'])
                        ->where(['Donations.category' => $case_old, 'Donations.active' => 1, 'Donations.url_title' => $url_title, $additional_donation_branch_filter])
                        ->andWhere(['Donations.category' => $filter])
                        ->first();
                } else {
                    $donation = $this->Donations
                        ->find()
                        ->contain(['FileUploads', 'TimeDonations', 'Users', 'Users.Addresses'])
                        ->where(['Donations.category' => $case_old, 'Donations.active' => 1, 'Donations.url_title' => $url_title, $additional_donation_branch_filter])
                        ->first();
                }
                if (!$donation) {
                    $this->Flash->error(__('Diese Spende existiert nicht.'));
                    return $this->redirect($this->referer());
                }
                if ($this->__checkAccess($donation->id)) {
                    $contact = $this->Helper->__returnAddress($donation->address_id);
                    $contact_user = $this->Helper->__returnUser($donation->user_id);
                    if ($contact && $contact_user) {
                        $contact_array = [
                            'email' => $contact_user->email,
                            'firstname' => $contact_user->firstname,
                            'lastname' => $contact_user->lastname,
                            'phone' => $contact_user->phone,
                            'fax' => $contact_user->fax,
                            'street' => $contact->street,
                            'zip' => $contact->zip,
                            'city' => $contact->city
                        ];
                        $this->set('contact_array', $contact_array);
                    }
                } else {
                    if (isset($filter) && strlen($filter)>0) {
                        $donation = $this->Donations
                            ->find()
                            ->contain(['FileUploads', 'Users'])
                            ->where(['Donations.category' => $case_old, 'Donations.url_title' => $url_title, $additional_donation_branch_filter])
                            ->andWhere(['Donations.category' => $filter])
                            ->first();
                    } else {
                        $donation = $this->Donations
                            ->find()
                            ->contain(['FileUploads', 'Users'])
                            ->where(['Donations.category' => $case_old, 'Donations.url_title' => $url_title, $additional_donation_branch_filter])
                            ->first();
                    }
                    $contact = $this->Helper->__returnAddress($donation->address_id);
                    if ($contact) {
                        $contact_array = [
                            'zip' => $contact->zip,
                            'city' => $contact->city
                        ];
                        $this->set('contact_array', $contact_array);
                    }
                }
                /**
                 * Titel setzen
                 */
                $this->set('title', 'Sachspende - ' . $donation->title);
                /**
                 * Spende setzen
                 */
                $this->set('donation', $donation);
                $donation_image = $this->FileUploads
                    ->find()
                    ->where(['FileUploads.donation_id' => $donation->id])
                    ->first();
                if ($donation_image) {
                    $this->set('donation_image', $donation_image);
                    $count_donation_images = $this->FileUploads
                        ->find()
                        ->where(['FileUploads.donation_id' => $donation->id])
                        ->count();
                    if ($count_donation_images > 1) {
                        $this->set('donation_images', $this->FileUploads
                            ->find()
                            ->where(['FileUploads.donation_id' => $donation->id]));
                    }
                }

                /**
                 * Prüfung ob Spender = aktueller Benutzer ist
                 */
                if ($this->Auth->user('id') > 0 &&
                    $donation->user->id == $this->Auth->user('id')
                ) {
                    $this->set('own_donation', true);
                } else {
                    $this->set('own_donation', false);
                }

                if ($this->Auth->user('id') > 0 && $donation->user->id != $this->Auth->user('id')) {
                    $this->set('contact_without_mail', false);
                } else {
                    if (isset($donation->user) && $donation->user->email == null && $donation->user->has('phone') && strlen($donation->user->phone) > 0) {
                        $this->set('contact_without_mail', true);
                    } else {
                        $this->set('contact_without_mail', true);
                    }
                }

                if ($this->Auth->user('id') > 0) {
                    $this->set('current_user', $this->Users->get($this->Auth->user('id')));
                }

                /**
                 * Verwandte Spenden finden
                 */
                $related_donation_ids = $this->Helper->__getRelatedDonations($donation->id);
                if ($related_donation_ids) {
                    $this->set('related_donations', $this->Donations
                        ->find()
                        ->contain(['FileUploads'])
                        ->where(['Donations.id IN' => $related_donation_ids, $additional_donation_branch_filter])
                        ->limit(3));
                }

                if (strlen($this->request->session()->read('referer')) > 0) {
                    $this->set('referer_for_button', $this->request->session()->read('referer'));
                    $this->request->session()->delete('referer');
                }
                break;
            /**
             * Gesuche
             */
            case "search":
                $this->loadModel('Searches');
                $case_old = $case;
                switch($case) {
                    /**
                     * Zeitgesuche
                     */
                    case "time":
                        $this->set('case', 'zeit');
                        $this->set('type', 'search');
                        break;
                    /**
                     * Sachgesuche
                     */
                    case "thing":
                        $this->set('case', 'sach');
                        $this->set('type', 'search');
                        break;
                }
                $search = $this->Searches
                    ->find()
                    ->contain(['FileUploads', 'TimeSearches', 'Users'])
                    ->where(['Searches.category' => $case_old, 'Searches.active' => 1, 'Searches.url_title' => $url_title, $additional_search_branch_filter])
                    ->first();
                if (!$search) {
                    $this->Flash->error(__('Dieses Gesuch existiert nicht.'));
                    return $this->redirect($this->referer());
                }
                if ($this->__checkAccess($search->id)) {
                    $contact = $this->Helper->__returnAddress($search->address_id);
                    $contact_user = $this->Helper->__returnUser($search->user_id);
                    if ($contact && $contact_user) {
                        $contact_array = [
                            'firstname' => $contact_user->firstname,
                            'lastname' => $contact_user->lastname,
                            'phone' => $contact_user->phone,
                            'fax' => $contact_user->fax,
                            'street' => $contact->street,
                            'zip' => $contact->zip,
                            'city' => $contact->city
                        ];
                        $this->set('contact_array', $contact_array);
                    }
                } else {
                    $contact = $this->Helper->__returnAddress($search->address_id);
                    if ($contact) {
                        $contact_array = [
                            'zip' => $contact->zip,
                            'city' => $contact->city
                        ];
                        $this->set('contact_array', $contact_array);
                    }
                }
                $this->set('title', 'Zeitgesuch - ' . $search->title);
                $this->set('search', $search);
                $search_image = $this->Helper->__returnFileUpload("search", $search->id);
                if ($search_image) {
                    $this->set('search_image', $search_image);
                    $count_search_images = $this->Helper->__returnFileUploads("search", $search->id, true);
                    if ($count_search_images || $count_search_images>1) {
                        $this->set('search_images', $this->Helper->__returnFileUploads("search", $search->id));
                    }
                }

                if ($this->Auth->user('id')>0 && $search->user_id == $this->Auth->user('id')) {
                    $this->set('own_search', true);
                } else {
                    $this->set('own_search', false);
                }

                if ($this->Auth->user('id')>0) {
                    $this->set('current_user', $this->Helper->__returnUser($this->Auth->user('id')));
                }

                if ($this->Auth->user('id')>0 && $search->user->id != $this->Auth->user('id')) {
                    $this->set('contact_without_mail', false);
                } else {
                    if ($search->user->email == null && $search->user->has('phone') && strlen($search->user->phone)>0) {
                        $this->set('contact_without_mail', true);
                    }
                }

                if (base64_decode($encoded_bool)=='true') {
                    $this->Flash->success(__('Das Gesuch wurde aufgegeben.'));
                }

                $related_search_ids = $this->Helper->__getRelatedSearches($search->id);
                if ($related_search_ids) {
                    $this->loadModel('FileUploads');
                    $this->set('related_searches', $this->Searches->find()->contain(['FileUploads'])->where(['Searches.id IN' => $related_search_ids, $additional_search_branch_filter])->limit(3));
                }

                if (strlen($this->request->session()->read('referer'))>0) {
                    $this->set('referer_for_button', $this->request->session()->read('referer'));
                    $this->request->session()->delete('referer');
                }
                break;
        }
    }

    /**
     * Methode um auf einen anderen Branch zu wechseln
     *
     * @param $branch_id
     */
    public function branchChange($branch_id) {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            /**
             * Hole Konfiguratíon für übergebene ID des Branches
             */
            $configuration = $this->Configurations->get($branch_id);
            if ($configuration) {
                /**
                 * Zielwebseite zusammensetzen
                 */
                $target_host = str_replace($_SERVER['HTTP_HOST'], $configuration->configuration_host, $_SERVER['HTTP_REFERER']);
                $response = [
                    'status' => 'success',
                    'target_host' => $target_host
                ];
            } else {
                $response = [
                    'status' => 'failed'
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     *
     * Schreibt Fehler und Meldungen in die Datenbank
     *
     */
    public function insertErrors() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $this->loadModel('Errors');

        /**
         * Error-Array vorbereiten
         */
        $new_error_array = [
            'subheading' => $this->request->data['subheading'],
            'exception_stack_trace' => $this->request->data['exception_stack_trace'],
            'file' => $this->request->data['file'],
            'templateName' => $this->request->data['templateName'],
            'exception_stack_trace_nav' => $this->request->data['exception_stack_trace_nav'],
        ];
        /**
         * Error in Datenbank speichern
         */
        $error_array = $this->Errors->newEntity($new_error_array);
        $this->Errors->save($error_array);
        $this->set('response', ['status' => 'success']);
        $this->render('response');
    }

    /**
     *
     * Schreibt Warnung in die Datenbank
     *
     */
    public function insertFails() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $this->loadModel('Fails');
        /**
         * Fehler-Array vorbereiten
         */
        $new_fail_array = [
            'user_id' => $this->Auth->user('id')>0 ? $this->Auth->user('id') : 0,
            'http_user_agent' => $this->request->data['http_user_agent'],
            'additional_vars' => $this->request->data['additional_vars'],
            'request_url' => $this->request->data['request_url'],
            'description' => $this->request->data['description'],
        ];
        /**
         * Fehler in Datenbank speichern
         */
        $fail_array = $this->Fails->newEntity($new_fail_array);
        $this->Fails->save($fail_array);
        $this->set('response', ['status' => 'success']);
        $this->render('response');
    }
}
