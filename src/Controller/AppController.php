<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Log\Log;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {

    }


    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }


        /**
         * Falls Benutzer eingeloggt
         */
        if ($this->Auth->user('id')>0) {
            /**
             * Setze Variable (Benutzer ist eingeloggt)
             */
            $this->set('logged_in', true);

            /**
             * Setze aktuelle Rolle des Benutzers als Variable
             */
            $this->set('role', $this->Auth->user('role'));

            /**
             * Falls Nutzer Rolle Vermittler besitzt
             */
            if ($this->Auth->user('role')=='broker') {

                /**
                 * Falls UsersController und register, add oder reset
                 */
                if (isset($this->request->params['controller']) &&
                    $this->request->params['controller']=='Users' &&
                    isset($this->request->params['action']) &&
                    ($this->request->params['action'] == 'register' || $this->request->params['action'] == 'add' || $this->request->params['action'] == 'reset')) {
                    return $this->redirect('/dashboard');
                }


                /**
                 * Falls Benutzer einfach Benutzer (Spender)
                 */
            } else if ($this->Auth->user('role')=='token_account') {
                /**
                 * Falls UsersController und register oder reset
                 */
                if (isset($this->request->params['controller']) &&
                    $this->request->params['controller']=='Users' &&
                    isset($this->request->params['action']) &&
                    ($this->request->params['action'] == 'register' || $this->request->params['action'] == 'reset')) {
                    return $this->redirect('/dashboard');
                }
            }

            /**
             * Debug: aktueller Webseitenaufruf inklusive aktuellen Datum
             */
            Log::debug($_SERVER['REQUEST_URI'] . " | " . date('Y-m-d H:i:s'));
        }
    }
}
