<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        /**
         * Erlaube actions "display" und "view" ohne Benutzerkonto
         */
        $this->Auth->allow(['display', 'view']);
    }

    /**
     * Methode zum Darstellen der Startseite
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        if (isset($path) && isset($path[0])) {
            /**
             * Setze Titel der Seite
             */
            $this->set('title', 'Startseite');

            /**
             * Notwendige Tabellen aus der Datenbank initialisieren
             */
            $this->loadModel('Calls');
            $this->loadModel('Donations');
            $this->loadModel('FileUploads');
            $this->loadModel('Partners');
            $this->loadModel('TimeDonations');
            $this->loadModel('Searches');
            $this->loadModel('TimeSearches');

            $additional_donation_branch_filter = "";
            $additional_search_branch_filter = "";
            if ($this->request->session()->read('configuration_id')>1) {
                $additional_donation_branch_filter = "Donations.configuration_id = " . $this->request->session()->read('configuration_id');
                $additional_search_branch_filter = "Searches.configuration_id = " . $this->request->session()->read('configuration_id');
            }

            /**
             * Hole die neuesten vier Spenden
             */
            $firstfour_donations = $this->Donations
                ->find()
                ->contain(['FileUploads'])
                ->where(['Donations.active' => 1, $additional_donation_branch_filter])
                ->order(['Donations.created' => 'DESC'])
                ->limit(4);
            $firstfour_donations_count = $firstfour_donations->count();

            /**
             * Hole die neuesten vier Gesuche
             */
            $firstfour_searches = $this->Searches
                ->find()
                ->contain(['FileUploads'])
                ->where(['Searches.active' => 1, $additional_search_branch_filter])
                ->order(['Searches.created' => 'DESC'])
                ->limit(4);
            $firstfour_searches_count = $firstfour_searches->count();

            $no_partner = true;
            $partner = $this->Partners
                ->find()
                ->select(['Partners.id'])
                ->where(['Partners.configuration_id' => $this->request->session()->read('configuration_id')])
                ->first();
            if ($partner && $partner->id>0) {
                $no_partner = false;
            }
            $this->set('no_partner', $no_partner);

            $no_donations_searches = true;
            if (isset($firstfour_searches) && $firstfour_searches && $firstfour_searches_count>0) {
                $no_donations_searches = false;
                $this->set(compact('firstfour_searches'));
                $this->set('_serialize', ['firstfour_searches']);
            }
            if (isset($firstfour_donations) && $firstfour_donations && $firstfour_donations_count>0) {
                $no_donations_searches = false;
                $this->set(compact('firstfour_donations'));
                $this->set('_serialize', ['firstfour_donations']);
            }

            $this->set('no_donations_searches', $no_donations_searches);

            /**
             * Titel für Startseite setzen
             */
            $this->set('title', 'Startseite');

            /**
             * Überprüfen ob Aufruf vorhanden ist, falls ja, setzen
             */
            $call = $this->Calls->find()->where(['Calls.configuration_id' => $this->request->session()->read('configuration_id')])->first();
            if ($call) {
                $this->set('call', $call);
            }
        }

        /**
         * CakePHP-Standardfunktion
         */
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Methode zum Darstellen von statischen Seiten aus der Datenbank
     *
     * @param $slug
     * @return \Cake\Network\Response|null
     */
    public function view($slug)
    {

        /**
         * Falls aktuelle Seite "fehler_gemeldet" und kein referer angegeben
         */
        if ($slug == "fehler_gemeldet" && strlen($this->request->session()->read('referer')) == 0) {
            $this->Flash->error(__('Ungültige Weiterleitung.'));
            return $this->redirect($this->referer());
            /**
             * Falls aktuelle Seite "erfolgreich_ausgeloggt" und Logout nicht gesetzt
             */
        } else if ($slug == "erfolgreich_ausgeloggt" && strlen($this->request->session()->read('logout')) == 0) {
            $this->Flash->error(__('Ungültige Weiterleitung.'));
            return $this->redirect($this->referer());
            /**
             * Falls aktuelle Seite "erfolgreich_bestaetigt" und Confirmed nicht gesetzt
             */
        } else if ($slug == "erfolgreich_bestaetigt" && strlen($this->request->session()->read('confirmed')) == 0) {
            $this->Flash->error(__('Ungültige Weiterleitung.'));
            return $this->redirect('/startseite');
        }

        /**
         * Hole Tabelle Pages mit allen dynamischen Seiten
         */
        $this->loadModel('Pages');

        /**
         * Hole aktuelle Seite mit aktuellen $slug
         */
        $page = $this->Pages
            ->find()
            ->where(['Pages.slug' => $slug])
            ->andWhere([$this->request->session()->read('branch_filter_pages')])
            ->first();

        if (!$page) {
            $page = $this->Pages
                ->find()
                ->where(['Pages.slug' => $slug])
                ->andWhere(['Pages.configuration_id' => 1])
                ->first();
        } else {

            /**
             * Falls aktuelle Seite "fehler_gemeldet"
             */
            if ($page->slug == 'fehler_gemeldet') {
                /**
                 * referer im aktueller Seite ersetzen, damit der Benutzer zurück auf die letzte Seite gelangen kann
                 */
                $page->content = str_replace("{{referer}}", $this->request->session()->read('referer'), $page->content);
                /**
                 * Anschließend referer in Session entfernen
                 */
                $this->request->session()->delete('referer');
            }

            /**
             * Falls aktuelle Seite "erfolgreich_ausgeloggt"
             * Meldung mit notie erzeugen ("Erfolgreich ausgeloggt")
             */
            if ($page->slug == 'erfolgreich_ausgeloggt') {
                $script =
                    '<script type="text/javascript">
                    jQuery(document).ready(function() {
                        setTimeout(function() {
                            location.href = "/startseite";
                        }, 3000);
                        notie.alert(1, "Erfolgreich ausgeloggt.", 3000);
                    });
                </script>';
                $this->request->session()->delete('logout');
                $page->content = str_replace("{{script}}", $script, $page->content);
                /**
                 * Falls aktuelle Seite "erfolgreich_bestaetigt"
                 * Meldung mit notie erzeugen ("Erfolgreich bestätigt")
                 */
            } else if ($page->slug == 'erfolgreich_bestaetigt') {
                $script =
                    '<script type="text/javascript">
                    jQuery(document).ready(function() {
                        setTimeout(function() {
                            location.href = "/login";
                        }, 3000);
                        notie.alert(1, "Erfolgreich bestätigt.", 3000);
                    });
                </script>';
                $this->request->session()->delete('confirmed');
                $page->content = str_replace("{{script}}", $script, $page->content);
            }
        }

        /**
         * Titel der Seite setzen
         */
        $this->set('title', $page->title);

        /**
         * Inhalt der Seite setzen
         */
        $this->set('content', $page->content);
    }
}
