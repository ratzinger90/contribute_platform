<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Event\Event;
use Cake\View\Helper;


/**
 * Searches Controller
 *
 * @property \App\Model\Table\SearchesTable $Searches
 */
class SearchesController extends AppController
{
    /**
     * Speichern einer Anfrage
     *
     * @param array $intercept_array
     * @return bool
     */
    private function __intercept(array $intercept_array) {
        $new_intercept = $this->Interceptions->newEntity($intercept_array);
        if ($this->Interceptions->save($new_intercept)) {
            return true;
        }
        return false;
    }

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        /**
         * Erlaubte Methoden in diesem Controller
         */
        $this->Auth->allow(['times', 'time', 'things', 'thing', 'allSearches']);
    }

    /**
     * Methode zum Handling einer Anfrage
     *
     * TODO: Überarbeiten ($logged ist nicht gesetzt) - Woher kommt $logged?! WTF
     */
    public function request()
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->is(['ajax'])) {

            /**
             * Anfragedaten initialisieren
             */
            $this->request->data['search_id'] = "XXXX";
            $this->request->data['sender_name'] = "XXXX";
            $this->request->data['sender_email'] = "XXXX";
            $this->request->data['message'] = "XXXX";

            /**
             * Anfragedaten-Array definieren
             */
            $intercept_array = [
                'user_id' => ($this->Auth->user('id') > 0) ? $this->Auth->user('id') : NULL,
                'search_id' => $this->request->data['search_id'],
                'intercepted' => 0,
                'contact' => $this->request->data['sender_email'],
            ];

            /**
             * Anfragedaten-Array speichern
             */
            $intercept_started = $this->__intercept($intercept_array);

            if ($logged && $intercept_started) {

                // TODO: Kopie an angegebene E-Mail-Adresse senden ??

                $response = [
                    'status' => 'success'
                ];
            } else {
                $response = [
                    'status' => 'failed',
                    'logged' => $logged,
                    'intercept_started' => $intercept_started
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode zur Darstellung aller Gesuche
     *
     * @param null $page
     * @param null $start_end_type
     * @param null $start_end_id
     * @return \Cake\Network\Response|null
     */
    public function allSearches($page = null, $start_end_type = null, $start_end_id = null)
    {
        if (isset($page) && $page) {
            $this->request->session()->write('page', $page);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_type', $start_end_type);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_id', $start_end_id);
        }
        if (!isset($page) && !isset($start_end_type) && !isset($start_end_id)) {
            $this->request->session()->delete('page');
            $this->request->session()->delete('start_end_type');
            $this->request->session()->delete('start_end_id');
        }
        return $this->redirect('/auflistung/' . base64_encode("search|all"));
    }

    /**
     * Methode zur Darstellung von Zeitgesuchen
     *
     */
    public function times($page = null, $start_end_type = null, $start_end_id = null)
    {
        if (isset($page) && $page) {
            $this->request->session()->write('page', $page);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_type', $start_end_type);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_id', $start_end_id);
        }
        if (!isset($page) && !isset($start_end_type) && !isset($start_end_id)) {
            $this->request->session()->delete('page');
            $this->request->session()->delete('start_end_type');
            $this->request->session()->delete('start_end_id');
        }
        return $this->redirect('/auflistung/' . base64_encode("search|time"));

    }

    /**
     * Methode zur Darstellung eines Zeitgesuchs
     *
     * @param $url_title
     * @param null $encoded_bool                    - Wo und wann (Beispiel) soll $encoded_bool gesetzt werden? TODO: Rausfinden!
     * @return \Cake\Network\Response|null
     */
    public function time($url_title, $encoded_bool = null)
    {
        return $this->redirect('/element/' . base64_encode('search|time|' . $url_title));

    }


    /**
     * Methode zur Darstellung eines Sachgesuchs
     *
     * @param $url_title
     * @return \Cake\Network\Response|null
     */
    public function thing($url_title)
    {
        return $this->redirect('/element/' . base64_encode('search|thing|' . $url_title));

    }

    /**
     * Methode zur Darstellung aller Sachgesuche
     */
    public function things($page = null, $start_end_type = null, $start_end_id = null)
    {
        if (isset($page) && $page) {
            $this->request->session()->write('page', $page);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_type', $start_end_type);
        }
        if (isset($start_end_type) && $start_end_type) {
            $this->request->session()->write('start_end_id', $start_end_id);
        }
        if (!isset($page) && !isset($start_end_type) && !isset($start_end_id)) {
            $this->request->session()->delete('page');
            $this->request->session()->delete('start_end_type');
            $this->request->session()->delete('start_end_id');
        }
        return $this->redirect('/auflistung/' . base64_encode("search|thing"));

    }
}
