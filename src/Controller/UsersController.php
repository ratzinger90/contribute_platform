<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Utility\Security;
use Cake\Log\Log;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    /**
     * Private Methode zur Überpüfung des Passwortds
     *
     * @param $input
     * @return bool
     */
    private function __checkAccess($input) {
        $user = $this->Users->get($this->Auth->user('id'));

        if ("XXXX") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Before filter method
     *
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'addSuccess', 'login', 'checkLogin', 'resetPassword', 'autoLogin', 'register', 'confirmAccount', 'registerSuccess']);
        $this->loadModel('Addresses');
        $this->loadModel('Cities');
        $this->loadModel('Configurations');
        $this->loadModel('EmailLogs');
        $this->loadModel('EmailTemplates');
        $this->loadModel('FileUploads');
        $this->loadModel('Searches');
        $this->loadModel('Zipcodes');
        $this->loadModel('Users');
        $this->loadModel('ValidZipcodes');
    }

    /**
     * Öffentliche Methode zur Überprüfung des Passworts
     */
    public function checkAccess() {
        $this->viewBuilder()->layout = "ajax";
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            if ($this->__checkAccess($this->request->data['value'])) {
                $response = [
                    'status' => 'success'
                ];
            } else {
                $response = [
                    'status' => 'failed'
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Methode zur Überprüfung des Logins
     *
     * @return \Cake\Network\Response|null
     */
    public function checkLogin() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $this->request->data['email'] = $this->request->data['XXXX'];
            $this->request->data['password'] = $this->request->data['XXXX'];
            $user = $this->Auth->identify();
            if ($user) {
                $user_account = $this->Users->find()->where(['Users.id' => $user['id']])->first();
                $user['role'] = $user_account->role;
                $user['firstname'] = $user_account->firstname;
                $user['lastname'] = $user_account->lastname;

                /* --- Normaler Login --- */
                $this->Auth->setUser($user);

                $response = [
                    'status' => 'success',
                    'failed_login' => false,
                    'location' => $this->Auth->redirectUrl()
                ];
                $this->request->session()->write('user_id', $this->Auth->user('id'));
            } else {
                $response = [
                    'status' => 'failed',
                    'failed_login' => true
                ];
            }
            $this->set('response', $response);
            $this->render('response');
        }
    }

    /**
     * Login method
     *
     * @return void
     */
    public function login() {
        if ($this->Auth->user('id')>0) {
            return $this->redirect(['action' => 'index']);
        }
        $this->set('title', 'Login');
    }

    /**
     * Logout method*
     *
     * @return \Cake\Network\Response|null
     */
    public function logout()
    {
        $this->request->session()->write('logout', base64_decode('yes'));
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Account method
     *
     * Stellt eine Übersicht der allgemeinen Informationen bereit
     */
    public function account() {
        $this->set('title', 'Kontodaten');
        $user = $this->Addresses->find()->select(['Addresses.id', 'Addresses.title', 'Addresses.first_name', 'Addresses.last_name', 'Addresses.phone', 'Addresses.fax', 'Addresses.status', 'Addresses.status_description'])->where(['Addresses.user_id' => $this->Auth->user('id')])->first();

        if ($this->request->is(['post', 'put'])) {
            $patch_address_entity = $this->Addresses->patchEntity($user, $this->request->data);

            if ($this->Addresses->save($patch_address_entity)) {
                $this->Flash->success(__('Änderungen erfolgreich gespeichert!'));
            } else {
                $this->Flash->error(__('Änderungen wurden nicht gespeichert!'));
            }
        }

        $this->set('user', $user);

        $this->set('anreden', ['herr' => 'Herr', 'frau' => 'Frau']);

    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('title', 'Geschützter Bereich');
        if ($this->Auth->user('role')=='super') {
            $this->redirect("/super/dashboard");
        /* --- Falls aktueller Benutzer Administrator ist --- */
        } else if ($this->Auth->user('role')=='admin') {
            $this->redirect("/admin/dashboard");
        } else if ($this->Auth->user('role')=='broker') {
            $this->redirect("/broker/dashboard");
        }

        $newest_searches = $this->Searches->find()->contain(['FileUploads'])->where(['Searches.active' => 1])->order(['Searches.created' => 'DESC'])->limit(5);
        $this->set('newest_searches', $newest_searches);
    }

    /**
     * Add method
     *
     * Stellt die Funktion zum Bewerben eines neuen Spendenvermittlers bereit
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Bewerbungsformular');

        if ($this->Auth->user('id') > 0) {
            $user = $this->Users->find()->where(['Users.id' => $this->Auth->user('id')])->first();
        } else {
            $user = $this->Users->newEntity($this->request->data);
        }
        if ($this->request->is(['post', 'patch', 'put'])) {

            if ($this->Auth->user('id') > 0) {

            } else {
                if ($_SERVER['HTTP_HOST']!='MAINPAGE') {
                    $response = file_get_contents("XXXX");
                    $response = json_decode($response);
                    if ($response->success == false) {
                        $this->Flash->error(__('Es ist ein Fehler aufgetreten, bitte kontaktiere den Support.'));
                        return $this->redirect('/bewerben');
                    }
                }
            }

            if (isset($this->request->data['email'])) {
                $exists = $this->Users->find()->where(['Users.email' => trim($this->request->data['email'])])->first();
            }

            if (!isset($exists)) {
                $exists = $this->Users->find()->where(['Users.id' => $this->Auth->user('id')])->first();
            }

            if ($exists) {
                $user = $exists;
            }

            /* --- Falls AGBs nicht akzeptiert wurden --- */
            if ($this->request->data['agb']=='0') {
                $this->Flash->error(__('Du musst die AGB akzeptieren.'));
                return $this->redirect('/bewerben');
            }

            $helpers_controller = new HelpersController();
            $valid = $helpers_controller->validateAdd("internal_request", $this->request->data);
            if (!$valid) {
                $this->Flash->error(__('Alle Pflichtfelder müssen ausgefüllt werden.'));
                return $this->redirect('/bewerben');
            }

            if (!$exists) {
                $password_decrypted = $this->request->data['password'];

                /* --- Neue Adressentität --- */
                $address = array();
                $create_new_address = false;
                if (isset($this->request->data['email']) &&
                    strlen($this->request->data['email']) > 0
                ) {

                    $zipcode = $this->Zipcodes->find()->where(['Zipcodes.zipcode' => trim($this->request->data['zip'])])->first();

                    if ($zipcode) {
                        $city = $this->Cities->find()->where(['Cities.id' => $zipcode->city_id])->first();

                        if ($city) {
                            $city_name = $city->name;
                        }
                    }

                    $address = [
                        'user_id' => '0',
                        'street' => $this->request->data['street'],
                        'zip' => $this->request->data['zip'],
                        'city' => $city_name,
                    ];
                    $create_new_address = true;
                }


                $password = "XXXX";

                $patch_entity_user = [
                    'email' => trim($this->request->data['email']),
                    'firstname' => trim($this->request->data['firstname']),
                    'lastname' => trim($this->request->data['lastname']),
                    'password' => $password,
                    'active' => 0,
                    'role' => 'broker',
                    'phone' => trim($this->request->data['phone']),
                    'fax' => trim($this->request->data['fax']),
                    'applied' => 1
                ];

                $generated_token = "XXXX";

                $patched_user = $this->Users->patchEntity($user, $patch_entity_user);
                $saved_user = $this->Users->save($patched_user);

                $token = "XXXX";
                $patch_entity_user['token'] = $token;

                $patched_token_user = $this->Users->patchEntity($saved_user, ['token' => $token]);
                $this->Users->save($patched_token_user);

                if (isset($create_new_address) && $create_new_address == true) {
                    $new_address = $this->Addresses->newEntity($address);
                    $patch_address = ['user_id' => $saved_user->id];
                    $patched_address = $this->Addresses->patchEntity($new_address, $patch_address);
                    $saved_address = $this->Addresses->save($patched_address);
                }
            } else {
                $patch_entity_user = [
                    'role' => 'broker',
                    'active' => 0,
                    'applied' => 1,
                    'broker_text' => trim($this->request->data['broker_text']),
                    'broker_partner' => trim($this->request->data['broker_partner']),
                ];
                $patched_user = $this->Users->patchEntity($user, $patch_entity_user);
                $saved_user = $this->Users->save($patched_user);

                $saved_address = $this->Addresses->find()->where(['Addresses.user_id' => $saved_user->id])->first();
            }

            if ($saved_user) {

                $partner_user_entity_array = [
                    'partner_id' => trim($this->request->data['broker_partner']),
                    'broker_id' => $saved_user->id
                ];
                $this->loadModel('PartnersBrokers');
                $partner_user_entity = $this->PartnersBrokers->newEntity($partner_user_entity_array);
                $this->PartnersBrokers->save($partner_user_entity);

                $configuration = $this->Configurations->find()->where(['Configurations.id' => 1])->first();

                $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'bewerbung'])->first();


                $mail_content = $mail_template->template_content;
                $mail_content = str_replace('{{firstname}}', $saved_user->firstname, $mail_content);
                $mail_content = str_replace('{{lastname}}', $saved_user->lastname, $mail_content);
                $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);

                $email = new email('default');
                $email->helpers(array('Html', 'Text'));
                $email->viewVars(
                    array(
                        'mail_content' => $mail_content
                    )
                );
                $email->template('default');
                $email->emailformat('text');
                $email->from(array($configuration->from_mail => $configuration->from_big_title));
                $email->to(array($saved_user->email => $saved_user->firstname . " " . $saved_user->lastname));
                $email->subject($mail_template->template_subject);
                if ($email->send()) {
                    $this->loadModel('EmailLogs');
                    $new_email_log_entity = [
                        'send' => 1,
                        'subject' => $mail_template->template__subject,
                        'content' => $mail_content,
                        'recipient_name' => "",
                        'recipient_email' => $saved_user->email,
                        'sender_name' => $configuration->from_big_title,
                        'sender_email' => $configuration->from_mail
                    ];
                    $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                    $this->EmailLogs->save($new_email_log);
                }

                $valid_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => $saved_address->zip])->first();
                if ($valid_zipcode) {
                    $this->loadModel('AdminsZipcodes');
                    $zipcode_admin = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.valid_zipcodes_id' => $valid_zipcode->id])->first();
                    if ($zipcode_admin) {
                        $admin = $this->Users->find()->where(['Users.id' => $zipcode_admin->admin_id, 'Users.role' => 'admin'])->first();
                        if ($admin) {
                            $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'bewerbung_benachrichtigung_admin'])->first();

                            $mail_content = $mail_template->template_content;
                            // TODO: undefined index 'firstname'
                            $mail_content = str_replace('{{admin_firstname}}', $admin->firstname, $mail_content);
                            $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
                            $mail_content = str_replace('{{zipcode}}', $valid_zipcode->zipcode, $mail_content);

                            $email = new email('default');
                            $email->helpers(array('Html', 'Text'));
                            $email->viewVars(
                                array(
                                    'mail_content' => $mail_content
                                )
                            );
                            $email->template('default');
                            $email->emailformat('text');
                            $email->from(array($configuration->from_mail => $configuration->from_big_title));
                            $email->to(array($admin->email => $admin->firstname));
                            $email->subject($mail_template->template_subject);
                            if ($email->send()) {
                                $this->loadModel('EmailLogs');
                                $new_email_log_entity = [
                                    'send' => 1,
                                    'subject' => $mail_template->template_subject,
                                    'content' => $mail_content,
                                    'recipient_name' => $admin->firstname,
                                    'recipient_email' => $admin->email,
                                    'sender_name' => $configuration->from_big_title,
                                    'sender_email' => $configuration->from_mail
                                ];
                                $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                                $this->EmailLogs->save($new_email_log);
                            }
                        }
                    }
                }

                $this->Flash->success(__('Bewerbung erfolgreich abgeschickt.'));
                $this->request->session()->write('add_success', 'yes');
                return $this->redirect('/bewerben_erfolgreich');
            } else {
                $this->Flash->error(__('Registrierung nicht erfolgreich.'));
                return $this->redirect('/bewerben');
            }
        }


        $this->set('anreden', ['herr' => 'Herr', 'frau' => 'Frau']);
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

        $this->loadModel('Partners');
        $partners = $this->Partners->find();

        $partners_array = array();

        foreach ($partners as $partner) {
            $partners_array[$partner->id] = $partner->name;
        }

        $this->set('partners', $partners_array);
    }

    /**
     * Register method
     *
     * Stellt die Funktion zum Registrieren eines neuen Benutzers bereit
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        $this->set('title', 'Registrierungsformular');
        $user = $this->Users->newEntity($this->request->data);
        if ($this->request->is(['post', 'patch', 'put'])) {

            if ($_SERVER['HTTP_HOST']!='MAINPAGE') {
                $response = file_get_contents("XXXX");
                $response = json_decode($response);
                if ($response->success == false) {
                    $this->Flash->error(__('Es ist ein Fehler aufgetreten, bitte kontaktiere den Support.'));
                    return $this->redirect('/registrieren');
                }
            }

            $helpers_controller = new HelpersController();
            $valid = $helpers_controller->validateRegister("internal_request", $this->request->data);
            if (!$valid) {
                $this->Flash->error(__('Alle Pflichtfelder müssen ausgefüllt werden.'));
                return $this->redirect('/registrieren');
            }

            $password_decrypted = $this->request->data['password'];

            /* --- Neue Adressentität --- */
            $address = array();
            $create_new_address = false;


            if (isset($this->request->data['email']) &&
                strlen($this->request->data['email'])>0) {

                $zipcode = $this->Zipcodes->find()->where(['Zipcodes.zipcode' => $this->request->data['zip']])->first();

                $city = $this->Cities->find()->where(['Cities.id' => $zipcode->city_id])->first();

                $address = [
                    'user_id' => '0',
                    'street' => $this->request->data['street'],
                    'zip' => $this->request->data['zip'],
                    'city' => $city->name
                ];
                $create_new_address = true;
            }

            $password = "XXXX";

            $patch_entity_user = [
                'email' => $this->request->data['email'],
                'firstname' => $this->request->data['firstname'],
                'lastname' => $this->request->data['lastname'],
                'password' => $password,
                'active' => '0',
                'role' => 'token_account',
                'phone' => $this->request->data['phone'],
                'fax' => $this->request->data['fax'],
            ];

            $generated_token = "XXXX";

            $patched_user = $this->Users->patchEntity($user, $patch_entity_user);
            $saved_user = $this->Users->save($patched_user);

            $token = "XXXX";
            $patch_entity_user['token'] = $token;

            $patched_token_user = $this->Users->patchEntity($saved_user, ['token' => $token]);
            $this->Users->save($patched_token_user);

            if (isset($create_new_address) && $create_new_address == true) {
                $new_address = $this->Addresses->newEntity($address);
                $patch_address = ['user_id' => $saved_user->id];
                $patched_address = $this->Addresses->patchEntity($new_address, $patch_address);
                $save_address = $this->Addresses->save($patched_address);
            }


            if ($saved_user) {

                $configuration = $this->Configurations->find()->where(['Configurations.id' => 1])->first();
                $confirm_link = $configuration->host . "/HkigKzexaedZ/" . $token;
                $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'registrieren'])->first();

                $title = isset($this->request->data['title']) && $this->request->data['title'] == 'frau' ? "Frau" : "Herr";

                $mail_content = $mail_template->template_content;
                $mail_content = str_replace('{{title}}', $title, $mail_content);
                $mail_content = str_replace('{{firstname}}', trim($this->request->data['firstname']), $mail_content);
                $mail_content = str_replace('{{lastname}}', trim($this->request->data['lastname']), $mail_content);
                $mail_content = str_replace('{{confirm_link}}', $confirm_link, $mail_content);
                $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);

                $email = new email('default');
                $email->helpers(array('Html', 'Text'));
                $email->viewVars(
                    array(
                        'mail_content' => $mail_content
                    )
                );
                $email->template('default');
                $email->emailformat('text');
                $email->from(array($configuration->from_mail => $configuration->from_big_title));
                $email->to(array($this->request->data['email']));
                $email->subject($mail_template->template_subject);
                if ($email->send()) {
                    $this->loadModel('EmailLogs');
                    $new_email_log_entity = [
                        'send' => 1,
                        'subject' => $mail_template->template_subject,
                        'content' => $mail_content,
                        'recipient_name' => "",
                        'recipient_email' => $this->request->data['email'],
                        'sender_name' => $configuration->from_big_title,
                        'sender_email' => $configuration->from_mail
                    ];
                    $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                    $this->EmailLogs->save($new_email_log);
                }

                $this->Flash->success(__('Registrierung erfolgreich.'));
                $this->request->session()->write('register_success', 'yes');
                return $this->redirect('/registrieren_erfolgreich');
            } else {
                $this->Flash->error(__('Registrierung nicht erfolgreich.'));
                return $this->redirect('/registrieren');
            }
        }


        $this->set('anreden', ['herr' => 'Herr', 'frau' => 'Frau']);
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Add success method
     *
     * Stellt eine Übersicht bei erfolgreicher Bewerbung bereit
     */
    public function addSuccess() {
        Log::debug("add_success: " . $this->request->session()->read('add_success'));
        if ($this->request->session()->read('add_success')=='yes') {
            $this->request->session()->delete('add_success');
            $this->set('title', 'Bewerbung erfolgreich');
        } else {
            return $this->redirect($this->referer());
        }
    }

    /**
     * Register success method
     *
     * Stellt eine Übersicht bei erfolgreicher Registrierung bereit
     */
    public function registerSuccess() {
        Log::debug("register_success: " . $this->request->session()->read('register_success'));
        if ($this->request->session()->read('register_success')=='yes') {
            $this->request->session()->delete('register_success');
            $this->set('title', 'Registrierung erfolgreich');
        } else {
            return $this->redirect($this->referer());
        }
    }

    /**
     * Edit method
     *
     * Methode zum Bearbeiten des eigenen Benutzers
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit() {
        $this->set('title', 'Konto bearbeiten');
        $address = $this->Addresses->find()->where(['Addresses.user_id' => $this->Auth->user('id')])->first();

        $user = $this->Users->find()->select(['Users.email', 'Users.phone'])->where(['Users.id' => $this->Auth->user('id')])->first();
        if (strlen($address->email)==0) {
            $address->email = $user->email;
        }

        if (strlen($address->phone)==0) {
            $address->phone = $user->phone;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {

            if (isset($this->request->data['email']) && $this->request->data['email'] != $this->Auth->user('email')) {
                $patch_user['email'] = $this->request->data['email'];
            }

            if (isset($this->request->data['phone']) && $this->request->data['phone'] != $this->Auth->user('phone')) {
                $patch_user['phone'] = $this->request->data['phone'];
            }

            if (isset($patch_user)) {
                $user = $this->Users->get($this->Auth->user('id'));
                $patched_user = $this->Users->patchEntity($user, $patch_user);
                $this->Users->save($patched_user);
            }

            $address = $this->Addresses->patchEntity($address, $this->request->data);
            if ($this->Addresses->save($address)) {
                $this->Flash->success(__('Änderungen gespeichert.'));
                return $this->redirect('/konto_bearbeiten');
            } else {
                $this->Flash->error(__('Änderungen wurden nicht gespeichert.'));
            }
        }
        $this->set(compact('address'));
        $this->set('_serialize', ['address']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
