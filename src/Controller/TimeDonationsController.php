<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * TimeDonations Controller
 *
 * @property \App\Model\Table\TimeDonationsTable $TimeDonations
 */
class TimeDonationsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Donations']
        ];
        $this->set('timeDonations', $this->paginate($this->TimeDonations));
        $this->set('_serialize', ['timeDonations']);
    }

    /**
     * View method
     *
     * @param string|null $id Time Donation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $timeDonation = $this->TimeDonations->get($id, [
            'contain' => ['Donations']
        ]);
        $this->set('timeDonation', $timeDonation);
        $this->set('_serialize', ['timeDonation']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {          

            // User Erstellung
            $this->loadModel('Users');
            if (isset($this->request->data['email']) && $this->request->data['email']!='') {
                $this->request->data['role'] = "token_account";
            } else {
                $this->request->data['role'] = "no_account";
            }

            if (isset($this->request->data['email']) && $this->request->data['email']!='') {

                if ($this->request->data['role'] == "token_account") {
                    $token = base64_encode($this->request->data['email'] . "|" . $this->request->data['role']);

                    $this->loadModel('Configurations');
                    $configuration = $this->Configurations->get('1');

                    $login_link = $configuration->host . "/CqikKzexaedh/" . $token;
                    if (isset($this->request->data['email']) && $this->request->data['email'] != '') {
                        // prüfen ob E-Mail schon vorhanden
                        $user = $this->Users->find()->where(['Users.role' => $this->request->data['role'], 'Users.email' => $this->request->data['email']])->first();
                        if (!$user) {

                            $email = new email('default');
                            $email->helpers(array('Html', 'Text'));
                            $email->viewvars(
                                array(
                                    'login_link' => $login_link
                                )
                            );
                            $email->template('new_donation');
                            $email->emailformat('text');
                            $email->from(array('c.ratz@tu-bs.de' => 'Christian Ratz'));
                            $email->to(array($this->request->data['email']));
                            $email->subject('Dein Gesuch ist nun online.');
                            $email->send();

                            $new_user_entity = [
                                'email' => $this->request->data['email'],
                                'phone' => $this->request->data['phone'],
                                'role' => $this->request->data['role']
                            ];
                            $new_user = $this->Users->newEntity($new_user_entity);

                            $update_user_entity = [
                                'token' => $token
                            ];

                            $new_user = $this->Users->patchEntity($new_user, $update_user_entity);

                            $save_user = $this->Users->save($new_user);
                            $this->request->data['user_id'] = $save_user->id;
                        } else {
                            $this->request->data['user_id'] = $user->id;
                        }

                    }

                }

            }

            // Donations Erstellung
            $this->loadModel('Donations'); 

            if ($this->request->data['role'] == "no_account") {
                $new_donation_entity = [
                                'title' => $this->request->data['title'],
                                'description' => $this->request->data['description'],
                                'category'  =>  'time'
                            ];
            }
            else {
                $new_donation_entity = [
                                'title' => $this->request->data['title'],
                                'description' => $this->request->data['description'],
                                'user_id' => $this->request->data['user_id'],
                                'category'  =>  'time'
                            ];
            }
            
            $new_donation = $this->Donations->newEntity($new_donation_entity);
            $save_donation = $this->Donations->save($new_donation);
            $this->request->data['donation_id'] = $save_donation->id;  

            $this->request->data['used'] = 0;
            
            unset($this->request->data['title']);
            unset($this->request->data['description']);
            unset($this->request->data['email']);
            unset($this->request->data['phone']);
            unset($this->request->data['user_id']);
            unset($this->request->data['street']);
            unset($this->request->data['zip']);
            unset($this->request->data['city']);

            $this->request->data['category'] = 'time';

            $exists = true;
            $counter = 1;
            while ($exists) {
                if (isset($this->request->data['start_date_'.$counter])) {
                    $new_timeDonation_entity = [
                        'donation_id' => $this->request->data['donation_id'],
                        'start_date' => $this->request->data['start_date_'.$counter],
                        'start_time' => $this->request->data['start_time_'.$counter],
                        'end_date' => $this->request->data['end_date_'.$counter],
                        'end_time' => $this->request->data['end_time_'.$counter],
                        'used'  => 0
                    ];
                    $new_timeDonation = $this->TimeDonations->newEntity($new_timeDonation_entity);
                    $timeDonation = $this->TimeDonations->save($new_timeDonation);
                    $counter += 1;
                } else {
                    $exists = false;
                }
            }            

            if ($save_donation && $timeDonation) {
                $this->Flash->success(__('The time donation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The time donation could not be saved. Please, try again.'));
            }
        }
        $donations = $this->TimeDonations->Donations->find('list', ['limit' => 200]);
        $this->set(compact('timeDonation', 'donations'));
        $this->set('_serialize', ['timeDonation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Time Donation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $timeDonation = $this->TimeDonations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $timeDonation = $this->TimeDonations->patchEntity($timeDonation, $this->request->data);
            if ($this->TimeDonations->save($timeDonation)) {
                $this->Flash->success(__('The time donation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The time donation could not be saved. Please, try again.'));
            }
        }
        $donations = $this->TimeDonations->Donations->find('list', ['limit' => 200]);
        $this->set(compact('timeDonation', 'donations'));
        $this->set('_serialize', ['timeDonation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Time Donation id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $timeDonation = $this->TimeDonations->get($id);
        if ($this->TimeDonations->delete($timeDonation)) {
            $this->Flash->success(__('The time donation has been deleted.'));
        } else {
            $this->Flash->error(__('The time donation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
