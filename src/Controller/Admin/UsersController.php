<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Utility\Security;
use Cake\Log\Log;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * @var array
     *
     * Komponente für Dateiupload
     */
    public $components = ['FileUpload'];

    /**
     * @return \Cake\Network\Response|null
     *
     * Methode zum Login
     */
    public function login() {
        return $this->redirect('/login');
    }

    /**
     * Logout method
     *
     * @return \Cake\Network\Response|null
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Index method - Stellt das Dashboard für den Administrator zur Verfügung
     *
     * @return void
     */
    public function index() {
        $this->set('title', 'Administrator Bereich');


    }

    /**
     * Generiert eine Anzeige aller Vermittler - nach Status (neu = inaktiv oder aktiv)
     *
     * @param null $case
     */
    public function brokers($case = null) {

        $this->loadModel('AdminsZipcodes');
        $this->loadModel('ValidZipcodes');

        // Hole Postleitzahlenbereiche für die der aktuelle Administrator zuständig ist
        /*
        $admins_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.admin_id' => $this->Auth->user('id')])->all();
        $related_zipcodes = array();
        foreach ($admins_zipcodes as $admin_zipcode) {
            $valid_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.id' => $admin_zipcode->valid_zipcodes_id])->first();
            if ($valid_zipcode) {
                $related_zipcodes[] = $valid_zipcode->zipcode;
            }
        }*/

        switch(base64_decode($case)) {
            // Neue Bewerber - Spendenvermittler
            case "new":
                $this->set('title', 'Bewerber verwalten');
                /*
                if (sizeof($related_zipcodes)==0 || empty($related_zipcodes)) {
                    $brokers = array();
                } else {
                */
                    $brokers = $this->Users->find()->contain(['Addresses'])->where(['OR' => [['Users.role' => 'broker'],['Users.applied' => 1]]])->andWhere(['Users.active' => 0])->order(['Users.created' => 'DESC']);
                //}
                $this->set('case', 'new');
                break;
            // Aktive Vermittler
            case "active":
                $this->set('title', 'Spendenvermittler verwalten');
                /*
                 if (sizeof($related_zipcodes)==0 || empty($related_zipcodes)) {
                    $brokers = array();
                } else {
                */
                    $brokers = $this->Users->find()->contain(['Addresses'])->where(['OR' => [['Users.role' => 'broker'], ['Users.applied' => 1]]])->andWhere(['Users.active' => 1])->order(['Users.created' => 'DESC']);
                //}
                $this->set('case', 'active');
                break;
        }

        if (isset($brokers)) {
            $this->set('brokers', $brokers);
        }
    }

    /**
     * Methode zum Aktivieren eines Vermittlers
     *
     * @param $id
     * @param $case
     * @return \Cake\Network\Response|null
     */
    public function activate($id, $case) {
        $broker = $this->Users->get($id);

        # Vermittler aktivieren
        $patched_broker = $this->Users->patchEntity($broker, ['active' => 1]);

        # --------------------------------------------------------------------------------------------------------------
        # Zuweisungen zu Zipcodes erstellen
        $this->loadModel('PartnersBrokers');
        $this->loadModel('PartnersZipcodes');
        $this->loadModel('BrokersZipcodes');
        $partner_user = $this->PartnersBrokers->find()->where(['broker_id' => $broker->id])->first();
        $partner_zipcodes = $this->PartnersZipcodes->find()->where(['PartnersZipcodes.partner_id' => $partner_user->partner_id]);

        foreach ($partner_zipcodes as $partner_zipcode) {
            $new_broker_zipcode_entity_array = [
                'broker_id' => $broker->id,
                'valid_zipcodes_id' => $partner_zipcode->valid_zipcode_id
            ];
            $new_broker_zipcode_entity = $this->BrokersZipcodes->newEntity($new_broker_zipcode_entity_array);
            $this->BrokersZipcodes->save($new_broker_zipcode_entity);
        }

        # --------------------------------------------------------------------------------------------------------------

        $this->loadModel('Configurations');
        $configuration = $this->Configurations->find()->where(['Configurations.id' => 1])->first();

        $token = $broker->token;

        if (strlen($token)==0) {
            $token = "XXXX";
            $patched_broker = $this->Users->patchEntity($broker, ['token' => $token]);
        }

        $login_link = "XXXX";

        $this->loadModel('EmailTemplates');
        $mail_template = $this->EmailTemplates->find()->where(['EmailTemplates.template_name' => 'bewerber_freigeschaltet'])->first();

        $title = isset($broker->title) && $this->request->data['title'] == 'frau' ? "Frau" : "Herr";

        $mail_content = $mail_template->template_content;
        $mail_content = str_replace('{{title}}', $title, $mail_content);
        $mail_content = str_replace('{{firstname}}', $broker->firstname, $mail_content);
        $mail_content = str_replace('{{lastname}}', $broker->lastname, $mail_content);
        $mail_content = str_replace('{{host}}', $configuration->host, $mail_content);
        $mail_content = str_replace('{{login_link}}', $login_link, $mail_content);

        if ($this->Users->save($patched_broker)) {

            $this->loadModel('BrokersZipcodes');
            $this->loadModel('PartnersZipcodes');
            $zipcodes = $this->PartnersZipcodes->find()->where(['PartnersZipcodes.partner_id' => $broker->broker_partner]);
            foreach ($zipcodes as $zipcode) {
                $new_broker_zipcode_entity_array = [
                    'broker_id' => $broker->id,
                    'valid_zipcodes_id' => $zipcode->valid_zipcode_id
                ];
                $new_broker_zipcode_entity = $this->BrokersZipcodes->newEntity($new_broker_zipcode_entity_array);
                $this->BrokersZipcodes->save($new_broker_zipcode_entity);
            }

            // E-Mail an Vermittler schicken mit Loginlink
            $email = new Email('default');
            $email->helpers(array('Html', 'Text'));
            $email->viewVars(
                array(
                    'mail_content' => $mail_content
                )
            );
            $email->template('default');
            $email->emailFormat('text');
            $email->from(array($configuration->from_mail => $configuration->from_big_title));
            $email->to(array($broker->email => $broker->firstname . " " . $broker->lastname));
            $email->subject($mail_template->template_subject);
            if ($email->send()) {
                $this->loadModel('EmailLogs');
                $new_email_log_entity = [
                    'send' => 1,
                    'subject' => $mail_template->template_subject,
                    'content' => $mail_content,
                    'recipient_name' => $broker->firstname . " " . $broker->lastname,
                    'recipient_email' => $broker->email,
                    'sender_name' => $configuration->from_big_title,
                    'sender_email' => $configuration->from_mail
                ];
                $new_email_log = $this->EmailLogs->newEntity($new_email_log_entity);
                $this->EmailLogs->save($new_email_log);
                $this->Flash->success(__('Spendenvermittler erfolgreich aktiviert.'));
            }
        } else {
            $this->Flash->error(__('Spendenvermittler wurde nicht aktiviert.'));
        }
        return $this->redirect(['action' => 'brokers', base64_encode($case)]);
    }

    /**
     * Methode zum Deaktivieren eines Vermittlers
     *
     * @param $id
     * @param $case
     * @return \Cake\Network\Response|null
     */
    public function deactivate($id, $case) {
        $broker = $this->Users->get($id);

        if ($broker->applied == 1) {
            $patched_broker = $this->Users->patchEntity($broker, ['active' => 1, 'applied' => 0, 'role' => 'token_account', 'broker_text' => '']);
        } else {
            $patched_broker = $this->Users->patchEntity($broker, ['active' => 0]);
        }

        if ($this->Users->save($patched_broker)) {
            $this->Flash->success(__('Spendenvermittler erfolgreich deaktiviert.'));
        } else {
            $this->Flash->error(__('Spendenvermittler wurde nicht deaktiviert.'));
        }
        return $this->redirect(['action' => 'brokers', base64_encode($case)]);
    }

    /**
     * Methode zum Löschen eines Vermittlers
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {

        // Benutzer holen
        $user = $this->Users->get($id);
        if ($user->applied == 1) {
            $patch_user = $this->Users->patchEntity($user, ['active' => 1, 'applied' => 0, 'role' => 'token_account', 'broker_text' => '']);
            if ($this->Users->save($patch_user)) {
                $this->Flash->success(__('Der Vermittler wurde erfolgreich als Spender zurückgesetzt.'));
            } else {
                $this->Flash->success(__('Vermittler wurde nicht als Spender zurückgesetzt.'));
            }
        } else {
            $this->loadModel('Addresses');
            $address = $this->Addresses->find()->where(['Addresses.user_id' => $id])->first();
            if ($address) {
                $this->Addresses->delete($address);
            }
            if ($this->Users->delete($user)) {
                $this->Flash->success(__('Der Vermittler wurde gelöscht.'));
            } else {
                $this->Flash->error(__('Der Vermittler wurde nicht gelöscht.'));
            }
        }
        return $this->redirect('/admin/vermittler_verwalten/new');
    }

    /**
     * Methode zum Bearbeiten eines Vermittlers
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function details($id = null, $case = null)
    {
        $this->set('title', 'Details');
        $this->loadModel('Addresses');
        $this->loadModel('AdminsZipcodes');
        $this->loadModel('BrokersZipcodes');
        $this->loadModel('ValidZipcodes');

        $available_zipcodes = $this->AdminsZipcodes->find()->where(['AdminsZipcodes.admin_id' => $this->Auth->user('id')])->all();
        if ($available_zipcodes) {
            $available_zipcodes_ids = array();
            foreach ($available_zipcodes as $available_zipcode) {
                $available_zipcodes_ids[] = $available_zipcode->valid_zipcodes_id;
            }
        }

        $user = $this->Users->get($id, ['contain' => ['Addresses']]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            // Alle zugewiesenen Postleitzahlenbereiche für aktuellen Vermittler speichern
            $new_related_zipcodes_ids = array();
            if (isset($this->request->data['related_zipcodes_ids']) && $this->request->data['related_zipcodes_ids']!='') {
                foreach ($this->request->data['related_zipcodes_ids'] as $related_zipcodes_id) {
                    $new_related_zipcodes_ids[] = $related_zipcodes_id;
                    $related_zipcode_exists = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id, 'BrokersZipcodes.valid_zipcodes_id' => $related_zipcodes_id])->first();
                    if ($related_zipcode_exists) {
                        continue;
                    }
                    $new_related_zipcode_entity = [
                        'broker_id' => $user->id,
                        'valid_zipcodes_id' => $related_zipcodes_id
                    ];
                    $new_related_zipcode = $this->BrokersZipcodes->newEntity($new_related_zipcode_entity);
                    $this->BrokersZipcodes->save($new_related_zipcode);
                }

                if (sizeof($new_related_zipcodes_ids)>0) {
                    $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id, 'BrokersZipcodes.valid_zipcodes_id NOT IN' => $new_related_zipcodes_ids])->all();
                } else {
                    $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id])->all();
                }

                if ($delete_brokers_zipcodes) {
                    foreach ($delete_brokers_zipcodes as $delete_brokers_zipcode) {
                        $this->BrokersZipcodes->delete($delete_brokers_zipcode);
                    }
                }

            } else {
                $delete_brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $user->id])->all();
                if ($delete_brokers_zipcodes) {
                    foreach ($delete_brokers_zipcodes as $delete_brokers_zipcode) {
                        $this->BrokersZipcodes->delete($delete_brokers_zipcode);
                    }
                }
            }

            $this->Flash->success(__('Vermittler erfolgreich gespeichert.'));
            return $this->redirect('/admin/vermittler_verwalten/' . base64_encode($case));
        }

        if (isset($case) && strlen($case)>0) {
            $this->set('case', $case);
        }

        // Zugewiesene Postleitzahlenbereiche für aktuellen Vermittler holen
        $related_zipcodes_ids = array();
        $brokers_zipcodes = $this->BrokersZipcodes->find()->where(['BrokersZipcodes.broker_id' => $id])->all();
        foreach ($brokers_zipcodes as $brokers_zipcode) {
            $related_zipcodes_ids[] = $brokers_zipcode->valid_zipcodes_id;
        }

        $user->related_zipcodes_ids = $related_zipcodes_ids;


        if (sizeof($available_zipcodes_ids)>0) {
            $valid_zipcodes = $this->ValidZipcodes->find()->where(['ValidZipcodes.id IN' => $available_zipcodes_ids])->all();
            if ($valid_zipcodes) {
                $valid_zipcodes_array = array();
                foreach ($valid_zipcodes as $valid_zipcode) {
                    $valid_zipcodes_array[$valid_zipcode->id] = $valid_zipcode->zipcode . " | " . $valid_zipcode->city;
                }
                $this->set('valid_zipcodes', $valid_zipcodes_array);
            }
        }
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Methode zur Darstellung aller Einrichtungen
     */
    public function institutions()
    {
        $this->set('title', __('Einrichtungen verwalten'));
        $this->loadModel('AdminsZipcodes');
        $this->loadModel('Institutions');
        $this->loadModel('InstitutionsZipcodes');
        $this->loadModel('ValidZipcodes');

        // Hole zugewiesene Postleitzahlenbereiche für aktuellen Administrator
        $valid_admin_zipcodes = $this->AdminsZipcodes
            ->find()
            ->select(['AdminsZipcodes.valid_zipcodes_id'])
            ->where(['AdminsZipcodes.admin_id' => $this->Auth->user('id')])
            ->hydrate(false);

        $valid_admin_zipcodes_ids = $this->InstitutionsZipcodes
            ->find()
            ->select(['InstitutionsZipcodes.institution_id'])
            ->where(['InstitutionsZipcodes.zipcode_id IN' => $valid_admin_zipcodes])
            ->hydrate(false);

        // Institutionen holen, die zu den zugewiesenen Postleitzahlen zugehörig sind
        $institutions = $this->Institutions->find()->where(['Institutions.id IN' => $valid_admin_zipcodes_ids, 'Institutions.created_by' => $this->Auth->user('id')]);

        $institutions_zipcodes = $this->InstitutionsZipcodes->find();
        $institutions_zipcodes_array = array();

        foreach ($institutions_zipcodes as $institutions_zipcode) {
            $zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.id' => $institutions_zipcode->zipcode_id])->first();
            $institutions_zipcodes_array[$institutions_zipcode->institution_id][$zipcode->zipcode] = $zipcode->zipcode;
        }

        $this->set('institution_zipcodes_array', $institutions_zipcodes_array);

        $this->set('institutions', $institutions);
    }

    /**
     * Methode zum Löschen einer Institution
     *
     * @param null $id
     * @return \Cake\Network\Response|null
     */
    public function deleteInstitution($id = null)
    {
        $this->loadModel('Institutions');
        $this->loadModel('InstitutionsZipcodes');

        $institution = $this->Institutions->find()->where(['Institutions.id' => $id])->first();

        $institution_zipcodes = $this->InstitutionsZipcodes->find()->where(['InstitutionsZipcodes.institution_id' => $id]);

        if ($institution_zipcodes) {
            foreach ($institution_zipcodes as $institution_zipcode) {
                $this->InstitutionsZipcodes->delete($institution_zipcode);
            }
        }

        if ($this->Institutions->delete($institution)) {
            $this->Flash->success(__('Einrichtung erfolgreich gelöscht.'));
            return $this->redirect('/admin/einrichtungen_verwalten');
        } else {
            $this->Flash->error(__('Einrichtung wurde nicht gelöscht.'));
            return $this->redirect($this->referer());
        }
    }

    /**
     * Methode zum Bearbeiten einer Institution
     *
     * @param null $id
     */
    public function editInstitution($id = null)
    {
        $this->loadModel('AdminsZipcodes');
        $this->loadModel('FileUploads');
        $this->loadModel('Institutions');
        $this->loadModel('InstitutionsZipcodes');
        $this->loadModel('ValidZipcodes');
        if (!$id) {
            $institution = $this->Institutions->newEntity($this->request->data);
            $this->set('title', __('Institution anlegen'));
        } else {
            $institution = $this->Institutions->find()->where(['Institutions.id' => $id])->first();
            $this->set('title', __('Institution bearbeiten'));
        }

        if ($this->request->is(['post', 'put', 'patch'])) {

            $this->request->data['created_by'] = $this->Auth->user('id');
            $patched_institution = $this->Institutions->patchEntity($institution, $this->request->data);
            if ($saved_institution = $this->Institutions->save($patched_institution)) {

                // Falls das aktuellen Bild gelöscht werden soll
                if (isset($this->request->data['delete_image']) && $this->request->data['delete_image'] == '1') {
                    $file_upload_exists = $this->FileUploads->find()->where(['FileUploads.institution_id' => $saved_institution->id])->first();
                    if ($file_upload_exists) {
                        @unlink($file_upload_exists->src);
                        $this->FileUploads->delete($file_upload_exists);
                    }
                }

                // Falls neues Bild hochgeladen wurde
                if (isset($this->request->data['image']) && $this->request->data['image']) {

                    // Neues Bild für Institution hochladen
                    $upload = $this->request->data['image'];

                    // Prüfe, ob bereits ein Dateiupload vorhanden ist - maximal ein Bild pro Einrichtung / Institution
                    $file_upload_exists = $this->FileUploads->find()->where(['FileUploads.institution_id' => $saved_institution->id])->first();
                    if ($file_upload_exists) {
                        @unlink($file_upload_exists->src);
                        $this->FileUploads->delete($file_upload_exists);
                    }

                    $file_upload = $this->FileUpload->process($upload);

                    $new_fileupload_entity = [
                        'institution_id' => $saved_institution->id,
                        'src' => $file_upload,
                        'type' => $upload['type'],
                        'filename' => $upload['name'],
                        'size' => $upload['size']
                    ];

                    $fileupload_entity = $this->FileUploads->newEntity($new_fileupload_entity);
                    $this->FileUploads->save($fileupload_entity);
                }

                // Zugewiesene Postleitzahlen zu Institution löschen
                $institution_zipcodes = $this->InstitutionsZipcodes->find()->where(['InstitutionsZipcodes.institution_id' => $saved_institution->id]);
                foreach ($institution_zipcodes as $institution_zipcode) {
                    $this->InstitutionsZipcodes->delete($institution_zipcode);
                }

                // Alle ausgewählten Postleitzahlen zu Institution speichern
                foreach ($this->request->data['zipcodes'] as $zipcode) {
                    $valid_zipcode = $this->ValidZipcodes->find()->where(['ValidZipcodes.zipcode' => $zipcode])->first();
                    $new_institution_zipcode_entity_array = [
                        'institution_id' => $saved_institution->id,
                        'zipcode_id' => $valid_zipcode->id
                    ];
                    $new_institution_zipcode_entity = $this->InstitutionsZipcodes->newEntity($new_institution_zipcode_entity_array);
                    $this->InstitutionsZipcodes->save($new_institution_zipcode_entity);
                }
                $this->Flash->success(__('Einrichtung erfolgreich gespeichert.'));
                return $this->redirect('/admin/einrichtungen_verwalten');
            } else {
                $this->Flash->error(__('Einrichtung wurde nicht gespeichert.'));
                return $this->redirect($this->referer());
            }
        }

        $user_admin_valid_zipcodes = $this->AdminsZipcodes
            ->find()
            ->select(['AdminsZipcodes.valid_zipcodes_id'])
            ->where(['AdminsZipcodes.admin_id' => $this->Auth->user('id')])
            ->hydrate(false);

        $zipcodes = $this->ValidZipcodes->find()
            ->select(['ValidZipcodes.zipcode', 'ValidZipcodes.city'])
            ->where(['ValidZipcodes.id IN' => $user_admin_valid_zipcodes]);

        $this->set('zipcodes', $zipcodes);

        $file_upload = $this->FileUploads->find()->where(['FileUploads.institution_id' => $institution->id])->first();

        if ($file_upload) {
            $institution->file_upload = $file_upload;
        }

        $this->set('institution', $institution);

        $institution_zipcodes = $this->InstitutionsZipcodes->find()->where(['InstitutionsZipcodes.institution_id' => $institution->id]);

        $institution_zipcodes_array = array();
        foreach ($institution_zipcodes as $institution_zipcode) {
            $zipcode_find = $this->ValidZipcodes->find()->where(['ValidZipcodes.id' => $institution_zipcode->zipcode_id])->first();
            if ($zipcode_find) {
                if (isset($institution_zipcodes_array[$zipcode_find->zipcode])) {
                    continue;
                }

                $institution_zipcodes_array[$zipcode_find->zipcode] = $zipcode_find->zipcode;
            }
        }

        $this->set('institution_zipcodes_array', $institution_zipcodes_array);

    }
}
