<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\Log\Log;
use Cake\Mailer\Email;

class CheckNewDonationsTask extends Shell {

    public function __checkNewDonations() {

        Log::debug('start function');

        $this->loadModel('Logs');
        $this->loadModel('Donations');

        $new_donations_count = $this->Donations->find()->where(['Donations.send' => 0])->count();

        if ($new_donations_count > 5) {
            $email = new email('default');
            $email->helpers(array('Html', 'Text'));
            $email->viewVars(
                array(
                    // TODO
                )
            );
            $email->template('new_donations');
            $email->emailformat('text');
            $email->from(array('matches@donate.de' => 'Spendenplattform'));
            $email->to(array(/* TODO: Spendenvermittler (Alle oder nur einer?) */));
            $email->subject('Deine Zugangsdaten für unseren Online-Bereich');
            $email->send();

            $new_log_entity = [
                'category' => 'donation_news',
                'internal_log' => 1,
                'user_id' => 1,
                'log_title' => 'Newsletter verschickt.',
                'log_text' => 'Newsletter für Spenden verschickt.'
            ];
            $new_log = $this->Logs->newEntity($new_log_entity);
            $this->Logs->save($new_log);
        } else {
            $log = $this->Logs->find()->where(['Logs.category' => 'donation_news', 'Logs.internal_log' => 1])->order(['Logs.created' => 'DESC'])->first();
            $date_1 = new \DateTime(date('Y-m-d H:i:s', strtotime($log->created)));
            $date_2 = new \DateTime(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'))));

            if ($date_1->diff($date_2)->d > 7) {
                $email = new email('default');
                $email->helpers(array('Html', 'Text'));
                $email->viewVars(
                    array(
                        // TODO
                    )
                );
                $email->template('new_donations');
                $email->emailformat('text');
                $email->from(array('matches@donate.de' => 'Spendenplattform'));
                $email->to(array(/* TODO: Spendenvermittler (Alle oder nur einer?) */));
                $email->subject('Deine Zugangsdaten für unseren Online-Bereich');
                $email->send();

                $new_log_entity = [
                    'category' => 'donation_news',
                    'internal_log' => 1,
                    'user_id' => 1,
                    'log_title' => 'Newsletter verschickt.',
                    'log_text' => 'Newsletter für Spenden verschickt.'
                ];
                $new_log = $this->Logs->newEntity($new_log_entity);
                $this->Logs->save($new_log);
            }
        }

        Log::debug('end function');
    }
}