<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\Log\Log;
use Cake\Mailer\Email;

class CheckMatchesTask extends Shell {

    public function __checkMatches() {

        Log::debug('start function');

        $this->loadModel('Matches');

        $matches_count = $this->Matches->find()->where(['Matches.seen' => 0])->count();

        if ($matches_count > 5) {
            $email = new email('default');
            $email->helpers(array('Html', 'Text'));
            $email->viewVars(
                array(
                    // TODO
                )
            );
            $email->template('new_matches');
            $email->emailformat('text');
            $email->from(array('matches@donate.de' => 'Spendenplattform'));
            /* $email->to(array(TODO: Spendenvermittler (Alle oder nur einer?)); */
            $email->subject('Deine Zugangsdaten für unseren Online-Bereich');
            $email->send();
        }

        Log::debug('end function');
    }
}