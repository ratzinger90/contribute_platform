<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\Log\Log;

class DeleteOldTimesTask extends Shell {

    public function __deleteOldTimes() {

        Log::debug('start function');

        $this->loadModel('Donations');
        $this->loadModel('Searches');
        $this->loadModel('TimeSearches');
        $this->loadModel('TimeDonations');


        // Zeitspenden überprüfen mit abgelaufenen Zeitangaben
        $old_time_donations = $this->TimeDonations->find()->where(['TimeDonations.end_date <' => date('Y-m-d')]);

        if ($old_time_donations) {
            foreach ($old_time_donations as $old_time_donation) {
                $check_old_donation = $this->TimeDonations->find()->where(['TimeDonations.id' => $old_time_donation->id])->first();
                if (!$check_old_donation) {
                    continue;
                }
                $old_donation = $this->TimeDonations->get($old_time_donation->id);
                $check_for_more = $this->TimeDonations->find()->where(['TimeDonations.donation_id' => $old_donation->donation_id])->count();
                if ($check_for_more>0) {
                    continue;
                } else {
                    $get_donation = $this->Donations->find()->where(['Donations.id' => $old_donation->donation_id])->first();
                    if ($get_donation) {
                        $donation = $this->Donations->get($get_donation->id);
                        $this->Donations->delete($donation);
                        Log::debug("Zeitspende gelöscht: " . $donation->id);
                    }
                }

                // Zeitangabe löschen
                $this->TimeDonations->delete($old_donation);
                Log::debug("Zeitangabe [Spende] gelöscht: " . $old_donation->id);
            }
        }

        // Zeitgesuche überprüfen mit abgelaufenen Zeitangaben
        $old_time_searches = $this->TimeSearches->find()->where(['TimeSearches.end_date <' => date('Y-m-d')]);

        if ($old_time_searches) {
            foreach ($old_time_searches as $old_time_search) {
                $check_old_search = $this->TimeSearches->find()->where(['TimeSearches.id' => $old_time_search->id])->first();
                if (!$check_old_search) {
                    continue;
                }
                $old_search = $this->TimeSearches->get($old_time_search->id);
                $check_for_more = $this->TimeSearches->find()->where(['TimeSearches.search_id' => $old_search->search_id])->count();
                if ($check_for_more>0) {
                    continue;
                } else {
                    $get_search = $this->Searches->find()->where(['Searches.id' => $old_search->search_id])->first();
                    if ($get_search) {
                        $search = $this->Searches->get($get_search->id);
                        $this->Searches->delete($search);
                        Log::debug("Zeitgesuch gelöscht: " . $search->id);
                    }
                }

                // Zeitangabe löschen
                $this->TimeSearches->delete($old_search);
                Log::debug("Zeitangabe [Gesuch] gelöscht: " . $old_search->id);
            }
        }

        Log::debug('end function');
    }
}