<?php

namespace App\Shell\Task;

use Cake\Console\Shell;

class LdapCronTask extends Shell {

    public function run() {
        $this->loadModel('Tubs');

        $server = "ldapk5.tu-bs.de";

        $ldap = @ldap_connect($server);

        $username = "y0054316";
        $password = "+S20h1h20m17c30+";

        $dn = "uid=$username,ou=people,dc=tu-bs,dc=de";

        $config = "ou=people,dc=tu-bs,dc=de";

        @ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        @ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($ldap, $dn, $password);

        if ($bind) {

            $array =
                [
                    'y0027048',
                    'y0032840',
                    'y0057775',
                    'y0058420',
                    'y0059832',
                    'y0059276',
                    'y0069855',
                    'y0070805',
                    'y0075976',
                    'y0073624',
                    'y0080459',
                    'y0081424',
                    'y0085312',
                ];

            //for ($i=44752;$i<10000000;$i++) {
                //$search_uid = "y" . str_pad($i, 7 ,'0', STR_PAD_LEFT);
            foreach ($array as $element) {
                $search = @ldap_search($ldap, $config, "uid=".$element);

                if ($search) {
                    $result = @ldap_get_entries($ldap, $search);
                    if ($result) {
                        $this->out('uid: ' . $element);
                        if ($result['count']==0) {
                            continue;
                        }
                        var_dump($result);
                        /*
                        $new_tubs_entry_array = [
                            'uid' => $result[0]['uid'][0],
                            'name' => $result[0]['cn'][0],
                            'matrikelnummer' => $result[0]['matrikelnummer'][0]
                        ];

                        $new_tubs_entry = $this->Tubs->newEntity($new_tubs_entry_array);
                        $this->Tubs->save($new_tubs_entry);
                        */
                    }
                }
            }
            die();
        }
    }
}

// y0080459