<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Call Entity.
 *
 * @property int $id
 * @property int $configuration_id
 * @property \App\Model\Entity\Configuration $configuration
 * @property string $call_title
 * @property string $call_text
 * @property string $link
 * @property string $link_title
 * @property \Cake\I18n\Time $start_date_time
 * @property \Cake\I18n\Time $end_date_time
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Call extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
