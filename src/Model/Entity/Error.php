<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Error Entity.
 *
 * @property int $id
 * @property string $subheading
 * @property string $exception_stack_trace
 * @property string $file
 * @property string $templateName
 * @property string $exception_stack_trace_nav
 * @property bool $send
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Error extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
