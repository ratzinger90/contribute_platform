<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BrokersZipcode Entity.
 *
 * @property int $id
 * @property int $broker_id
 * @property int $valid_zipcodes_id
 * @property \App\Model\Entity\ValidZipcode $valid_zipcode
 * @property \Cake\I18n\Time $created
 */
class BrokersZipcode extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
