<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Support Entity.
 *
 * @property int $id
 * @property bool $done
 * @property string $comment
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $current_url
 * @property string $http_user_agent
 * @property string $controller
 * @property string $action
 * @property string $pass
 * @property string $message
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Support extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
