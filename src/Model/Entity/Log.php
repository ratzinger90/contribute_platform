<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Log Entity.
 *
 * @property int $id
 * @property string $category
 * @property bool $internal_log
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $donate_id
 * @property \App\Model\Entity\Donate $donate
 * @property int $search_id
 * @property \App\Model\Entity\Search $search
 * @property string $log_title
 * @property string $log_text
 * @property \Cake\I18n\Time $created
 */
class Log extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
