<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fail Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $http_user_agent
 * @property string $additional_vars
 * @property string $request_url
 * @property string $description
 * @property bool $send
 * @property \Cake\I18n\Time $created
 */
class Fail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
