<?php
namespace App\Model\Table;

use App\Model\Entity\FileUpload;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FileUploads Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Donations
 */
class FileUploadsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('file_uploads');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->belongsTo('Donations', [
            'foreignKey' => 'donation_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Searches', [
            'foreignKey' => 'search_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('src', 'create')
            ->notEmpty('src');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('filename', 'create')
            ->notEmpty('filename');

        $validator
            ->requirePresence('size', 'create')
            ->notEmpty('size');

        return $validator;
    }

    protected function _initializeSchema(\Cake\Database\Schema\Table $table)
    {
        $table->columnType('image', 'proffer.file');
        return $table;
    }
}
