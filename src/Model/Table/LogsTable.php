<?php
namespace App\Model\Table;

use App\Model\Entity\Log;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Logs Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Donates
 * @property \Cake\ORM\Association\BelongsTo $Searches
 */
class LogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('logs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Donates', [
            'foreignKey' => 'donate_id'
        ]);
        $this->belongsTo('Searches', [
            'foreignKey' => 'search_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('category');

        $validator
            ->add('internal_log', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('internal_log');

        $validator
            ->allowEmpty('log_title');

        $validator
            ->allowEmpty('log_text');

        return $validator;
    }
}
