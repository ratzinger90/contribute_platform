<?php
namespace App\Model\Table;

use App\Model\Entity\Error;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Errors Model
 *
 */
class ErrorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('errors');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('subheading');

        $validator
            ->allowEmpty('exception_stack_trace');

        $validator
            ->allowEmpty('file');

        $validator
            ->allowEmpty('templateName');

        $validator
            ->allowEmpty('exception_stack_trace_nav');

        $validator
            ->boolean('send')
            ->allowEmpty('send');

        return $validator;
    }
}
