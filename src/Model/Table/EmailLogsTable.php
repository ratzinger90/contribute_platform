<?php
namespace App\Model\Table;

use App\Model\Entity\EmailLog;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailLogs Model
 *
 */
class EmailLogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('email_logs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('send', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('send');

        $validator
            ->allowEmpty('subject');

        $validator
            ->allowEmpty('content');

        $validator
            ->allowEmpty('recipient_name');

        $validator
            ->allowEmpty('recipient_email');

        $validator
            ->allowEmpty('sender_name');

        $validator
            ->allowEmpty('sender_email');

        return $validator;
    }
}
