<?php
namespace App\Model\Table;

use App\Model\Entity\Keyword;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Keywords Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Donations
 * @property \Cake\ORM\Association\BelongsToMany $Searches
 */
class KeywordsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('keywords');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Donations', [
            'foreignKey' => 'keyword_id',
            'targetForeignKey' => 'donation_id',
            'joinTable' => 'keywords_donations'
        ]);
        $this->belongsToMany('Searches', [
            'foreignKey' => 'keyword_id',
            'targetForeignKey' => 'search_id',
            'joinTable' => 'keywords_searches'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('keyword', 'create')
            ->notEmpty('keyword');

        return $validator;
    }
}
