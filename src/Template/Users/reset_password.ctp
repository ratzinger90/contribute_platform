<div class="row">
    <div id="login_form_div" class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;">
        <?= $this->Flash->render() ?>
        <?= $this->Form->create() ?>
        <div class="row">
            <div class="col-xs-12">
                <?= $this->Form->input(__('value'), ['label' => 'E-Mail-Adresse', 'class' => 'form-control', 'placeholder' => 'E-Mail-Adresse eingeben ...']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-12">
                <?php
                $key_tag = $this->Html->tag('i', '', ['class' => 'fa fa-key', 'escape' => false]); ?>
                <?= $this->Form->button('Passwort anfordern ' . $key_tag, ['class' => 'btn btn-u reset_submit', 'escape' => false]) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    </div>
</div>


<script type="text/javascript" src="/js/login_helper.js"></script>