<div class="row">
    <div class="col-md-12 md-margin-bottom-40" style="padding-left:0px">
        <div class="posts">
            <div class="headline"><h2>Neueste Gesuche</h2></div>
            <?php
            foreach ($newest_searches as $search) {
                switch($search->category) {
                    case "thing":
                        $href = '/sachgesuch/' . $search->url_title;
                        $placeholder = '/img/thing_placeholder.png';
                        break;
                    case "time":
                        $href = '/zeitgesuch/' . $search->url_title;
                        $placeholder = '/img/time_placeholder.png';
                        break;
                }

                ?>
                <div class="row">

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><a href="<?= $href ?>">
                            <?php
                            if (isset($search->file_uploads[0]) && file_exists(rtrim(WWW_ROOT, '/') . $search->file_uploads[0]->src)) {
                                ?>
                                <img src="<?= $search->file_uploads[0]->src ?>" style="max-width:125px;max-height:100px" />
                            <?php
                            } else {
                                ?>
                                <img src="<?= $placeholder ?>" style="width:125px"/>
                                <?php
                            }
                            ?>
                        </a></div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <h3><a class="hover-effect" href="<?= $href ?>"> <?php
                                if (strlen($search->title)>30) {
                                    echo substr($search->title,0,27) . "...";
                                } else {
                                    echo $search->title;
                                } ?> </a></h3>
                        <p style="text-align:justify">
                                <?php
                                if (strlen($search->description)>200) {
                                    echo substr($search->description, 0, 197). " ...";
                                } else {
                                    echo $search->description;
                                }
                                ?>
                        </p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <a style="color:#ffffff" href="<?= $href ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-search"></i></a>
                    </div>
                </div>
                <hr style="margin-top:0px;margin-bottom:15px">
            <?php
            }
            ?>
        </div>
    </div>
</div>