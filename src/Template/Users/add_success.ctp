<div class="col-xs-12" style="padding-left:0px;padding-right:0px;text-align:center">
    <h3>Deine Bewerbung wurde erfolgreich abgeschickt. Sobald wir diese überprüft haben, erhälst Du eine E-Mail und dein Konto als Vermittler wird freigeschaltet!</h3>
    <br/><p style="text-align:center">Du wirst in wenigen Sekunden zurück zur Startseite geleitet ...</p>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        setTimeout(function() {
            location.href = "/";
        }, 5000);
    });
</script>
