<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:0px;">
    <div class="row">
        <div id="login_form_div" class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;">
            <?= $this->Form->create() ?>
            <div class="row" style="margin-top:10px;margin-bottom:-20px;">
                <div class="col-xs-6">
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" id="email" name="email" class="form-control" placeholder="E-Mail-Adresse" style="height:36px">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Passwort" style="height:36px">
                    </div>
                </div>
            </div>
            <br/>
            <div id="failed_login_div" style="display: none;" class="row">
                <div class="col-xs-12">
                    <p style="font-size: 12px;"><a href="/passwort_vergessen" class="red_anchor">Passwort vergessen?</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php $login_tag = $this->Html->tag('i', '', ['class' => 'fa fa-sign-in', 'escape' => false]) ?>
                    <?= $this->Form->button('Einloggen '.$login_tag, ['class' => 'btn btn-u login_submit', 'escape' => false]) ?>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

        </div>
    </div>
    <div class="row">
        <hr>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p style="font-size: 12px;">Du hast mehrere Spenden abzugeben?
                <a href="/registrieren"><i class="fa fa-angle-double-right"></i>&nbsp;Jetzt Spenderkonto anlegen ...</a>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p style="font-size: 12px;">Zugangsdaten vergessen?
                <a href="/passwort_vergessen"><i class="fa fa-angle-double-right"></i>&nbsp;Zugangsdaten anfordern ...</a>
            </p>
        </div>
    </div>
    <br/>
    <div class="row">
        <hr>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p style="font-size: 12px;">Du möchtest Spendenvermittler werden?
                <a href="/bewerben"><i class="fa fa-angle-double-right"></i>&nbsp;Jetzt bewerben ...</a>
            </p>
        </div>
    </div>
</div>


<script type="text/javascript" src="/js/login_helper.js"></script>