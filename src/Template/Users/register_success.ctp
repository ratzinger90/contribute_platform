<div class="col-xs-12" style="padding-left:0px;padding-right:0px;text-align:center">
    <h3>Dein Konto wurde erfolgreich erstellt. Wir haben dir eine E-Mail gesendet mit einem Bestätigungslink. Öffne den Link in deinem Browser um dein Konto zu aktivieren.</h3>
    <br/><p style="text-align:center">Du wirst in wenigen Sekunden zurück zur Startseite geleitet ...</p>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        setTimeout(function() {
            location.href = "/";
        }, 5000);
    });
</script>
