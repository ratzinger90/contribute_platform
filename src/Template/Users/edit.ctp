<div class="row">
    <?= $this->Form->create($address) ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px">
        <div class="headline"><h2><?= __('Konto bearbeiten') ?></h2></div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>E-Mail-Adresse</label>
                <?= $this->Form->input(__('email'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Telefonnummer</label>
                <?= $this->Form->input(__('phone'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Anschrift</label>
                <?= $this->Form->input(__('street'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <label>Postleitzahl</label>
                <?= $this->Form->input(__('zip'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Ort</label>
                <?= $this->Form->input(__('city'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= $this->Form->button(__('Änderungen speichern'), ['class' => 'btn btn-u']) ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>