<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
    <?= $this->element('new_add_help_sidebar') ?>
</div>
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px">
    <div class="headline"><h2>Bewerbung als Spendenvermittler</h2></div>
    <br/>
    <br/>
    <?= $this->Form->create($user, ['id' => 'register_form']) ?>
    <?php
    if (!isset($logged_in)) {
        ?>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('firstname'), ['label' => 'Dein Vorname', 'required' => true, 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('lastname'), ['label' => 'Dein Name', 'required' => true, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $this->Form->input(__('street'), ['label' => 'Straße und Hausnummer', 'required' => true, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('zip'), ['label' => 'Deine Postleitzahl', 'required' => true, 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('city'), ['label' => 'Dein Ort [wird automatisch ausgefüllt]', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('phone'), ['label' => 'Deine Telefonnummer', 'required' => true, 'class' => 'form-control no-border-radius']) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('fax'), ['label' => 'Telefax', 'class' => 'form-control no-border-radius']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'required' => true, 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('email_repeat'), ['label' => 'E-Mail-Adresse (Wiederholung)', 'required' => true, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('password'), ['label' => 'Gewünschtes Passwort', 'required' => true, 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <?= $this->Form->input(__('password_repeat'), ['type' => 'password', 'label' => 'Passwort wiederholen', 'required' => true, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <?php
    }
    ?>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <label style="display:inline;float:left;">Kurze Beschreibung deiner Person und Motivation Dich als Spendenvermittler zu betätigen:</label>
            <p style="display:inline;float:left;" id="broker_text_length"></p>
            <?= $this->Form->textarea(__('broker_text'), ['class' => 'form-control']) ?>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <p><em>Wähle hier den Partner aus, über den Du gerne als Spendenvermittler tätig wärst.</em></p>
            <?= $this->Form->select(__('broker_partner'), $partners, ['placeholder' => 'Bitte Partner auswählen', 'label' => 'Gewünschte Postleitzahl', 'class' => 'form-control']) ?>
        </div>
    </div>
    <br/>
    <br/>
    <div class="row">
        <div class="col-xs-1" style="width: 30px">
            <?= $this->Form->checkbox(__('agb'), ['class' => 'form-control no-border-radius', 'style' => 'width:15px;height:15px']) ?>
        </div>
        <div class="col-xs-11">
            <label><strong>Ja</strong>, ich akzeptiere die <a data-toggle="modal" href="#agb_modal">AGB</a> und den <a data-toggle="modal" href="#datenschutz_modal">Datenschutz</a>.</label>
        </div>
    </div>
    <?php
    if (!isset($logged_in) && !in_array($_SERVER['HTTP_HOST'], $this->request->session()->read('captcha_free_hosts'))) {
        ?>
        <br/>
        <div class="row">
            <div class="col-xs-12">
                <div class="g-recaptcha" data-sitekey="XXXX"></div>
            </div>
        </div>
        <?php
    }
    ?>
    <br/>
    <?php $register_tag = $this->Html->tag('i', '', ['class' => 'fa fa-check', 'escape' => false]); ?>
    <?= $this->Form->button('Bewerbung abschicken '.$register_tag, ['class' => 'btn btn-u register_submit', 'escape' => false, 'style' => 'float:left;']) ?>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript" src="/js/register_helper.js"></script>

<?= $this->element('agb') ?>
<?= $this->element('datenschutz') ?>
