<?php
if ($this->request->params['controller'] == 'Helpers' && $this->request->params['action'] == 'institutions') {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo $this->fetch('content');
    echo '</div>';
}else if (isset($logged_in) &&
    $logged_in == true &&
    isset($role) &&
    $role == 'super' &&
    (($this->request->params['controller'] == 'Users' || $this->request->params['controller'] == 'ValidZipcodes' || $this->request->params['controller'] == 'EmailTemplates' || ($this->request->params['controller'] == 'Pages' && ($this->request->params['action'] == 'edit' || $this->request->params['action'] == 'index')) || $this->request->params['controller'] == 'Helpers' || ($this->request->params['controller'] == 'Searches' && $this->request->params['action'] == 'add')) && $this->request->params['action'] != 'keywordDonations' && $this->request->params['action'] != 'keywordSearches')) {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo '<div class="col-md-9" style="padding-left:0px;padding-right:0px;">'.$this->fetch('content').'</div>';
    echo '<div class="col-md-3 md-margin-bottom-40" style="background-color:#eeeeee">'.$this->element('super_sidebar').'</div>';
    echo '</div>';
}else if (isset($logged_in) &&
    $logged_in == true &&
    isset($role) &&
    $role == 'broker' &&
    (($this->request->params['controller'] == 'Users' || $this->request->params['controller'] == 'Helpers' || ($this->request->params['controller'] == 'Searches' && $this->request->params['action'] == 'add') || ($this->request->params['controller'] == 'Donations' && $this->request->params['action'] == 'add')) && $this->request->params['action'] != 'keywordDonations' && $this->request->params['action'] != 'keywordSearches')) {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo '<div class="col-md-9" style="padding-left:0px;padding-right:0px;">'.$this->fetch('content').'</div>';
    echo '<div class="col-md-3 md-margin-bottom-40" style="background-color:#eeeeee">'.$this->element('vermittler_sidebar').'</div>';
    echo '</div>';
} else if (isset($logged_in) &&
    $logged_in == true &&
    isset($role) &&
    $role == 'admin' &&
    ($this->request->params['controller'] == 'Users' || $this->request->params['controller'] == 'Helpers' || $this->request->params['controller'] == 'EmailTemplates' || ($this->request->params['controller'] == 'Pages' && ($this->request->params['action'] == 'index' || $this->request->params['action'] == 'edit')))) {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo '<div class="col-md-9">' . $this->fetch('content') . '</div>';
    echo '<div class="col-md-3 md-margin-bottom-40" style="background-color:#eeeeee">' . $this->element('admin_sidebar') . '</div>';
    echo '</div>';
} else if (isset($logged_in) &&
    $logged_in == true &&
    isset($role) &&
    $role == 'token_account' &&
    ($this->request->params['controller'] == 'Users' && $this->request->params['action'] == 'add')) {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo '<div class="col-md-12">' . $this->fetch('content') . '</div>';
    echo '</div>';
} else if (isset($logged_in) &&
    $logged_in == true &&
    isset($role) &&
    $role == 'token_account' &&
    (($this->request->params['controller'] == 'Users' || $this->request->params['controller'] == 'Helpers') || ($this->request->params['controller'] == 'Donations' && ($this->request->params['action'] == 'add' || $this->request->params['action'] == 'edit')))) {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo '<div class="col-md-9">' . $this->fetch('content') . '</div>';
    echo '<div class="col-md-3 md-margin-bottom-40" style="background-color:#eeeeee">' . $this->element('nutzer_sidebar') . '</div>';
    echo '</div>';
} else if ($this->request->params['controller']=='Users') {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo $this->fetch('content');
    echo '</div>';
} else if ($this->request->params['controller']=='Donations' || $this->request->params['controller']=='Searches' || ($this->request->params['controller']=='Helpers' && ($this->request->params['action']=='keywordDonations' || $this->request->params['action']=='keywordSearches'))) {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo $this->fetch('content');
    echo '</div>';
} else if ($this->request->params['controller']=='Pages') {
    echo '<div class="row smartphone_no_margin" style="margin-left:0px;margin-right:0px;padding-top:10px;padding-bottom:10px">';
    echo $this->fetch('content');
    echo '</div>';
} else {
    echo '<div class="row smartphone_no_margin" style="margin-left:15px;margin-right:15px;padding-top:10px;padding-bottom:10px">';
    echo $this->fetch('content');
    echo '</div>';
}