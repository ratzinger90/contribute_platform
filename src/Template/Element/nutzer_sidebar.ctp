<div class="posts" style="text-align:right">
    <div class="headline" style="text-align:right"><h2>Verwaltung</h2></div>
    <div class="col-xs-12">
        <p><a href="/meine_spenden/<?php echo base64_encode('spender_spenden')."|"; ?>">Meine Spenden&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <p><a href="/spende_abgeben">Neue Spende abgeben&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p>Du möchtest Spenden vermitteln?</p>
        <p><a href="/bewerben">Jetzt als Vermittler bewerben&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/konto_bearbeiten">Konto bearbeiten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <p><a href="/passwort_vergeben">Passwort ändern&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/logout">Ausloggen&nbsp;<i class="fa fa-sign-out"></i></a></p>
    </div>
</div>