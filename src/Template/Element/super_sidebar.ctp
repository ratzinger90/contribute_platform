<div class="posts" style="text-align:right">
    <div class="headline" style="text-align:right"><h2>Verwaltung</h2></div>
    <div class="col-xs-12">
        <p><a href="/super/nutzer_verwalten">Nutzer verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <p><a href="/super/plz_verwalten">Regionen verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <p><a href="/super/partner_verwalten">Partner verwalten <i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/super/seiten_verwalten">Seiten verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <p><a href="/super/emails_verwalten">E-Mails verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <p><a href="/super/meldungen_verwalten">Meldungen verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/super/konfiguration">Konfiguration bearbeiten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/super/passwort_aendern">Passwort ändern&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/logout">Ausloggen&nbsp;<i class="fa fa-sign-out"></i></a></p>
    </div>
</div>