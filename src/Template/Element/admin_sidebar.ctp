<div class="posts" style="text-align:right">
    <div class="headline" style="text-align:right"><h2>Verwaltung</h2></div>
    <div class="col-xs-12">
        <p><a href="/admin/vermittler_verwalten/<?php echo base64_encode('new')."|"; ?>">Bewerber verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <p><a href="/admin/vermittler_verwalten/<?php echo base64_encode('active')."|"; ?>">Vermittler verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/admin/einrichtungen_verwalten">Einrichtungen verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/admin/konto_bearbeiten">Passwort ändern&nbsp;<i class="fa fa-angle-double-right"></i></a></p>
        <br/>
        <p><a href="/logout">Ausloggen&nbsp;<i class="fa fa-sign-out"></i></a></p>
    </div>
</div>