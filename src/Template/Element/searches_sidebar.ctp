<div class="row searches_sidebar_row">
    <div class="headline" style="text-align:left"><h2 style="font-size:16px">Durchsuche alle Gesuche</h2></div>
    <!-- Suchformular -->
    <div class="row">
        <form id="searches_sidebar_form" method="post" action="/suche/">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-6">
                <input type="text" name="searches_sidebar_input" class="form-control" style="width:209px" />
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-6">
                <button class="btn btn-u searches_sidebar_search" style="height:34px;margin-left:-60px">Suchen&nbsp;<i class="fa fa-angle-double-right"></i></button>
            </div>
        </form>
    </div>
    <hr>
    <!-- Schlagwörter -->
    <!--
    <div class="row hidden-xs">
        <div class="col-xs-12">
            <ul class="keywords_sidebar" style="list-style: none">
                <?php
                /* TODO: Schlagwörter erstmal raus
                $counter = 0;
                foreach ($keywords as $word) {
                    if ($counter >= 24) {
                        break;
                    }
                    ?>
                    <li><a href="/gesuche/<?= $case ?>/<?= $word['name'] ?>"><i class="fa fa-angle-double-right"></i>&nbsp;<?= $word['word']; ?>&nbsp;(<?= $word['count'] ?>)</a></li>
                    <?php
                    $counter += 1;
                }
                */
                ?>
            </ul>
        </div>
    </div>
    -->
</div>