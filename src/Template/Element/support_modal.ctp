<div class="modal fade" id="support_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form id="support_anfragen_form" method="post" action="/fehler_melden">
        <?= $this->Form->hidden(__('current_url'), ['value' => $_SERVER['REQUEST_URI']]) ?>
        <?= $this->Form->hidden(__('http_user_agent'), ['value' => isset($_SERVER['HTTP_USER_AGENT']) && strlen($_SERVER['HTTP_USER_AGENT'])>0 ? $_SERVER['HTTP_USER_AGENT'] : '']) ?>
        <?= $this->Form->hidden(__('controller'), ['value' => $this->request->params['controller']]) ?>
        <?= $this->Form->hidden(__('action'), ['value' => $this->request->params['action']]) ?>
        <?= $this->Form->hidden(__('pass'), ['value' => isset($this->request->params['pass']) && isset($this->request->params['pass'][0]) ? $this->request->params['pass'][0] : '']) ?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Fehler melden</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Deine Nachricht</label>
                            <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                    <button type="button" class="btn-u btn-u-primary support_senden">Fehler melden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    jQuery('form#support_anfragen_form textarea[name="message"]').on('keyup', function() {
        jQuery('p.error').fadeOut('fast').remove();
        jQuery(this).css('border', '1px solid #ccc');
    });
    jQuery('button.support_senden').on('click', function(e) {
        jQuery('p.error').fadeOut('fast').remove();
        jQuery(this).css('border', '1px solid #ccc');
        e.preventDefault();

        if (jQuery('form#support_anfragen_form textarea[name="message"]').val().length===0) {
            jQuery('form#support_anfragen_form textarea[name="message"]').css('border', '1px solid #ae0f35');
            jQuery('form#support_anfragen_form textarea[name="message"]').after('<p class="error" id="message" style="color:red">- Nachricht ist leer.</p>');
            return;
        }

        jQuery('form#support_anfragen_form').submit();
    })
</script>