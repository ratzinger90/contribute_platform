<script type="text/javascript">
    jQuery(document).ready(function() {
        switch (<?php echo json_encode($params['class']); ?>) {
            case "error":
                notie.alert(3, <?php echo json_encode($message); ?>, 5);
                break;
            case "success":
                notie.alert(1, <?php echo json_encode($message); ?>, 5);
        }
    });
</script>
