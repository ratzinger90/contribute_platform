<div class="container" style="width:100%;background-color:#585f69">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:60px">
            <div class="container">
                <ul class="bottombar pull-right" style="list-style-type:none;display:inline;line-height:45px;">
                    <li><a href="/agb"><?= __('AGB') ?></a></li>
                    <li class="bottombar-devider"></li>
                    <li><a href="/impressum"><?= __('Impressum') ?></a></li>
                    <li class="bottombar-devider"></li>
                    <li><a href="/datenschutz"><?= __('Datenschutz') ?></a></li>
                    <li class="bottombar-devider"></li>
                    <li><a href="/faq"><?= __('FAQ') ?></a></li>
                    <li class="bottombar-devider"></li>
                    <li><a href="#support_modal" data-toggle="modal"><?= __('Fehler melden?') ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>