<div class="posts" style="text-align:right">
    <div class="headline" style="text-align:right"><h2>Verwaltung</h2></div>
    <div class="col-xs-12">
        <?php if (isset($public_pin) && strlen($public_pin)>0) { echo '<p>Öffentliche&nbsp;Chiffre:&nbsp;' . $public_pin . '</p>'; } ?>
        <ul class="list-group sidebar-nav-v1" id="sidebar-nav">

        <li class="list-group-item"><a href="/broker/gesuch_aufgeben">Neues Gesuch aufgeben&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
        <li class="list-group-item"><a href="/broker/spende_abgeben">Neue Spende abgeben&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
        <li class="list-group-item"><a href="/broker/meine_spenden/<?php echo base64_encode('spenderspenden')."|"; ?>">Meine Spenden&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
        <li class="list-group-item"><a href="/broker/kontaktdaten_verwalten">Kontaktdaten verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
        <li class="list-group-item"><a href="/broker/passwort_vergeben">Passwort ändern&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
        <br/>
        <!--<br/>
        <p><a href="/broker/matches">Matches&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;[BETA]</a></p> -->
            <li class="list-group-item list-toggle">
                <a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-typography">Erweiterte Verwaltung</a>
                <ul id="collapse-typography" class="collapse">
                    <li><a href="/broker/spenden_verwalten/<?php echo base64_encode('spenden')."|"; ?>">Spenden verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
                    <li><a href="/broker/gesuche_verwalten/<?php echo base64_encode('gesuche')."|"; ?>">Gesuche verwalten&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
                    <li><a href="/broker/archiv_spenden_verwalten/<?php echo base64_encode('spenden')."|"; ?>">Archivierte Spenden&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
                    <li><a href="/broker/archiv_gesuche_verwalten/<?php echo base64_encode('gesuche')."|"; ?>">Archivierte Gesuche&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
                </ul>
            </li>
        <br/>
        <li class="list-group-item"><a href="/logout">Ausloggen&nbsp;<i class="fa fa-sign-out"></i></a></li>
        <br/>
        </ul>

    </div>
</div>