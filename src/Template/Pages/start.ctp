<!-- Service Blocks -->
<?php
if (isset($call) && $call) {
    ?>
    <div class="callforhelp row">
        <div class="callforhelp_title" style="background-color: #D90016; height: 40px">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-6 col-xs-12 col-lg-6" id="alerttitle">
                <span><?= $call->call_title ?></span>
            </div>
            <div class="col-md-3 col-lg-3"></div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12" id="alertcontent">
            <?= $call->call_text ?>
        </div>
        <?php
        if ($call->has('link') && strlen($call->link)>0) {
            ?>
            <div class="callforhelp_title">
                <div class="col-md-3 col-lg-3"></div>
                <div class="col-md-6 col-xs-12 col-lg-6">
                    <span><a href="<?= $call->link ?>" target="_blank"><?= $call->link_title ?></a></span>
                </div>
                <div class="col-md-3 col-lg-3"></div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}
?>
<div class="row margin-bottom-30">
    <a href="/spende_abgeben">
        <div class="col-md-6">
            <div class="service">
                <i class="fa fa-plus service-icon"></i>
                <div class="desc">
                    <h4>SPENDE ABGEBEN</h4>
                    <p>Sachspende abzugeben? Oder möchtest du lieber Zeit spenden?</p>
                </div>
            </div>
        </div>
    </a>
    <?php
    if (isset($logged_in) && $logged_in == true &&
        isset($role) && $role == 'broker') {
        ?>
        <a href="/broker/gesuch_aufgeben">
            <div class="col-md-6">
                <div class="service">
                    <i class="fa fa-plus service-icon"></i>

                    <div class="desc">
                        <h4>GESUCH AUFGEBEN</h4>

                        <p>Es wird etwas benötigt?</p>
                    </div>
                </div>
            </div>
        </a>
        <?php
    }
    ?>
</div>

<?php
if (isset($no_donations_searches) && $no_donations_searches==true) {
    switch($no_partner) {
        case false:
            ?>
            <h4>Für Deine Stadt haben wir noch keine Spenden oder Gesuche. <a href="/spende_abgeben">Spende</a> als erste/r.</h4>
            <?php
            break;
        case true:
            ?>
            <h4>Für Deine Stadt haben wir noch keine Partner.</h4>
            <?php
            break;
    }
}
?>

<?php if (isset($firstfour_searches) && $firstfour_searches) { ?>
    <div class="headline"><h2>Neueste Gesuche</h2><a href="/alle_gesuche"><h5 style="margin-left:20px;display:inline;">
                <i class="fa fa-angle-double-right"></i>&nbsp;Alle anzeigen</h5></a></div>
    <div class="row margin-bottom-20">
        <?php
        foreach ($firstfour_searches as $firstfour_search) { ?>
            <?php
            if ($firstfour_search->category == 'thing') {
                $href = "/sachgesuch/";
                $placeholder = "/img/thing_placeholder_search.png";
            } else if ($firstfour_search->category == 'time') {
                $href = "/zeitgesuch/";
                $placeholder = "/img/time_placeholder_search.png";
            }

            ?>
            <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <a href="<?= $href . $firstfour_search->url_title ?>">
                                <?php
                                if (isset($firstfour_search->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $firstfour_search->file_uploads[0]->src)) { ?>
                                    <?php
                                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/') . $firstfour_search->file_uploads[0]->src, 'IFD0');
                                    if (!empty($exif_donation_image['Orientation'])) {
                                        switch ($exif_donation_image['Orientation']) {
                                            case 6:
                                                $rotation = "rotate(90deg)";
                                                break;
                                            default:
                                                $rotation = "";
                                                break;

                                        }
                                    }
                                    ?>
                                    <img class="img-responsive"
                                         src="<?php echo $firstfour_search->file_uploads[0]->src; ?>"
                                         alt=""
                                         style="<?php if (isset($rotation) && strlen($rotation) > 0) { ?> transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?><?php } ?>"/>
                                    <?php
                                } else { ?>
                                    <img class="img-responsive" src="<?= $placeholder ?>"
                                         />
                                    <?php
                                }
                                ?>
                            </a>
                        </div>
                        <a class="btn-more hover-effect" href="<?= $href . $firstfour_search->url_title ?>">Details
                            +</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="<?= $href . $firstfour_search->url_title ?>"> <?php
                                if (strlen($firstfour_search->title) > 30) {
                                    echo substr($firstfour_search->title, 0, 27) . "...";
                                } else {
                                    echo $firstfour_search->title;
                                } ?> </a></h3>
                        <p>
                            <?php
                            $description_array = explode(" ", $firstfour_search->description);
                            $description_length = 0;
                            $continue = false;
                            foreach ($description_array as $key => $description) {
                                $description_length += strlen($description);
                                if ($description_length < 140) {
                                    echo $description . " ";
                                } else if ($description_length > 140 && !$continue) {
                                    echo "...";
                                    $continue = true;
                                } else {
                                    continue;
                                }
                            }
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}
?>

<?php
if (isset($firstfour_donations) && $firstfour_donations) { ?>
    <div class="headline"><h2>Neueste Spenden</h2><a href="/alle_spenden"><h5 style="margin-left:20px;display:inline;">
                <i class="fa fa-angle-double-right"></i>&nbsp;Alle anzeigen</h5></a></div>
    <div class="row margin-bottom-20">
        <?php
        foreach ($firstfour_donations as $firstfour_donation) { ?>
            <?php
            if ($firstfour_donation->category == 'thing') {
                $href = "/sachspende/";
                $placeholder = "/img/thing_placeholder.png";
            } else if ($firstfour_donation->category == 'time') {
                $href = "/zeitspende/";
                $placeholder = "/img/time_placeholder.png";
            }
            ?>
            <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <a href="<?= $href . $firstfour_donation->url_title ?>">
                                <?php
                                if (isset($firstfour_donation->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $firstfour_donation->file_uploads[0]->src)) { ?>
                                    <!-- Ausgabe des Thumbnails -->
                                    <?php
                                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/') . $firstfour_donation->file_uploads[0]->src, 'IFD0');
                                    if (!empty($exif_donation_image['Orientation'])) {
                                        switch ($exif_donation_image['Orientation']) {
                                            case 6:
                                                $rotation = "rotate(90deg)";
                                                break;
                                            default:
                                                $rotation = "";
                                                break;

                                        }
                                    }
                                    ?>
                                    <img class="img-responsive"
                                         src="<?php echo $firstfour_donation->file_uploads[0]->src ?>"
                                         alt=""
                                         style="<?php if (isset($rotation) && strlen($rotation) > 0) { ?> transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?><?php } ?>"/>
                                    <?php
                                } else { ?>
                                    <img class="img-responsive" src="<?= $placeholder ?>"
                                         />
                                    <?php
                                }
                                ?>
                            </a>
                        </div>
                        <a class="btn-more hover-effect" href="<?= $href . $firstfour_donation->url_title ?>">Details
                            +</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="<?= $href . $firstfour_donation->url_title ?>"> <?php
                                if (strlen($firstfour_donation->title) > 30) {
                                    echo substr($firstfour_donation->title, 0, 27) . "...";
                                } else {
                                    echo $firstfour_donation->title;
                                } ?> </a></h3>
                        <p>
                            <?php
                            $description_array = explode(" ", $firstfour_donation->description);
                            $description_length = 0;
                            $continue = false;
                            foreach ($description_array as $key => $description) {
                                $description_length += strlen($description);
                                if ($description_length < 140) {
                                    echo $description . " ";
                                } else if ($description_length > 140 && !$continue) {
                                    echo "...";
                                    $continue = true;
                                } else {
                                    continue;
                                }
                            }
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}