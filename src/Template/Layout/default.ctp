<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php if (isset($title) && strlen($title)>0) { echo $title; } ?>
    </title>
    <?= $this->Html->meta('icon') ?>


    <!-- CSS Global Compulsory -->
    <?= $this->Html->css('/plugins/bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->css('style.css') ?>

    <!-- CSS Implementing Plugins -->
    <?= $this->Html->css('/plugins/line-icons/line-icons.css') ?>
    <?= $this->Html->css('/plugins/font-awesome/css/font-awesome.min.css') ?>
    <?= $this->Html->css('/plugins/flexslider/flexslider.css') ?>
    <?= $this->Html->css('/plugins/parallax-slider/css/parallax-slider.css') ?>

    <!-- CSS Theme -->
    <?= $this->Html->css('themes/default.css') ?>

    <!-- Further CSS -->
    <?= $this->Html->css('custom.css') ?>
    <?= $this->Html->css('select2.css') ?>
    <?= $this->Html->css('pages/feature_timeline1.css') ?>

    <!-- DataTables CSS -->
    <?= $this->Html->css('jquery.dataTables.min.css') ?>

    <!-- Datepicker CSS -->
    <?= $this->Html->css('daterangepicker.css') ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <!-- JS Global Compulsory -->
    <?= $this->Html->script('/plugins/jquery-1.10.2.min.js') ?>
    <?= $this->Html->script('/plugins/jquery-migrate-1.2.1.min.js') ?>
    <?= $this->Html->script('/plugins/bootstrap/js/bootstrap.min.js') ?>

    <!-- JS Implementing Plugins -->
    <?= $this->Html->script('/plugins/back-to-top.js') ?>
    <?= $this->Html->script('/plugins/flexslider/jquery.flexslider-min.js') ?>
    <?= $this->Html->script('/plugins/parallax-slider/js/modernizr.js') ?>
    <?= $this->Html->script('/plugins/parallax-slider/js/jquery.cslider.js') ?>

    <!-- Further JS -->
    <?= $this->Html->script('jquery.bootpag.js') ?>
    <?= $this->Html->script('select2.js') ?>

    <!-- DataTables JS -->
    <?= $this->Html->script('jquery.dataTables.min.js') ?>

    <!-- Moment JS -->
    <?= $this->Html->script('moment.min.js') ?>

    <!-- EXIF JS -->
    <?= $this->Html->script('exif.js') ?>

    <!-- Datepicker JS -->
    <?= $this->Html->script('daterangepicker.js') ?>

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <?php
    if (isset($this->request->params['controller']) && $this->request->params['controller']=='Pages' &&
        isset($this->request->params['action']) && $this->request->params['action']=='edit' &&
        isset($this->request->params['prefix']) && $this->request->params['prefix']=='admin') {
        ?>
        <script src="/js/tinymce.min.js"></script>
        <?php
    }
    ?>

    <?= $this->fetch('script') ?>

    <?php
    if (isset($logged_in) && $logged_in == true) {
        // NO COOKIES
    } else {
        ?>

        <script type="text/javascript" src="/js/cookieconsent.min.js"></script>
        <?php
    }
    ?>

    <!-- Piwik -->
      <!-- End Piwik Code -->
</head>
<body>
<div class="no_javascript" style="padding-top:5px;height:70px;position:absolute;z-index:9999;font-size:18px;border:3px solid #333;padding-left:100px;padding-right:100px;background-color:#ffffff;top:280px">
    <p style="color:#be1e3c">Hallo, leider kann donaki.de seine Funktionen nicht ohne JavaScript anbieten. Bitte habe Verständnis dafür, dass einige Funktionen deshalb ohne das Aktivieren von JavaScript in deinem Browser nicht ordnungsgemäß funktionieren.</p>
</div>
<script type="text/javascript">
    jQuery('div.no_javascript').fadeOut('fast').remove();
</script>
<div class="wrapper">
    <div class="header">
        <div class="topbar">
            <div class="container">
                <ul class="loginbar pull-right">
                    <!--
                     Eventuell später
                     <li>
                        <i class="fa fa-globe"></i>
                        <a>Sprachen</a>
                        <ul class="lenguages">
                            <li class="active">
                                <a href="#">German <i class="fa fa-check"></i></a>
                            </li>
                            <li><a href="#">English</a></li>
                        </ul>
                    </li> -->
                    <?php if (isset($logged_in) && $logged_in) { ?>
                        <li><a href="/hilfe"><?= __('Hilfe') ?></a></li>
                        <li class="topbar-devider"></li>
                        <li><a href="/faq"><?= __('FAQ') ?></a></li>
                        <li class="topbar-devider"></li>
                        <li><a href="/partner"><?= __('Partner') ?></a></li>
                        <li class="topbar-devider"></li>
                        <li><a href="/dashboard"><?= __('Übersicht') ?></a></li>
                    <?php } else { ?>
                        <li><a href="/hilfe"><?= __('Hilfe') ?></a></li>
                        <li class="topbar-devider"></li>
                        <li><a href="/faq"><?= __('FAQ') ?></a></li>
                        <li class="topbar-devider"></li>
                        <li><a href="/partner"><?= __('Partner') ?></a></li>
                    <?php } ?>
                    <li class="topbar-devider"></li>
                    <?php if (isset($logged_in) && $logged_in) { ?>
                        <li><a href="/logout"><?= __('Logout') ?></a></li>
                    <?php } else { ?>
                        <li><a href="/registrieren"><?= __('Registrieren') ?></a></li>
                        <li class="topbar-devider"></li>
                        <li><a href="/login"><?= __('Login') ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>

        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="fa fa-bars"></span>
                            </button>
                            <a style="height: 52px;padding-top: 27px;margin-top: -27px;margin-bottom: -7px;" class="navbar-brand">
                                <?php
                                if (strlen($this->request->session()->read('configuration_logo_src'))>0 && file_exists("/var/www/contribute_platform/webroot" . $this->request->session()->read('configuration_logo_src'))) {
                                ?><div class="col-xs-4"><img id="logo-header" src="<?=$this->request->session()->read('configuration_logo_src')?>" alt="Logo" style="margin-top:-6px;width:135px" /></div><?php
                                ?><div class="col-xs-8"><?php
                                    } else {
                                    ?>
                                    <div class="col-xs-12">
                                        <?php
                                        }
                                        ?>
                                        <h3 style="margin-top:-10px;"><?=$this->request->session()->read('configuration_branch_title')?></h3>
                                        <?php
                                        if ($this->request->session()->read('configuration_not_exists')==true || $this->request->session()->read('configuration_id')==1) {
                                            ?>
                                            <select name="branches_select" class="form-control" placeholder="Region wählen">
                                                <option>Region wählen</option>
                                                <?php
                                                foreach ($this->request->session()->read('branches') as $key => $branch) {
                                                    ?>
                                                    <option value="<?=$key?>"><?=$branch?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        <?php
                                        }
                                        ?>
                                        <p><?= $this->request->session()->read('configuration_branch_subtitle')?></p>
                                    </div>
                            </a>
                            <style type="text/css">
                                a.navbar-brand:hover {
                                    cursor: default;
                                }
                            </style>
                        </div>
                    </div>
                </div>

                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="/startseite" class="">
                                Startseite
                            </a>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                Spenden
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <li><a href="/sachspenden">Sachspenden</a></li>
                                    <li><a href="/zeitspenden">Zeitspenden</a></li>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                Gesuche
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <li><a href="/sachgesuche">Sachgesuche</a></li>
                                    <li><a href="/zeitgesuche">Zeitgesuche</a></li>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <i class="search fa fa-search search-btn"></i>
                            <div class="search-open" style="width:400px !important">
                                <form id="nav_search_form" action="/suche/" method="post"
                                <div class="input-group animated fadeInDown" style="width:400px !important">
                                    <div class="col-xs-8">
                                        <input type="text" name="nav_search_input" style="width:268px" class="form-control" placeholder="Suchen ...">
                                    </div>
                                    <div class="col-xs-4">
                                        <span class="input-group-btn">
                                            <button class="btn-u main_nav_search" type="button">Suchen</button>
                                        </span>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?= $this->element('donaki_banner') ?>

    <div class="container">
        <?= $this->Flash->render() ?>
        <?= $this->Flash->render('auth') ?>
        <?= $this->element('content') ?>
    </div>

    <?= $this->element('footer_links') ?>

    <?= $this->element('support_modal') ?>

    <script type="text/javascript" src="/js/notie.js"></script>
</div>
</body>
</html>

<!-- JS Page Level -->
<?= $this->Html->script('app.js') ?>
<?= $this->Html->script('pages/index.js') ?>
<script type="text/javascript">
    $(document).ready(function() {
        App.init();
        App.initSliders();
        Index.initParallaxSlider();
        $('select[name="branches_select"]').on('change', function() {
            var branch = $(this).val();
            if (isNaN(branch)) return;
            $.ajax({
                url: 'XXXX',
                type: 'post',
                dataType: 'json'
            }).always(function(data) {
               if (data.response.status === 'success') {
                   window.location.href = data.response.target_host;
               } else {
                   notie.alert(3, 'Städteseite konnte nicht aufgerufen werden', 5);
               }
            });
        });
    });
</script>

<?php if ($this->request->params['controller']=='Pages' && $this->request->params['pass'][0]=='start') { ?>
<!-- <?= $this->element('register_speech_bubble') ?> --> <!-- temporär ausblenden, beim Wiedereinblenden UNBEDINGT auf das Styling achten! -->
<?php } ?>

<script type="text/javascript" src="/js/search_helper.js"></script>


<script type="text/javascript">
        jQuery('img').load(function () {
            jQuery('img').exifLoad(function () {
                try {
                    var orientation = $(this).exif("Orientation");
                    switch (orientation[0]) {
                        case 6:
                            jQuery(this).css('-ms-transform', 'rotate(90deg)').css('-webkit-transform', 'rotate(90deg)').css('transform', 'rotate(90deg)');
                            break;
                    }
                }
                catch (e) {
                }
            });
        });
</script>
