<div class="headline"><h2>Passende Gesuche wurden gefunden</h2></div>
<div class="row margin-bottom-20">
    <?php foreach ($searches as $search) { ?>
        <?php
        if ($search->category=='thing') {
            $href = "/sachgesuch/";
            $placeholder = "/img/thing_placeholder.png";
        } else if ($search->category=='time') {
            $href = "/zeitgesuch/";
            $placeholder = "/img/time_placeholder.png";
        }
        ?>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden">
                        <a href="<?= $href . $search->url_title ?>">
                            <?php
                            if (isset($search->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $search->file_uploads[0]->src)) { ?>
                                <!-- Ausgabe des Thumbnails -->
                                <img class="img-responsive" src="<?php echo $search->file_uploads[0]->src ?>"
                                     alt=""/>
                                <?php
                            } else { ?>
                                <img class="img-responsive" src="<?= $placeholder ?>" width="150px" />
                                <?php
                            }
                            ?>
                        </a>
                    </div>
                    <a class="btn-more hover-effect" href="<?= $href . $search->url_title ?>">Details +</a>
                </div>
                <div class="caption">
                    <h3><a class="hover-effect" href="<?= $href . $search->url_title ?>"> <?php echo $search->title; ?> </a></h3>
                    <p>
                        <?php
                        $description_array = explode(" ", $search->description);
                        $description_length = 0;
                        $continue = false;
                        foreach ($description_array as $key => $description) {
                            $description_length += strlen($description);
                            if ($description_length<140) {
                                echo $description . " ";
                            } else if ($description_length>140 && !$continue) {
                                echo "...";
                                $continue = true;
                            } else {
                                continue;
                            }
                        }
                        ?>
                    </p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>