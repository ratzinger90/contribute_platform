<script src="/js/jquery.tagsinput.js"></script>
<script src="/js/jquery.autocomplete.js"></script>
<script src="/js/jquery.MultiFile.js"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="/css/jquery.autocomplete.css" />

<div class="row smartphone_no_margin">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar_centered hidden-xs">
        <?= $this->element('new_donation_help_sidebar') ?>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
        <?= $this->Form->create($donation, ['id' => 'new_form', 'enctype' => 'multipart/form-data']) ?>
        <div class="row" id="spendendaten">
            <div class="col-md-6">
                <div class="headline"><h2>Spende abgeben</h2></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $this->Form->input(__('firstname'), ['label' => 'Dein Vorname', 'required' => true, 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                <?= $this->Form->input(__('lastname'), ['label' => 'Dein Name', 'required' => true, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <?= $this->Form->checkbox(__('time_checkbox'), ['label' => false, 'id' => 'time_checkbox']) ?><label class="time_donation" style="margin-left: 10px;margin-top: 2px">Du möchtest Zeit spenden? Dann setze im Kästchen ein Häkchen.</label>
            </div>
        </div>
        <div class="row">
            <div id="time_div" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-0" style="margin-left:15px;display:none">
                <br/>
                <table style="width:100%" id="time_table" cellpadding="5"></table>
                <br/><a class="btn btn-primary add_more_times" style="margin-bottom: 10px"><i class="fa fa-plus"></i>&nbsp;Weitere Zeitangabe hinzufügen</a>
                <br/><a class="btn btn-u delete_last_time"><i class="fa fa-minus"></i>&nbsp;Letzte Zeitangabe entfernen</a>
                <br/>
                <br/>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-9 col-md-offset-0">
                <label>Gebe eine kurze Beschreibung deiner Spende ein [maximal 64 Zeichen]</label><br/>
                <?= $this->Form->input(__('title'), ['label' => false, 'id' => 'title', 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-3">
                <label>Deine Postleitzahl</label><br/>
                <?= $this->Form->input(__('zip'), ['label' => false, 'class' => 'form-control', 'value' => isset($address) && strlen($address->zip)>0 ? $address->zip : '']) ?>
            </div>
            <div class="col-md-5">
                <label>Stadt [wird automatisch ausgefüllt]</label><br/>
                <?= $this->Form->input(__('city'), ['label' => false, 'class' => 'form-control', 'disabled' => 'disabled',
                    'value' => isset($address) && strlen($address->city)>0 ? $address->city : '']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-9 col-md-offset-0">
                <label>Hier hast du die Möglichkeit deine Spende ausführlicher zu beschreiben:</label>
                <?= $this->Form->input(__('description'), ['label' => false, 'id' => 'description', 'class' => 'form-control', 'style' => 'max-width:555px']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-md-offset-0">
                <p id="keywords_separator"></p>
                <div id="keywords_div" class="row" style="display: none">
                    <div class="col-md-12 col-md-offset-0">
                        <b>Schlagwörter:</b> <p id="keywords_paragraph"></p>
                        <input name="keywords" id="keywords" />
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <label>Hier hast du die Möglichkeit Bilder hochzuladen:</label><br/>
                <?= $this->Form->file(__('uploads.'), ['accept' => 'gif|jpg|png|jpeg', 'data-maxfile' => '4096', 'class' => 'multi with-preview', 'type' => 'file', 'multiple' => true, 'style' => 'width:110px']) ?>
                <p id="rights_uploads" style="display:none;margin-top:20px"><input type="checkbox" name="rights" style="display:none;margin-top:0px;margin-right:12px" />Ich besitze alle Rechte an den hochgeladenen Bildern</p>
            </div>
        </div>
        <hr>
        <div class="row" id="kontaktdaten">
            <div class="col-md-6">
                <div class="headline"><h2>Kontaktdaten</h2></div>
                <p style="font-style:italic">Wenn du keine E-Mail-Adresse angibst, kannst du deine Spende nicht mehr bearbeiten oder nachverfolgen.</p>
            </div>
        </div>
        <?php
        if (isset($logged_in) && $logged_in == true) {
            ?>
            <div class="row">
                <div class="col-md-4" style="margin-right:20px">
                    <label>Deine E-Mail-Adresse</label><br/>
                    <?= $this->Form->input(__('email'), ['label' => false, 'class' => 'form-control', 'value' => isset($user) && strlen($user->email) > 0 ? $user->email : '']) ?>
                </div>
                <div class="col-md-4">
                    <label>Deine Telefonnummer</label><br/>
                    <?= $this->Form->input(__('phone'), ['label' => false, 'class' => 'form-control', 'value' => isset($user) && strlen($user->phone) > 0 ? $user->phone : '']) ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-6">
                    <label>Deine Straße und Hausnummer [optional]</label><br/>
                    <?= $this->Form->input(__('street'), ['label' => false, 'class' => 'form-control', 'value' => isset($address) && strlen($address->street)>0 ? $address->street : '']) ?>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="row">
                <div class="col-md-4">
                    <label>Deine E-Mail-Adresse</label><br/>
                    <?= $this->Form->input(__('email'), ['label' => false, 'class' => 'form-control', 'value' => isset($user) && strlen($user->email) > 0 ? $user->email : '']) ?>
                </div>
                <div class="col-md-4">
                    <label>Deine E-Mail-Adresse [Bestätigung]</label><br/>
                    <?= $this->Form->input(__('email_confirm'), ['label' => false, 'class' => 'form-control', 'value' => isset($user) && strlen($user->email) > 0 ? $user->email : '']) ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-4">
                    <label>Deine Telefonnummer</label><br/>
                    <?php
                    if (isset($user)) {
                        ?>
                        <?= $this->Form->input(__('phone'), ['label' => false, 'class' => 'form-control', 'value' => isset($user) && strlen($user->phone) > 0 ? $user->phone : '']) ?>
                        <?php
                    } else {
                        ?>
                        <?= $this->Form->input(__('phone'), ['label' => false, 'required' => true, 'class' => 'form-control']) ?>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Deine Straße und Hausnummer [optional]</label><br/>
                    <?= $this->Form->input(__('street'), ['label' => false, 'class' => 'form-control', 'value' => isset($address) && strlen($address->street)>0 ? $address->street : '']) ?>
                </div>
            </div>
        <?php
        }
        if (isset($no_user) && $no_user && !in_array($_SERVER['HTTP_HOST'], $this->request->session()->read('captcha_free_hosts'))) {
            ?>
            <br/>
            <div class="row">
                <div class="col-xs-12">
                    <div class="g-recaptcha" data-sitekey="XXXX"></div>
                </div>
            </div>
            <?php
        }
        ?>
        <br/>
        <div class="row">
            <div class="col-md-4">
                <button type="submit" class="btn-u submit_new_form">Spende jetzt abgeben&nbsp;<i class="fa fa-check"></i></button>
                <?= $this->Form->hidden(__('role')) ?>
                <?= $this->Form->hidden(__('user_id')) ?>
                <?= $this->Form->hidden(__('url_title')) ?>
                <?= $this->Form->hidden(__('address_id')) ?>
                <?= $this->Form->hidden(__('category')) ?>
            </div>
        </div>
        <?= $this->Form->hidden(__('active'), ['value' => 1]) ?>
        <?= $this->Form->hidden(__('hidden_inactive_mail')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/add_helper.js"></script>

<?php
if (isset($keywords_string) && strlen($keywords_string)>0) {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            notie.confirm("Schlagwörter übernehmen?",
                "Ja",
                "Nein",
                function() {
                    $('#keywords').importTags(<?php echo json_encode($keywords_string); ?>);
                    $('div#keywords_div').fadeIn('fast').css('height', 'auto');
                    var keywords_string = <?php echo json_encode($keywords_string); ?>;
                    var posted_data = {
                        keywords_string: keywords_string
                    };
                    $.ajax({
                        url: "XXXX",
                        type: 'POST',
                        data: posted_data,
                        dataType: 'json',
                        async: true,
                        cache: false
                    });
                }
            );

        });
    </script>
<?php
}
