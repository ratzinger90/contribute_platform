    <!--=== Content Part ===-->
    <div class="container content"> 	
    	<div class="row portfolio-item margin-bottom-50"> 
            <!-- Carousel -->
            <div class="col-md-7">
                <div class="carousel slide carousel-v1" id="myCarousel">
                    
                    <div class="carousel-inner">
                    <?php
                        $i = -1;
                        foreach ($donation->file_uploads as $file_uploads) {
                        $i++;
                    ?>
                        <div class="item <?php if($i == 0) {echo "active";} ?>">
                            <img alt="" src="../../<?php echo $file_uploads->src; ?>">
                            <div class="carousel-caption">
                                <p>Bild <?php echo $i+1; ?></p>
                            </div>
                        </div>

                    <?php } ?>
                        
                    </div>
                    
                    <?php if ($i > 0 ) { ?>
                    <div class="carousel-arrow">
                        <a data-slide="prev" href="#myCarousel" class="left carousel-control">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a data-slide="next" href="#myCarousel" class="right carousel-control">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                    <?php } ?>

                </div>
            </div>
            <!-- End Carousel -->

            <!-- Content Info -->        
            <div class="col-md-5">
            	<h2><?= h($donation->title) ?></h2>
                <p><?= $this->Text->autoParagraph(h($donation->description)); ?></p>
                <ul class="list-unstyled">
                	<li><i class="fa fa-user color-green"></i> <?= $donation->user_id != 0 ? $this->Html->link($donation->user_name, ['controller' => 'Users', 'action' => 'view', $donation->user_id]) : '' ?>
                    </li>
                	<li><i class="fa fa-calendar color-green"></i> <?= h($donation->created) ?></li>
                    <li><i class="fa fa-cogs color-green"></i> <?= h($donation->modified) ?></li>
                	<li><i class="fa fa-tags color-green"></i>
                        <?php 
                            foreach ($donation->keywords as $keywords) { 
                                if ($keywords != $donation->keywords[0]) { echo ", "; }
                                echo $keywords; 
                            } 
                        ?>
                     </li>
                </ul>
                <buton type="button" class="btn-u btn-u-large">Spende vermitteln</button>
            </div>
            <!-- End Content Info -->        
        </div><!--/row-->

        <div class="tag-box tag-box-v2">
            <p>Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat.</p>
        </div>

        <div class="margin-bottom-20 clearfix"></div>    
                
    </div><!--/container-->	 	
    <!--=== End Content Part ===-->