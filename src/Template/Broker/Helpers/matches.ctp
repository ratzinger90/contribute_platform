<div class="row" style="padding-right:20px">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="headline"><h2><?= __('Matches') ?></h2></div>

    <table id="matches_table" class="table">
        <thead>
        <tr>
            <th>Spende</th>
            <th>Gesuch</th>
            <th>Kontakt aufgenommen</th>
            <th>Erstellt</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($matches as $match) {
            switch($match->donation->category) {
                case "thing":
                    $donation_href = '/sachspende/' . $match->donation->url_title;
                    break;
                case "time":
                    $donation_href = '/zeitspende/' . $match->donation->url_title;
                    break;
            }

            switch($match->search->category) {
                case "thing":
                    $search_href = '/sachgesuch/' . $match->search->url_title;
                    break;
                case "time":
                    $search_href = '/zeitgesuch/' . $match->search->url_title;
                    break;
            }
            ?>
            <tr>
                <td><a href="<?= $donation_href ?>"><?= $match->donation->title ?></a></td>
                <td><a href="<?= $search_href ?>"><?= $match->search->title ?></a></td>
                <td><?php
                    if (isset($logs[$match->donation_id]) && $logs[$match->donation_id]==1) {
                        ?>
                        <span class="label label-success">Kontakt aufgenommen</span>
                        <?php
                    } else {
                        ?>
                        <span class="label label-warning">Kontakt nicht aufgenommen</span>
                        <?php
                    }
                    ?>
                </td>
                <td><?= $match->created->i18nFormat('dd.MM.yyyy HH:mm') ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#matches_table').dataTable({
            "columnDefs" : [
                { type : 'de_date', targets: [3] },
            ]
        });
    });

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "de_date-asc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity; // = l'an 1000 ...
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
            return z;
        },

        "de_date-desc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity;
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
            return z;
        }
    } );
</script>