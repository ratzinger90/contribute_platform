<div class="row" style="padding-right:20px">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="manage_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
            <thead>
            <tr>
                <?php
                foreach ($columns as $column) {
                    ?>
                    <th><?= $column ?></th>
                    <?php
                }
                ?>
            </tr>
            </thead>
            <tbody>
            <?php
            switch ($type) {
                case "gesuche":
                    foreach ($data as $row) {
                        ?>
                        <tr>
                            <td><?= $row->id ?></td>
                            <td><?= $row->title ?></td>
                            <td><?php if ($row->category == 'thing') { echo "Sachgesuch"; } else if ($row->category == 'time') { echo "Zeitgesuch"; } ?></td>
                            <td><?= $row->created->i18nFormat('dd.MM.yyyy hh:mm') ?></td>
                            <td><?= $row->modified->i18nFormat('dd.MM.yyyy hh:mm') ?></td>
                            <td>
                                <a href="/broker/details/<?php echo base64_encode($row->id."|".$row->category."|gesuche"); ?>" class="btn btn-warning">Details&nbsp;<i class="fa fa-search"></i></a>

                                <a href="/broker/aktivieren/<?php echo base64_encode($row->id."|".$row->category."|gesuche"); ?>" class="btn btn-success">Aktivieren&nbsp;<i class="fa fa-check"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    break;
                case "spenden":
                    foreach ($data as $row) {
                        ?>
                        <tr>
                            <td><?= $row->id ?></td>
                            <td><?= $row->title ?></td>
                            <td><?php if ($row->category == 'thing') { echo "Sachspende"; } else if ($row->category == 'time') { echo "Zeitspende"; } ?></td>
                            <td><?= $row->created->i18nFormat('dd.MM.yyyy hh:mm') ?></td>
                            <td><?= $row->modified->i18nFormat('dd.MM.yyyy hh:mm') ?></td>
                            <td>
                                <a href="/broker/details/<?php echo base64_encode($row->id."|".$row->category."|spenden"); ?>" class="btn btn-warning">Details&nbsp;<i class="fa fa-search"></i></a>

                                <a href="/broker/aktivieren/<?php echo base64_encode($row->id."|".$row->category."|spenden"); ?>" class="btn btn-success">Aktivieren&nbsp;<i class="fa fa-check"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    break;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#manage_table').dataTable();
    });
</script>