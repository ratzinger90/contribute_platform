<?php
switch ($type) {
    case "gesuche":
        ?>
        <div class="row">
            <a href="/broker/gesuche_verwalten/<?php echo base64_encode('gesuche') . "|"; ?>" class="btn btn-u"><i
                    class="fa fa-angle-double-left"></i>&nbsp;Zurück zur Auflistung</a>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="row" style="padding:30px">
                    <?php
                    if (isset($search->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $search->file_uploads[0]->src)) { ?>
                        <img src="<?= $search->file_uploads[0]->src ?>" style="max-width:150px;max-height:150px" />
                        <?php
                    } else {
                        if ($search->category == 'time') {
                            ?>
                            <img src="/img/time_placeholder.png" style="width:125px"/>
                            <?php
                        } else {
                            ?>
                            <img src="/img/thing_placeholder.png" style="width:125px" />
                            <?php
                        }
                    }
                    ?>
                </div>
                <br/>

                <div class="row">
                    <div class="headline"><h2>Anschrift</h2></div>
                    <p><?php
                    if (isset($contact_array)) {
                        if (isset($contact_array['firstname']) || isset($contact_array['lastname'])) {
                            echo $contact_array['firstname'] . " " . $contact_array['lastname'];
                        }
                        if (isset($contact_array['street']) || isset($contact_array['zip']) || isset($contact_array['city'])) {
                            echo '<br/>';
                            if (isset($contact_array['street']) && strlen($contact_array['street'])>0) {
                                echo $contact_array['street'] . ", ";
                            }
                            echo $contact_array['zip'] . " " . $contact_array['city'];
                        }
                        if (isset($contact_array['phone'])) {
                            echo '<br/>';
                            echo '<a href="tel:' . $contact_array['phone'] . '">' . $contact_array['phone'] . '</a>';
                        }
                    }
                    ?></p>
                </div>
                <div class="row">
                    <button data-target="#kontakt_aufnehmen" data-toggle="modal" class="btn btn-u anfrage_senden">
                        Spende anbieten&nbsp;<i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="headline"><h2><?= $search->title ?></h2></div>
                </div>
                <div class="row">
                    <p><?= $search->description ?></p>
                </div>
                <hr>
                <?php
                if ($search->category == 'time') { ?>
                    <div class="row">
                        <h4>Zeitangaben</h4>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Anfang</th>
                                <th>Ende</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($search->time_searches as $time_search) {
                                echo '<tr><td>';
                                echo $time_search->start_date->i18nFormat('dd.MM.yyyy');
                                echo ' - ';
                                echo $time_search->start_time->i18nFormat('hh:mm');
                                echo '</td><td>';
                                echo $time_search->end_date->i18nFormat('dd.MM.yyyy');
                                echo ' - ';
                                echo $time_search->end_time->i18nFormat('hh:mm');
                                echo '</td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

        <?php
        if ($search->has('active') && $search->active == 1) { ?>

            <div class="modal fade" id="kontakt_aufnehmen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <form id="anfrage_senden_form" method="post" action="/Helpers/request">
                    <?= $this->Form->hidden(__('search'), ['value' => base64_encode($donation->id)]) ?>
                    <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Neue Anfrage</h4>
                            </div>
                            <div class="modal-body">
                                <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($broker) && $broker->firstname ? $broker->firstname . " " . $broker->lastname : '']) ?>
                                <br/>
                                <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($broker) && $broker->email ? $broker->email : '']) ?>
                                <br/>
                                <label>Nachricht</label>
                                <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen
                                </button>
                                <button type="button" class="btn-u btn-u-primary anfrage_senden_submit">Spende anbieten&nbsp;<i
                                        class="fa fa-angle-double-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <script type="text/javascript" src="/js/anfrage_helper.js"></script>

            <?php
        }
        break;
    case "spenden":
        ?>
        <div class="row">
            <a href="/broker/spenden_verwalten/<?php echo base64_encode('spenden') . "|"; ?>" class="btn btn-u"><i
                    class="fa fa-angle-double-left"></i>&nbsp;Zurück zur Auflistung</a>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="row" style="padding:30px">
                    <?php
                    if (isset($donation->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $donation->file_uploads[0]->src)) { ?>
                        <img src="<?= $donation->file_uploads[0]->src ?>" style="max-width:100px;max-height:100px" />
                        <?php
                    } else {
                        if ($donation->category == 'time') {
                            ?>
                            <img src="/img/time_placeholder.png" style="max-width:100px"/>
                            <?php
                        } else {
                            ?>
                            <img src="/img/thing_placeholder.png" style="max-width:100px" />
                            <?php
                        }
                    }
                    ?>
                </div>
                <br/>

                <div class="row">
                    <div class="headline"><h2>Anschrift</h2></div>
                    <p><?php
                        if (isset($contact_array)) {
                            if (isset($contact_array['firstname']) || isset($contact_array['lastname'])) {
                                echo $contact_array['firstname'] . " " . $contact_array['lastname'];
                            }
                            if (isset($contact_array['street']) || isset($contact_array['zip']) || isset($contact_array['city'])) {
                                echo '<br/>';
                                if (isset($contact_array['street']) && strlen($contact_array['street'])>0) {
                                    echo $contact_array['street'] . ", ";
                                }
                                echo $contact_array['zip'] . " " . $contact_array['city'];
                            }
                            if (isset($contact_array['phone'])) {
                                echo '<br/>';
                                echo '<a href="tel:' . $contact_array['phone'] . '">' . $contact_array['phone'] . '</a>';
                            }
                        }
                        ?></p>
                </div>
                <?php if ($donation->has('active') && $donation->active ==1) { ?>
                <div class="row">
                    <button data-target="#kontakt_aufnehmen" data-toggle="modal" class="btn btn-u anfrage_senden">
                        Spende anbieten&nbsp;<i class="fa fa-angle-double-right"></i></button>
                </div>
                <?php } ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="headline"><h2><?= $donation->title ?></h2></div>
                </div>
                <div class="row">
                    <p><?= $donation->description ?></p>
                </div>
                <hr>
                <?php
                if ($donation->category == 'time') { ?>
                    <div class="row">
                        <h4>Zeitangaben</h4>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Anfang</th>
                                <th>Ende</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($donation->time_donations as $time_donation) {
                                echo '<tr><td>';
                                echo $time_donation->start_date->i18nFormat('dd.MM.yyyy');
                                echo ' - ';
                                echo $time_donation->start_time->i18nFormat('hh:mm');
                                echo '</td><td>';
                                echo $time_donation->end_date->i18nFormat('dd.MM.yyyy');
                                echo ' - ';
                                echo $time_donation->end_time->i18nFormat('hh:mm');
                                echo '</td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

        <?php
        if ($donation->has('active') && $donation->active == 1) {
            ?>

            <div class="modal fade" id="kontakt_aufnehmen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <form id="anfrage_senden_form" method="post" action="/Helpers/request">
                    <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                    <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Neue Anfrage</h4>
                            </div>
                            <div class="modal-body">
                                <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($broker) && $broker->firstname ? $broker->firstname . " " . $broker->lastname : '']) ?>
                                <br/>
                                <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($broker) && $broker->email ? $broker->email : '']) ?>
                                <br/>
                                <label>Nachricht</label>
                                <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen
                                </button>
                                <button type="button" class="btn-u btn-u-primary anfrage_senden_submit">Spende anbieten&nbsp;<i
                                        class="fa fa-angle-double-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <script type="text/javascript" src="/js/anfrage_helper.js"></script>

            <?php
        }


        break;
}
?>

