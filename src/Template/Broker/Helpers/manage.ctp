<div class="row" style="padding-right:20px">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="manage_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
            <thead>
            <tr>
                <?php
                foreach ($columns as $column) {
                    ?>
                    <th><?= $column ?></th>
                    <?php
                }
                ?>
            </tr>
            </thead>
            <tbody>
            <?php
            switch ($type) {
                case "gesuche":
                    foreach ($data as $row) {
                        ?>
                        <tr>
                            <td><?= $row->id ?></td>
                            <td><?= $row->title ?></td>
                            <td><?php if ($row->category == 'thing') { echo "Sachgesuch"; } else if ($row->category == 'time') { echo "Zeitgesuch"; } ?></td>
                            <td><?= $row->created->i18nFormat('dd.MM.yyyy HH:mm') ?></td>
                            <td><?= $row->modified->i18nFormat('dd.MM.yyyy HH:mm') ?></td>
                            <td>
                                <a href="/broker/details/<?php echo base64_encode($row->id."|".$row->category."|gesuche"); ?>" class="btn btn-warning">Details&nbsp;<i class="fa fa-search"></i></a>
                                <a href="/broker/abgeschlossen/<?php echo base64_encode($row->id."|".$row->category."|gesuche"); ?>" class="btn btn-success">Abgeschlossen&nbsp;<i class="fa fa-check"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    break;
                case "spenden":
                case "spenderspenden":
                    foreach ($data as $row) {
                        ?>
                        <tr>
                            <td><?= $row->id ?></td>
                            <td><?= $row->title ?></td>
                            <td><?php if ($row->category == 'thing') { echo "Sachspende"; } else if ($row->category == 'time') { echo "Zeitspende"; } ?></td>
                            <td><?= $row->created->i18nFormat('dd.MM.yyyy HH:mm') ?></td>
                            <td><?= $row->modified->i18nFormat('dd.MM.yyyy HH:mm') ?></td>
                            <td>
                                <a href="/broker/details/<?php echo base64_encode($row->id."|".$row->category."|spenden"); ?>" class="btn btn-warning">Details&nbsp;<i class="fa fa-search"></i></a>
                                <a data-toggle="modal" data-target="#abgeschlossen" data-url="/broker/abgeschlossen/<?php echo base64_encode($row->id."|".$row->category."|spenden"); ?>" class="btn btn-success open_modal">Abgeschlossen&nbsp;<i class="fa fa-check"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    break;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="abgeschlossen" tabindex="-1" role="dialog">
    <form method="post" action="" id="abgeschlossen">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Begründung</h4>
                </div>
                <div class="modal-body">
                    <label for="spende_abgeschlossen" style="margin-right:10px;">Spende erfolgreich abgeschlossen</label><input checked id="spende_abgeschlossen" type="radio" name="reason" value="spende_abgeschlossen" /><br/>
                    <label for="spende_nicht_verfuegbar" style="margin-right:10px;">Spende nicht mehr verfügbar</label><input id="spende_nicht_verfuegbar" type="radio" name="reason" value="spende_nicht_verfuegbar" /><br/>
                    <label for="spende_erfolgreich" style="margin-right:10px;">Spende erfolgreich an mich oder andere vermittelt</label><input id="spende_erfolgreich" type="radio" name="reason" value="spende_erfolgreich" /><br/>
                    <textarea class="form-control" name="reason_optional" style="display:none" placeholder="Optionalen Text eingeben (Bemerkungen) ..."></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    <button type="submit" class="btn btn-success send_reason">Absenden</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    jQuery('.open_modal').off().on('click', function(e) {
        jQuery('form#abgeschlossen').attr('action', jQuery(this).attr('data-url'));
    });
    jQuery('input[type="radio"]').off().on('click', function() {
        var element_value = jQuery(this).val();
        if (element_value === 'spende_nicht_verfuegbar' || element_value === 'spende_erfolgreich') {
            jQuery('textarea[name="reason_optional"]').fadeIn('fast');
        } else {
            jQuery('textarea[name="reason_optional"]').fadeOut('fast');
        }
    });
    jQuery(document).ready(function() {
        jQuery('#manage_table').dataTable({
            "order": []
            /*"columnDefs" : [
                { type : 'de_date', targets: [3, 4] },
            ]*/
        });
    });

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "de_date-asc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity; // = l'an 1000 ...
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
            return z;
        },

        "de_date-desc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity;
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
            return z;
        }
    } );
</script>
