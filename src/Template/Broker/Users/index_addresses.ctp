<div class="row" style="padding-right:20px">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="headline"><h2><?= __('Kontaktdaten verwalten') ?></h2></div>
        <table id="addresses_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
            <thead>
            <tr>
                <th>Anschrift</th>
                <th>Postleitzahl</th>
                <th>Ort</th>
                <th>E-Mail-Adresse</th>
                <th>Telefonnummer</th>
                <th>Aktion</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($addresses as $address) {
                ?>
                <tr>
                    <td><?= $address->street ?></td>
                    <td><?= $address->zip ?></td>
                    <td><?= $address->city ?></td>
                    <td><?= $address->email ?></td>
                    <td><?= $address->phone ?></td>
                    <td>
                        <a href="/broker/kontaktdaten_bearbeiten/<?= $address->id ?>" class="btn btn-warning">Bearbeiten&nbsp;<i class="fa fa-pencil-square-o"></i></a>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#addresses_table').dataTable({
            "order": []
            /*"columnDefs" : [
             { type : 'de_date', targets: [3, 4] },
             ]*/
        });
    });

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "de_date-asc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity; // = l'an 1000 ...
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
            return z;
        },

        "de_date-desc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity;
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
            return z;
        }
    } );
</script>