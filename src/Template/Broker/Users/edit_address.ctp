<div class="row" style="padding-right:20px">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="headline"><h2><?= __('Kontaktdaten bearbeiten') ?></h2></div>
        <br/>
        <?= $this->Form->create($address) ?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Anschrift</label>
                <?= $this->Form->input(__('street'), ['class' => 'form-control', 'label' => false]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Postleitzahl</label>
                <?= $this->Form->input(__('zip'), ['class' => 'form-control', 'label' => false]) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Ort</label>
                <?= $this->Form->input(__('city'), ['class' => 'form-control', 'label' => false]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>E-Mail-Adresse</label>
                <?= $this->Form->input(__('email'), ['class' => 'form-control', 'label' => false]) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Telefonnummer</label>
                <?= $this->Form->input(__('phone'), ['class' => 'form-control', 'label' => false]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?= $this->Form->button(__('Speichern'), ['class' => 'btn btn-u']) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

