
<div class="row">
    <!--
    <div class="col-md-5 md-margin-bottom-40">
        <div class="posts">
            <div class="headline"><h2>Matches</h2></div>
            <?php
    /*
            foreach ($matches as $match) {
                ?>
                <dl class="dl-horizontal">
                    <p style="text-align:center;margin-bottom:-5px;border:1px solid #009D00"><a href="<?= $match['search']['href'] ?>"><?= $match['search']['title'] ?></a></p>
                    <p style="text-align:center;margin-bottom:-5px;"><i class="fa fa-angle-double-up"></i></p>
                    <p style="text-align:center;margin-top:-10px"><i class="fa fa-angle-double-down"></i></p>
                    <p style="text-align:center;margin-top:-15px;border:1px solid green"><a href="<?= $match['donation']['href'] ?>"><?= $match['donation']['title'] ?></a></p>
                </dl>
                <hr style="margin-top:0px;margin-bottom:15px;">
            <?php
            } */
            ?>
        </div>
    </div>
    -->

    <div class="col-md-12 md-margin-bottom-40">
        <div class="posts">
            <div class="headline"><h2>Suche</h2></div>
            <div class="row">
                <form id="search_donate_form" method="post" action="/suche/">
                    <div class="col-xs-4" style="margin-right:-30px">
                        <h5>Spenden suchen</h5>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" name="donate_input" class="form-control" style="height:32px" />
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-u donate_search" style="margin-left:-30px">Suchen&nbsp;<i class="fa fa-angle-double-right"></i></button>
                    </div>
                </form>
            </div>
            <br/><br/>
            <div class="row">
                <form id="search_search_form" method="post" action="/suche/">
                    <div class="col-xs-4" style="margin-right:-30px">
                        <h5>Gesuche suchen</h5>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" name="search_input" class="form-control" style="height:32px" />
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-u search_search" style="margin-left:-30px">Suchen&nbsp;<i class="fa fa-angle-double-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>