<?= $this->assign('title', 'Gesuche anzeigen') ?>
<div class="searches index large-12 medium-12 columns content">
    <h3><?= __('Alle Gesuche') ?></h3>
    <table id="data-table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
        <thead>
            <tr>
                <th>Titel</th>
                <th>Beschreibung</th>
                <th>Schlagwörter</th>
                <th>Erstellt</th>
                <th>Letzte Änderung</th>
                <th class="actions"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($searches as $search) { ?>
            <tr>
                <td><?= h($search->title) ?></td>
                <td><?= h($search->description) ?></td>
                <td><?php
                    foreach ($search->keywords as $keyword) {
                        echo $keyword;
                    }
                    ?></td>
                <td><?= h($search->created->i18nFormat('dd.MMMM YYYY, hh:MM')) ?></td>
                <td><?= h($search->modified->i18nFormat('dd.MMMM YYYY, hh:MM')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Details'), ['action' => 'view', $search->id]) ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
