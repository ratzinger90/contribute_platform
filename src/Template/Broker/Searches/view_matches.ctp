<div class="headline"><h2>Passende Spenden wurden gefunden</h2></div>
<div class="row margin-bottom-20">
    <?php foreach ($donations as $donation) { ?>
        <?php
        if ($donation->category=='thing') {
            $href = "/sachspende/";
            $placeholder = "/img/thing_placeholder.png";
        } else if ($donation->category=='time') {
            $href = "/zeitspende/";
            $placeholder = "/img/time_placeholder.png";
        }
        ?>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden">
                        <a href="<?= $href . $donation->url_title ?>">
                            <?php
                            if (isset($donation->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $donation->file_uploads[0]->src)) { ?>
                                <!-- Ausgabe des Thumbnails -->
                                <img class="img-responsive" src="<?php echo $donation->file_uploads[0]->src ?>"
                                     alt="" style="max-width:150px;max-height:150px" />
                                <?php
                            } else { ?>
                                <img class="img-responsive" src="<?= $placeholder ?>" style="max-width:150px;max-height:150px" />
                                <?php
                            }
                            ?>
                        </a>
                    </div>
                    <a class="btn-more hover-effect" href="<?= $href . $donation->url_title ?>">Details +</a>
                </div>
                <div class="caption">
                    <h3><a class="hover-effect" href="<?= $href . $donation->url_title ?>"> <?php echo $donation->title; ?> </a></h3>
                    <p>
                        <?php
                        $description_array = explode(" ", $donation->description);
                        $description_length = 0;
                        $continue = false;
                        foreach ($description_array as $key => $description) {
                            $description_length += strlen($description);
                            if ($description_length<140) {
                                echo $description . " ";
                            } else if ($description_length>140 && !$continue) {
                                echo "...";
                                $continue = true;
                            } else {
                                continue;
                            }
                        }
                        ?>
                    </p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>