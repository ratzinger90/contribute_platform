<div class="searches view large-12 medium-12 columns content">
    <div class="row">
        <h3><?= h($search->title) ?></h3>
    </div>
    <div class="row">
    <table class="vertical-table">
        <tr>
            <th><?= __('Erstellt') ?></th>
            <td style="text-align: left"><?= h($search->created->i18nFormat('dd. MMMM YYYY, hh:MM')) ?></td>
            <th><?= __('Zuletzt geändert') ?></th>
            <td style="text-align: left"><?= h($search->modified->i18nFormat('dd. MMMM YYYY, hh:MM')) ?></td>
        </tr>
    </table>
    </div>
    <div class="row">
        <hr>
        <p style="color: #68655d">Schlagwörter:&nbsp;
        <?php
        foreach ($search->keywords as $keyword) {
            echo h($keyword);
        }
        ?>
        </p>
    </div>
    <div class="row">
        <h4><?= __('Beschreibung') ?></h4>
        <?= $this->Text->autoParagraph(h($search->description)); ?>
    </div>
</div>
