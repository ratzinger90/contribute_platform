<?php

$zipcodes_array = array();
foreach ($zipcodes as $zipcode) {
    $zipcodes_array[$zipcode->zipcode] = $zipcode->zipcode . " " . $zipcode->city;
}
?>

<div class="row">
    <?= $this->Form->create($institution, ['enctype' => 'multipart/form-data']) ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:40px">
        <?php if ($institution->has('id') && $institution->id>0) { ?><div class="headline"><h2><?= __('Einrichtung bearbeiten') ?></h2></div><?php
        } else { ?><div class="headline"><h2><?= __('Einrichtung anlegen') ?></h2></div><?php }  ?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Titel</label>
                <?= $this->Form->input(__('title'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Beschreibung</label>
                <?= $this->Form->textarea(__('description'), ['type' => 'password', 'label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Anschrift</label>
                <?= $this->Form->input(__('street'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <label>Postleitzahl</label>
                <?= $this->Form->input(__('zip'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Ort</label>
                <?= $this->Form->input(__('city'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Telefonnummer</label>
                <?= $this->Form->input(__('phone'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <br/>
        <?php
        if ($institution->has('id') && $institution->id>0) {
            ?>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <?php
                    if ($institution->has('file_upload') && $institution->file_upload->has('src')) {
                        ?>
                        <img src="<?= $institution->file_upload->src ?>" style="max-width:100%;"/>
                        <label>Bild löschen (Häkchen auswählen)</label><?= $this->Form->checkbox(__('delete_image'), ['class' => 'form-control', 'style' => 'width:15px;height:15px;']) ?>
                        <br/>
                        <?php
                    }
                    ?>
                    <label>Neues Foto hochladen:</label>
                    <?= $this->Form->file(__('image'), ['type' => 'file', 'class' => 'form-control']) ?>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Foto hochladen:</label>
                    <?= $this->Form->file(__('image'), ['type' => 'file', 'class' => 'form-control']) ?>
                </div>
            </div>
            <?php
        }
        ?>
        <br/>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Zugewiesene Postleitzahlen</label><br/>
                <?= $this->Form->select(__('zipcodes'), $zipcodes_array, ['id' => 'zipcodes_select', 'multiple' => true]) ?>
            </div>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?php if ($institution->has('id') && $institution->id>0) { echo $this->Form->button(__('Änderungen speichern'), ['class' => 'btn btn-u']);
                } else { echo $this->Form->button(__('Einrichtung anlegen'), ['class' => 'btn btn-u']); } ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>

<?php
if (isset($institution_zipcodes_array) && sizeof($institution_zipcodes_array)>0) {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var zipcodes = <?php echo json_encode($institution_zipcodes_array); ?>;
            var zipcodes_array = [];
            for (zipcode in zipcodes) {
                zipcodes_array[zipcode] = zipcode;
            }
            jQuery('#zipcodes_select').val(zipcodes_array).select2();
        });
    </script>
    <?php
} else {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#zipcodes_select').select2({
                'placeholder': 'Bitte auswählen'
            });
        });
    </script>
    <?php
}
