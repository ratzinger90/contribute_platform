<div class="row" style="padding-right:20px">
    <table id="manage_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
        <thead>
        <tr>
            <th>Vorname</th>
            <th>Nachname</th>
            <th>Chiffre</th>
            <th>Erstellt</th>
            <th>Aktionen</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($brokers)) {
            foreach ($brokers as $row) {
                $broker_pin = "XXXX";
                ?>
                <tr>
                    <td><?= $row->firstname ?></td>
                    <td><?= $row->lastname ?></td>
                    <td><?= $broker_pin ?></td>
                    <td><?= $row->created->i18nFormat('dd.MM.yyyy hh:mm') ?></td>
                    <td>
                        <?php
                        if ($row->active == 0) {
                            ?>
                            <a href="/admin/vermittler_aktivieren/<?= $row->id ?>/<?= $case ?>" class="btn btn-success"><i
                                    class="fa fa-check"></i>&nbsp;Freischalten</a>
                            <a data-url="/admin/vermittler_loeschen/<?= $row->id ?>/<?= $case ?>" class="btn btn-danger delete_broker"><i
                                    class="fa fa-trash-o"></i>&nbsp;Löschen</a>
                            <?php
                        } else {
                            ?>
                            <a href="/admin/vermittler_deaktivieren/<?= $row->id ?>/<?= $case ?>"
                               class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Deaktivieren</a>
                            <?php
                        }
                        ?>
                        <a href="/admin/vermittler_ansehen/<?= $row->id ?>/<?= $case ?>" class="btn btn-warning"><i
                                class="fa fa-search"></i>&nbsp;Details</a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#manage_table').dataTable();
    });

    jQuery('a.delete_broker').on('click', function(e) {
        e.preventDefault();
        var data_url = jQuery(this).attr('data-url');
        notie.confirm('Diesen Nutzer wirklich löschen?', 'Ja', 'Nein', function() {
                location.href = data_url
            }
        );
    });
</script>
