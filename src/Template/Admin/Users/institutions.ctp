<div class="row" style="padding-right:20px">
    <a href="/admin/einrichtung_anlegen" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Neue Einrichtung anlegen</a>
    <br/>
    <br/>
    <table id="manage_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
        <thead>
        <tr>
            <th>Einrichtung</th>
            <th>Zugewiesene Regionen</th>
            <th>Aktionen</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($institutions)) {
            foreach ($institutions as $row) {
                ?>
                <tr>
                    <td><?= $row->title ?></td>
                    <td>
                        <?php
                        if (isset($institution_zipcodes_array[$row->id])) {
                            $zipcodes = $institution_zipcodes_array[$row->id];
                            $counter = 0;
                            foreach ($zipcodes as $zipcode) {
                                if ($counter > 0) {
                                    echo ", ";
                                }
                                echo $zipcode;
                                $counter += 1;
                            }
                        }
                        ?>
                    </td>
                    <td>
                        <a href="/admin/einrichtung_bearbeiten/<?= $row->id ?>" class="btn btn-warning"><i
                                class="fa fa-pencil"></i>&nbsp;Bearbeiten</a>
                        <a href="/admin/einrichtung_loeschen/<?= $row->id ?>" class="btn btn-danger delete"><i
                                class="fa fa-ban"></i> Löschen</a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#manage_table').dataTable();
    });
    jQuery('.delete').off().on('click', function(e) {
        e.preventDefault();
        var location = jQuery(this).attr('href');
        notie.confirm(
            'Einrichtung wirklich löschen?',
            'Ja',
            'Nein',
            function() {
                window.location.href = location;
            }
        );
    });
</script>