<?php
if (isset($institutions) && $institutions) {
    ?>
    <h2>Einrichtungen</h2>
    <ul class="timeline-v1">
        <?php
        foreach ($institutions as $key => $institution) {
            if ($key % 2 == 1) {
                ?>
                <li>
                    <div class="timeline-badge primary"><i class="glyphicon glyphicon-record"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <?php
                            if (isset($institution->file_upload) && $institution->file_upload->src) {
                                ?>
                                <img class="img-responsive" src="<?= $institution->file_upload->src ?>" alt=""/>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="timeline-body text-justify">
                            <h2 class="font-light"><?= $institution->title ?></h2>
                            <p><?= $institution->description ?></p>
                        </div>
                        <?php
                        if ($institution->has('street') && strlen($institution->street) > 0 &&
                            $institution->has('zip') && strlen($institution->zip) > 0
                        ) {
                            ?>
                            <div class="timeline-footer">
                                <ul class="list-unstyled list-inline blog-info">
                                    <li><i class="fa fa-home"></i> <?= $institution->street ?>
                                        , <?= $institution->zip ?> <?= $institution->city ?></li>
                                    <?php if ($institution->has('phone') && strlen($institution->phone) > 0) {
                                        ?>
                                        <li><i class="fa fa-phone"></i> <a
                                                    href="tel:<?= $institution->phone ?>"><?= $institution->phone ?></a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </li>
                <?php
            } else {
                ?>
                <li class="timeline-inverted">
                    <div class="timeline-badge primary"><i class="glyphicon glyphicon-record invert"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <?php
                            if (isset($institution->file_upload) && $institution->file_upload->src) {
                                ?>
                                <img class="img-responsive" src="<?= $institution->file_upload->src ?>" alt=""/>
                                <?php
                            }
                            ?>                    </div>
                        <div class="timeline-body text-justify">
                            <h2 class="font-light"><?= $institution->title ?></h2>
                            <p><?= $institution->description ?></p>
                        </div>
                        <?php
                        if ($institution->has('street') && strlen($institution->street) > 0 &&
                            $institution->has('zip') && strlen($institution->zip) > 0
                        ) {
                            ?>
                            <div class="timeline-footer">
                                <ul class="list-unstyled list-inline blog-info">
                                    <li><i class="fa fa-home"></i> <?= $institution->street ?>
                                        , <?= $institution->zip ?> <?= $institution->city ?></li>
                                    <?php if ($institution->has('phone') && strlen($institution->phone) > 0) {
                                        ?>
                                        <li><i class="fa fa-phone"></i> <a
                                                    href="tel:<?= $institution->phone ?>"><?= $institution->phone ?></a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </li>
                <?php
            }
        }
        ?>
        <li class="clearfix" style="float: none;"></li>
    </ul>
    <?php
} else {
    ?>
    <h4>Für Deine Stadt haben wir noch keinen Partner und noch keine Einrichtungen.</h4>
    <?php
}
