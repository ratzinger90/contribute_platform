<?php
switch ($type) {
    case "donation":
        switch ($case) {
            case "sach":
                ?>
<link rel="stylesheet" href="/plugins/fancybox/source/jquery.fancybox.css">
    <script type="text/javascript" src="/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="row"><?= $this->element('donations_help_sidebar') ?></div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="row">
            <a href="<?php
            if (isset($referer_for_button) && strlen($referer_for_button)>0) {
                echo $referer_for_button;
            } else if (isset($_SERVER['HTTP_REFERER'])) {
                echo $_SERVER['HTTP_REFERER'];
            } else {
                echo "/sachspenden";
            } ?>" class="btn btn-u"><i class="fa fa-angle-double-left"></i>&nbsp;Zurück</a>
        </div>
        <br/>
        <div class="row">
            <div class="headline"><h2 style="font-size:17px"><?= $donation->title ?></h2></div>
        </div>
        <div class="row" style="padding-top:10px;padding-bottom:10px">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <?php
                if (isset($donation_image) && file_exists(rtrim(WWW_ROOT, '/').$donation_image->src)) { ?>
                    <a class="thumbnail fancybox-button zoomer" data-rel="fancybox-button" title="<?= $donation->title ?>" href="<?= $donation_image->src ?>">
                        <?php
                        $first_rotation = "";
                        $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$donation_image->src, 'IFD0');
                        if (!empty($exif_donation_image['Orientation'])){
                            switch($exif_donation_image['Orientation']) {
                                case 6:
                                    $first_rotation = "rotate(90deg)";
                                    break;
                            }
                        }
                        ?>
                        <span class="overlay-zoom" style="<?php if (isset($first_rotation)) { ?>transform:<?= $first_rotation ?>;-moz-transform:<?= $first_rotation ?>;-webkit-transform:<?= $first_rotation; ?><?php } ?>">
                    <img class="img-responsive" src="<?= $donation_image->src ?>" style="max-width:100%;max-height:100%;" />
                    <span class="zoom-icon"></span>
                </span>
                    </a>
                    <?php
                } else {
                    ?>
                    <img src="/img/thing_placeholder.png" style="max-width:100%;max-height:100%" />
                    <?php
                }
                ?>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <p><?= $donation->description ?></p>
            </div>
        </div>
        <?php
        if (isset($donation_images) && $donation_images) {
        ?>
        <div class="row">
            <div class="headline"><h2 style="font-size:16px"><?= __('Weitere Bilder') ?></h2></div>
        </div>
        <div class="row">
            <div id="myCarousel" class="carousel slide carousel-v1" style="max-width:100%;max-height:100%">
                <div class="carousel-inner" style="height:350px">
                    <?php
                    foreach ($donation_images as $key => $image) {
                    if (!file_exists($image->src)) {
                        continue;
                    }
                    if ($key == 0) {
                        continue;
                    } else if ($key == 1) {
                    ?>
                    <div class="item active">
                        <?php
                        } else {
                        ?>
                        <div class="item">
                            <?php
                            }
                            ?>
                            <?php
                            $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$image->src, 'IFD0');
                            if (!empty($exif_donation_image['Orientation'])){
                                switch($exif_donation_image['Orientation']) {
                                    case 6:
                                        $rotation = "rotate(90deg)";
                                        break;
                                    default:
                                        $rotation = "";
                                        break;

                                }
                            }
                            ?>
                            <img src="<?= $image->src ?>" style="max-width:100%;max-height:350px;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>" />
                        </div>
                        <?php
                        }
                        ?>
                    </div>

                    <div class="carousel-arrow">
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
            <div class="row">
                <?php
                if (isset($contact_array)) {
                    ?>
                    <div class="headline"><h2 style="font-size:16px">Kontaktdaten</h2></div>
                    <?php
                }
                ?>
                <p><?php
                    if (isset($contact_array) && isset($contact_array['firstname'])) { ?>
                        <?= $contact_array['firstname'] . " " . $contact_array['lastname'] ?>
                        <br/>
                        <?php
                        if (isset($contact_array['street']) && strlen($contact_array['street'])>0) {
                            ?>
                            <?= $contact_array['street'] . ", " . $contact_array['zip'] . " " . $contact_array['city'] ?>
                            <?php
                        } else {
                            ?>
                            <?= $contact_array['zip'] . " " . $contact_array['city'] ?>
                            <?php
                        }
                        ?>
                        <?php
                    } else if (isset($contact_array['zip']) && $contact_array['zip']!='') { ?>
                        <?= $contact_array['zip'] . " " . $contact_array['city'] ?>
                        <?php
                    }

                    if (isset($contact_array['phone']) && strlen($contact_array['phone'])>0) {
                        ?>
                        <br/>
                        <a href="tel:<?= $contact_array['phone'] ?>" class="btn btn-sm btn-warning"><?= $contact_array['phone'] ?>&nbsp;<i class="fa fa-phone"></i></a>
                        <?php
                    }
                    ?>
                </p>
            </div>
            <?php
            if (!$own_donation) { ?>
            <div class="row">
                <?php
                if (isset($logged_in) && $logged_in && isset($role) && $role == 'broker') {
                } else {
                    ?>
                    <button data-target="#interesse_spende" data-toggle="modal" class="btn btn-u interesse_modal">Das
                        brauche ich
                    </button>
                    <?php
                }
                ?>
                <br/>
                <br/>
                <a href="/spende_abgeben/<?php echo base64_encode($donation->id); ?>" class="btn btn-u">Ähnliche Spende aufgeben&nbsp;<i
                        class="fa fa-angle-double-right"></i></a>
                <br/>
                <?php
                if (isset($logged_in) && $logged_in && isset($role) && $role == 'broker') {
                    if (isset($contact_array['email']) && strlen($contact_array['email'])>0) {
                        ?>
                        <br/>
                        <button data-target="#kontakt_aufnehmen" data-toggle="modal" class="btn btn-u anfrage_modal">Anfrage
                            senden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                        <br/>
                        <?php
                    }
                }
                if (isset($logged_in) && $logged_in) {
                    if (isset($role) && $role == 'token_account') {
                        ?>
                        <br/>
                        <button data-target="#beitrag_melden" data-toggle="modal" class="btn btn-u meldungs_modal">Beitrag
                            melden&nbsp;<i class="fa fa-exclamation-circle"></i></button>
                        <br/>
                        <?php
                    }
                }
                }?>
            </div>
        </div>
        <?php
        if (isset($related_donations)) {
            ?>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 related_donations" style="padding-left:40px">
                <div class="row">
                    <div class="headline" style="text-align:right"><h2 style="font-size:16px">Verwandte Spenden</h2></div>
                    <?php
                    foreach ($related_donations as $related_donation) {
                        ?>
                        <div class="row">
                            <div class="col-xs-5">
                                <?php
                                if (isset($related_donation->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $related_donation->file_uploads[0]->src)) {
                                    ?>
                                    <?php
                                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/') . $related_donation->file_uploads[0]->src, 'IFD0');
                                    if (!empty($exif_donation_image['Orientation'])) {
                                        switch ($exif_donation_image['Orientation']) {
                                            case 6:
                                                $rotation = "rotate(90deg)";
                                                break;
                                            default:
                                                $rotation = "";
                                                break;

                                        }
                                    }
                                    ?>
                                    <img src="<?= $related_donation->file_uploads[0]->src ?>"
                                         style="max-width:75px;max-height:75px;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>"/>
                                    <?php
                                } else {
                                    switch ($related_donation->category) {
                                        case "time":
                                            echo '<img src="/img/time_placeholder.png" style="max-width:100%;max-height:100%" />';
                                            break;
                                        case "thing":
                                            echo '<img src="/img/thing_placeholder.png" style="max-width:100%;max-height:100%" />';
                                            break;
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-xs-7">
                                <div class="row">
                                    <?= $related_donation->title ?>
                                </div>
                                <div class="row">
                                    <?php
                                    switch ($related_donation->category) {
                                        case "time":
                                            echo '<a style="float:right;margin-top:10px;" href="/zeitspende/' . $related_donation->url_title . '" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>';
                                            break;
                                        case "thing":
                                            echo '<a style="float:right;margin-top:10px;" href="/sachspende/' . $related_donation->url_title . '" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>';
                                            break;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
        }
        ?>

        <?php
        if (isset($logged_in) && $logged_in && isset($role) && $role != 'token_account') {
            ?>

            <div class="modal fade" id="kontakt_aufnehmen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <form id="anfrage_senden_form" method="post" action="/anfrage_senden">
                    <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                    <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                    <?= $this->Form->hidden(__('referer'), ['value' => $_SERVER['HTTP_REFERER']]) ?>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Neue Anfrage</h4>
                            </div>
                            <div class="modal-body">
                                <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                                <br/>
                                <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->email ? $current_user->email : ""]) ?>
                                <br/>
                                <?php
                                if (isset($contact_without_mail) && $contact_without_mail) {
                                    ?>
                                    <label>Deine Telefonnummer</label>
                                    <?= $this->Form->input(__('sender_number'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                                    <?= $this->Form->hidden(__('message')) ?>
                                    <?php
                                } else {
                                    if ($donation->user->has('phone') &&
                                        strlen($donation->user->phone) > 0 &&
                                        isset($logged) && $logged == true
                                    ) {
                                        ?>
                                        <label>Telefonnummer</label><br/>
                                        <span class="label label-default" style="font-size:18px"><a
                                                href="tel:<?= $donation->user->phone ?>"><i
                                                    class="fa fa-phone"></i>&nbsp;<?= $donation->user->phone ?></a></span>
                                        <br/><br/>
                                        <?php
                                    }
                                    ?>
                                    <label>Nachricht</label>
                                    <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                                    <?php
                                }
                                ?>
                            </div>
                            <?= $this->Form->hidden(__('action'), ['value' => $this->request->params['action']]) ?>
                            <div class="modal-footer">
                                <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                                <button data-url="/anfrage_senden/<?= $donation->url_title ?>" type="button"
                                        class="btn-u btn-u-primary anfrage_senden_submit">Spender kontaktieren&nbsp;<i
                                        class="fa fa-angle-double-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <script type="text/javascript" src="/js/anfrage_helper.js"></script>

        <?php
        } else if (isset($logged_in) && $logged_in && isset($role) && $role == 'token_account') {
        ?>

            <div class="modal fade" id="beitrag_melden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <form id="meldung_senden_form">
                    <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                    <?= $this->Form->hidden(__('current_action'), ['value' => 'thing']) ?>
                    <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                    <?= $this->Form->hidden(__('referer'), ['value' => $_SERVER['HTTP_REFERER']]) ?>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Beitrag melden</h4>
                            </div>
                            <div class="modal-body">
                                <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                                <br/>
                                <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->email ? $current_user->email : ""]) ?>
                                <br/>
                                <label>Grund für die Meldung</label>
                                <?= $this->Form->textarea(__('reason'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                            </div>
                            <?= $this->Form->hidden(__('action'), ['value' => $this->request->params['action']]) ?>
                            <div class="modal-footer">
                                <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                                <button data-url="/beitrag_melden/<?= $donation->url_title ?>" type="button" class="btn-u btn-u-primary meldung_senden">Beitrag melden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <script type="text/javascript" src="/js/meldung_helper.js"></script>

            <?php
        }
        ?>

        <div class="modal fade" id="interesse_spende" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Interesse an dieser Spende</h4>
                    </div>
                    <div class="modal-body">
                        <div class="tab-v1">
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="check">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>
                                                Hier kannst Du dem Spendenvermittler in Deiner Nähe bescheid sagen, dass du diese Spende gebrauchen kannst. Er kontaktiert dann den Spender für Dich.
                                            </p>
                                            <br/>
                                            <form id="interesse_ueberpruefen" action="/BHmCsp49JS2S7Daw">
                                                <label>Gebe die Chiffre des Spendenvermittlers ein:</label>
                                                <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                                                <?= $this->Form->input(__('code'), ['label' => false, 'class' => 'form-control']) ?>
                                                <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                                                <br/>
                                                <a href="#pin_anfragen" class="pin_anfragen_modal" data-toggle="modal">Hier kannst du die Chiffre anfragen&nbsp;<i class="fa fa-angle-double-right"></i></a>
                                                <br/>
                                                <br/>
                                                <?= $this->Form->button(__('Überprüfen'), ['class' => 'btn btn-u code_ueberpruefen']) ?>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="message">
                                    <form id="interesse_form" method="post" action="/interesse_senden">
                                        <?= $this->Form->hidden(__('broker_id')) ?>
                                        <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                                        <?= $this->Form->input(__('name'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true]) ?>
                                        <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                                        <?= $this->Form->hidden(__('referer'), ['value' => isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0 ? $_SERVER['HTTP_REFERER'] : '']) ?>
                                        <br/>
                                        <label>Deine Nachricht</label>
                                        <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                                        <br/>
                                        <hr>
                                        <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control']) ?>
                                        <?= $this->Form->input(__('phone'), ['label' => 'Deine Telefonnummer', 'class' => 'form-control']) ?>
                                        <br/>
                                        <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                                        <button type="button" class="btn-u btn-u-primary interesse_senden">Absenden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="/js/interesse_helper.js"></script>

        <div class="modal fade" id="pin_anfragen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Chiffre des Vermittlers anfragen</h4>
                    </div>
                    <div class="modal-body">
                        <form id="pin_anfragen_form" method="post" action="/pin_anfragen">
                            <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                            <?= $this->Form->input(__('name'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                            <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                            <?= $this->Form->hidden(__('referer'), ['value' => isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0 ? $_SERVER['HTTP_REFERER'] : '']) ?>
                            <br/>
                            <label>Deine Nachricht</label>
                            <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                            <br/>
                            <hr>
                            <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control']) ?>
                            <br/>
                            <?= $this->Form->input(__('zip'), ['label' => 'Deine Postleitzahl', 'class' => 'form-control']) ?>
                            <br/>
                            <?= $this->Form->input(__('additional'), ['label' => 'Einrichtung / Institution / Gruppe', 'class' => 'form-control']) ?>
                            <br/>
                            <?php
                            if (isset($logged_in) && $logged_in) {
                                // no captcha
                            } else { ?>
                                <div class="g-recaptcha" data-sitekey="XXXXX"></div>
                                <?php
                            }
                            ?>
                            <br/>
                            <br/>
                            <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                            <button type="button" class="btn-u btn-u-primary pin_anfragen">Absenden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="/js/pin_anfragen_helper.js"></script>

        <script type="text/javascript">
            jQuery(document).ready(function() {
                App.initFancybox();
            });
        </script>
        <?php /* if (isset($first_rotation) && strlen($first_rotation)>0) { ?>
    <script type="text/javascript">
        jQuery('a.thumbnail.fancybox-button').on('click', function() {
             jQuery('img.fancybox-image').css('transform', <?php echo json_encode($first_rotation); ?>).css('-webkit-transform', <?php echo json_encode($first_rotation); ?>).css('-moz-transform', <?php echo json_encode($first_rotation); ?>);
        });
    </script>
<?php } */ ?>
                <?php
                break;
            case "zeit":
                ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="row"><?= $this->element('donations_help_sidebar') ?></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <a href="<?php
                    if (isset($referer_for_button) && strlen($referer_for_button)>0) {
                        echo $referer_for_button;
                    } else if (isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0) {
                        echo $_SERVER['HTTP_REFERER'];
                    } else {
                        echo "/zeitspenden";
                    } ?>" class="btn btn-u"><i class="fa fa-angle-double-left"></i>&nbsp;Zurück</a>
                </div>
                <br/>
                <div class="row">
                    <div class="headline"><h2 style="font-size:17px"><?= $donation->title ?></h2></div>
                </div>
                <div class="row" style="padding-top:10px">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?php
                        if (isset($donation_image) && file_exists(rtrim(WWW_ROOT, '/').$donation_image->src)) { ?>
                            <a class="thumbnail fancybox-button zoomer" data-rel="fancybox-button" title="<?= $donation->title ?>" href="<?= $donation_image->src ?>">
                                <?php
                                $first_rotation = "";
                                $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$donation_image->src, 'IFD0');
                                if (!empty($exif_donation_image['Orientation'])){
                                    switch($exif_donation_image['Orientation']) {
                                        case 6:
                                            $first_rotation = "rotate(90deg)";
                                            break;
                                    }
                                }
                                ?>
                                <span class="overlay-zoom" style="transform:<?= $first_rotation ?>;-moz-transform:<?= $first_rotation ?>;-webkit-transform:<?= $first_rotation; ?>">
                    <img class="img-responsive" src="<?= $donation_image->src ?>" style="max-width:100%;max-height:100%" />
                    <span class="zoom-icon"></span>
                </span>
                            </a>
                            <?php
                        } else {
                            ?>
                            <img src="/img/time_placeholder.png" style="max-width:100%;max-height:100%" />
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="row">
                            <p><?= $donation->description ?></p>
                        </div>
                        <?php
                        if (isset($donation->time_donations) &&
                            isset($donation->time_donations[0]) &&
                            (isset($donation->time_donations[0]->start_date) &&
                                $donation->time_donations[0]->start_date) ||
                            (isset($donation->time_donations[0]->end_date) &&
                                $donation->time_donations[0]->end_date)) {
                            ?>
                            <div class="row" style="margin-bottom:-20px">
                                <div class="headline"><h2 style="font-size:16px"><?= __('Zeitangaben') ?></h2></div>
                            </div>
                            <div class="row">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Beginn</th>
                                        <th>Ende</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($donation->time_donations as $time_donation) {
                                        ?>
                                        <tr>
                                            <td><?= $time_donation->start_date->i18nFormat('dd.MM.yyyy') . " - " . $time_donation->start_time->i18nFormat('HH:mm') ?></td>
                                            <td><?= $time_donation->end_date->i18nFormat('dd.MM.yyyy') . " - " . $time_donation->end_time->i18nFormat('HH:mm') ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            <?php
            if (isset($donation_images) && $donation_images) {
            ?>
                <div class="row">
                    <div class="headline"><h2 style="font-size:16px"><?= __('Weitere Bilder') ?></h2></div>
                </div>
                <div class="row">
                <div id="myCarousel" class="carousel slide carousel-v1" style="height:350px">
                    <div class="carousel-inner" style="height:350px">
                        <?php
                        foreach ($donation_images as $key => $image) {
                        if ($key == 0) {
                            continue;
                        } else if ($key == 1) {
                        ?>
                        <div class="item active">
                            <?php
                            } else {
                            ?>
                            <div class="item">
                                <?php
                                }
                                ?>
                                <?php
                                $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$image->src, 'IFD0');
                                if (!empty($exif_donation_image['Orientation'])){
                                    switch($exif_donation_image['Orientation']) {
                                        case 6:
                                            $rotation = "rotate(90deg)";
                                            break;
                                        default:
                                            $rotation = "";
                                            break;

                                    }
                                }
                                ?>
                                <img src="<?= $image->src ?>" style="max-width:100%;max-height:350px;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>" />
                            </div>
                            <?php
                            }
                            ?>
                        </div>

                        <div class="carousel-arrow">
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
                <div class="row" style="margin-top:-10px">
                    <?php
                    if (isset($contact_array)) {
                        ?>
                        <div class="headline"><h2 style="font-size:16px">Kontaktdaten</h2></div>
                        <?php
                    }
                    ?>
                    <p><?php
                        if (isset($contact_array) && isset($contact_array['firstname'])) { ?>
                            <?= $contact_array['firstname'] . " " . $contact_array['lastname'] ?>
                            <br/>
                            <?php
                            if (isset($contact_array['street']) && strlen($contact_array['street'])>0) {
                                ?>
                                <?= $contact_array['street'] . ", " . $contact_array['zip'] . " " . $contact_array['city'] ?>
                                <?php
                            } else {
                                ?>
                                <?= $contact_array['zip'] . " " . $contact_array['city'] ?>
                                <?php
                            }
                            ?>
                            <?php
                        } else if (isset($contact_array['zip']) && $contact_array['zip']!='') { ?>
                            <?= $contact_array['zip'] . " " . $contact_array['city'] ?>
                            <?php
                        }

                        if (isset($contact_array['phone']) && strlen($contact_array['phone'])>0) {
                            ?>
                            <br/>
                            <a href="tel:<?= $contact_array['phone'] ?>" class="btn btn-sm btn-warning"><?= $contact_array['phone'] ?>&nbsp;<i class="fa fa-phone"></i></a>
                            <?php
                        }
                        ?>
                    </p>
                </div>
            <?php
            if (!$own_donation) { ?>
                <div class="row">
                    <?php
                    if (isset($logged_in) && $logged_in && isset($role) && $role == 'broker') {
                    } else {
                        ?>
                        <button data-target="#interesse_spende" data-toggle="modal" class="btn btn-u interesse_modal">Das
                            brauche ich
                        </button>
                        <?php
                    }
                    ?>
                    <br/>
                    <br/>
                    <a href="/spende_abgeben/<?php echo base64_encode($donation->id); ?>" class="btn btn-u">Ähnliche Spende aufgeben&nbsp;<i class="fa fa-angle-double-right"></i></a>
                    <br/>
                    <?php
                    if (isset($logged_in) && $logged_in && isset($role) && $role == 'broker') {
                        if (isset($contact_array['email']) && strlen($contact_array['email'])>0) {
                            ?>
                            <br/>
                            <button data-target="#kontakt_aufnehmen" data-toggle="modal" class="btn btn-u anfrage_modal">Anfrage
                                senden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                            <br/>
                            <?php
                        }
                    }
                    if (isset($logged_in) && $logged_in) {
                        if (isset($role) && $role == 'token_account') {
                            ?>
                            <br/>
                            <button data-target="#beitrag_melden" data-toggle="modal" class="btn btn-u meldungs_modal">Beitrag
                                melden&nbsp;<i class="fa fa-exclamation-circle"></i></button>
                            <br/>
                            <?php
                        }
                    }
                    } ?>
                </div>
            <?php
            if (isset($related_donations)) {
            ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 related_donations" style="padding-left:40px">
                    <div class="row">
                        <div class="headline" style="text-align:right"><h2 style="font-size:16px">Verwandte Spenden</h2></div>
                        <?php
                        foreach ($related_donations as $related_donation) {
                            ?>
                            <div class="row">
                                <div class="col-xs-4">
                                    <?php
                                    if (isset($related_donation->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $related_donation->file_uploads[0]->src)) {
                                        ?>
                                        <?php
                                        $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/') . $related_donation->file_uploads[0]->src, 'IFD0');
                                        if (!empty($exif_donation_image['Orientation'])) {
                                            switch ($exif_donation_image['Orientation']) {
                                                case 6:
                                                    $rotation = "rotate(90deg)";
                                                    break;
                                                default:
                                                    $rotation = "";
                                                    break;

                                            }
                                        }
                                        ?>
                                        <img src="<?= $related_donation->file_uploads[0]->src ?>"
                                             style="max-width:75px;max-height:75px;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>"/>
                                        <?php
                                    } else {
                                        switch ($related_donation->category) {
                                            case "time":
                                                echo '<img src="/img/time_placeholder.png" style="max-width:75px;max-height:75px" />';
                                                break;
                                            case "thing":
                                                echo '<img src="/img/thing_placeholder.png" style="max-width:75px;max-height:75px" />';
                                                break;
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?= $related_donation->title ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?php
                                            switch ($related_donation->category) {
                                                case "time":
                                                    echo '<a style="float:right;margin-top:10px;" href="/zeitspende/' . $related_donation->url_title . '" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>';
                                                    break;
                                                case "thing":
                                                    echo '<a style="float:right;margin-top:10px;" href="/sachspende/' . $related_donation->url_title . '" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>';
                                                    break;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            <?php
            }
            ?>

            <?php
            if (isset($logged_in) && $logged_in && isset($role) && $role != 'token_account') {
            ?>

                <div class="modal fade" id="kontakt_aufnehmen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true" style="display: none;">
                    <form id="anfrage_senden_form" method="post" action="/anfrage_senden">
                        <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                        <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                        <?= $this->Form->hidden(__('referer'), ['value' => $_SERVER['HTTP_REFERER']]) ?>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Neue Anfrage</h4>
                                </div>
                                <div class="modal-body">
                                    <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                                    <br/>
                                    <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->email ? $current_user->email : ""]) ?>
                                    <br/>
                                    <?php
                                    if (isset($contact_without_mail) && $contact_without_mail) {
                                        ?>
                                        <label>Deine Telefonnummer</label>
                                        <?= $this->Form->input(__('sender_number'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                                        <?= $this->Form->hidden(__('message')) ?>
                                        <?php
                                    } else {
                                        if ($donation->user->has('phone') &&
                                            strlen($donation->user->phone) > 0 &&
                                            isset($logged) && $logged == true
                                        ) {
                                            ?>
                                            <label>Telefonnummer</label><br/>
                                            <span class="label label-default" style="font-size:18px"><a
                                                    href="tel:<?= $donation->user->phone ?>"><i
                                                        class="fa fa-phone"></i>&nbsp;<?= $donation->user->phone ?></a></span><br/>
                                            <br/>
                                            <?php
                                        }
                                        ?>
                                        <label>Nachricht</label>
                                        <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?= $this->Form->hidden(__('action'), ['value' => $this->request->params['action']]) ?>
                                <div class="modal-footer">
                                    <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                                    <button data-url="/anfrage_senden/<?= $donation->url_title ?>" type="button"
                                            class="btn-u btn-u-primary anfrage_senden">Spender kontaktieren&nbsp;<i
                                            class="fa fa-angle-double-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <script type="text/javascript" src="/js/anfrage_helper.js"></script>

            <?php
            } else if (isset($logged_in) && $logged_in && isset($role) && $role == 'token_account') {
            ?>

                <div class="modal fade" id="beitrag_melden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true" style="display: none;">
                    <form id="meldung_senden_form">
                        <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                        <?= $this->Form->hidden(__('current_action'), ['value' => 'time']) ?>
                        <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                        <?= $this->Form->hidden(__('referer'), ['value' => $_SERVER['HTTP_REFERER']]) ?>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Beitrag melden</h4>
                                </div>
                                <div class="modal-body">
                                    <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                                    <br/>
                                    <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->email ? $current_user->email : ""]) ?>
                                    <br/>
                                    <label>Grund für die Meldung</label>
                                    <?= $this->Form->textarea(__('reason'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                                </div>
                                <?= $this->Form->hidden(__('action'), ['value' => $this->request->params['action']]) ?>
                                <div class="modal-footer">
                                    <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                                    <button data-url="/beitrag_melden/<?= $donation->url_title ?>" type="button" class="btn-u btn-u-primary meldung_senden">Beitrag melden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <script type="text/javascript" src="/js/meldung_helper.js"></script>

            <?php
            }
            ?>

                <div class="modal fade" id="interesse_spende" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Interesse an dieser Spende</h4>
                            </div>
                            <div class="modal-body">
                                <div class="tab-v1">
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="check">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p>
                                                        Hier kannst Du dem Spendenvermittler in Deiner Nähe bescheid sagen, dass du diese Spende gebrauchen kannst. Er kontaktiert dann den Spender für Dich.
                                                    </p>
                                                    <br/>
                                                    <form id="interesse_ueberpruefen" action="/BHmCsp49JS2S7Daw">
                                                        <label>Gebe die Chiffre des Spendenvermittlers ein:</label>
                                                        <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                                                        <?= $this->Form->input(__('code'), ['label' => false, 'class' => 'form-control']) ?>
                                                        <br/>
                                                        <a href="#pin_anfragen" class="pin_anfragen_modal" data-toggle="modal">Hier kannst du die Chiffre anfragen&nbsp;<i class="fa fa-angle-double-right"></i></a>
                                                        <br/>
                                                        <br/>
                                                        <?= $this->Form->button(__('Überprüfen'), ['class' => 'btn btn-u code_ueberpruefen']) ?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="message">
                                            <form id="pin_anfragen_form" method="post" action="/interesse_senden">
                                                <?= $this->Form->hidden(__('broker_id')) ?>
                                                <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                                                <?= $this->Form->input(__('name'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                                                <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                                                <?= $this->Form->hidden(__('referer'), ['value' => isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0 ? $_SERVER['HTTP_REFERER'] : '']) ?>
                                                <br/>
                                                <label>Deine Nachricht</label>
                                                <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                                                <br/>
                                                <hr>
                                                <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control']) ?>
                                                <?= $this->Form->input(__('phone'), ['label' => 'Deine Telefonnummer', 'class' => 'form-control']) ?>
                                                <br/>
                                                <div class="g-recaptcha" data-sitekey="XXXX"></div>
                                                <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                                                <button type="button" class="btn-u btn-u-primary interesse_senden">Absenden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript" src="/js/interesse_helper.js"></script>

                <div class="modal fade" id="pin_anfragen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Chiffre des Vermittlers anfragen</h4>
                            </div>
                            <div class="modal-body">
                                <form id="pin_anfragen_form" method="post" action="/pin_anfragen">
                                    <?= $this->Form->hidden(__('donation'), ['value' => base64_encode($donation->id)]) ?>
                                    <?= $this->Form->input(__('name'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                                    <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
                                    <?= $this->Form->hidden(__('referer'), ['value' => isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0 ? $_SERVER['HTTP_REFERER'] : '']) ?>
                                    <br/>
                                    <label>Deine Nachricht</label>
                                    <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                                    <br/>
                                    <hr>
                                    <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control']) ?>
                                    <br/>
                                    <?= $this->Form->input(__('zip'), ['label' => 'Deine Postleitzahl', 'class' => 'form-control']) ?>
                                    <br/>
                                    <?= $this->Form->input(__('additional'), ['label' => 'Einrichtung / Institution / Gruppe', 'class' => 'form-control']) ?>
                                    <br/>
                                    <?php
                                    if (isset($logged_in) && $logged_in) {
                                        // no captcha
                                    } else { ?>
                                        <div class="g-recaptcha" data-sitekey="XXXX"></div>
                                        <?php
                                    }
                                    ?>
                                    <br/>
                                    <br/>
                                    <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                                    <button type="button" class="btn-u btn-u-primary pin_anfragen">Absenden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript" src="/js/pin_anfragen_helper.js"></script>

                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        App.initFancybox();
                    });
                </script>
                <?php
                break;
        }
        break;
    case "search":
        switch ($case) {
            case "sach":
                ?>
                <link rel="stylesheet" href="/plugins/fancybox/source/jquery.fancybox.css">
<script type="text/javascript" src="/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
    <div class="row"><?= $this->element('searches_help_sidebar') ?></div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="row">
        <a href="<?php
        if (isset($referer_for_button) && strlen($referer_for_button)>0) {
            echo $referer_for_button;
        } else if (isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0) {
            echo $_SERVER['HTTP_REFERER'];
        } else {
            echo "/sachgesuche";
        } ?>" class="btn btn-u"><i class="fa fa-angle-double-left"></i>&nbsp;Zurück</a>
    </div>
    <br/>
    <div class="row">
        <div class="headline"><h2 style="font-size:17px"><?= $search->title ?></h2></div>
    </div>
    <div class="row" style="padding-top:10px;padding-bottom:10px">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?php
            if (isset($search_image) && file_exists(rtrim(WWW_ROOT, '/').$search_image->src)) { ?>
                <a class="thumbnail fancybox-button zoomer" data-rel="fancybox-button" title="<?= $search->title ?>" href="<?= $search_image->src ?>">
                    <?php
                    $first_rotation = "";
                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$search_image->src, 'IFD0');
                    if (!empty($exif_donation_image['Orientation'])){
                        switch($exif_donation_image['Orientation']) {
                            case 6:
                                $first_rotation = "rotate(90deg)";
                                break;
                        }
                    }
                    ?>
                <span class="overlay-zoom" style="transform:<?= $first_rotation ?>;-moz-transform:<?= $first_rotation ?>;-webkit-transform:<?= $first_rotation; ?>">
                    <img class="img-responsive" src="<?= $search_image->src ?>" style="max-width:75px;max-height:75px" />
                    <span class="zoom-icon"></span>
                </span>
                </a>

                <?php
            } else {
                ?>
                <img src="/img/thing_placeholder_search.png" style="max-width:75px;max-height:75px" />
                <?php
            }
            ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <p><?= $search->description ?></p>
        </div>
    </div>
    <?php
    if (isset($search_images) && $search_images) {
        ?>
        <div class="row">
            <div class="headline"><h2 style="font-size:16px"><?= __('Weitere Bilder') ?></h2></div>
        </div>
        <div class="row">
            <div id="myCarousel" class="carousel slide carousel-v1" style="height:350px">
                <div class="carousel-inner" style="height:350px">
                    <?php
                    foreach ($search_images as $key => $image) {
                    if ($key == 0) {
                        continue;
                    } else if ($key == 1) {
                    ?>
                    <div class="item active">
                        <?php
                        } else {
                        ?>
                        <div class="item">
                            <?php
                            }
                            ?>
                            <?php
                            $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$image->src, 'IFD0');
                            if (!empty($exif_donation_image['Orientation'])){
                                switch($exif_donation_image['Orientation']) {
                                    case 6:
                                        $rotation = "rotate(90deg)";
                                        break;
                                    default:
                                        $rotation = "";
                                        break;

                                }
                            }
                            ?>
                            <img src="<?= $image->src ?>" style="max-width:100%;max-height:350px;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>" />
                        </div>
                        <?php
                        }
                        ?>
                    </div>

                    <div class="carousel-arrow">
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
        </div>
        <?php
    }
    ?>
    <div class="row">
        <?php
        if (isset($contact_array)) {
            ?>
            <div class="headline"><h2 style="font-size:16px">Kontaktdaten</h2></div>
            <?php
        }
        ?>
        <p><?php
            if (isset($contact_array) && isset($contact_array['firstname'])) { ?>
                <?= $contact_array['firstname'] . " " . $contact_array['lastname'] ?>
                <br/>
                <?= $contact_array['street'] . ", " . $contact_array['zip'] . " " . $contact_array['city'] ?>
                <?php
            } else if (isset($contact_array['zip']) && $contact_array['zip']!='') { ?>
                <?= $contact_array['zip'] . " " . $contact_array['city'] ?>
                <?php
            }
            ?>
        </p>
    </div>
    <?php
    if (!$own_search) { ?>
        <div class="row">
            <button data-target="#kontakt_aufnehmen" data-toggle="modal" class="btn btn-u anfrage_modal">Spende anbieten&nbsp;<i class="fa fa-angle-double-right"></i></button>
            <br/>
            <?php
            if (isset($logged_in) && $logged_in && isset($role) && $role == 'token_account') {
                ?>
                <br/>
                <button data-target="#beitrag_melden" data-toggle="modal" class="btn btn-u meldungs_modal">Beitrag melden&nbsp;<i class="fa fa-exclamation-circle"></i></button>
                <?php
            }
            ?>
        </div>
    <?php } ?>
</div>
    <?php
    if (isset($related_searches)) {
        ?>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 related_searches" style="padding-left:40px">
            <div class="row">
                <div class="headline" style="text-align:right"><h2 style="font-size:16px">Verwandte Gesuche</h2></div>
                <?php
                foreach ($related_searches as $related_search) {
                    ?>
                    <div class="row">
                        <div class="col-xs-5">
                            <?php
                            if (isset($related_search->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $related_search->file_uploads[0]->src)) {
                                ?>
                                <?php
                                $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/') . $related_search->file_uploads[0]->src, 'IFD0');
                                if (!empty($exif_donation_image['Orientation'])) {
                                    switch ($exif_donation_image['Orientation']) {
                                        case 6:
                                            $rotation = "rotate(90deg)";
                                            break;
                                        default:
                                            $rotation = "";
                                            break;

                                    }
                                }
                                ?>
                                <img src="<?= $related_search->file_uploads[0]->src ?>"
                                     style="max-width:75px;max-height:75px;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>"/>
                                <?php
                            } else {
                                switch ($related_search->category) {
                                    case "time":
                                        echo '<img src="/img/time_placeholder.png" style="max-width:100%;max-height:100%" />';
                                        break;
                                    case "thing":
                                        echo '<img src="/img/thing_placeholder.png" style="max-width:100%;max-height:100%" />';
                                        break;
                                }
                            }
                            ?>
                        </div>
                        <div class="col-xs-7">
                            <div class="row">
                                <?= $related_search->title ?>
                            </div>
                            <div class="row">
                                <?php
                                switch ($related_search->category) {
                                    case "time":
                                        echo '<a style="float:right;margin-top:10px;" href="/zeitgesuch/' . $related_search->url_title . '" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>';
                                        break;
                                    case "thing":
                                        echo '<a style="float:right;margin-top:10px;" href="/sachgesuch/' . $related_search->url_title . '" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>';
                                        break;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <?php
                }

                ?>
            </div>
        </div>
        <?php
    }
    ?>

<div class="modal fade" id="alternativ_auswahl" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Bitte treffen Sie eine Auswahl</h4>
            </div>
            <div class="modal-body">
                <button type="button" data-url="/vorbereiten/<?= $search->id ?>" class="btn-u btn-u-default wahl_spende" data-dismiss="modal">Ich möchte eine Spende dazu anlegen&nbsp;<i class="fa fa-angle-double-right"></i></button>
                <br/>
                <br/>
                <button type="button" class="btn-u btn-u-primary wahl_anfrage">Ich möchte eine Mitteilung an den Vermittler senden&nbsp;<i class="fa fa-angle-double-right"></i></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/alternativ_helper.js"></script>

<div class="modal fade" id="kontakt_aufnehmen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form id="anfrage_senden_form" method="post" action="/anfrage_senden">
        <?= $this->Form->hidden(__('search'), ['value' => base64_encode($search->id)]) ?>
        <?= $this->Form->hidden(__('current_action'), ['value' => 'thing']) ?>
        <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
        <?= $this->Form->hidden(__('referer'), ['value' => isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0 ? $_SERVER['HTTP_REFERER'] : '']) ?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Neues Angebot</h4>
                </div>
                <div class="modal-body">
                    <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true]) ?>
                    <br/>
                    <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true]) ?>
                    <br/>
                    <?php
                    if (isset($contact_without_mail) && $contact_without_mail) {
                        ?>
                        <label>Deine Telefonnummer</label>
                        <?= $this->Form->input(__('sender_number'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                        <?= $this->Form->hidden(__('message')) ?>
                        <?php
                    } else {
                        if ($search->user->has('phone') &&
                            strlen($search->user->phone)>0 &&
                            isset($logged) && $logged == true) {
                            ?>
                            <label>Telefonnummer</label><br/>
                            <span class="label label-default" style="font-size:18px"><a href="tel:<?= $search->user->phone ?>"><i class="fa fa-phone"></i>&nbsp;<?= $search->user->phone ?></a></span><br/><br/>
                            <?php
                        }
                        ?>
                        <?php
                    }
                    ?>
                    <label>Nachricht</label>
                    <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                    <button type="button" class="btn-u btn-u-primary anfrage_senden_submit">Spende anbieten&nbsp;<i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="/js/anfrage_helper.js"></script>

<?php
if (isset($logged_in) && $logged_in) {
    ?>

    <div class="modal fade" id="beitrag_melden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <form id="meldung_senden_form">
            <?= $this->Form->hidden(__('search'), ['value' => base64_encode($search->id)]) ?>
            <?= $this->Form->hidden(__('current_action'), ['value' => 'thing']) ?>
            <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
            <?= $this->Form->hidden(__('referer'), ['value' => $_SERVER['HTTP_REFERER']]) ?>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Beitrag melden</h4>
                    </div>
                    <div class="modal-body">
                        <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                        <br/>
                        <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->email ? $current_user->email : ""]) ?>
                        <br/>
                        <label>Grund für die Meldung</label>
                        <?= $this->Form->textarea(__('reason'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                        <button data-url="/beitrag_melden/<?= $search->url_title ?>" type="button" class="btn-u btn-u-primary meldung_senden">Beitrag melden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript" src="/js/meldung_helper.js"></script>

    <?php
}
?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        App.initFancybox();
    });
</script>

                <?php
                break;
            case "zeit":
                ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
    <div class="row"><?= $this->element('searches_help_sidebar') ?></div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="row">
        <a href="<?php
        if (isset($referer_for_button) && strlen($referer_for_button)>0) {
            echo $referer_for_button;
        } else if (isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0) {
            echo $_SERVER['HTTP_REFERER'];
        } else {
            echo "/zeitspenden";
        } ?>" class="btn btn-u"><i class="fa fa-angle-double-left"></i>&nbsp;Zurück</a>
    </div>
    <br/>
    <div class="row">
        <div class="headline"><h2 style="font-size:17px"><?= $search->title ?></h2></div>
    </div>
    <div class="row" style="padding-top:10px;padding-bottom:10px">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?php
            if (isset($search->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/').$search->file_uploads[0]->src)) { ?>
                <a class="thumbnail fancybox-button zoomer" data-rel="fancybox-button" title="<?= $search->title ?>" href="<?= $search->file_uploads[0]->src ?>">
                    <?php
                    $first_rotation = "";
                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$search->file_uploads[0]->src, 'IFD0');
                    if (!empty($exif_donation_image['Orientation'])){
                        switch($exif_donation_image['Orientation']) {
                            case 6:
                                $first_rotation = "rotate(90deg)";
                                break;
                        }
                    }
                    ?>
                <span class="overlay-zoom" style="transform:<?= $first_rotation ?>;-moz-transform:<?= $first_rotation ?>;-webkit-transform:<?= $first_rotation; ?>">
                    <img class="img-responsive" src="<?= $search->file_uploads[0]->src ?>" style="max-width:100%;max-height:100%" />
                    <span class="zoom-icon"></span>
                </span>
                </a>                <?php
            } else {
                ?>
                <img src="/img/time_placeholder_search.png" style="max-width:100%;max-height:100%" />
                <?php
            }
            ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class="row">
                <p><?= $search->description ?></p>
            </div>
            <?php
            if (isset($search->time_searches) &&
                isset($search->time_searches[0]) &&
                (isset($search->time_searches[0]->start_date) &&
                $search->time_searches[0]->start_date) ||
                (isset($search->time_searches[0]->end_date) &&
                $search->time_searches[0]->end_date)) {
                ?>
                <div class="row" style="margin-bottom:-20px">
                    <div class="headline"><h2 style="font-size:16px"><?= __('Zeitangaben') ?></h2></div>
                </div>
                <div class="row">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Beginn</th>
                            <th>Ende</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($search->time_searches as $time_search) {
                            ?>
                            <tr>
                                <td><?= $time_search->start_date->i18nFormat('dd.MM.yyyy') . " - " . $time_search->start_time->i18nFormat('HH:mm') ?></td>
                                <td><?= $time_search->end_date->i18nFormat('dd.MM.yyyy') . " - " . $time_search->end_time->i18nFormat('HH:mm') ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
    if (isset($search->file_uploads[1]) && $search->file_uploads[1]) {
        ?>
        <div class="row">
            <div class="headline"><h2 style="font-size:16px"><?= __('Weitere Bilder') ?></h2></div>
        </div>
        <div class="row">
            <?php
            foreach ($search->file_uploads as $key => $image) {
                if ($key == 0) {
                    continue;
                }
                ?>
                <?php
                $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$image->src, 'IFD0');
                if (!empty($exif_donation_image['Orientation'])){
                    switch($exif_donation_image['Orientation']) {
                        case 6:
                            $rotation = "rotate(90deg)";
                            break;
                        default:
                            $rotation = "";
                            break;

                    }
                }
                ?>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <img src="<?= $image->src ?>" style="max-width:100%;max-height:350px;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>" />
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
    ?>
    <div class="row">
        <?php
        if (isset($contact_array)) {
            ?>
            <div class="headline"><h2 style="font-size:16px">Kontaktdaten</h2></div>
            <?php
        }
        ?>
        <p><?php
            if (isset($contact_array) && isset($contact_array['firstname'])) { ?>
                <?= $contact_array['firstname'] . " " . $contact_array['lastname'] ?>
                <br/>
                <?= $contact_array['street'] . ", " . $contact_array['zip'] . " " . $contact_array['city'] ?>
                <?php
            } else if (isset($contact_array['zip']) && $contact_array['zip']!='') { ?>
                <?= $contact_array['zip'] . " " . $contact_array['city'] ?>
                <?php
            }
            ?>
        </p>
    </div>
    <?php
    if (!$own_search) { ?>
        <div class="row">
            <button data-target="#kontakt_aufnehmen" data-toggle="modal" class="btn btn-u anfrage_modal">Spende anbieten&nbsp;<i class="fa fa-angle-double-right"></i></button>
            <br/>
            <?php
            if (isset($logged_in) && $logged_in && isset($role) && $role == 'token_account') {
                ?>
                <br/>
                <button data-target="#beitrag_melden" data-toggle="modal" class="btn btn-u meldungs_modal">Beitrag melden&nbsp;<i class="fa fa-exclamation-circle"></i></button>
                <?php
            }
            ?>
        </div>
    <?php } ?>
</div>
<?php
if (isset($related_searches)) {

    ?>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 related_searches" style="padding-left:40px">
        <div class="row">
            <div class="headline" style="text-align:right"><h2 style="font-size:16px">Verwandte Spenden</h2></div>
            <?php
            foreach ($related_searches as $related_search) {
                ?>
                <div class="row">
                    <div class="col-xs-4">
                        <?php
                        if (isset($related_search->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $related_search->file_uploads[0]->src)) {
                            ?>
                            <?php
                            $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/') . $related_search->file_uploads[0]->src, 'IFD0');
                            if (!empty($exif_donation_image['Orientation'])) {
                                switch ($exif_donation_image['Orientation']) {
                                    case 6:
                                        $rotation = "rotate(90deg)";
                                        break;
                                    default:
                                        $rotation = "";
                                        break;

                                }
                            }
                            ?>
                            <img src="<?= $related_search->file_uploads[0]->src ?>"
                                 style="max-width:75px;max-height:75px;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>"/>
                            <?php
                        } else {
                            switch ($related_search->category) {
                                case "time":
                                    echo '<img src="/img/time_placeholder.png" style="max-width:100%;max-height:100%" />';
                                    break;
                                case "thing":
                                    echo '<img src="/img/thing_placeholder.png" style="max-width:100%;max-height:100%" />';
                                    break;
                            }
                        }
                        ?>
                    </div>
                    <div class="col-xs-8">
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $related_search->title ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                switch ($related_search->category) {
                                    case "time":
                                        echo '<a style="float:right;margin-top:10px;" href="/zeitgesuch/' . $related_search->url_title . '" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>';
                                        break;
                                    case "thing":
                                        echo '<a style="float:right;margin-top:10px;" href="/sachgesuch/' . $related_search->url_title . '" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>';
                                        break;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}
?>

<div class="modal fade" id="kontakt_aufnehmen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form id="anfrage_senden_form" method="post" action="/anfrage_senden">
        <?= $this->Form->hidden(__('search'), ['value' => base64_encode($search->id)]) ?>
        <?= $this->Form->hidden(__('current_action'), ['value' => 'time']) ?>
        <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
        <?= $this->Form->hidden(__('referer'), ['value' => isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER'])>0 ? $_SERVER['HTTP_REFERER'] : '']) ?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Neues Angebot</h4>
                </div>
                <div class="modal-body">
                    <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                    <br/>
                    <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->email ? $current_user->email : ""]) ?>
                    <br/>
                    <?php
                    if (isset($contact_without_mail) && $contact_without_mail) {
                        ?>
                        <label>Deine Telefonnummer</label>
                        <?= $this->Form->input(__('sender_number'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                        <?= $this->Form->hidden(__('message')) ?>
                        <?php
                    } else {
                        if ($search->user->has('phone') &&
                            strlen($search->user->phone)>0 &&
                            isset($logged) && $logged == true) {
                            ?>
                            <label>Telefonnummer</label><br/>
                            <span class="label label-default" style="font-size:18px"><a href="tel:<?= $search->user->phone ?>"><i class="fa fa-phone"></i>&nbsp;<?= $search->user->phone ?></a></span><br/><br/>
                            <?php
                        }
                        ?>

                        <?php
                    }
                    ?>
                    <label>Nachricht</label>
                    <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                    <button data-url="/anfrage_senden/<?= $search->url_title ?>" type="button" class="btn-u btn-u-primary anfrage_senden_submit">Spende anbieten&nbsp;<i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="/js/anfrage_helper.js"></script>

<?php
if (isset($logged_in) && $logged_in) {
    ?>

    <div class="modal fade" id="beitrag_melden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <form id="meldung_senden_form">
            <?= $this->Form->hidden(__('search'), ['value' => base64_encode($search->id)]) ?>
            <?= $this->Form->hidden(__('current_action'), ['value' => 'time']) ?>
            <?= $this->Form->hidden(__('cu'), ['value' => $_SERVER['REQUEST_URI']]) ?>
            <?= $this->Form->hidden(__('referer'), ['value' => $_SERVER['HTTP_REFERER']]) ?>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Beitrag melden</h4>
                    </div>
                    <div class="modal-body">
                        <?= $this->Form->input(__('sender'), ['label' => 'Dein Name', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->firstname ? $current_user->firstname . " " . $current_user->lastname : ""]) ?>
                        <br/>
                        <?= $this->Form->input(__('email'), ['label' => 'Deine E-Mail-Adresse', 'class' => 'form-control', 'required' => true, 'value' => isset($current_user) && $current_user->email ? $current_user->email : ""]) ?>
                        <br/>
                        <label>Grund für die Meldung</label>
                        <?= $this->Form->textarea(__('reason'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                        <button data-url="/beitrag_melden/<?= $search->url_title ?>" type="button" class="btn-u btn-u-primary meldung_senden">Beitrag melden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript" src="/js/meldung_helper.js"></script>

    <?php
}
?>
                <?php
                break;
        }
        break;

}
