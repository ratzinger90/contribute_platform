<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-right:30px;border-right:1px solid #ddd">
<?php
switch ($type) {
    case "donation":
        echo $this->element('donations_sidebar');
        ?>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
        <div id="content">
            <?php
        switch($case) {
            case "sach":
            $counter = 0;
            $start_id = $end_id = 0;
            foreach ($items as $donation_thing) {
                if ($counter == 0) {
                    $end_id = $donation_thing->id;
                    $counter += 1;
                }
                ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 donation_things">
                    <div class="row">
                        <div class="headline">
                            <a href="/sachspende/<?= $donation_thing->url_title ?>"><h2 style="font-size:16px"><?= $donation_thing->title ?></h2></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="overflow:hidden">
                            <?php
                            if (isset($donation_thing->file_uploads[0])) {
                                if ($donation_thing->file_uploads[0]->src && file_exists(rtrim(WWW_ROOT, '/').$donation_thing->file_uploads[0]->src)) {
                                    ?>
                                    <?php
                                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$donation_thing->file_uploads[0]->src, 'IFD0');
                                    if (!empty($exif_donation_image['Orientation'])){
                                        switch($exif_donation_image['Orientation']) {
                                            case 6:
                                                $rotation = "rotate(90deg)";
                                                break;
                                            default:
                                                $rotation = "";
                                                break;

                                        }
                                    }
                                    ?>
                                    <a href="/sachspende/<?= $donation_thing->url_title ?>"><img src="<?= $donation_thing->file_uploads[0]->src ?>"
                                                                                                 style="width:auto;max-width:100%;height:auto;max-height:100%;<?php if (isset($rotation) && strlen($rotation)>0) { ?>transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?><?php } ?>"/></a>
                                    <?php
                                }
                            } else {
                                ?>
                                <a href="/sachspende/<?= $donation_thing->url_title ?>"><img src="/img/thing_placeholder.png" style="max-width:100%;max-height:100%" /></a>
                                <?php
                            }

                            ?>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="row donations_description"><?= $donation_thing->description ?></div>
                            <div class="row" style="text-align:right"><a href="/sachspende/<?= $donation_thing->url_title ?>" class="btn btn-u donations_details_anchor">Details&nbsp;<i class="fa fa-angle-double-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <?php
                $start_id = $donation_thing->id;
            }
            ?>
        </div>
                <?php
                break;
            case "zeit":
                $counter = 0;
                $start_id = $end_id = 0;
                foreach ($items as $donation_time) {
                    if ($counter == 0) {
                        $end_id = $donation_time->id;
                        $counter += 1;
                    }
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 donation_times">
                        <div class="row">
                            <div class="headline">
                                <a href="/zeitspende/<?= $donation_time->url_title ?>"><h2><?= $donation_time->title ?></h2></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="overflow:hidden">
                                <?php
                                if (isset($donation_time->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $donation_time->file_uploads[0]->src)) {
                                    ?>
                                    <?php
                                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$donation_time->file_uploads[0]->src, 'IFD0');
                                    if (!empty($exif_donation_image['Orientation'])){
                                        switch($exif_donation_image['Orientation']) {
                                            case 6:
                                                $rotation = "rotate(90deg)";
                                                break;
                                            default:
                                                $rotation = "";
                                                break;

                                        }
                                    }
                                    ?>
                                    <a href="/zeitspende/<?= $donation_time->url_title ?>"><img src="<?= $donation_time->file_uploads[0]->src ?>" style="max-width:100%;max-height:auto;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>" /></a>
                                    <?php
                                } else {
                                    ?>
                                    <a href="/zeitspende/<?= $donation_time->url_title ?>"><img src="/img/time_placeholder.png" style="max-width:100%;max-height:100%"/></a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 donations_description">
                                <?= $donation_time->description ?>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <table class="table">
                                    <thead><tr><th>Anfang</th><th>Ende</th></tr></thead>
                                    <?php
                                    foreach ($donation_time->time_donations as $key => $time_donation) {
                                        echo '<tr><td>';
                                        echo $time_donation->start_date->i18nFormat('dd.MM.yyyy');
                                        echo ' - ';
                                        echo $time_donation->start_time->i18nFormat('hh:mm');
                                        echo '</td><td>';
                                        echo $time_donation->end_date->i18nFormat('dd.MM.yyyy');
                                        echo ' - ';
                                        echo $time_donation->end_time->i18nFormat('hh:mm');
                                        echo '</td></tr>';
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="text-align:right">
                            <a href="/zeitspende/<?= $donation_time->url_title ?>" class="btn btn-u donations_details_anchor">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <?php
                    $start_id = $donation_time->id;
                }
                ?>
    </div>
                <?php
                break;
            case "alle":
                $counter = 0;
                $start_id = $end_id = 0;
                foreach ($items as $donation) {
                    if ($counter == 0) {
                        $end_id = $donation->id;
                        $counter += 1;
                    }
                    if ($donation->category == 'time') {
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 donations">
                            <div class="row">
                                <div class="headline">
                                    <a href="/zeitspende/<?= $donation->url_title ?>"><h2><?= $donation->title ?></h2></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="overflow:hidden">
                                    <?php
                                    if (isset($donation->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $donation->file_uploads[0]->src)) {
                                        ?>
                                        <?php
                                        $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/') . $donation->file_uploads[0]->src, 'IFD0');
                                        if (!empty($exif_donation_image['Orientation'])) {
                                            switch ($exif_donation_image['Orientation']) {
                                                case 6:
                                                    $rotation = "rotate(90deg)";
                                                    break;
                                                default:
                                                    $rotation = "";
                                                    break;

                                            }
                                        }
                                        ?>
                                        <a href="/zeitspende/<?= $donation->url_title ?>"><img
                                                    src="<?= $donation->file_uploads[0]->src ?>"
                                                    style="max-width:100%;max-height:auto;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>"/></a>
                                        <?php
                                    } else {
                                        ?>
                                        <a href="/zeitspende/<?= $donation->url_title ?>"><img
                                                    src="/img/time_placeholder.png" style="max-width:100%;max-height:100%"/></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 donations_description">
                                    <?= $donation->description ?>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Anfang</th>
                                            <th>Ende</th>
                                        </tr>
                                        </thead>
                                        <?php
                                        foreach ($donation->time_donations as $key => $time_donation) {
                                            echo '<tr><td>';
                                            echo $time_donation->start_date->i18nFormat('dd.MM.yyyy');
                                            echo ' - ';
                                            echo $time_donation->start_time->i18nFormat('hh:mm');
                                            echo '</td><td>';
                                            echo $time_donation->end_date->i18nFormat('dd.MM.yyyy');
                                            echo ' - ';
                                            echo $time_donation->end_time->i18nFormat('hh:mm');
                                            echo '</td></tr>';
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                            <div class="row" style="text-align:right">
                                <a href="/zeitspende/<?= $donation->url_title ?>"
                                   class="btn btn-u donations_details_anchor">Details&nbsp;<i
                                            class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 donations">
                            <div class="row">
                                <div class="headline">
                                    <a href="/sachspende/<?= $donation->url_title ?>"><h2 style="font-size:16px"><?= $donation->title ?></h2></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="overflow:hidden">
                                    <?php
                                    if (isset($donation->file_uploads[0])) {
                                        if ($donation->file_uploads[0]->src && file_exists(rtrim(WWW_ROOT, '/').$donation->file_uploads[0]->src)) {
                                            ?>
                                            <?php
                                            $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$donation->file_uploads[0]->src, 'IFD0');
                                            if (!empty($exif_donation_image['Orientation'])){
                                                switch($exif_donation_image['Orientation']) {
                                                    case 6:
                                                        $rotation = "rotate(90deg)";
                                                        break;
                                                    default:
                                                        $rotation = "";
                                                        break;

                                                }
                                            }
                                            ?>
                                            <a href="/sachspende/<?= $donation->url_title ?>"><img src="<?= $donation->file_uploads[0]->src ?>"
                                                                                                   style="width:auto;max-width:100%;height:auto;max-height:100%;<?php if (isset($rotation) && strlen($rotation)>0) { ?>transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?><?php } ?>"/></a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <a href="/sachspende/<?= $donation->url_title ?>"><img src="/img/thing_placeholder.png" style="max-width:100%;max-height:100%" /></a>
                                        <?php
                                    }

                                    ?>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div class="row donations_description"><?= $donation->description ?></div>
                                    <div class="row" style="text-align:right"><a href="/sachspende/<?= $donation->url_title ?>" class="btn btn-u donations_details_anchor">Details&nbsp;<i class="fa fa-angle-double-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    $start_id = $donation->id;
                }
                ?>
</div>
<?php
                break;
        }
        break;
    case "search":
        echo $this->element('searches_sidebar');
        ?>
    </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div id="content">
        <?php
        switch($case) {
            case "sach":
                $counter = 0;
                $start_id = $end_id = 0;
                foreach ($items as $search_thing) {
                    if ($counter == 0) {
                        $end_id = $search_thing->id;
                        $counter += 1;
                    }
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_things">
                        <div class="row">
                            <div class="headline">
                                <a href="/sachgesuch/<?= $search_thing->url_title ?>"><h2><?= $search_thing->title ?></h2></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="overflow:hidden">
                                <?php
                                if (isset($search_thing->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/').$search_thing->file_uploads[0]->src)) {
                                    ?>
                                    <?php
                                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$search_thing->file_uploads[0]->src, 'IFD0');
                                    if (!empty($exif_donation_image['Orientation'])){
                                        switch($exif_donation_image['Orientation']) {
                                            case 6:
                                                $rotation = "rotate(90deg)";
                                                break;
                                            default:
                                                $rotation = "";
                                                break;

                                        }
                                    }
                                    ?>
                                    <a href="/sachgesuch/<?= $search_thing->url_title ?>"><img src="<?= $search_thing->file_uploads[0]->src ?>" style="max-width:100%; max-height:100%;<?php if (isset($rotation) && strlen($rotation)>0) { ?>transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?><?php } ?>" /></a>
                                    <?php
                                } else {
                                    ?>
                                    <a href="/sachgesuch/<?= $search_thing->url_title ?>"><img src="/img/thing_placeholder_search.png" style="max-width:100%;max-height:100%" /></a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 searches_description">
                                <?= $search_thing->description ?>
                            </div>
                        </div>
                        <div class="row" style="text-align:right">
                            <a href="/sachgesuch/<?= $search_thing->url_title ?>" class="btn btn-u searches_details_anchor">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <?php
                    $start_id = $search_thing->id;
                }
                ?>
            </div>
                <?php
                break;
            case "zeit":
                $counter = 0;
                $start_id = $end_id = 0;
                foreach ($items as $search_time) {
                    if ($counter == 0) {
                        $end_id = $search_time->id;
                        $counter += 1;
                    }
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_times">
                        <div class="row">
                            <div class="headline">
                                <a href="/zeitgesuch/<?= $search_time->url_title ?>"><h2>
                                        <?php if ($search_time->project) { ?>
                                            <span class="label label-red" style="vertical-align: middle;background-color:#009D00">Projekt</span>
                                        <?php } ?>

                                        <?= $search_time->title ?>
                                    </h2></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="overflow:hidden">
                                <?php
                                if ($search_time->file_uploads && file_exists($search_time->file_uploads)) {
                                    ?>
                                    <?php
                                    $exif_donation_image = exif_read_data(rtrim(WWW_ROOT, '/').$search_time->file_uploads[0]->src, 'IFD0');
                                    if (!empty($exif_donation_image['Orientation'])){
                                        switch($exif_donation_image['Orientation']) {
                                            case 6:
                                                $rotation = "rotate(90deg)";
                                                break;
                                            default:
                                                $rotation = "";
                                                break;

                                        }
                                    }
                                    ?>
                                    <a href="/zeitgesuch/<?= $search_time->url_title ?>"><img src="<?= $search_time->file_uploads[0]->src ?>" style="max-width:100%;max-height:100%;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>" /></a>
                                    <?php
                                } else {
                                    ?>
                                    <a href="/zeitgesuch/<?= $search_time->url_title ?>"><img src="/img/time_placeholder_search.png" style="max-width:100%;max-height:100%"/></a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 searches_description">
                                <?= $search_time->description ?>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <table class="table">
                                    <thead><tr><th>Anfang</th><th>Ende</th></tr></thead>
                                    <?php
                                    foreach ($search_time->time_searches as $key => $time_search) {
                                        echo '<tr><td>';
                                        echo $time_search->start_date->i18nFormat('dd.MM.yyyy');
                                        echo ' - ';
                                        echo $time_search->start_time->i18nFormat('hh:mm');
                                        echo '</td><td>';
                                        echo $time_search->end_date->i18nFormat('dd.MM.yyyy');
                                        echo ' - ';
                                        echo $time_search->end_time->i18nFormat('hh:mm');
                                        echo '</td></tr>';
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="text-align:right">
                            <a href="/zeitgesuch/<?= $search_time->url_title ?>" class="btn btn-u searches_details_anchor">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <?php
                    $start_id = $search_time->id;
                }
                ?>
        </div>
                <?php
                break;
            case "alle":
                $counter = 0;
                $start_id = $end_id = 0;
                foreach ($items as $search) {
                    if ($counter == 0) {
                        $end_id = $search->id;
                        $counter += 1;
                    }
                    if ($search->category == 'time') {
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 searches">
                            <div class="row">
                                <div class="headline">
                                    <a href="/zeitgesuch/<?= $search->url_title ?>"><h2><?= $search->title ?></h2></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="overflow:hidden">
                                    <?php
                                    if (isset($search->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $search->file_uploads[0]->src)) {
                                        ?>
                                        <?php
                                        $exif_search_image = exif_read_data(rtrim(WWW_ROOT, '/') . $search->file_uploads[0]->src, 'IFD0');
                                        if (!empty($exif_search_image['Orientation'])) {
                                            switch ($exif_search_image['Orientation']) {
                                                case 6:
                                                    $rotation = "rotate(90deg)";
                                                    break;
                                                default:
                                                    $rotation = "";
                                                    break;

                                            }
                                        }
                                        ?>
                                        <a href="/zeitgesuch/<?= $search->url_title ?>"><img
                                                    src="<?= $search->file_uploads[0]->src ?>"
                                                    style="max-width:100%;max-height:auto;transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?>"/></a>
                                        <?php
                                    } else {
                                        ?>
                                        <a href="/zeitgesuch/<?= $search->url_title ?>"><img
                                                    src="/img/time_placeholder_search.png" style="max-width:100%;max-height:100%"/></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 searches_description">
                                    <?= $search->description ?>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Anfang</th>
                                            <th>Ende</th>
                                        </tr>
                                        </thead>
                                        <?php
                                        foreach ($search->time_searches as $key => $time_search) {
                                            echo '<tr><td>';
                                            echo $time_search->start_date->i18nFormat('dd.MM.yyyy');
                                            echo ' - ';
                                            echo $time_search->start_time->i18nFormat('hh:mm');
                                            echo '</td><td>';
                                            echo $time_search->end_date->i18nFormat('dd.MM.yyyy');
                                            echo ' - ';
                                            echo $time_search->end_time->i18nFormat('hh:mm');
                                            echo '</td></tr>';
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                            <div class="row" style="text-align:right">
                                <a href="/zeitgesuch/<?= $search->url_title ?>"
                                   class="btn btn-u searches_details_anchor">Details&nbsp;<i
                                            class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 searches">
                            <div class="row">
                                <div class="headline">
                                    <a href="/sachgesuch/<?= $search->url_title ?>"><h2 style="font-size:16px"><?= $search->title ?></h2></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="overflow:hidden">
                                    <?php
                                    if (isset($search->file_uploads[0])) {
                                        if ($search->file_uploads[0]->src && file_exists(rtrim(WWW_ROOT, '/').$search->file_uploads[0]->src)) {
                                            ?>
                                            <?php
                                            $exif_search_image = exif_read_data(rtrim(WWW_ROOT, '/').$search->file_uploads[0]->src, 'IFD0');
                                            if (!empty($exif_search_image['Orientation'])){
                                                switch($exif_search_image['Orientation']) {
                                                    case 6:
                                                        $rotation = "rotate(90deg)";
                                                        break;
                                                    default:
                                                        $rotation = "";
                                                        break;

                                                }
                                            }
                                            ?>
                                            <a href="/sachgesuch/<?= $search->url_title ?>"><img src="<?= $search->file_uploads[0]->src ?>"
                                                                                                   style="width:auto;max-width:100%;height:auto;max-height:100%;<?php if (isset($rotation) && strlen($rotation)>0) { ?>transform:<?= $rotation ?>;-moz-transform:<?= $rotation ?>;-webkit-transform:<?= $rotation; ?><?php } ?>"/></a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <a href="/sachgesuch/<?= $search->url_title ?>"><img src="/img/thing_placeholder_search.png" style="max-width:100%;max-height:100%" /></a>
                                        <?php
                                    }

                                    ?>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div class="row searches_description"><?= $search->description ?></div>
                                    <div class="row" style="text-align:right"><a href="/sachgesuch/<?= $search->url_title ?>" class="btn btn-u searches_details_anchor">Details&nbsp;<i class="fa fa-angle-double-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    $start_id = $search->id;
                }
                ?>
                </div>
                <?php
                break;
        }
        break;
}

        if (isset($total_items) && $total_items>0) {
            ?>
            <div id="page-selection">
                <nav>
                    <ul class="pagination">
                        <?php
                        switch($type) {
                            case "donation":
                                switch ($case) {
                                    case "sach":
                                        if (isset($page) && $page > 1) {
                                            ?>
                                            <li>
                                                <a href="/sachspenden/<?= $page - 1 ?>/last/<?= $end_id ?>"><span>&laquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        ?><li class="active"><a><?= $page ?></a></li><?php
                                        if (isset($total_items) && $total_items > 10) {
                                            ?>
                                            <li>
                                                <a href="/sachspenden/<?= $page + 1 ?>/next/<?= $start_id ?>"><span>&raquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        break;
                                    case "zeit":
                                        if (isset($page) && $page > 1) {
                                            ?>
                                            <li>
                                                <a href="/zeitspenden/<?= $page - 1 ?>/last/<?= $end_id ?>"><span>&laquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        ?><li class="active"><a><?= $page ?></a></li><?php
                                        if (isset($total_items) && $total_items > 10) {
                                            ?>
                                            <li>
                                                <a href="/zeitspenden/<?= $page + 1 ?>/next/<?= $start_id ?>"><span>&raquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        break;
                                    case "alle":
                                        if (isset($page) && $page > 1) {
                                            ?>
                                            <li>
                                                <a href="/alle_spenden/<?= $page - 1 ?>/last/<?= $end_id ?>"><span>&laquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        ?><li class="active"><a><?= $page ?></a></li><?php
                                        if (isset($total_items) && $total_items > 10) {
                                            ?>
                                            <li>
                                                <a href="/alle_spenden/<?= $page + 1 ?>/next/<?= $start_id ?>"><span>&raquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        break;
                                }
                                break;
                            case "search":
                                switch ($case) {
                                    case "sach":
                                        if (isset($page) && $page > 1) {
                                            ?>
                                            <li>
                                                <a href="/sachgesuche/<?= $page - 1 ?>/last/<?= $end_id ?>"><span>&laquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        ?><li class="active"><a><?= $page ?></a></li><?php
                                        if (isset($total_items) && $total_items > 10) {
                                            ?>
                                            <li>
                                                <a href="/sachgesuche/<?= $page + 1 ?>/next/<?= $start_id ?>"><span>&raquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        break;
                                    case "zeit":
                                        if (isset($page) && $page > 1) {
                                            ?>
                                            <li>
                                                <a href="/zeitgesuche/<?= $page - 1 ?>/last/<?= $end_id ?>"><span>&laquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        ?><li class="active"><a><?= $page ?></a></li><?php
                                        if (isset($total_items) && $total_items > 10) {
                                            ?>
                                            <li>
                                                <a href="/zeitgesuche/<?= $page + 1 ?>/next/<?= $start_id ?>"><span>&raquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        break;
                                    case "alle":
                                        if (isset($page) && $page > 1) {
                                            ?>
                                            <li>
                                                <a href="/alle_gesuche/<?= $page - 1 ?>/last/<?= $end_id ?>"><span>&laquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        ?><li class="active"><a><?= $page ?></a></li><?php
                                        if (isset($total_items) && $total_items > 10) {
                                            ?>
                                            <li>
                                                <a href="/alle_gesuche/<?= $page + 1 ?>/next/<?= $start_id ?>"><span>&raquo;</span></a>
                                            </li>
                                            <?php
                                        }
                                        break;
                                }
                                break;
                        }
                        ?>
                    </ul>
                </nav>
            </div>
            <?php
        }
        ?>
    </div>
</div>
