<?php
switch($case) {
    case "thing":
        ?>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <?php
                foreach ($searches as $search_thing) {
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="headline">
                                <h2><?= $search_thing->title ?></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <?php
                                if (isset($search_thing->file_uploads[0]) && file_exists(rtrim(WWW_ROOT, '/') . $search_thing->file_uploads[0]->src)) {
                                    ?>
                                    <img src="<?= $search_thing->file_uploads[0]->src ?>" style="max-width:75px;max-height:75px" />
                                    <?php
                                } else {
                                    ?>
                                    <img src="/img/empty.png" style="max-width:75px;max-height:75px" />
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <?= $search_thing->description ?>
                            </div>
                        </div>
                        <div class="row" style="text-align:right">
                            <a href="/sachgesuch/<?= $search_thing->url_title ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <?= $this->element('searches_sidebar') ?>
            </div>
        </div>
        <?php
        break;
    case "time":
        ?>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <?php
                foreach ($searches as $search_time) {
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="headline">
                                <h2>
                                    <?php if ($search_time->project) { ?>
                                        <span class="label label-red" style="vertical-align: middle;background-color:#009D00">Projekt</span>
                                    <?php } ?>

                                    <?= $search_time->title ?>
                                </h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <?php
                                if (isset($search_time->file_uploads[0]) && file_exists(rtrim(WWW_ROOT, '/') . $search_time->file_uploads[0]->src)) {
                                    ?>
                                    <img src="<?= $search_time->file_uploads[0]->src ?>" style="max-width:75px;max-height:75px" />
                                    <?php
                                } else {
                                    ?>
                                    <img src="/img/empty.png" style="max-width:75px;max-width:75px"/>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                <?= $search_time->description ?>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <table class="table">
                                    <thead><tr><th>Anfang</th><th>Ende</th></tr></thead>
                                    <?php
                                    if (isset($search_time->time_searches)) {
                                        foreach ($search_time->time_searches as $key => $time_search) {
                                            echo '<tr><td>';
                                            echo $time_search->start_date->i18nFormat('dd.MM.yyyy');
                                            echo ' - ';
                                            echo $time_search->start_time->i18nFormat('hh:mm');
                                            echo '</td><td>';
                                            echo $time_search->end_date->i18nFormat('dd.MM.yyyy');
                                            echo ' - ';
                                            echo $time_search->end_time->i18nFormat('hh:mm');
                                            echo '</td></tr>';
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="text-align:right">
                            <a href="/zeitgesuch/<?= $search_time->url_title ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <?= $this->element('searches_sidebar') ?>
            </div>
        </div>
        <?php
        break;
}