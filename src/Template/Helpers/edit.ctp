<script src="/js/jquery.MultiFile.js"></script>
<div class="row">
    <div class="headline"><h2><?= __('Spende bearbeiten') ?></h2></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:40px">
        <?= $this->Form->create($donation, ['enctype' => 'multipart/form-data']) ?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>Öffentlich?</label>
                <?= $this->Form->checkbox(__('active')) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>Titel</label>
                <?= $this->Form->input(__('title'), ['class' => 'form-control', 'label' => false]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>Beschreibung</label>
                <?= $this->Form->textarea(__('description'), ['class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <?php
        if (isset($donation_uploads) && $donation_uploads) {
            ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php
                    $counter = 1;
                    foreach ($donation_uploads as $key => $donation_upload) {
                        if ($key == 0) {
                            echo '<label>Vorhandene Bilder löschen</label><br/>';
                        }
                        echo '<div class="row">';
                        echo '<div class="col-xs-2">';
                        echo $this->Form->checkbox(__('upload_id'.$counter), ['class' => 'form-control', 'style' => 'width:15px;height:15px;float:left;', 'value' => $donation_upload->id]);
                        echo '</div><div class="col-xs-10">';
                        echo '<img src="'.$donation_upload->src.'" style="max-width:200px;height:auto" />';
                        echo '</div></div>';
                        $counter += 1;
                    }
                    ?>
                </div>
            </div>
            <br/>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>Neue Bilder hochladen:</label>
                <?= $this->Form->file(__('uploads.'), ['accept' => 'gif|jpg|png|jpeg', 'data-maxfile' => '4096', 'class' => 'multi with-preview', 'type' => 'file', 'multiple' => true, 'style' => 'width:110px']) ?>
                <p id="rights_uploads" style="display:none;margin-top:20px"><input type="checkbox" name="rights" style="display:none;margin-top:0px;margin-right:12px" />Ich besitze alle Rechte an den hochgeladenen Bildern</p>
            </div>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= $this->Form->button(__('Änderungen speichern'), ['class' => 'btn btn-u']) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>


<script type="text/javascript">
    jQuery('input[type="file"]').on('change', function() {
        if (jQuery(this).val().length>0) {
            jQuery('p#rights_uploads, p#rights_uploads > input[type="checkbox"]').fadeIn('fast');
        } else {
            jQuery('p#rights_uploads, p#rights_uploads > input[type="checkbox"]').fadeOut('fast');
        }
    });

    jQuery('input[type="checkbox"][name="rights"]').on('click', function() {
        if (jQuery(this).prop('checked')) {
            jQuery('p#rights.error').fadeOut('fast').remove();
        } else {
            jQuery('p#rights_uploads').after('<p class="error" style="color:#c0392b" id="rights">- Bitte bestätigen Sie die Rechte an den Bildern.</p>');
        }
    });
</script>