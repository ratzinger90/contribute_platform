<?php
switch($case) {
    case "thing":
        ?>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <?php
                foreach ($donations as $donation_thing) {
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="headline">
                                <h2 style="font-size:16px"><?= $donation_thing->title ?></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <?php
                                if (isset($donation_thing->file_uploads[0]) && $donation_thing->file_uploads[0]->src && file_exists(rtrim(WWW_ROOT, '/').$donation_thing->file_uploads[0]->src)) {
                                    ?>
                                    <img src="<?= $donation_thing->file_uploads[0]->src ?>" style="max-width:75px;max-height:75px" />
                                    <?php
                                } else {
                                    ?>
                                    <img src="/img/thing_placeholder.png" style="width:75px;height:75px" />
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="row"><?= $donation_thing->description ?></div>
                                <div class="row" style="text-align:right"><a href="/sachspende/<?= $donation_thing->url_title ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <?= $this->element('donations_sidebar') ?>
            </div>
        </div>
        <?php
        break;
    case "time":
        ?>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <?php
                foreach ($donations as $donation_time) {
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="headline">
                                <h2><?= $donation_time->title ?></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <?php
                                if (isset($donation_time->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/') . $donation_time->file_uploads[0]->src)) {
                                    ?>
                                    <img src="<?= $donation_time->file_uploads[0]->src ?>" style="max-width:75px;max-height:75px" />
                                    <?php
                                } else {
                                    ?>
                                    <img src="/img/empty.png" style="max-width:75px;max-height:75px"/>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                <?= $donation_time->description ?>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <table class="table">
                                    <thead><tr><th>Anfang</th><th>Ende</th></tr></thead>
                                    <?php
                                    foreach ($donation_time->time_donations as $key => $time_donation) {
                                        echo '<tr><td>';
                                        echo $time_donation->start_date->i18nFormat('dd.MM.yyyy');
                                        echo ' - ';
                                        echo $time_donation->start_time->i18nFormat('hh:mm');
                                        echo '</td><td>';
                                        echo $time_donation->end_date->i18nFormat('dd.MM.yyyy');
                                        echo ' - ';
                                        echo $time_donation->end_time->i18nFormat('hh:mm');
                                        echo '</td></tr>';
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="text-align:right">
                            <a href="/zeitspende/<?= $donation_time->url_title ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <?= $this->element('donations_sidebar') ?>
            </div>
        </div>
        <?php
        break;

}