<div class="row" style="padding-right:40px">
    <?= $this->Form->create($user) ?>
    <?= $this->Form->hidden(__('password')) ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px">
        <div class="headline"><h2><?= __('Passwort vergeben') ?></h2></div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>Neues Passwort</label>
                <?= $this->Form->input(__('new_password'), ['type' => 'password', 'label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>Neues Passwort wiederholen</label>
                <?= $this->Form->input(__('new_password_confirm'), ['type' => 'password', 'label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= $this->Form->button(__('Änderungen speichern'), ['class' => 'btn btn-u']) ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>