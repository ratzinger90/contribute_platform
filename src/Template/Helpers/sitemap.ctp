<h3>Sitemap</h3>
<ul>
<?php
foreach ($hard_pages as $key => $hard_page) {
    ?>
    <li><a href="<?= $hard_page[$key]['slug'] ?>"><?= $hard_page[$key]['title'] ?></a></li>
    <?php
}
foreach ($dynamic_pages as $dynamic_page) {
    ?>
    <li><a href="<?= $dynamic_page->slug ?>"><?= $dynamic_page->title ?></a></li>
    <?php
}
?>
</ul>
