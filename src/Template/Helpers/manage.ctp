<div class="row" style="padding-right:20px">
    <table id="manage_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
        <thead>
            <tr>
            <?php
            foreach ($columns as $column) {
                ?>
                <th><?= $column ?></th>
                <?php
            }
            ?>
            </tr>
        </thead>
        <tbody>
        <?php
        switch ($type) {
            case "spender_spenden":
                foreach ($data as $row) {
                    switch($row->category) {
                        case "thing":
                            $case = "sachspende";
                            break;
                        case "time":
                            $case = "zeitspende";
                            break;
                    }
                    ?>
                    <tr>
                        <td><?php
                        if (isset($row->active) && $row->active == true) {
                            echo '<span class="label label-success">Online</span>';
                        } else {
                            echo '<span class="label label-danger">Offline</span>';
                        }
                            ?>
                        </td>
                        <td><?php
                            if (strlen($row->title)>17) {
                                echo substr($row->title,0,20) . " ...";
                            } else {
                                echo h($row->title);
                            }
                            ?></td>
                        <td><?php if ($row->category == 'thing') { echo "Sachspende"; } else if ($row->category == 'time') { echo "Zeitspende"; } ?></td>
                        <td><?= $row->created->i18nFormat('dd.MM.yyyy HH:mm') ?></td>
                        <td>
                            <a href="/<?php echo $case; ?>/<?php echo $row->url_title; ?>" class="btn btn-info">Details&nbsp;<i class="fa fa-search"></i></a>
                            <a href="/bearbeiten/<?php echo base64_encode($row->id."|".$row->category."|spenden"); ?>" class="btn btn-default">Bearbeiten&nbsp;<i class="fa fa-pencil"></i></a>

                            <a data-url="/loeschen/<?php echo base64_encode($row->id."|".$row->category."|spenden"); ?>" class="btn btn-danger delete_donation">Löschen&nbsp;<i class="fa fa-ban"></i></a>
                        </td>
                    </tr>
                    <?php
                }
                break;
        }
        ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#manage_table').dataTable({
            "columnDefs" : [
                { type : 'de_date', targets: [3, 4] },
            ]
        });
    });

    jQuery('a.delete_donation').on('click', function(e) {
        e.preventDefault();
        var data_url = jQuery(this).attr('data-url');
        notie.confirm('Diese Spende wirklich löschen?', 'Ja', 'Nein', function() {
                location.href = data_url
            }
        );
    });

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "de_date-asc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity; // = l'an 1000 ...
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
            return z;
        },

        "de_date-desc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity;
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
            return z;
        }
    } );
</script>