<form id="search_main_form" method="post" action="/suche/">
<div class="row">
    <div class="col-xs-8">
        <input type="text" name="main_search_input" placeholder="Gebe hier Deinen Suchbegriff ein ..." class="form-control" value="<?php echo (isset($search_user_input) && strlen($search_user_input)>0) ? $search_user_input : ''; ?>" />
    </div>
    <div class="col-xs-2">
        <button class="btn btn-u main_search">Suchen&nbsp;<i class="fa fa-angle-double-right"></i></button>
    </div>
</div>
</form>
<hr>
<?php
if (isset($search_user_input) && strlen($search_user_input)>0) {
    ?>
    <div class="row" style="padding-right:20px">
        <?php
        if (isset($results) && count($results)>0) {
            foreach ($results as $result) {
                $link = "";
                switch ($result->category) {
                    case "thing":
                        switch ($result->type) {
                            case "gesuche":
                                $link = '/sachgesuch/' . $result->url_title;
                                break;
                            case "spenden":
                                $link = '/sachspende/' . $result->url_title;
                                break;
                        }
                        break;
                    case "time":
                        switch ($result->type) {
                            case "gesuche":
                                $link = '/zeitgesuch/' . $result->url_title;
                                break;
                            case "spenden":
                                $link = '/zeitspende/' . $result->url_title;
                                break;
                        }
                        break;
                }
                ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="headline">
                            <a href="<?= $link ?>">
                            <h2><?php
                            switch($result->type) {
                                case "gesuche":
                                    echo "Gesuch | ";
                                    break;
                                case "spenden":
                                    echo "Spende | ";
                                    break;
                            }
                                ?>
                                <?= $result->title ?></h2></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <?php
                            if (isset($result->file_uploads[0]->src) && file_exists(rtrim(WWW_ROOT, '/').$result->file_uploads[0]->src)) {
                                ?>
                                <img src="<?= $result->file_uploads[0]->src ?>" style="max-height:125px;max-width:125px" />
                                <?php
                            } else {
                                switch($result->category) {
                                    case "thing":
                                        if ($result->type == 'gesuche') {
                                            echo '<a href="' . $link . '"><img src="/img/thing_placeholder_search.png" style="max-height:75px;max-width:75px" /></a>';
                                        } else {
                                            echo '<a href="' . $link . '"><img src="/img/thing_placeholder.png" style="max-height:75px;max-width:75px" /></a>';
                                        }
                                        break;
                                    case "time":
                                        if ($result->type == 'gesuche') {
                                            echo '<a href="' . $link . '"<img src="/img/time_placeholder_search.png" style="max-height:75px;max-width:75px" /></a>';
                                        } else {
                                            echo '<a href="' . $link . '"><img src="/img/time_placeholder.png" style="max-height:75px;max-width:75px" /></a>';
                                        }
                                        break;
                                }
                            }
                            ?>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <?= $result->description ?>
                        </div>
                    </div>
                    <div class="row" style="text-align:right">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right:0px;">
                        <?php $result_title = str_replace(' ', '-', $result->url_title); ?>
                        <?php
                        switch ($result->type) {
                            case "gesuche":
                                if ($result->category=='thing') {
                                    ?>
                                    <a href="<?= $link ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                                    <?php
                                } else if ($result->category=='time') {
                                    ?>
                                    <a href="<?= $link ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                                    <?php
                                }
                                break;
                                break;
                            case "spenden":
                                if ($result->category=='thing') {
                                    ?>
                                    <a href="<?= $link ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                                    <?php
                                } else if ($result->category=='time') {
                                    ?>
                                    <a href="<?= $link ?>" class="btn btn-u">Details&nbsp;<i class="fa fa-angle-double-right"></i></a>
                                    <?php
                                }
                                break;
                        }
                        ?>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
        ?>
    </div>
    <?php
}
?>