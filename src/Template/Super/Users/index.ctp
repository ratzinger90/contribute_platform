<div class="row" style="padding-right:20px">
    <div class="headline"><h2>Administrator-Bereich</h2></div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <span class="label label-success" style="background-color:#00acac">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<label>Behoben</label>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <span class="label label-warning" style="background-color:#ef9e0f">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<label>Offen</label>
        </div>
    </div>
    <ul class="timeline-v1">
        <?php
        foreach ($supports as $key => $support) {
            $additional_style = "";
            if ($support->has('done') && $support->done == false) {
                $additional_style = "background-color:#ef9e0f;color:#ffffff";
            } else {
                $additional_style = "background-color:#00acac;color:#ffffff";
            }
            if ($key == 0 || $key % 2 == 0) {
                ?>
                <li>
                    <div class="timeline-badge primary"><i class="glyphicon glyphicon-record"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-body text-justify" style="<?= $additional_style ?>">
                            <b>Aktuelle URL</b><p><?= $support->current_url ?></p>
                            <b>Client-Information</b><p><?= $support->http_user_agent ?></p>
                            <?php
                            if ($support->has('user_id') && $support->user_id != 0 && isset($users_array[$support->user_id])) {
                                echo '<h2>' . $users_array[$support->user_id] . '</h2>';
                            }
                            ?>
                            <b>Nachricht</b><p><?= $support->message ?></p>
                            <?php
                            if ($support->has('comment') && strlen($support->comment)>0) {
                                echo '<br/>';
                                echo '<b>--</b>&nbsp;';
                                echo '<b>Kommentar (Support)</b>';
                                echo '<p>' . $support->comment . '</p>';
                            }
                            ?>
                        </div>
                        <div class="timeline-footer">
                            <ul class="list-unstyled list-inline blog-info">
                                <li><i class="fa fa-clock-o"></i> <?= $support->created->i18nFormat('dd.MM.yyyy HH:mm') ?></li>
                            </ul>
                            <!--<a class="btn btn-sm btn-success pull-right mark_as_done" data-url="/super/fehler_behoben/<?= $support->id ?>"><i class="fa fa-check"></i>&nbsp;Behoben</a>-->
                        </div>
                    </div>
                </li>
                <?php
            } else {
                ?>
                <li class="timeline-inverted">
                    <div class="timeline-badge primary"><i class="glyphicon glyphicon-record"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-body text-justify" style="<?= $additional_style ?>">
                            <b>Aktuelle URL</b><p><?= $support->current_url ?></p>
                            <b>Client-Information</b><p><?= $support->http_user_agent ?></p>
                            <?php
                            if ($support->has('user_id') && $support->user_id != 0 && isset($users_array[$support->user_id])) {
                                echo '<h2>' . $users_array[$support->user_id] . '</h2>';
                            }
                            ?>
                            <b>Nachricht</b><p><?= $support->message ?></p>
                            <?php
                            if ($support->has('comment') && strlen($support->comment)>0) {
                                echo '<br/>';
                                echo '<b>--</b>&nbsp;';
                                echo '<b>Kommentar (Support)</b>';
                                echo '<p>' . $support->comment . '</p>';
                            }
                            ?>
                        </div>
                        <div class="timeline-footer">
                            <ul class="list-unstyled list-inline blog-info">
                                <li><i class="fa fa-clock-o"></i> <?= $support->created->i18nFormat('dd.MM.yyyy HH:mm') ?></li>
                            </ul>
                            <!--<a class="btn btn-sm btn-success pull-right mark_as_done" data-url="/super/fehler_behoben/<?= $support->id ?>"><i class="fa fa-check"></i>&nbsp;Behoben</a>-->
                        </div>
                    </div>
                </li>
                <?php
            }
        }
        ?>
        <li class="clearfix" style="float: none;"></li>
    </ul>
</div>