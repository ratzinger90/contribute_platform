<?php

$zipcodes_array = array();
foreach ($zipcodes as $zipcode) {
    $zipcodes_array[$zipcode->zipcode] = $zipcode->zipcode . " " . $zipcode->city;
}
?>

    <div class="row">
        <?= $this->Form->create($partner) ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:40px">
            <?php if ($partner->has('id') && $partner->id>0) { ?><div class="headline"><h2><?= __('Partner bearbeiten') ?></h2></div><?= $this->assign('title', __('Partner bearbeiten')) ?><?php
            } else { ?><div class="headline"><h2><?= __('Partner anlegen') ?></h2></div><?= $this->assign('title', __('Partner anlegen')) ?><?php }  ?>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Titel</label>
                    <?= $this->Form->input(__('name'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Anschrift</label>
                    <?= $this->Form->input(__('street'), ['label' => false, 'class' => 'form-control']) ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <label>Postleitzahl</label>
                    <?= $this->Form->input(__('zip'), ['label' => false, 'class' => 'form-control']) ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Ort</label>
                    <?= $this->Form->input(__('city'), ['label' => false, 'class' => 'form-control']) ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Telefonnummer</label>
                    <?= $this->Form->input(__('phone'), ['label' => false, 'class' => 'form-control']) ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>E-Mail-Adresse</label>
                    <?= $this->Form->input(__('email'), ['label' => false, 'class' => 'form-control']) ?>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Zugewiesene Postleitzahlen</label><br/>
                    <?= $this->Form->select(__('zipcodes'), $zipcodes_array, ['id' => 'zipcodes_select', 'multiple' => true]) ?>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <?php if ($partner->has('id') && $partner->id>0) { echo $this->Form->button(__('Änderungen speichern'), ['class' => 'btn btn-u']);
                    } else { echo $this->Form->button(__('Partner anlegen'), ['class' => 'btn btn-u']); } ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>

<?php
if (isset($partner_zipcodes_array) && sizeof($partner_zipcodes_array)>0) {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var zipcodes = <?php echo json_encode($partner_zipcodes_array); ?>;
            var zipcodes_array = [];
            for (zipcode in zipcodes) {
                zipcodes_array[zipcode] = zipcode;
            }
            jQuery('#zipcodes_select').val(zipcodes_array).select2();
        });
    </script>
    <?php
} else {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#zipcodes_select').select2({
                'placeholder': 'Bitte auswählen'
            });
        });
    </script>
    <?php
}
