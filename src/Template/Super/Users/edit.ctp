<div class="row">
    <?= $this->Form->create($user) ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:40px">
        <div class="headline"><h2><?= __('Konto bearbeiten') ?></h2></div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Aktuelles Passwort</label>
                <?= $this->Form->input(__('password'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Neues Passwort</label>
                <?= $this->Form->input(__('password_new'), ['type' => 'password', 'label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Neues Passwort wiederholen</label>
                <?= $this->Form->input(__('password_repeat'), ['type' => 'password', 'label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?= $this->Form->button(__('Änderungen speichern'), ['class' => 'btn btn-u']) ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>