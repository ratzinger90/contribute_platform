<div class="row">
    <div class="headline"><h2><?= __('Meldung bearbeiten') ?></h2></div>
    <?= $this->Form->create($message) ?>
    <div class="row">
        <div class="col-xs-4">Typ</div>
        <div class="col-xs-6"><?= h($message->type) ?></div>
    </div>
    <br/>
    <div class="row">
        <div class="col-xs-4">Name</div>
        <div class="col-xs-6"><?= h($message->name) ?></div>
    </div>
    <br/>
    <div class="row">
        <div class="col-xs-4">Text</div>
        <div class="col-xs-6"><?= $this->Form->input(__('text'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?></div>
    </div>
    <button type="submit" class="btn btn-warning">Speichern</button>
    <?= $this->Form->end() ?>
</div>