<div class="row">
    <?= $this->Form->create($user) ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:40px">
        <div class="headline"><h2><?= __('Nutzer bearbeiten') ?></h2></div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?= $this->Form->input(__('firstname'), ['class' => 'form-control', 'label' => false, 'disabled' => 'disabled']) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?= $this->Form->input(__('lastname'), ['class' => 'form-control', 'label' => false, 'disabled' => 'disabled']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?php
                $roles = [
                    'admin' => 'Administrator',
                    'broker' => 'Vermittler',
                    'token_account' => 'Spender'
                ];
                ?>
                <label>Rolle</label>
                <?= $this->Form->select(__('role'), $roles, ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
            </div>
            <?php
            if ($user->has('role') && ($user->role == 'broker' || $user->role == 'admin')) {
                ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Zugewiesene Postleitzahlen</label>
                    <?= $this->Form->select(__('related_zipcodes_ids'), $valid_zipcodes, ['id' => 'related_zipcodes_ids', 'label' => false, 'class' => 'form-control', 'multiple' => true]) ?>
                </div>
                <?php
            }
            ?>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= $this->Form->button(__('Änderungen speichern'), ['class' => 'btn btn-u']) ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('select#related_zipcodes_ids').select2();
    });
</script>