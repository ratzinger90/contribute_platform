<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="headline"><h2><?= __('Persönliche Angaben') ?></h2></div>
        <div class="row">
            <div class="col-xs-4">Vorname</div>
            <div class="col-xs-6"><?= $user->firstname ?></div>
        </div>
        <div class="row">
            <div class="col-xs-4">Nachname</div>
            <div class="col-xs-6"><?= $user->lastname ?></div>
        </div>
        <br/>
        <h4><?= __('Anschrift') ?></h4>
        <div class="row">
            <div class="col-xs-4">Straße</div>
            <div class="col-xs-6"><?= $user->address->street ?></div>
        </div>
        <div class="row">
            <div class="col-xs-4">Postleitzahl</div>
            <div class="col-xs-6"><?= $user->address->zip ?></div>
        </div>
        <div class="row">
            <div class="col-xs-4">Ort</div>
            <div class="col-xs-6"><?= $user->address->city ?></div>
        </div>
        <br/>
        <h4><?= __('Kontaktdaten') ?></h4>
        <div class="row">
            <div class="col-xs-4">E-Mail</div>
            <div class="col-xs-6"><a data-toggle="modal" href="#kontakt_aufnehmen"><?= $user->email ?></a></div>
        </div>
        <div class="row">
            <div class="col-xs-4">Telefon</div>
            <div class="col-xs-6"><a href="tel:<?= $user->phone ?>"><?= $user->phone ?></a></div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="headline"><h2><?= __('Bewerbung') ?></h2></div>
        <div class="row">
            <p><?= h($user->broker_text) ?></p>
        </div>
        <div class="row">
            <?php
            if ($user->active==0) {
            ?>
            <a href="/admin/vermittler_aktivieren/<?= $user->id ?>/<?= $case ?>" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Freischalten</a>
            <a href="/admin/vermittler_loeschen/<?= $user->id ?>/<?= $case ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i>&nbsp;Löschen</a>
                <?php
            } else {
                ?>
                <a href="/admin/vermittler_deaktivieren/<?= $user->id ?>/<?= $case ?>" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Deaktivieren</a>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="modal fade" id="kontakt_aufnehmen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form id="kontakt_senden_form">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Neue Nachricht</h4>
                </div>
                <div class="modal-body">
                    <?= $this->Form->hidden(__('user_id')) ?>
                    <?= $this->Form->input(__('receiver_email'), ['label' => 'Empfänger', 'class' => 'form-control', 'required' => true, 'value' => $user->email]) ?>
                    <br/>
                    <label>Nachricht</label>
                    <?= $this->Form->textarea(__('message'), ['label' => false, 'class' => 'form-control', 'required' => true]) ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Schließen</button>
                    <button type="button" class="btn-u btn-u-primary anfrage_senden">Nachricht senden&nbsp;<i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>