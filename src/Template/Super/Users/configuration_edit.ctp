<div class="row">
    <div class="headline"><h2><?= __('Konfiguration bearbeiten') ?></h2></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:40px">
        <?= $this->Form->create($configuration) ?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Host</label>
                <?= $this->Form->input(__('host'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>E-Mail-Adresse (Versand)</label>
                <?= $this->Form->input(__('from_mail'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Name (Versand)</label>
                <?= $this->Form->input(__('from_big_title'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>E-Mail-Adresse (Support)</label>
                <?= $this->Form->input(__('support_mail'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Name (Support)</label>
                <?= $this->Form->input(__('support_big_title'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= $this->Form->button(__('Speichern'), ['class' => 'btn btn-u']) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>