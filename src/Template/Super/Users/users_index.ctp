<div class="row" style="padding-right:20px">
    <table id="manage_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
        <thead>
        <tr>
            <th>Vorname</th>
            <th>Nachname</th>
            <th>Rolle</th>
            <th>Erstellt</th>
            <th>Aktionen</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($users)) {
            foreach ($users as $row) {
                ?>
                <tr>
                    <td><?= $row->firstname ?></td>
                    <td><?= $row->lastname ?></td>
                    <td><?php
                    switch ($row->role) {
                        case "admin":
                            echo "Administrator";
                            break;
                        case "broker":
                            echo "Vermittler";
                            break;
                        case "token_account";
                            echo "Spender";
                            break;
                    }
                        ?></td>
                    <td><?= $row->created->i18nFormat('dd.MM.yyyy hh:mm') ?></td>
                    <td>
                        <?php
                        if ($row->active == 0) {
                            ?>
                            <a href="/super/nutzer_aktivieren/<?= $row->id ?>" class="btn btn-success"><i
                                    class="fa fa-check"></i>&nbsp;Freischalten</a>
                            <a data-url="/super/nutzer_loeschen/<?= $row->id ?>" class="btn btn-danger delete_user"><i
                                    class="fa fa-trash-o"></i>&nbsp;Löschen</a>
                            <?php
                        } else {
                            ?>
                            <a href="/super/nutzer_deaktivieren/<?= $row->id ?>"
                               class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Deaktivieren</a>
                            <?php
                        }
                        ?>
                        <a href="/super/nutzer_bearbeiten/<?= $row->id ?>" class="btn btn-warning"><i
                                class="fa fa-pencil-square-o"></i>&nbsp;Bearbeiten</a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#manage_table').dataTable();
    });

    jQuery('a.delete_user').on('click', function(e) {
        e.preventDefault();
        var data_url = jQuery(this).attr('data-url');
        notie.confirm('Diesen Nutzer wirklich löschen?', 'Ja', 'Nein', function() {
                location.href = data_url
            }
        );
    });
</script>