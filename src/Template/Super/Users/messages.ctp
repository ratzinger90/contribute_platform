<div class="row" style="padding-right:20px">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="manage_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
            <thead>
            <tr>
                <th>Typ</th>
                <th>Name</th>
                <th>Text</th>
                <th>Aktion</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($messages as $row) {
                ?>
                <tr>
                    <td><?php
                        if ($row->type=="error") {
                            echo 'Fehler';
                        } else {
                            echo 'Warnung';
                        }
                        ?></td>
                    <td><?= $row->name ?></td>
                    <td><?= $row->text ?></td>
                    <td>
                        <a href="/super/meldung_bearbeiten/<?= $row->id ?>" class="btn btn-warning"><i class="fa fa-pencil"></i>&nbsp;Bearbeiten</a>
                    </td>
                </tr>
                <?php
            }

            ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#manage_table').dataTable();
    });
</script>