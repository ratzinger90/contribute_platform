<div class="row" style="padding-right:20px">
    <a href="/super/partner_anlegen" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Neuen Partner anlegen</a>
    <br/>
    <br/>
    <table id="manage_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
        <thead>
        <tr>
            <th>Partner</th>
            <th>Zugewiesene Regionen</th>
            <th>Aktionen</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($partners)) {
            foreach ($partners as $row) {
                ?>
                <tr>
                    <td><?= $row->name ?></td>
                    <td>
                        <?php
                        foreach ($row->valid_zipcodes as $key => $valid_zipcode) {
                            if ($key > 0) {
                                echo ', ';
                            }
                            echo $valid_zipcode->zipcode;
                        }
                        ?>
                    </td>
                    <td>
                        <a href="/super/partner_bearbeiten/<?= $row->id ?>" class="btn btn-warning"><i
                                class="fa fa-pencil"></i>&nbsp;Bearbeiten</a>
                        <a href="/super/partner_loeschen/<?= $row->id ?>" class="btn btn-danger delete"><i
                                class="fa fa-ban"></i> Löschen</a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#manage_table').dataTable();
    });
    jQuery('.delete').off().on('click', function(e) {
        e.preventDefault();
        var location = jQuery(this).attr('href');
        notie.confirm(
            'Partner wirklich löschen?',
            'Ja',
            'Nein',
            function() {
                window.location.href = location;
            }
        );
    });
</script>