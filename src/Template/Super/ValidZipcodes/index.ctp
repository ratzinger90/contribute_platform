<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:40px">
        <div class="headline"><h2><?= __('Gültige Postleitzahlen') ?></h2></div>
        <a href="/super/plz_anlegen" class="btn btn-warning"><i class="fa fa-plus"></i>&nbsp;Postleitzahl anlegen</a>
        <br/>
        <br/>
        <table id="zipcodes_table" class="table table-striped table-bordered dataTable no-footer dtr-inline" width="100%">
            <thead>
            <tr>
                <th>Postleitzahl</th>
                <th>Stadt</th>
                <th>Erstellt</th>
                <th>Geändert</th>
                <th class="actions">Aktionen</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($validZipcodes as $validZipcode) { ?>
                <tr>
                    <td><?= h($validZipcode->zipcode) ?></td>
                    <td><?= h($validZipcode->city) ?></td>
                    <td><?= h($validZipcode->created->i18nFormat('dd.MM.yyyy HH:mm')) ?></td>
                    <td><?= h($validZipcode->modified->i18nFormat('dd.MM.yyyy HH:mm')) ?></td>
                    <td class="actions">
                        <a href="/super/plz_bearbeiten/<?= $validZipcode->id ?>" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i>&nbsp;Bearbeiten</a>
                        <a data-url="/super/plz_loeschen" class="btn btn-danger delete_zipcode"><i class="fa fa-times"></i>&nbsp;Löschen</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#zipcodes_table').dataTable();
    });

    jQuery('a.delete_zipcode').on('click', function(e) {
        e.preventDefault();
        var data_url = jQuery(this).attr('data-url');
        notie.confirm('Diesen Eintrag wirklich löschen?', 'Ja', 'Nein', function() {
                location.href = data_url
            }
        );
    });
</script>