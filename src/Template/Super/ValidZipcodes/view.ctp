<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Valid Zipcode'), ['action' => 'edit', $validZipcode->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Valid Zipcode'), ['action' => 'delete', $validZipcode->id], ['confirm' => __('Are you sure you want to delete # {0}?', $validZipcode->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Valid Zipcodes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Valid Zipcode'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="validZipcodes view large-9 medium-8 columns content">
    <h3><?= h($validZipcode->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Zipcode') ?></th>
            <td><?= h($validZipcode->zipcode) ?></td>
        </tr>
        <tr>
            <th><?= __('City') ?></th>
            <td><?= h($validZipcode->city) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($validZipcode->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($validZipcode->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($validZipcode->modified) ?></td>
        </tr>
    </table>
</div>
