<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Valid Zipcodes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="validZipcodes form large-9 medium-8 columns content">
    <?= $this->Form->create($validZipcode) ?>
    <fieldset>
        <legend><?= __('Add Valid Zipcode') ?></legend>
        <?php
            echo $this->Form->input('zipcode');
            echo $this->Form->input('city');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
