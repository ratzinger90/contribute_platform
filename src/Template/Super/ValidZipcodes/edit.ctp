<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:40px">
        <?php
        if ($validZipcode->has('id') && is_numeric($validZipcode->id)) {
            ?>
            <div class="headline"><h2><?= __('Postleitzahl bearbeiten') ?></h2></div>
            <?php
        } else {
            ?>
            <div class="headline"><h2><?= __('Postleitzahl anlegen') ?></h2></div>
            <?php
        }
        ?>
        <?= $this->Form->create($validZipcode) ?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Postleitzahl</label>
                <?= $this->Form->input(__('zipcode'), ['class' => 'form-control', 'label' => false, 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Ort</label>
                <?= $this->Form->input(__('city'), ['class' => 'form-control', 'label' => false, 'required' => true]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Zugewiesene Vermittler</label>
                <?= $this->Form->select(__('brokers_ids'), $brokers, ['id' => 'zipcode_brokers', 'multiple' => true, 'label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Zugewiesene Administratoren</label>
                <?= $this->Form->select(__('admins_ids'), $admins, ['id' => 'zipcode_admins', 'multiple' => true, 'label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?php
                if ($validZipcode->has('id') && is_numeric($validZipcode->id)) {
                    echo $this->Form->button(__('Änderungen speichern'), ['class' => 'btn btn-u']);
                } else {
                    echo $this->Form->button(__('Eintrag anlegen'), ['class' => 'btn btn-u']);
                }
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('select#zipcode_brokers').select2();
        jQuery('select#zipcode_admins').select2();
    });
</script>
