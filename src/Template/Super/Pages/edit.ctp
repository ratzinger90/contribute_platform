<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?= $this->Form->create($page) ?>
        <div class="headline"><h2><?= __('Seite bearbeiten') ?></h2></div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Slug</label>
                <?= $this->Form->input(__('slug'), ['class' => 'form-control', 'label' => false]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Seitentitel</label>
                <?= $this->Form->input(__('title'), ['class' => 'form-control', 'label' => false]) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>Inhalt</label>
                <?= $this->Form->textarea(__('content'), ['class' => 'form-control', 'label' => false, 'id' => 'page_content']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= $this->Form->button(__('Speichern'), ['class' => 'btn btn-warning']) ?>
            </div>
        </div>
    <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        tinymce.init({
            selector : 'textarea',
            plugins : 'code',

        });
    });
</script>