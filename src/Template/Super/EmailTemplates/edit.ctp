<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?= $this->Form->create($emailTemplate) ?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Name</label>
                <?= $this->Form->input(__('template_name'), ['label' => false, 'class' => 'form-control', 'disabled' => 'disabled']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>Inhalt</label>
                <?= $this->Form->input(__('template_content'), ['label' => false, 'class' => 'form-control', 'rows' => '15']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Betreff</label>
                <?= $this->Form->input(__('template_subject'), ['label' => false, 'class' => 'form-control']) ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= $this->Form->button(__('Speichern'), ['class' => 'btn btn-u']) ?>
            </div>
        </div>
    <?= $this->Form->end() ?>
    </div>
</div>
