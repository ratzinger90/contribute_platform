<div class="row" style="padding-right:20px">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="headline"><h2><?= __('E-Mails') ?></h2></div>
        <table id="emails_templates_table" class="table table-striped table-bordered dataTable no-footer dtr-inline">
            <thead>
            <tr>
                <th>Titel</th>
                <th>Betreff</th>
                <th>Geändert</th>
                <th class="actions"><?= __('Aktionen') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($emailTemplates as $emailTemplate) { ?>
                <tr>
                    <td><?= h($emailTemplate->template_name) ?></td>
                    <td><?= h($emailTemplate->template_subject) ?></td>
                    <td><?php
                        if ($emailTemplate->has('modified')) {
                            echo h($emailTemplate->modified->i18nFormat('dd.MM.yyyy HH:mm'));
                        } else {
                            echo "";
                        } ?></td>
                    <td class="actions">
                        <a href="/super/email_bearbeiten/<?= $emailTemplate->id ?>" class="btn btn-warning">Bearbeiten&nbsp;<i class="fa fa-pencil-square-o"></i></a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#emails_templates_table').dataTable({
            "columnDefs" : [
                { type : 'de_date', targets: [3, 4] },
            ]
        });
    });

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "de_date-asc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity; // = l'an 1000 ...
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
            return z;
        },

        "de_date-desc": function ( a, b ) {
            var x, y;
            if (jQuery.trim(a) !== '') {
                var deDatea = jQuery.trim(a).split('.');
                x = (deDatea[2] + deDatea[1] + deDatea[0]) * 1;
            } else {
                x = Infinity;
            }

            if (jQuery.trim(b) !== '') {
                var deDateb = jQuery.trim(b).split('.');
                y = (deDateb[2] + deDateb[1] + deDateb[0]) * 1;
            } else {
                y = Infinity;
            }
            var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
            return z;
        }
    } );
</script>
