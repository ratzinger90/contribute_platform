<?php $this->set('title', '500 - Fehler aufgetreten'); ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
    <h3>Es ist ein Fehler aufgetreten.</h3>
    <br /> <a class="btn btn-u" href="/">&nbsp;Zur&uuml;ck zur Startseite</a></div>

<?php
$subheading = $this->fetch('subheading');
$exception_stack_trace = $this->element('exception_stack_trace');
$file = $this->fetch('file');
$templateName = $this->fetch('templateName');
$exception_stack_trace_nav = $this->element('exception_stack_trace_nav');
?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var posted_data = {
            subheading: <?php echo json_encode($subheading); ?>,
            exception_stack_trace: <?php echo json_encode($exception_stack_trace); ?>,
            file: <?php echo json_encode($file); ?>,
            templateName: <?php echo json_encode($templateName); ?>,
            exception_stack_trace_nav: <?php echo json_encode($exception_stack_trace); ?>
        };

        $.ajax({
            url: '/UMmJqyvnhrDhF2aZdqaqmqc5UYWK8HYR',
            type: 'POST',
            data: posted_data,
            dataType: 'json',
            async: true,
            cache: false
        });
        setTimeout(function() {
            location.href = '/';
        }, 3000);
    });
</script>