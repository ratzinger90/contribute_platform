<?php $this->set('title', '500 - Schwerwiegender Fehler aufgetreten') ?>
    <div class="col-lg-12" style="text-align:center">
        <h1>500 - Fehler</h1>
        <br><p style="font-size: 17px;">Es ist ein schwerwiegender Fehler aufgetreten.</p><br>
        <br/><p>Der Support wurde kontaktiert.</p>
    </div>

<?php

$subheading = $this->fetch('subheading');
$exception_stack_trace = $this->element('exception_stack_trace');
$file = $this->fetch('file');
$templateName = $this->fetch('templateName');
$exception_stack_trace_nav = $this->element('exception_stack_trace_nav');

var_dump($subheading);
var_dump($exception_stack_trace);
var_dump($file);
var_dump($templateName);
var_dump($exception_stack_trace_nav);

die();
?>